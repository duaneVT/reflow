
#OLD:
#if key in ["t47-ht-flat", "t48-ht-closed", "t23-ht-flat", "t24-ht-closed"]:
#if key in ["t49-ht-cp100", "t51-ht-cp-flat", "t25-ht-cp100", "t28-ht-cp-flat"]:
#if key in ["t49-ht-cp100", "t50-ht-cp-e3", "t25-ht-cp100", "t26-ht-cp-e3"]:
#if key in ["t47-ht-flat", "t48-ht-closed", "t49-ht-cp100", "t51-ht-cp-flat", "t50-ht-cp-e3"]:
#if key in "t52-ht-flat t53-ht-closed t54-ht-cp100 t55-ht-cp-e3 t56-ht-cp-e7 t57-ht-cp-flat".split():
#if key in "t58-ht-flat t59-ht-closed t60-ht-cp100 t61-ht-cp-e3 t62-ht-cp-e7 t63-ht-cp-flat".split():
#if key in "t52-ht-flat  t59-ht-closed t65-ht-closed t67-ht-closed".split():
#if key in "t62-ht-cp-e7 t67-ht-closed t68-ht-flat t64-ht-cp-zero".split():
#if key in "t67-ht-closed t68-ht-cp100 t69-ht-cp-e7".split():
#if key in " t69-sl-flat t70-sl-closed t71-sl-cp100 t72-sl-cp-e7 t73-sl-cp-zero".split():
#if key in "t76-sl-flat t75-sl-closed t73-sl-cp-e7".split():

#NEW:
#if key in "t81-sl-flat t93-sl-closed t120-sl-cp100 t121-sl-cp-e7 t122-sl-cp-zero t123-sl-cp-flat".split(): # SL 4/8 ops
																									# TODO: redo ckpt on lost
#if key in "t87-ht-flat t88-ht-closed t124-ht-cp100 t125-ht-cp-e7 t126-ht-cp-zero t127-ht-cp-flat".split(): # HT 32/64 obj, 4/8ops 
																									#TODO: redo ckpt on lost
#if key in "t94-bank-flat t95-bank-closed t128-bank-cp100 t129-bank-cp-flat t130-bank-cp-zero".split(): #Bank by objs mario
#if key in "t099-tpcc-flat t136-tpcc-cp100 t131-tpcc-cp-zero t132-tpcc-cp-flat t133-tpcc-cp-e3 t134-tpcc-cp-e7".split(): #TPCC mario
#if key in "t105-bst-flat t106-bst-closed t135-bst-cp100 t108-bst-cp-e3 t109-bst-cp-e7 t110-bst-cp-flat t111-bst-cp-zero".split(): #BST by ops mario
#if key in "t112-rbt-flat t113-rbt-closed t114-rbt-cp100 t115-rbt-cp-e3 t116-rbt-cp-e7 t117-rbt-cp-flat t118-rbt-cp-zero".split: # RBT by %r lost?
#TODO: redo lost: t120-sl-cp100 t121-sl-cp-e7 t122-sl-cp-zero t123-sl-cp-flat t124-ht-cp100 t125-ht-cp-e7 t126-ht-cp-zero t127-ht-cp-flat
#mario: t94-bank-flat t95-bank-closed t128-bank-cp100 t129-bank-cp-flat t130-bank-cp-zero t099-tpcc-flat t136-tpcc-cp100 t131-tpcc-cp-zero t132-tpcc-cp-flat t133-tpcc-cp-e3 t134-tpcc-cp-e7 t105-bst-flat t106-bst-closed t135-bst-cp100 t108-bst-cp-e3 t109-bst-cp-e7 t110-bst-cp-flat t111-bst-cp-zero


groups = {
	"skiplist" : "t81-sl-flat t93-sl-closed t120-sl-cp100 t121-sl-cp-e7 t122-sl-cp-zero t123-sl-cp-flat",
	"hashtable": "t87-ht-flat t88-ht-closed t124-ht-cp100 t125-ht-cp-e7 t126-ht-cp-zero t127-ht-cp-flat",
	"bank": "t94-bank-flat t95-bank-closed t128-bank-cp100 t129-bank-cp-flat t130-bank-cp-zero",
	"tpcc": "t099-tpcc-flat t136-tpcc-cp100 t131-tpcc-cp-zero t132-tpcc-cp-flat t133-tpcc-cp-e3 t134-tpcc-cp-e7",
	"bst": "t105-bst-flat t106-bst-closed t135-bst-cp100 t108-bst-cp-e3 t109-bst-cp-e7 t110-bst-cp-flat t111-bst-cp-zero",
	"rbt": "t112-rbt-flat t113-rbt-closed t114-rbt-cp100 t115-rbt-cp-e3 t116-rbt-cp-e7 t117-rbt-cp-flat t118-rbt-cp-zero",
	# Additional plots
	"bst-no-bkoff": "t136-bst-flat t137-bst-closed t138-bst-cp100 t139-bst-cp-e3 t140-bst-cp-e7 t141-bst-cp-flat t142-bst-cp-zero",
	"bst-more-bkoff" : "t143-bst-flat t144-bst-closed t145-bst-cp100 t146-bst-cp-e3 t147-bst-cp-e7 t148-bst-cp-flat t149-bst-cp-zero",
	"skiplist-gc": "t150-sl-flat t151-sl-closed t152-sl-cp100 t153-sl-cp-e3 t154-sl-cp-e5 t155-sl-cp-e7 t156-sl-cp-e10 t157-sl-cp-e15 t158-sl-cp-e20 t159-sl-cp-e25 t160-sl-cp-zero t161-sl-cp-flat",
	# multi-node:
	"skiplist-dist": "t74-sl-flat t75-sl-closed t76-sl-cp100",
	# After the deadline extension
	#sl by rr
	"skiplist-rr": "t162-sl-flat t163-sl-closed t164-sl-cp100 t165-sl-cp-flat t166-sl-cp-zero t167-sl-cp-e7 t168-sl-cp-e25", # t176-sl-flat-j7 t177-sl-closed-j7
	"tpcc-2": "t170-tpcc-flat t171-tpcc-cp100 t172-tpcc-cp-zero t173-tpcc-cp-flat t174-tpcc-cp-e3 t175-tpcc-cp-e7", # t178-tpcc-flat-j7
}

subgroup_filters = {
	"skiplist": {
		"4ops": lambda x: x.filter("hyflow.workload.ops", 4),
		"8ops": lambda x: x.filter("hyflow.workload.ops", 8),
	},
	"hashtable": {
		"8ops-32obj": lambda x: x.filter("hyflow.workload.ops", 8).filter("hyflow.workload.objects", 32),
		"4ops-64obj": lambda x: x.filter("hyflow.workload.ops", 4).filter("hyflow.workload.objects", 64),
	}
}

xaxis = {
	"skiplist": "n",
	"hashtable": "n",
	"bank": ("objs", True),
	"tpcc": "n",
	"bst": ("ops", True),
	"rbt": ("writes", True),
	# Additional
	"bst-no-bkoff": ("ops", True),
	"bst-more-bkoff": ("ops", True),
	"skiplist-gc": "ckpt-every",
	"skiplist-dist": "n",
	"skiplist-rr": "reads",
	"tpcc-2": "n",
}


