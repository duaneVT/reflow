
cmd = "bin/hytest"
#cmd = "false"

def cmd_line2(choices, descr):
	#print choices, descr
	nodel = [x for x in choices if x[:2] == "-n"]
	other = [x for x in choices if x[:2] != "-n"]
	nodes = int(nodel[0][2:])
	other.append("-o%d" % (nodes * 12))
	return ["bin/distrib.py" ] + nodel + [ '-r%s %s' % (" ".join(other), descr["ARGS"])]

templates = {
	# Per benchmark templates
	"_BANK": {
		"REPEAT": 2, 
		"ARGS": "-w150 -e60 --time-out=260 --cores=2 ", 
		"b": "bank", 
		"O": 3, 
		"n": [12], 
		"t": 2, 
		"r": [50], 
		"o": [10, 40, 160, 640], 
	},
	"_TPCC" : {
		"REPEAT": 3,
		"ARGS": "-w150 -e60 --time-out=400 --cores=2 ",
		"b": "tpcc",
		"n": [4, 6, 8, 10, 12],
		"t": [2],
	},
	"_LL" : {
		"REPEAT" : 2,
		"ARGS": "-w150 -e30 --time-out=300 ",
		"b": "linkedlist",
		"n": [2, 8, 24],
		"t": 2,
		"r": [5, 50],
		"O": [2, 4],
		"o": 4,
	},
	"_SL" : {
		"REPEAT" : 3,
		"ARGS": "-w140 -e60 --time-out=260 --cores=2 ",
		"b": "skiplist",
		"n": [24], # 4 12 24 on lost
		"t": 2,
		"r": [1, 10, 50, 90, 99],
		"O": [8], # 4 and 8
		"o": 64,
	},
	"_HT" : {
		"REPEAT" : 2,
		"ARGS": "-w140 -e60 --time-out=260 --cores=2 ",
		"b": "hashtable",
		"n": [4, 12, 24],
		"t": 2,
		"r": [50],
		"O": [4, 8], #16
		"o": [32, 64],
	},
	"_RBT" : {
                "REPEAT" : 2,
                "ARGS": "-w150 -e60 --time-out=300 --cores=2 ",
                "b": "rbt",
                "n": [24],
                "t": 2,
                "r": [0, 50, 75, 88, 94, 97, 99],
                "O": [4],
                "o": 64,
        },
	"_BST" : {
                "REPEAT" : 2,
                "ARGS": "-w150 -e60 --time-out=300 --cores=2 ",
                "b": "bst",
                "n": [12],
                "t": 2,
                "r": [50],
                "O": [1, 2, 4, 8, 16],
                "o": 64,
        },
	"_CTR": {
		"REPEAT": 2,
		"ARGS": "-w120 -e30 --time-out=250 --cores=2 ",
		"t": 2,
		"r": 50,
		"n": 12,
		"b": "counter",
		"": ["hyflow.workload.counter._step2.pool=1 ", "hyflow.workload.counter._step2.pool=3 ", "hyflow.workload.counter._step2.pool=9 " ],
	},
	#"1-17": { "ARGS": "hyflow.workload.counter._step1.ops=1 hyflow.workload.counter._step3.ops=17 " },
	#"3-15": { "ARGS": "hyflow.workload.counter._step1.ops=3 hyflow.workload.counter._step3.ops=15 " },
	"6-12": { "ARGS": "hyflow.workload.counter._step1.ops=6 hyflow.workload.counter._step3.ops=12 " },
	"9-9": { "ARGS": "hyflow.workload.counter._step1.ops=9 hyflow.workload.counter._step3.ops=9 " },
	"12-6": { "ARGS": "hyflow.workload.counter._step1.ops=12 hyflow.workload.counter._step3.ops=6 " },
	"15-3": { "ARGS": "hyflow.workload.counter._step1.ops=15 hyflow.workload.counter._step3.ops=3 " },
	"17-1": { "ARGS": "hyflow.workload.counter._step1.ops=17 hyflow.workload.counter._step3.ops=1 " },
	
	#"5-25": { "ARGS": "hyflow.workload.counter._step1.ops=5 hyflow.workload.counter._step3.ops=25 " },
	"10-20": { "ARGS": "hyflow.workload.counter._step1.ops=10 hyflow.workload.counter._step3.ops=20 " },
	"15-15": { "ARGS": "hyflow.workload.counter._step1.ops=15 hyflow.workload.counter._step3.ops=15 " },
	"20-10": { "ARGS": "hyflow.workload.counter._step1.ops=20 hyflow.workload.counter._step3.ops=10 " },
	"25-5": { "ARGS": "hyflow.workload.counter._step1.ops=25 hyflow.workload.counter._step3.ops=5 " },
	"28-2": { "ARGS": "hyflow.workload.counter._step1.ops=28 hyflow.workload.counter._step3.ops=2 " },


	
	# Per nesting model templates
	"_FLAT": 		{ "script": "bench" },
	"_CLOSED": 		{ "script": "bench", "ARGS": "hyflow.motstm.closedNesting=true " },
	"_CKPT": 		{ "script": "check" },
	"_CKPT_FLAT":{ "script": "check", "ARGS": "hyflow.haistm.emulateFlat=true " },
	"_CKPT_0":   { "script": "check", "ARGS": "hyflow.haistm.checkpointEvery=0 " },
	"_CKPT_3":   { "script": "check", "ARGS": "hyflow.haistm.checkpointEvery=3 " },
	"_CKPT_5":   { "script": "check", "ARGS": "hyflow.haistm.checkpointEvery=5 " },
	"_CKPT_7":   { "script": "check", "ARGS": "hyflow.haistm.checkpointEvery=7 " },
        "_CKPT_10":  { "script": "check", "ARGS": "hyflow.haistm.checkpointEvery=10 " },
        "_CKPT_15":  { "script": "check", "ARGS": "hyflow.haistm.checkpointEvery=15 " },
	"_CKPT_20":  { "script": "check", "ARGS": "hyflow.haistm.checkpointEvery=20 " },
        "_CKPT_25":  { "script": "check", "ARGS": "hyflow.haistm.checkpointEvery=25 " },
	"_BKOFF_0":  { "ARGS": "hyflow.backoff.locked.base=0 hyflow.backoff.lock_acq.base=0 hyflow.backoff.readset.base=0 " },
	"_BKOFF_X":  { "ARGS": "hyflow.backoff.locked.base=100 hyflow.backoff.lock_acq.base=100 hyflow.backoff.readset.base=60 " }
}

# Test descriptions
tests = {
	"t1-bank-flat": [ "_BANK", "_FLAT", "-w2", "-e2"],
	"t2-bank-closed": [ "_BANK", "_CLOSED" ],
	"t3-bank-cp100": [ "_BANK", "_CKPT" ],
	"t4-bank-cp-flat": [ "_BANK", "_CKPT_FLAT" ],
	# t5-t8 repeat of the above, with backoff
	"t9-tpcc-flat": [ "_TPCC", "_FLAT" ],
	"t10-tpcc-cp100": [ "_TPCC", "_CKPT" ],
	"t11-tpcc-cp-e3": [ "_TPCC", "_CKPT_3" ],
	"t12-tpcc-cp-flat": [ "_TPCC", "_CKPT_FLAT"],
	# t13-t16 repeat of above, increased backoff
	"t13-tpcc-flat": [ "_TPCC", "_FLAT" ],
	"t14-tpcc-cp100": [ "_TPCC", "_CKPT" ],
	"t15-tpcc-cp-e3": [ "_TPCC", "_CKPT_3" ],
	"t16-tpcc-cp-flat": [ "_TPCC", "_CKPT_FLAT"],
	# Linked-List
	"t17-ll-flat": ["_LL", "_FLAT" ],
	"t18-ll-closed": ["_LL", "_CLOSED"],
	"t19-ll-cp100": ["_LL", "_CKPT"],
	"t21-ll-cp-e3": ["_LL", "_CKPT_3"],
	"t21-ll-cp-e7": ["_LL", "_CKPT_7"],
	"t22-ll-cp-flat": ["_LL", "_CKPT_FLAT"],
	# Hash-table
	"t23-ht-flat": ["_HT", "_FLAT" ],
	"t24-ht-closed": ["_HT", "_CLOSED"],
	"t25-ht-cp100": ["_HT", "_CKPT"],
	"t26-ht-cp-e3": ["_HT", "_CKPT_3"],
	"t28-ht-cp-flat": ["_HT", "_CKPT_FLAT"],
	# Skip-List
	"t29-sl-flat": ["_SL", "_FLAT" ],
	"t30-sl-closed": ["_SL", "_CLOSED"],
	"t31-sl-cp100": ["_SL", "_CKPT"],
	"t32-sl-cp-e3": ["_SL", "_CKPT_3"],
	"t33-sl-cp-e7": ["_SL", "_CKPT_7"],
	"t34-sl-cp-flat": ["_SL", "_CKPT_FLAT"],
	# RBT
	"t35-rbt-flat": ["_RBT", "_FLAT" ],
        "t36-rbt-closed": ["_RBT", "_CLOSED"],
        "t37-rbt-cp100": ["_RBT", "_CKPT"],
        "t38-rbt-cp-e3": ["_RBT", "_CKPT_3"],
        "t39-rbt-cp-e7": ["_RBT", "_CKPT_7"],
        "t40-rbt-cp-flat": ["_RBT", "_CKPT_FLAT"],
	# BST
	"t41-bst-flat": ["_BST", "_FLAT" ],
        "t42-bst-closed": ["_BST", "_CLOSED"],
        "t43-bst-cp100": ["_BST", "_CKPT"],
        "t44-bst-cp-e3": ["_BST", "_CKPT_3"],
        "t45-bst-cp-e7": ["_BST", "_CKPT_7"],
        "t46-bst-cp-flat": ["_BST", "_CKPT_FLAT"],
	# 47-51: Rerun after fixing backoff
	# 52-57: Rerun with longer time
	# 58-64: Reduce time again, resettable metrics, only on 24nodes
	# Hash-table
        "t58-ht-flat": ["_HT", "_FLAT" ],
        "t59-ht-closed": ["_HT", "_CLOSED"],
        "t60-ht-cp100": ["_HT", "_CKPT"],
        "t61-ht-cp-e3": ["_HT", "_CKPT_3"],
	"t62-ht-cp-e7": ["_HT", "_CKPT_7"],
        "t63-ht-cp-flat": ["_HT", "_CKPT_FLAT"],
	"t64-ht-cp-zero": ["_HT", "_CKPT_0"],
	# 65-67: fixed closed nesting metrics and backoff
	"t67-ht-closed": ["_HT", "_CLOSED"],
	"t68-ht-flat": ["_HT", "_FLAT"],
	# 69-73: Skiplist, Added level metrics
	"t69-sl-flat": ["_SL", "_FLAT"],
	"t70-sl-closed": ["_SL", "_CLOSED"],
	"t71-sl-cp100": ["_SL", "_CKPT"],
	"t72-sl-cp-e7": ["_SL", "_CKPT_7"],
	"t73-sl-cp-zero": ["_SL", "_CKPT_0"],
	# 74-79: multi machine test
	# 81-86: Added contention metrics. Fixed MotSTM backoff.
	# 93: Added closed nesting to SL
	"t81-sl-flat": ["_SL", "_FLAT"],
        "t93-sl-closed": ["_SL", "_CLOSED"],
        "t120-sl-cp100": ["_SL", "_CKPT"],
        "t121-sl-cp-e7": ["_SL", "_CKPT_7"],
        "t122-sl-cp-zero": ["_SL", "_CKPT_0"],
	"t123-sl-cp-flat": ["_SL", "_CKPT_FLAT"],
	# 80
	"t80-bank-flat": ["_BANK", "_FLAT"],
	# 87-92 HT
	"t87-ht-flat": ["_HT", "_FLAT"],
        "t88-ht-closed": ["_HT", "_CLOSED"],
        "t124-ht-cp100": ["_HT", "_CKPT"],
        "t125-ht-cp-e7": ["_HT", "_CKPT_7"],
        "t126-ht-cp-zero": ["_HT", "_CKPT_0"],
        "t127-ht-cp-flat": ["_HT", "_CKPT_FLAT"],
	# 94-98: Bank, by contention
	"t94-bank-flat": [ "_BANK", "_FLAT"],
        "t95-bank-closed": [ "_BANK", "_CLOSED" ],
        "t128-bank-cp100": [ "_BANK", "_CKPT" ],
        "t129-bank-cp-flat": [ "_BANK", "_CKPT_FLAT" ],
	"t130-bank-cp-zero": [ "_BANK", "_CKPT_0" ],
	#TPCC
	"t099-tpcc-flat" : ["_TPCC", "_FLAT"],
	"t136-tpcc-cp100" : ["_TPCC", "_CKPT"],
	"t131-tpcc-cp-zero": ["_TPCC", "_CKPT_0"],
	"t132-tpcc-cp-flat": ["_TPCC", "_CKPT_FLAT"],
	"t133-tpcc-cp-e3": ["_TPCC", "_CKPT_3"],
	"t134-tpcc-cp-e7": ["_TPCC", "_CKPT_7"],
	# BST
        "t105-bst-flat": ["_BST", "_FLAT" ],
        "t106-bst-closed": ["_BST", "_CLOSED"],
        "t135-bst-cp100": ["_BST", "_CKPT"],
        "t108-bst-cp-e3": ["_BST", "_CKPT_3"],
        "t109-bst-cp-e7": ["_BST", "_CKPT_7"],
        "t110-bst-cp-flat": ["_BST", "_CKPT_FLAT"],
	"t111-bst-cp-zero": ["_BST", "_CKPT_0"],
	# RBT
        "t112-rbt-flat": ["_RBT", "_FLAT" ],
        "t113-rbt-closed": ["_RBT", "_CLOSED"],
        "t114-rbt-cp100": ["_RBT", "_CKPT"],
        "t115-rbt-cp-e3": ["_RBT", "_CKPT_3"],
        "t116-rbt-cp-e7": ["_RBT", "_CKPT_7"],
        "t117-rbt-cp-flat": ["_RBT", "_CKPT_FLAT"],
	"t118-rbt-cp-zero": ["_RBT", "_CKPT_0"],
	#120-135: redo bad ckpt tests
	# BST no backoff
        "t136-bst-flat": ["_BST", "_FLAT", "_BKOFF_0" ],
        "t137-bst-closed": ["_BST", "_CLOSED", "_BKOFF_0" ],
        "t138-bst-cp100": ["_BST", "_CKPT", "_BKOFF_0" ],
        "t139-bst-cp-e3": ["_BST", "_CKPT_3", "_BKOFF_0" ],
        "t140-bst-cp-e7": ["_BST", "_CKPT_7", "_BKOFF_0" ],
        "t141-bst-cp-flat": ["_BST", "_CKPT_FLAT", "_BKOFF_0" ],
        "t142-bst-cp-zero": ["_BST", "_CKPT_0", "_BKOFF_0" ],
	# BST extra backoff
        "t143-bst-flat": ["_BST", "_FLAT", "_BKOFF_X" ],
        "t144-bst-closed": ["_BST", "_CLOSED", "_BKOFF_X" ],
        "t145-bst-cp100": ["_BST", "_CKPT", "_BKOFF_X" ],
        "t146-bst-cp-e3": ["_BST", "_CKPT_3", "_BKOFF_X" ],
        "t147-bst-cp-e7": ["_BST", "_CKPT_7", "_BKOFF_X" ],
        "t148-bst-cp-flat": ["_BST", "_CKPT_FLAT", "_BKOFF_X" ],
        "t149-bst-cp-zero": ["_BST", "_CKPT_0", "_BKOFF_X"],
	# Skiplist with GC metrics and additional CP-e settings
	"t150-sl-flat": ["_SL", "_FLAT"],
        "t151-sl-closed": ["_SL", "_CLOSED"],
        "t152-sl-cp100": ["_SL", "_CKPT"],
	"t153-sl-cp-e3": ["_SL", "_CKPT_3"],
	"t154-sl-cp-e5": ["_SL", "_CKPT_5"],
        "t155-sl-cp-e7": ["_SL", "_CKPT_7"],
	"t156-sl-cp-e10": ["_SL", "_CKPT_10"],
	"t157-sl-cp-e15": ["_SL", "_CKPT_15"],
	"t158-sl-cp-e20": ["_SL", "_CKPT_20"],
	"t159-sl-cp-e25": ["_SL", "_CKPT_25"],
        "t160-sl-cp-zero": ["_SL", "_CKPT_0"],
        "t161-sl-cp-flat": ["_SL", "_CKPT_FLAT"],
	# Additional tests. Vary read ratio on SkipList.
	"t162-sl-flat": ["_SL", "_FLAT"],
        "t163-sl-closed": ["_SL", "_CLOSED"],
        "t164-sl-cp100": ["_SL", "_CKPT"],
        "t165-sl-cp-flat": ["_SL", "_CKPT_FLAT"],
        "t166-sl-cp-zero": ["_SL", "_CKPT_0"],
        "t167-sl-cp-e7": ["_SL", "_CKPT_7"],
	"t168-sl-cp-e25": ["_SL", "_CKPT_25"],
	#TPCC again
        "t170-tpcc-flat" : ["_TPCC", "_FLAT"],
        "t171-tpcc-cp100" : ["_TPCC", "_CKPT"],
        "t172-tpcc-cp-zero": ["_TPCC", "_CKPT_0"],
        "t173-tpcc-cp-flat": ["_TPCC", "_CKPT_FLAT"],
        "t174-tpcc-cp-e3": ["_TPCC", "_CKPT_3"],
        "t175-tpcc-cp-e7": ["_TPCC", "_CKPT_7"],
	# Run with up-to-date JVM
	"t176-sl-flat-j7": ["_SL", "_FLAT"],
        "t177-sl-closed-j7": ["_SL", "_CLOSED"],
	"t178-tpcc-flat-j7" : ["_TPCC", "_FLAT"],
	# Counter experimental 180-181
	# Counter test: 182-184
	"t182-ctr-flat": ["_CTR", "_FLAT"],
	"t183-ctr-cp-man": ["_CTR", "_CKPT"],
	"t184-ctr-closed": ["_CTR", "_FLAT"],
	# New counter tests: 185+
	"t185-ctr-9-9-flat" : ["_CTR", "9-9", "_FLAT"],
	"t186-ctr-9-9-cpman" : ["_CTR", "9-9", "_CKPT"],
	"t187-ctr-9-9-closed" : ["_CTR", "9-9", "_CLOSED"],
	
	"t185-ctr-12-6-flat" : ["_CTR", "12-6", "_FLAT"],
        "t186-ctr-12-6-cpman" : ["_CTR", "12-6", "_CKPT"],
        "t187-ctr-12-6-closed" : ["_CTR", "12-6", "_CLOSED"],

	"t185-ctr-15-3-flat" : ["_CTR", "15-3", "_FLAT"],
        "t186-ctr-15-3-cpman" : ["_CTR", "15-3", "_CKPT"],
        "t187-ctr-15-3-closed" : ["_CTR", "15-3", "_CLOSED"],

	"t185-ctr-17-1-flat" : ["_CTR", "17-1", "_FLAT"],
        "t186-ctr-17-1-cpman" : ["_CTR", "17-1", "_CKPT"],
        "t187-ctr-17-1-closed" : ["_CTR", "17-1", "_CLOSED"],
	
	"t185-ctr-6-12-flat" : ["_CTR", "6-12", "_FLAT"],
        "t186-ctr-6-12-cpman" : ["_CTR", "6-12", "_CKPT"],
        "t187-ctr-6-12-closed" : ["_CTR", "6-12", "_CLOSED"],

	#30
	"t185-ctr-15-15-flat" : ["_CTR", "15-15", "_FLAT"],
        "t186-ctr-15-15-cpman" : ["_CTR", "15-15", "_CKPT"],
        "t187-ctr-15-15-closed" : ["_CTR", "15-15", "_CLOSED"],

        "t185-ctr-20-10-flat" : ["_CTR", "20-10", "_FLAT"],
        "t186-ctr-20-10-cpman" : ["_CTR", "20-10", "_CKPT"],
        "t187-ctr-20-10-closed" : ["_CTR", "20-10", "_CLOSED"],

        "t185-ctr-25-5-flat" : ["_CTR", "25-5", "_FLAT"],
        "t186-ctr-25-5-cpman" : ["_CTR", "25-5", "_CKPT"],
        "t187-ctr-25-5-closed" : ["_CTR", "25-5", "_CLOSED"],

        "t185-ctr-28-2-flat" : ["_CTR", "28-2", "_FLAT"],
        "t186-ctr-28-2-cpman" : ["_CTR", "28-2", "_CKPT"],
        "t187-ctr-28-2-closed" : ["_CTR", "28-2", "_CLOSED"],

        "t185-ctr-10-20-flat" : ["_CTR", "10-20", "_FLAT"],
        "t186-ctr-10-20-cpman" : ["_CTR", "10-20", "_CKPT"],
        "t187-ctr-10-20-closed" : ["_CTR", "10-20", "_CLOSED"],



}


