/**
 * Extension of ScalaSTM
 * Main Transaction Code (Top-Level)
 */

package scala.concurrent.stm
package motstm

import skel._
import akka.actor._
import scala.concurrent.{ Await, Future, ExecutionContext, Promise }
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import org.hyflow.Hyflow
import org.hyflow.api._
import org.hyflow.core.directory._
import org.hyflow.core._
import org.hyflow.core.store._
import org.hyflow.core.util._
import com.typesafe.scalalogging.slf4j.Logging
import scala.collection.mutable
import scala.annotation.tailrec
import scala.util.Random
import java.util.concurrent.atomic.{AtomicLong, AtomicBoolean, AtomicInteger}
import scala.concurrent.stm.motstm.MotUtil.MotCfg
import scala.util.{ Try, Success, Failure }
import com.yammer.metrics.core.Clock
import java.util.concurrent.TimeUnit

/** Thread-local current transaction **/
private[motstm] object MotInTxn extends ThreadLocal[MotInTxn] {
	override def initialValue = new MotInTxn

	def apply()(implicit mt: MaybeTxn): MotInTxn = mt match {
		case x: MotInTxn => x
		case _ => get // Create a new one for the current thread.
	}

	private def active(txn: MotInTxn): MotInTxn = if (txn.internalCurrentLevel != null) txn else null
	def dynCurrentOrNull: MotInTxn = active(get)

	def currentOrNull(implicit mt: MaybeTxn) = active(apply())
}

object TFAClock extends PayloadHandler with Logging {
	private val _clk = new AtomicLong(0)

	def inc = _clk.incrementAndGet
	def get = _clk.get

	val name = "tfaclock"

	// Handle incoming object messages to update the TFA Clock.
	def incoming(msg: Message) {
		// Retrieve clock from message.
		val rclk = msg.payloads.get("tfaclock")
		if (rclk == None)
			logger.warn("Message received with missing <tfaclock> payload. | msg = {}", msg)
		val rclkval = rclk.get.asInstanceOf[Long]
		logger.trace("Received message. | clk = {} | msg = {}",
			rclkval toString,
			msg toString)

		// Update local clock if necessary.
		while (true) {
			val lclk = _clk.get
			if (lclk > rclkval)
				return
			if (_clk.compareAndSet(lclk, rclkval))
				return
			logger.debug("CAS failed. (do not change TFAClock.incoming :)")
		}
	}

	def outgoing = Some(_clk.get)
}

private[motstm] class MotInTxn extends MotInTxnRefOps with AbstractInTxn with Logging with ReInstrumented
{
	import Txn._
	import MotUtil._
	import scala.collection.mutable.ArrayBuffer

	// Pre-transaction state.
	private var _alternatives: List[InTxn => Any] = Nil
	private var _commitHandler: InTxn => Any = null
	private var _abortHandler: InTxn => Any = null

	val _hy_txnid = scala.util.Random.nextInt
	def getTxnId: Long = (Hyflow._hy_nodeid << 32L) | this._hy_txnid

	implicit val timeout = Timeout(20 seconds)
	private var _pendingFailure: Throwable = null
	protected var _pessimisticMode: Boolean = MotCfg.ALWAYS_PESSIMISTIC
	def pessimisticMode: Boolean = _pessimisticMode

	// Current Level.
	protected def internalCurrentLevel: MotTxnLevel = _currentLevel
	private var _currentLevel: MotTxnLevel = null
	private var _currentStats: MotStats = null

	// Ordering number (used only for parallel sub-Txn). Further, it is used for the Future array.
	private var _orderNumber: Int = 0
	private var _futuresBuffer = new ArrayBuffer[Future[_]]()

	// Track the number of committed children. In PS, it is the sequential order.
	// In PR, it is just the number of children committed already (out-of-order).
	private var _committed: Int = 0

	// Tracking sub-transactions for relaxed protocol.
	private var _txnBlocks = mutable.Map[Int, Tuple2[TxnExecutor, (InTxn => _)]]()
	val _syncLock = new AtomicBoolean(true)
	var _readMap = mutable.Map[Int, Tuple2[Handle[_], mutable.Map[Int, ArrayBuffer[Int]]]]()    // Map(ObjId -> Tuple[ObjHandle, Map(v0 -> {T1, T2}, etc.)])
	var _writeMap = mutable.Map[Int, mutable.Map[Int, Tuple2[Handle[_], _]]]()                  // Map(ObjId -> Map(v0 -> (ObjHandle, V0), etc.))
	var _txnWrites = mutable.Map[Int, ArrayBuffer[Int]]()                                       // Map(TxnNo -> (ObjId0, ObjId1, ObjId2, etc.))
	val _atomicCommit = new AtomicBoolean(false)
	var _restartTxns = new ArrayBuffer[Int]()

	// Reset any of the relevant data for the Parallel Txn's.
	private def resetData() {
	  //  /*
	    _orderNumber = 0
	    _futuresBuffer.clear()
        _committed = 0
        _txnBlocks.clear()
        //_syncLock.set(true)
        _readMap.clear()
        _writeMap.clear()
        _txnWrites.clear()
        _atomicCommit.set(false)
        _restartTxns.clear()
    //    */
        /*
	    _orderNumber = 0
	    _futuresBuffer = new ArrayBuffer[Future[_]]()
	    _committed = 0
	    _txnBlocks = mutable.Map[Int, Tuple2[TxnExecutor, (InTxn => _)]]()
	    _readMap = mutable.Map[Int, Tuple2[Handle[_], mutable.Map[Int, ArrayBuffer[Int]]]]()
	    _writeMap = mutable.Map[Int, mutable.Map[Int, Tuple2[Handle[_], _]]]()
	    _txnWrites = mutable.Map[Int, ArrayBuffer[Int]]()
	    _atomicCommit.set(false)
	    _restartTxns = new ArrayBuffer[Int]()
	    */
	}

	// Interface.
	def executor: TxnExecutor = throw new AbstractMethodError
	def status: Status = _currentLevel.statusAsCurrent
	override def rootLevel: MotTxnLevel = internalCurrentLevel.root
	def currentLevel: NestingLevel = throw new AbstractMethodError
	def rollback(cause: RollbackCause): Nothing = {
		// We need to grab the version numbers from writes and pessimistic reads
		// before the status is set to rollback, because as soon as the top-level
		// txn is marked rollback other threads can steal ownership.  This is
		// harmless if some other type of rollback occurs.

		_currentLevel.forceRollback(cause)
		throw RollbackError
	}

	@throws(classOf[InterruptedException])
	def retry(): Nothing = rollback(ExplicitRetryCause(None))

	def retryFor(timeoutNanos: Long) { throw new AbstractMethodError }

	// Pre-transaction state.
	def pushAlternative(block: InTxn => Any): Boolean = {
		val z = _alternatives.isEmpty
		_alternatives ::= block
		z
	}

	private def takeAlternatives(): List[InTxn => Any] = {
		val z = _alternatives
		_alternatives = Nil
		z
	}

	def setCommitHandler(block: InTxn => Any): Boolean = {
		val z = _commitHandler == null && _abortHandler == null
		_commitHandler = block
		z
	}

	def setAbortHandler(block: InTxn => Any): Boolean = {
		val z = _commitHandler == null && _abortHandler == null
		_abortHandler = block
		z
	}

	private def takeHandlers(): Tuple2[InTxn => Any, InTxn => Any] = {
		val z = (_commitHandler, _abortHandler)
		_commitHandler = null
		_abortHandler = null
		z
	}

	// Cache open object.
	def recordOpen(obj: HObj) {
		_currentLevel.ocRecOpen(obj)
	}

	// Deferred register and cache open object.
	def recordRegister(obj: HObj) {
		_currentLevel.drRecRegister(obj)
		_currentLevel.ocRecOpen(obj)
	}

	// Not used?
	def recordDelete(id: String) {
		//_currentLevel.ddRecDelete(id)
	}

	/** Determine which type of atomic block to use. **/
	def atomic[Z](exec: TxnExecutor, block: InTxn => Z): Z = {
		// Execute appropriate level.
		if (_currentLevel == null) {
			openAtomic(exec, block)
		}
		else if (MotCfg.PARALLEL_STRICT) {
			parallelStrictAtomic(exec, block)
			null.asInstanceOf[Z]
		}
		else if (MotCfg.PARALLEL_RELAXED) {
			parallelRelaxedAtomic(exec, block)
			null.asInstanceOf[Z]
		}
		else if (MotCfg.CLOSED_NESTING) {
		    closedNestedAtomic(exec, block)
		    //null.asInstanceOf[Z]
		}
		else {
			flatNestedAtomic(exec, block)
		}
	}

	def configAtomic[Z](exec: TxnExecutor, block: InTxn => Z): Z =
		HyflowConfig.cfg.getString("hyflow.motstm.atomicConfig") match {
			case "flat" => flatNestedAtomic(exec, block)
			case "closed" => closedNestedAtomic(exec, block)
			case "open" => openAtomic(exec, block)
			case "parallelStrict" => { parallelStrictAtomic(exec, block); null.asInstanceOf[Z] }
			case "parallelRelaxed" => { parallelRelaxedAtomic(exec, block); null.asInstanceOf[Z] }
			case _ => throw new Exception("Hyflow config error: hyflow.motstm.atomicConfig must be one of flat, closed, open, parallelStrict, or parallelRelaxed.")
		}

	/** PARALLEL (STRICT) NESTING **/
	private def parallelStrictAtomic[Z](exec: TxnExecutor, block: InTxn => Z) {
		logger.debug("Root {}: Creating parallel (strict) Txn({}).", this.toString, _orderNumber.toString)

		// Set up Txn's order.
		val txnOrderNumber = _orderNumber
		_orderNumber += 1

		// Set up Txn's previous sibling reference (the other Txn to wait on).
		val prevSibling =
		    if (txnOrderNumber > 0) { _futuresBuffer(txnOrderNumber - 1) }
		    else { null }

		// Create a Future for the Txn, add to the root's buffer, and start it. The result isn't necessary (for now).
		implicit val executionContext = Hyflow.system.dispatchers.lookup("akka.actor.parallel-dispatcher")
		_futuresBuffer += Future {
		    val parallelTxn = new ParallelTxn(this, _currentLevel, txnOrderNumber, prevSibling)
		    val res = parallelTxn.runStrict(exec, block)
		}
	}

	/** PARALLEL (RELAXED) NESTING **/
	private def parallelRelaxedAtomic[Z](exec: TxnExecutor, block: InTxn => Z) {
	    logger.debug("Root {}: Creating parallel (relaxed) Txn({}).", this.toString, _orderNumber.toString)

	    // Set up Txn's order.
        val txnOrderNumber = _orderNumber
        _orderNumber += 1

        // Track the sub-transaction.
        _txnBlocks += (txnOrderNumber -> Tuple2(exec, block))

        // Create a Future for the Txn, add to the root's buffer, and start it. The result isn't necessary (for now).
        implicit val executionContext = Hyflow.system.dispatchers.lookup("akka.actor.parallel-dispatcher")
        _futuresBuffer += Future {
	        val parallelTxn = new ParallelTxn(this, _currentLevel, txnOrderNumber, null)
	        val res = parallelTxn.runRelaxed(exec, block)
	    }
	}

	private def parallelRelaxedRetry[Z](child: Int) {
        logger.debug("Root {}: Retrying parallel Txn({}).", this.toString, _orderNumber.toString)

        // Grab the executor and block.
        val (exec, block) = _txnBlocks(child)

        // Restart the child, but use a NEW Future object and a new Txn.
        implicit val executionContext = Hyflow.system.dispatchers.lookup("akka.actor.parallel-dispatcher")
        _futuresBuffer(child) = Future {
            val parallelTxn = new ParallelTxn(this, _currentLevel, child, null)
            val res = parallelTxn.runRelaxed(exec, block)
        }
    }

	/** OPEN NESTING **/
	def openAtomic[Z](exec: TxnExecutor, block: InTxn => Z): Z = {
		val handlers = takeHandlers()

		var prevFailures = 0
		var prevRetries = 0
		_pessimisticMode = MotCfg.ALWAYS_PESSIMISTIC

		val topLevel = _currentLevel == null
		val topLevelDbg = if (topLevel) "TOP LEVEL" else "OPEN NESTED"
		//tmr_onTxnStart()

		(while (true) {
			logger.debug("BEGIN {} BLOCK. | prevFailures = {} | block = {}",
			             topLevelDbg,
			             prevFailures toString,
			             block.##.toHexString)

			// Create a new level for this transaction (root or child).
			var level = new MotTxnLevel(this, null, exec, _currentLevel, true, MotStats(block))
			level.rootAbortCounter = prevFailures

			// Pre-determined pessimistic mode. By default, false.
			if (level.stats.isPessimisticRequired)
				_pessimisticMode = true

			try {
			    logger.warn("Root starting.")
				// Successful attempt or permanent rollback either returns a Z or throws an exception if != RollbackError.
				val res = openAttempt(prevFailures, level, block)

				// Successful attempt here, so update the stats.
				if (topLevel) {
					Hyflow._topAborts += prevFailures
					Hyflow._topCommits += 1
				} else {
					Hyflow._openNestedAborts += prevFailures
					Hyflow._openNestedCommits += 1
				}

				// Execute commit handlers registered with this level by its children.
				logger.trace("Executing Children's onCommit handlers")
				for (commitHandler <- level.onrGetOnCommitHandlers) {
					openAtomic(exec, commitHandler)
				}

				// Register own handlers into parent.
				if (level.parLevel != null)
					level.parLevel.onrAddHandlers(handlers._1, handlers._2)

				//tmr_onTxnCommit()
				logger.warn("Root returning.")
				return res
			} catch {
				case RollbackError =>
				// Pass through.
			}

			// We are only here for a transient rollback or an explicit
			// retry. We have to release locks regardless of the cause.
			releasePessimisticLocks(level)

			val waitingFor = if (isExplicitRetry(level.status)) {
				logger.trace("{} block: explicit retry.", topLevelDbg)
				if (MotCfg.CONDITIONAL_SYNC) {
					// Conditional sync: await retry using notifications.
					val updNotif = prepareAwaitRetry(level)
					level = null
					Hyflow._topAborts += prevFailures
					prevFailures = 0
					prevRetries += 1
					Some(updNotif)
				} else {
					// Backoff for explicit retry.
					val backoff = ContentionManager.prepareBackoff(level, prevRetries)
					Hyflow._topAborts += prevFailures
					prevFailures = 0
					prevRetries += 1
					backoff
				}
			} else {
				logger.trace("{} block: abort.", topLevelDbg)
				prevFailures += 1 // Transient rollback, retry.
				prevRetries = 0

				val backoff = if (!level.root.rootAborting) {
					//tmr_onTxnAbort()
					//updateAbortLevelHists(level)
					val backoff = ContentionManager.prepareBackoff(level, level.rootAbortCounter)
					level.root.rootAbortCounter += 1
					backoff
				} else {
					None
				}
				backoff
			}

			// Waiting for some set time limit.
			if (waitingFor != None) {
				Await.ready(waitingFor.get, 30 seconds)
			}

			// For Parallel Relaxed, if the children haven't all aborted, wait on them.
            if (MotCfg.PARALLEL_STRICT || MotCfg.PARALLEL_RELAXED) {
                // Flag the root as invalid to the children.
                level.valid = false

                // Wait on the children to complete.
                if (_futuresBuffer.size > 0) {
                    logger.trace("Root waiting for the parallel Txn's to fail.")
                    for (futureX <- _futuresBuffer) {
                        Await.ready(futureX, 30 seconds)
                    }
                }

                // Reset the concurrency data.
                resetData()
            }

			// Hack: work around resetting level.root.rootAbortCounter.
			prevFailures = level.rootAbortCounter
		}).asInstanceOf[Nothing]
	}

	private def openAttempt[Z](prevFailures: Int, level: MotTxnLevel, block: InTxn => Z): Z = {
		beginAttempt(level)
		TaggedOps.__tag_TXN_BEGIN(this, block.getClass.getName)
		checkPessimistic(prevFailures, block)
		try {
			runBlock(block)
		} finally {
			rethrowFromStatus(openComplete())
		}
	}

	// Open-atomic complete now allows for parallel children.
    private def openComplete(): Status = {
        // Get the number of parallel children (if the buffer has any).
        val parallelTxns = _futuresBuffer.size

        // Parallel Strict: Wait sequentially upon all children.
        if (MotCfg.PARALLEL_STRICT && parallelTxns > 0) {
            logger.warn("Root waiting on strict children.")
            for (futureX <- _futuresBuffer) {
                try { Await.ready(futureX, 30 seconds) }
                catch {
                    case _: java.util.concurrent.TimeoutException =>
                        logger.warn("Root got a timeout on Txn({})", _committed.toString())
                }

                // Signal that the next sequential child has completed.
                _committed += 1
            }
            logger.warn("Root past waiting on children.")
        }

        // Parallel Relaxed: Root waits for all commits (and restarts children as necessary).
        if (MotCfg.PARALLEL_RELAXED && parallelTxns > 0) {
            logger.warn("Root waiting on relaxed children.")
            // Cycle until all of the children are committed.
            while (_committed < parallelTxns) {
                // If atomicCommit == false, no child has committed at this time, so wait.
                // (Note that a synchronized wait wasn't working here.)
                while (!_atomicCommit.get()) { }

                logger.warn("Root handling next child commit.")

                val toRestart = _restartTxns.clone()
                _atomicCommit.set(false)
                logger.warn("Root signaled the child to go.")
                _restartTxns = new ArrayBuffer[Int]()

                // Check on all children flagged to restart.
                for (child <- toRestart) {
                    parallelRelaxedRetry(child)
                    _committed -= 1
                    Hyflow._parallelAborts += 1
                }

                _committed += 1
            }
            logger.warn("Root finishing things up.")
            // Set the read-set and the write-set to most recent data.
            for (objHash <- _readMap.keys) {
                _currentLevel.rsRecRead(_readMap(objHash)._1)
            }

            for (objHash <- _writeMap.keys) {
                val latest = _writeMap(objHash).keys.max
                val (handle, v) = _writeMap(objHash)(latest)
                _currentLevel.wsRecWrite(handle.asInstanceOf[Handle[Any]], v)
            }
            logger.warn("Root finished RS and WS.")

            // Ensure that the children are completed before committing the root.
            if (_futuresBuffer.size > 0) {
                for (futureX <- _futuresBuffer) {
                    Await.ready(futureX, 30 seconds)
                }
            }
            logger.warn("Root past waiting on children.")
        }

        if (_currentLevel.valid && attemptOpenComplete()) {
            TaggedOps.__tag_TXN_COMMIT(this)
            finishOpenCommit()
            Committed
        } else {
            TaggedOps.__tag_TXN_ABORT(this)
            val s = this.status
            val c = s.asInstanceOf[RolledBack].cause
            if (isExplicitRetry(c)) {
                logger.info("Open-atomic commit failed (explicit retry). | cause = {}", c)
                finishOpenRetry(s, c)
            } else {
                logger.info("Open-atomic commit failed (rollback). | cause = {}", c)
                finishOpenRollback(s, c)
            }
            s
        }
    }

    // Completion of open-atomic transaction.
    private def attemptOpenComplete(): Boolean = {
        val root = _currentLevel
        fireBeforeCommitCallbacks()

        // TFA prepare commit goes here.

        if (!root.tryActiveToPreparing()) {
            return false
        }

        //logger.warn("ROOT COMMITTING READ SET:\n{}", _currentLevel.rsHandleSet.map(_._hy_id).toString)
        //logger.warn("ROOT COMMITTING WRITE SET:\n{}\n\n", _currentLevel.wsGetHandles.map(_._hy_id).toString)

        if (!acquireLocks()) {
            return false
        }

        // Re-validate read-set.
        if (!rsValidate(false)) {
            releaseLocks()
            return false
        }

        // Do the final clock forwarding as necessary due to parallel transactions.
        if ((MotCfg.PARALLEL_STRICT || MotCfg.PARALLEL_RELAXED) && _futuresBuffer.size > 0) {
            if (!_currentLevel.rsHandleSet.isEmpty) {
                _currentLevel.root.startTime = _currentLevel.rsHandleSet.map(_._hy_ver).toSet.max
            }
        }

        //logger.warn("COMMITTING READ SET:\n{}", _currentLevel.rsHandleSet.map(_._hy_id).toString)
        //logger.warn("COMMITTING WRITE SET:\n{}", _currentLevel.wsGetHandles.map(_._hy_id).toString)

        fireWhilePreparingCallbacks()

        if (externalDecider != null) {
            if (!root.tryPreparingToPrepared() || !consultExternalDecider()) {
                releaseLocks()
                return false
            }

            root.setCommitting()
        } else {
            if (!root.tryPreparingToCommitting()) {
                releaseLocks()
                return false
            }
        }

        _pendingFailure = fireWhileCommittingCallbacks(_currentLevel.executor)

        // Short circuit commit for read-only transactions, does not increment clock.
        if (root.wsCount == 0) {
            releaseLocks()
            root.setCommitted()
            return true
        }

        // Incrementing node-local clock is the linearization point.
        val clk = TFAClock.inc
        commitWrites(clk)

        // Update all of the objects written.
        finalizeWrites()
        root.setCommitted()

        logger.info("Open-atomic atomic block commit succeeded.")

        return true
    }

	/** CLOSED NESTING **/
	private def closedNestedAtomic[Z](exec: TxnExecutor, block: InTxn => Z): Z = {
		// Clear handlers.
		takeHandlers()

		var prevFailures = 0
		(while (true) {
			logger.debug("Begin closed nested atomic block. | nestedPrevFailures = {} | block = {}",
			             prevFailures toString,
			             block.##.toHexString)

			_currentLevel.requireActive()

			val level = new MotTxnLevel(this, null, exec, _currentLevel, false, MotStats(block))
			level.root.rootAborting = false
			try {
				val res = closedNestedAttempt(prevFailures, level, block, -1)
				Hyflow._closedNestedAborts += prevFailures
				Hyflow._closedNestedCommits += 1
				return res
			} catch {
				case RollbackError =>
				// Pass through.
			}

			// We are only here if it is a transient rollback or an explicit retry.
			val cause = level.status.asInstanceOf[RolledBack].cause

			// Have to release locks regardless of the cause.
			releasePessimisticLocks(level)

			if (isExplicitRetry(cause)) {
				logger.debug("Closed nested atomic block: explicit retry.")
				_currentLevel.forceRollback(cause)
				throw RollbackError
			}

			logger.debug("Closed nested atomic block: abort.")
			prevFailures += 1 // Transient rollback, retry.

			if (_pessimisticMode && prevFailures > 2) {
				logger.debug("Aborting transaction chain due to too many sub-tx aborts in pessimistic mode.")
				_currentLevel.root.chainRollback(Txn.RolledBack(
					OptimisticFailureCause('max_nested_attempts_exceeded, None)))
				throw RollbackError
			}

			// Normal backoff.
			if (!level.root.rootAborting) {
				//tmr_onTxnAbort()
				updateAbortLevelHists(level)
				level.root.rootAborting = true
				val backoff = ContentionManager.prepareBackoff(level, level.root.rootAbortCounter)
				level.root.rootAbortCounter += 1
				if (backoff != None) {
					Await.ready(backoff.get, 30 seconds)
				}
			}
		}).asInstanceOf[Nothing]
	}

	private def closedNestedAttempt[Z](prevFailures: Int, level: MotTxnLevel, block: InTxn => Z, reusedReadThreshold: Int): Z = {
		beginAttempt(level)
		checkPessimistic(prevFailures, block)
		try {
			runBlock(block)
		} finally {
			rethrowFromStatus(closedNestedComplete())
		}
	}

	/** FLAT NESTING **/
	private def flatNestedAtomic[Z](exec: TxnExecutor, block: InTxn => Z): Z = {
		// Clear handlers.
		takeHandlers()

		_currentLevel.flatNestedDepth += 1
		logger.trace("Begin flat nested atomic block. | depth = {}", _currentLevel.flatNestedDepth toString)
		try {
			block(this)
		} catch {
			case x if x != RollbackError && !_currentLevel.executor.isControlFlow(x) =>
				// (Alex) TODO: Fix this [Partial rollback is required, but we can't do it here].
				// _flatNestingAllowed = false
				_currentLevel.forceRollback(OptimisticFailureCause('restart_to_enable_partial_rollback, Some(x)))
				throw RollbackError
		} finally {
			logger.trace("End flat nested atomic block. | depth = {}", _currentLevel.flatNestedDepth toString)
			_currentLevel.flatNestedDepth -= 1
		}
	}

	// Begin the attempt of child transaction.
	protected def beginAttempt(level: MotTxnLevel) {
		val par = level.parLevel

		if (par != null)
			if (!par._state.compareAndSet(null, level)) {
				logger.warn("beginAttempt: _state CAS failed. | val = {}", par._state.get)
				par._state.set(level)
			}
		_currentLevel = level
	}

	// Check pessimistic mode issues.
	private def checkPessimistic(prevFailures: Int, block: InTxn => Any) {
		if (MotCfg.ALLOW_PESSIMISTIC && !_pessimisticMode && prevFailures > 6) {
			_pessimisticMode = true
			_currentLevel.root.stats.countPessimistic()

			// Restart if this is not an open transaction.
			if (!_currentLevel.openNested) {
				//(Alex) TODO: I think this way of aborting causes inconsistencies.
				logger.debug("Restarting to enter pessimistic mode.")
				_currentLevel.root.chainRollback(Txn.RolledBack(OptimisticFailureCause('restart_in_pessimistic_mode, None)))
				throw RollbackError
			}
		}
	}

	// Release locks done in pessimistic mode.
	private def releasePessimisticLocks(level: MotTxnLevel) {
		if (_pessimisticMode) {
			logger.debug("Releasing pessimistic locks.")
			val txnid = getTxnId
			val handles = level.ocGetHandles()
			val lockSet = handles.map(_._hy_obj).toSet
			for (obj <- lockSet)
				if (obj._owner == null)
					logger.warn("Trying to release lock with null owner. | obj id = {}", obj._id)
				else
					Hyflow.locks.unlock(obj, obj._hy_id, txnid)
		}
	}

	// Execute the actual block of code.
	private def runBlock[Z](block: InTxn => Z): Z = {
		try {
			block(this)
		} catch {
			case x if x != RollbackError && !_currentLevel.executor.isControlFlow(x) => {
				logger.warn("Exception during transaction", x)
				_currentLevel.forceRollback(UncaughtExceptionCause(x))
				null.asInstanceOf[Z]
			}
		}
	}

	// Throw exceptions from MotTxnLevel Status.
	private def rethrowFromStatus(status: Status) {
		status match {
			case rb: RolledBack => {
				rb.cause match {
					case UncaughtExceptionCause(x) => throw x
					case _: TransientRollbackCause => throw RollbackError
				}
			}
			case _ =>
		}
	}

	private def consultExternalDecider(): Boolean = {
		try {
			if (!externalDecider.shouldCommit(this))
				_currentLevel.forceRollback(OptimisticFailureCause('external_decision, None))
		} catch {
			case x => _currentLevel.forceRollback(UncaughtExceptionCause(x))
		}
		this.status eq Prepared
	}

	// Forward the clock for TFA.
	def forward(rclk: Long) {
		if (rclk > _currentLevel.root.startTime) {
			logger.debug("Attempting to forward transaction. | rclk = {}", rclk toString)

			// Check for read-set validity.
			if (rsValidate(true)) {
				// Valid read-set, update Txn start time.
				_currentLevel.root.startTime = rclk
			} else {
				// Invalid read-set, abort.
			}
		}
	}

	// Update the objects in the write-set with data and versions (clocks).
	private def commitWrites(clk: Long) {
		logger.debug("Committing writes. | clk = {}", clk toString)
		val items = _currentLevel.wsGetValueList
		for ((handle, v) <- items) {
			handle._hy_data = v
			handle._hy_ver = clk
			handle._hy_obj._hy_ver = clk
		}
	}

	// Release all locks for objects.
	private def releaseLocks() {
		// Release all locks (in parallel, no need to sort).
		val root = _currentLevel
		val txnid = getTxnId

		val handles = if (_pessimisticMode) {
			// Handles are retrieved from the open-cache.
			root.ocGetHandles
		} else {
			// Handles are retrieved from the write-set items.
			root.wsGetHandles
		}
		val lockSet = handles.map(_._hy_obj).toSet

		for (o <- lockSet)
			if (o._owner == null)
				logger.warn("Trying to release lock with null owner. | obj id = {}", o._id)
			else
				Hyflow.locks.unlock(o, o._hy_id, txnid)
	}

	// At the end of Commit (upon success), completely finalize written objects.
	private def finalizeWrites() {
		logger.trace("Finalizing writes.")

		for ((id, obj) <- _currentLevel.drGetDeferred) {
			logger.warn("Registering new object. | id = {}", id)
			Hyflow.store.put(obj)
			Tracker.register0(obj, false)
		}

		val items = _currentLevel.wsGetValueList
		var updated = mutable.Set[HObj]()
		val txnid = getTxnId

		// Unlock, register, update items in the write-set.
		for ((handle, v) <- items) {
			_currentLevel.ocGetCached[HObj](handle._hy_obj._id) match {
				case Some(obj) =>
					if (obj != None) {
					    // Check if the object is locally-owned.
						if (obj.isLocalObject) {
							// Release lock from local object. No need to update ownership.
							logger.trace("Unlocking already local object. | id = {} | ver = {}",
							             handle._hy_obj._id toString,
							             handle._hy_obj._hy_ver toString)

							// The set just keeps track of unique updated objects.
							if (!updated.contains(obj)) {
								updated += obj
								Hyflow.locks.unlock(obj, obj._hy_id, txnid)

								// For update notifications, we have to notify tracker of an update.
								if (MotCfg.CONDITIONAL_SYNC)
									Tracker.responsiblePeer(obj._id) ! MessagesTracker.UpdateVerMsg(obj._id, obj._hy_ver)
							}
						} else {
						    logger.trace("Updating a remote object. | id = {}", obj._id)

							// Update the object with our ownership.
						    /*
							if ((!updated.contains(obj)) && (obj._owner != null)) {
								updated += obj

								// Notify old owner of loss and to release their lock. New LockAcq should fail.
								Hyflow.store.lost(obj, txnid)

								logger.trace("Informing tracker that I'm object's new owner. | id = {}", obj._id)
								Hyflow.store.put(obj)
								Tracker.register0(obj, false)
							}
							*/

						    // Update the object by sending it to the owner.
						    if ((!updated.contains(obj)) && (obj._owner != null)) {
						        updated += obj
						        Hyflow.store.update(obj, txnid)
						        Tracker.responsiblePeer(obj._id) ! MessagesTracker.UpdateVerMsg(obj._id, obj._hy_ver)
						    }
						}
					}
				case None =>
			}
		}

		// Also release other pessimistic locks.
		if (_pessimisticMode) {
			logger.trace("Releasing pessimistic locks.")
			val lockSet = _currentLevel.ocGetHandles.map(_._hy_obj).toSet
			for (obj <- lockSet) {
				if (!updated.contains(obj)) {
					updated += obj
					if (obj._owner == null)
						logger.warn("Trying to release lock with null owner. | obj id = {}", obj._id)
					else
						Hyflow.locks.unlock(obj, obj._hy_id, txnid)
				}
			}
		}
	}

	// Read-set validation.
	private def rsValidate(abort: Boolean): Boolean = {
		if (_pessimisticMode) {
			logger.trace("Skip validating readset (pessimistic mode).")
			return true
		}
		logger.trace("(Root) Validating read-set.")

		// Need an ExecutionContext, so let's use our ActorSystem's dispatcher.
		implicit val executor: ExecutionContext = Hyflow.system.dispatcher

		// Collect a set of all handles to validate.
		var crt = _currentLevel
		val txnid = getTxnId
		val handles = mutable.Set[Handle[_]]()

		var fidCache = mutable.Map[MotTxnLevel, Set[Handle.FID]]()
		while (crt != null) {
			val crtSet = crt.rsHandleSet
			fidCache(crt) = crtSet.map(_._hy_obj._hy_id)
			handles ++= crtSet
			crt = crt.parLevel
		}

		// Group handles based on the node that they're from.
		val handle_grps = mutable.Map[ActorRef, mutable.Set[Handle[_]]]()
		for (h <- handles) {
			val grp = handle_grps.getOrElseUpdate(h._hy_obj._owner, mutable.Set())
			grp += h._hy_obj
		}

		// Send all of the validation requests.
		val resp_f0 = for ((remote, grp) <- handle_grps if remote != null) yield Hyflow.store.validate(remote, grp.toList, txnid)
		val resp_f = Future.sequence(resp_f0)

		// Block! Await validation results.
		val resp = Await.result(resp_f, 30 seconds)

		// We want to mark all invalid objects (so we know which level to abort to), thus:
		// wait for all of the responses, and do not use resp.exists(...).
		val fidCacheIm = fidCache.toMap
		val res = resp.map(checkValidationResp(_, fidCacheIm)).foldLeft(true)(_ && _)

		// Rollback transactions if needed.
		if (!res) {
			logger.debug("Invalid read-set. Rolling back...")
			_currentLevel.root.verifyChainOrRollback(OptimisticFailureCause('invalid_readset, None))
			if (abort) {
				throw RollbackError
			}
		} else {
			logger.trace("Validation succeeded.")
		}
		res
	}

	// TFA: Compare newest object versions with the Txn starting time.
	private def checkValidationResp(valRespMsg: StoreProvider.ValidateResp,
		fidCache: Map[MotTxnLevel, Set[Handle.FID]]): Boolean = {

		def failed(fid: (String, Int), ver: Option[Long]) {
			// Validation failed.
			logger.trace("Validating object failed. | fid = {} | rmtver = {} | txstart = {}",
				fid toString,
				ver toString,
				_currentLevel.root.startTime toString)

			// Mark MotTxnLevel containing the invalid object.
			var crt = _currentLevel
			while (crt != null) {
				logger.trace("In loop. | fidCache(crt) = {} | fid = {}", fidCache(crt), fid)
				if (fidCache(crt).contains(fid)) {
					crt.valid = false
				}
				crt = crt.parLevel
			}
		}

		TFAClock.incoming(valRespMsg)

		// Run through the responses. If versions are invalid, mark as failed.
		var res = true
		for ((fid, ver) <- valRespMsg.vers) {
			logger.trace("Processing validation response for entry. | fid = {} | ver = {}",
				fid, ver toString)
			ver match {
				case None =>
					failed(fid, ver)
					res = false
				case Some(ver2) if ver2 > _currentLevel.root.startTime =>
					failed(fid, ver)
					res = false
				case _ =>
					logger.debug("Validating object succeeded. | fid = {} | rmtver = {} | txstart = {}",
						fid,
						ver toString,
						_currentLevel.root.startTime toString)
			}
		}
		res
	}

	// Acquire locks for written objects.
	private def acquireLocks(): Boolean = {
		if (_pessimisticMode) {
			logger.trace("Skip acquiring locks (already acquired: pessimistic mode).")
			return true
		}

		val txnid = getTxnId
		logger.trace("Trying to acquire locks.")

		// Need an ExecutionContext, so let's use our ActorSystem's dispatcher.
		implicit val executor: ExecutionContext = Hyflow.system.dispatcher

		// Acquire all locks (in parallel, no need to sort).
		val root = _currentLevel
		val handles = root.wsGetHandles

		// Per-object locks.
		val objMap = handles.map(x => (x._hy_obj._id, x._hy_obj)).toMap

		val resp_f = Future.sequence(for ((key, obj) <- objMap if obj._owner != null)
			yield Hyflow.locks.lock(obj, obj._hy_id, txnid))

		// Block! Await lock results.
		val resp = Await.result(resp_f, 30 seconds)

		// Check if any of the locks failed.
		if (resp.exists(_.result == false)) {
			// If any of the locks failed, release all of them.
			for (msg <- resp) {
				if (msg.result == true) {
					Hyflow.locks.unlock(objMap(msg.fid._1), msg.fid, getTxnId)
				}
				else {
					logger.debug("Lock failed. | fid = {}", msg.fid)
				}
			}
			root.forceRollback(OptimisticFailureCause('acquire_locks_failed, None))
			false
		} else {
			logger.trace("Acquired all locks. | count = {}", resp.size toString)
			true
		}
	}

	protected def finishOpenCommit() {
		val handlers = resetCallbacks()
		val exec = _currentLevel.executor

		_currentLevel.stats.countCommit()
		_currentLevel = _currentLevel.parLevel

        // Reset all relative data.
		resetData()

		val f = _pendingFailure
		_pendingFailure = null
		fireAfterCompletionAndThrow(handlers, exec, Committed, f)
	}

	private def finishOpenRollback(s: Status, c: RollbackCause) {
		val handlers = rollbackCallbacks()
		val exec = _currentLevel.executor

		_currentLevel.stats.countAbort()
		_currentLevel = _currentLevel.parLevel

		val f = _pendingFailure
		_pendingFailure = null
		fireAfterCompletionAndThrow(handlers, exec, s, f)
	}

	private def finishOpenRetry(s: Status, c: RollbackCause) {
		val handlers = rollbackCallbacks()

		// Don't detach, but we do need to give up the current level.
		val exec = _currentLevel.executor
		assert(_currentLevel.wsCount == 0)
		_currentLevel = _currentLevel.parLevel

		if (handlers != null)
			_pendingFailure = fireAfterCompletion(handlers, exec, s, _pendingFailure)

		if (_pendingFailure != null) {
			// Scuttle the retry.
			val f = _pendingFailure
			_pendingFailure = null
			throw f
		}
	}

	// Closed Nesting completion.
	private def closedNestedComplete(): Status = {
		val child = _currentLevel

		if (child.attemptMerge()) {
			child.parLevel.mergeFrom(child)
			_currentLevel.stats.countCommit()
			_currentLevel = child.parLevel
			Committed
		} else {
			val s = this.status

			// Callbacks must be last, because they might throw an exception.
			val handlers = rollbackCallbacks()
			_currentLevel.stats.countAbort()
			_currentLevel = child.parLevel
			if (handlers != null)
				fireAfterCompletionAndThrow(handlers, child.executor, s, null)
			s
		}
	}

	// Creates Future to await Explicit Retry using Conditional Sync.
	private def prepareAwaitRetry(level: MotTxnLevel): Future[_] = {
		// Object modified notification policy.
		implicit val executor = Hyflow.system.dispatcher
		val timeout = 20 seconds      // level.minRetryTimeoutNanos
		val waitOn = level.rsHandleSet.map(_._hy_obj)
		val waitFutures = for (obj <- waitOn) yield ask(Tracker.responsiblePeer(obj._id), MessagesTracker.ReqUpdNotifMsg(obj._id, obj._hy_ver))(timeout)
		val firstResponse = Future.firstCompletedOf(waitFutures)

		// End object modified notifications.
		firstResponse
	}

	// Attempt to find objects cached in OC.
	def findCachedObject[T <: HObj](id: String): Option[T] = {
		var level = _currentLevel
		do {
			val res = level.ocGetCached[T](id)
			logger.trace("Trying to find cached object. | id = {} | level = {} | res = {}", id, level, res)
			if (res != None) { return res }
			level = level.parLevel
		} while (level != null)
		None
	}

	private var attemptStartTime: Option[Long] = None
	private var hasAbort = false
	/*
	val tmr_firstAbort = metrics.timer("firstAbort", durationUnit = TimeUnit.MICROSECONDS)
	val tmr_failedRetry = metrics.timer("failedRetry", durationUnit = TimeUnit.MICROSECONDS)
	val tmr_succRetry = metrics.timer("succRetry", durationUnit = TimeUnit.MICROSECONDS)
	val tmr_noAbortTxn = metrics.timer("noAbortTxn", durationUnit = TimeUnit.MICROSECONDS)
	val hst_abortedLevels = metrics.histogram("abortedLevels", biased = false)
	val hst_abortToLevel = metrics.histogram("abortToLevel", biased = false)
    */

	private def tmr_onTxnAbort() {
		val startTime = attemptStartTime.get
		val finalTime = Clock.defaultClock().tick()
		val duration = finalTime - startTime
		attemptStartTime = Some(finalTime)

		if (hasAbort) {
			// This is a failed retry.
			//tmr_failedRetry.update(duration, TimeUnit.NANOSECONDS)
		} else {
			// This is the first abort.
			//tmr_firstAbort.update(duration, TimeUnit.NANOSECONDS)
		}

		hasAbort = true
	}

	private def tmr_onTxnCommit() {
		// Top transaction committed.
		val finalTime = Clock.defaultClock().tick()
		val duration = finalTime - attemptStartTime.get
		attemptStartTime = None

		if (hasAbort) {
			//tmr_succRetry.update(duration, TimeUnit.NANOSECONDS)
		} else {
			//tmr_noAbortTxn.update(duration, TimeUnit.NANOSECONDS)
		}
	}

	private def tmr_onTxnStart() {
		attemptStartTime = Some(Clock.defaultClock().tick())
		hasAbort = false
	}

	private def updateAbortLevelHists(level: MotTxnLevel) {
		val crtLevel = level.depth
		var next = level
		while (next.parLevel != null && next.parLevel.status != Txn.Active) {
			next = next.parLevel
		}
		val nextLevel = next.depth
		//hst_abortToLevel.update(nextLevel)
		//hst_abortedLevels.update(1 + crtLevel - nextLevel)
	}

}
