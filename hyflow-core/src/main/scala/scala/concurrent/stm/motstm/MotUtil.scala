/**
 * General Config Setup.
 */

package scala.concurrent.stm
package motstm


import Txn._
import org.hyflow.core.util.HyflowConfig
import skel.RollbackError

object MotUtil {
	// Retry cause.
	def isExplicitRetry(status: Status): Boolean = isExplicitRetry(status.asInstanceOf[RolledBack].cause)
	def isExplicitRetry(cause: RollbackCause): Boolean = cause.isInstanceOf[ExplicitRetryCause]
	
	// Exceptional termination.
	def rethrowFromStatus(status: Status) {
		status match {
			case rb: RolledBack => {
				rb.cause match {
					case UncaughtExceptionCause(x) => throw x
					case _: TransientRollbackCause => throw RollbackError
				}
			}
			case _ =>
		}
	}
	
	// Configuration.
	object MotCfg {
		val CONDITIONAL_SYNC = HyflowConfig.cfg.getBoolean("hyflow.motstm.conditionalSync")
		val CLOSED_NESTING = HyflowConfig.cfg.getBoolean("hyflow.motstm.closedNesting")
		val ALWAYS_PESSIMISTIC = HyflowConfig.cfg.getBoolean("hyflow.motstm.alwaysPessimistic")
		val ALLOW_PESSIMISTIC = HyflowConfig.cfg.getBoolean("hyflow.motstm.allowPessimistic")
		val PARALLEL_STRICT = HyflowConfig.cfg.getBoolean("hyflow.motstm.parallelStrict")
		val PARALLEL_RELAXED = HyflowConfig.cfg.getBoolean("hyflow.motstm.parallelRelaxed")
	}
}
