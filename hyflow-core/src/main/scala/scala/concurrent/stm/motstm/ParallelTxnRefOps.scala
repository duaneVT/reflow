/**
 * Parallel Transaction Operations
 */

package scala.concurrent.stm
package motstm

import org.hyflow.api.Handle
import scala.concurrent.stm.motstm.MotUtil.MotCfg

/** ParallelTxn inherits this trait. **/
trait ParallelTxnRefOps {
    // Running object must be a parallel transaction.
    this: ParallelTxn =>

    // Read an object from its Handle.
	def get[T](handle: Handle[T]): T = {
	    /** Method 1: Parallel Strict **/
	    if (MotCfg.PARALLEL_STRICT) {
    		// Obtain the current sub-transaction.
    		var crtLevel = internalCurrentLevel

    		// Check for the object in the sub-transaction's write-set.
    		var wsval = crtLevel.wsGet(handle)

    		// Move upwards through the transactions to search for the object, if needed.
    		while (wsval == None && crtLevel != null) {
    			wsval = crtLevel.wsGet(handle)
    			crtLevel = crtLevel.parLevel
    		}

    		// Record the object in the read-set.
    		internalCurrentLevel.rsRecRead(handle)

    		// Take the object, or if it is empty, return the Handle's data.
    		val res = wsval.getOrElse(handle._hy_data)

    		// Log and return the result.
    		/*
    		if (res.isInstanceOf[Iterable[_]] || res.isInstanceOf[Iterator[_]]) {
                logger.trace("Getting field. | fid = {} | result = {} | from write-set = {}",
                              handle._hy_id.toString,
                              "Iterator too large to print",
                              (wsval != None).toString)
            }
            else {
                logger.trace("Getting field. | fid = {} | result = {} | from write-set = {}",
                              handle._hy_id.toString,
                              res.toString,
                              (wsval != None).toString)
            }
            */

    		res
	    }

		/** Method 2: Parallel Relaxed **/
	    else {
	        // Record the read by handle.
	        internalCurrentLevel.rsRecRead(handle)

	        // Gather the relevant info.
	        val writeMap = getRoot._writeMap
	        val order = getOrder
	        val hash = handle._hy_hash

	        // Attempt to read the root write-map.
	        if (writeMap.contains(hash)) {
	            val entry = writeMap(hash)
	            val lesserVersions = entry.keys.filter(_ < order)

	            // If there are earlier versions, read the latest one and track it.
	            if (!lesserVersions.isEmpty) {
	                val previous = lesserVersions.max
	                val data = entry(previous)._2.asInstanceOf[T]
	                internalCurrentLevel.vtAdd(hash, previous, data)
	                /*
	                if (data != null) {
	                    if (data.isInstanceOf[Iterable[_]] || data.isInstanceOf[Iterator[_]]) {
                            logger.trace("Getting field. | fid = {} | result = {} | from write-set = {}",
                                          handle._hy_id.toString,
                                          "Iterator too large to print",
                                          previous.toString)
	                    }
                        else {
                            logger.trace("Getting field. | fid = {} | result = {} | from write-set = {}",
                                          handle._hy_id.toString,
                                          data.toString,
                                          previous.toString)
                        }
	                }
	                else {
	                    logger.trace("Field not found. | fid = {}", handle._hy_id.toString)
	                }
	                */

	                return data
	            }
	        }

	        // No earlier versions existed.
	        internalCurrentLevel.vtAdd(handle._hy_hash, 0, handle._hy_data)
	        /*
	        if (handle._hy_data != null) {
	            if (handle._hy_data.isInstanceOf[Iterable[_]] || handle._hy_data.isInstanceOf[Iterator[_]]) {
    	            logger.trace("Getting field. | fid = {} | result = {} | version = external",
    	                         handle._hy_id.toString,
    	                         "Iterator too large to print")
    	        }
    	        else {
                    logger.trace("Getting field. | fid = {} | result = {} | version = external",
                                 handle._hy_id.toString,
                                 handle._hy_data.toString)
    	        }
	        }
	        else {
	            logger.trace("Field not found. | fid = {}", handle._hy_id.toString)
	        }
	        */

            handle._hy_data
	    }
	}

	// Unused (or disallowed) reading functions.
	def getWith[T, Z](handle: Handle[T], f: T => Z): Z = throw new AbstractMethodError
	def relaxedGet[T](handle: Handle[T], equiv: (T, T) => Boolean): T = throw new AbstractMethodError
	def unrecordedRead[T](handle: Handle[T]): Unit /*UnrecordedRead[T]*/ = throw new AbstractMethodError

	// Write an object using its Handle.
	def set[T](handle: Handle[T], v: T): Unit = {
		// Simply log and store the value in the write-set immediately.
		logger.trace("Setting field. | fid = {} | value = {}", handle._hy_id, v)
		internalCurrentLevel.wsRecWrite(handle, v)
	}

	// Unused (or disallowed) writing functions.
	def swap[T](handle: Handle[T], v: T): T = throw new AbstractMethodError
	def trySet[T](handle: Handle[T], v: T): Boolean = throw new AbstractMethodError
	def compareAndSet[T](handle: Handle[T], before: T, after: T): Boolean = throw new AbstractMethodError
	def compareAndSetIdentity[T, R <: T with AnyRef](handle: Handle[T], before: R, after: T): Boolean = throw new AbstractMethodError
	def getAndTransform[T](handle: Handle[T], func: T => T): T = throw new AbstractMethodError
	def transformAndGet[T](handle: Handle[T], func: T => T): T = throw new AbstractMethodError
	def transformIfDefined[T](handle: Handle[T], pf: PartialFunction[T, T]): Boolean = throw new AbstractMethodError
	def getAndAdd(handle: Handle[Int], delta: Int): Int = throw new AbstractMethodError

	// Unused (or disallowed) TxnLocal functions.
	/*
	 * We store transactional local values in the write buffer by pretending
	 * that they are proper handles, but their data and metadata aren't actually
	 * backed by anything.
	 *
	 * def txnLocalFind(local: TxnLocalImpl[_]): Int = findWrite(local)
	 * def txnLocalGet[T](index: Int): T = getWriteSpecValue[T](index)
	 * def txnLocalInsert[T](local: TxnLocalImpl[T], v: T) { writeAppend(local, false, v) }
	 * def txnLocalUpdate[T](index: Int, v: T) { writeUpdate(index, v) }
	 *
	 */
}