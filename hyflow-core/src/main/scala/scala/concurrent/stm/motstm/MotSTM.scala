package scala.concurrent.stm
package motstm

import scala.concurrent.stm._
import java.util.concurrent.TimeUnit
import com.typesafe.scalalogging.slf4j.Logging

object MotSTM extends MotTxnExecutor with impl.STMImpl with MotRefFactory with Logging {
	def findCurrent(implicit mt: MaybeTxn): Option[InTxn] = {
	    val c = Option(MotInTxn.currentOrNull)
	    if (c != None)
	        return c

	    Option(ParallelTxn.currentOrNull)
	}
	
	def dynCurrentOrNull: InTxn = {
	    val c = MotInTxn.dynCurrentOrNull
	    if (c != null)
	        return c

	    ParallelTxn.dynCurrentOrNull
	}

	/*
    def findCurrent(implicit mt: MaybeTxn): Option[InTxn] = Option(MotInTxn.currentOrNull)
    def dynCurrentOrNull: InTxn = MotInTxn.dynCurrentOrNull
    */

	/** Hashes 'base' with 'offset'. **/
	def hash(base: AnyRef, offset: Int): Int = hash(base) ^ (0x40108097 * offset)

	/** Hashes 'base' using its identity hash code. **/
	def hash(base: AnyRef): Int = {
		val h = System.identityHashCode(base)
		// Multiplying by -127 is recommended by java.util.IdentityHashMap.
		h - (h << 7)
	}

	// Commit barrier.
	def newCommitBarrier(timeout: Long, unit: TimeUnit): CommitBarrier = throw new AbstractMethodError
}
