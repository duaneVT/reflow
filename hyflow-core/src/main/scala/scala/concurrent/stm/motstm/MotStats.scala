package scala.concurrent.stm.motstm

import scala.concurrent.stm.InTxn
import java.util.concurrent._
import java.util.concurrent.atomic._
import com.typesafe.scalalogging.slf4j.Logging

object MotStats extends Logging {
	val counters = new ConcurrentHashMap[String, MotStats]()
	def apply(block: InTxn => Any): MotStats = {
		val bcn = block.getClass.getName
		val old = counters.get(bcn)
		if (old == null) {
			val repl = new MotStats(bcn)
			val old = counters.putIfAbsent(bcn, repl)
			if (old != null) old else repl
		} else {
			old
		}
	}
	
	def logAll() {
		import scala.collection.JavaConversions.asScalaSet
		for (entry <- counters.keySet) {
			logger.info(s"${entry} -> ${counters.get(entry)}") 
		}
	}
}

class MotStats(val blockClassName: String) extends Logging {
	val aborts = new AtomicLong(0)
	val commits = new AtomicLong(0)
	val pessimistic = new AtomicLong(0)
	val permanentPessimistic = new AtomicBoolean(false)
	
	def countAbort() {
		aborts.incrementAndGet()
	}
	
	def countCommit() {
		commits.incrementAndGet()
	}
	
	def countPessimistic() {
		logger.debug(s"Block executed in pessimistic mode. | block = ${blockClassName} | stats = ${this}")
		pessimistic.incrementAndGet()
	}
	
	// (Alex) TODO: turn into some kind of a moving average, to revert back to 
	// optimistic mode when contention has lowered
	def isPessimisticRequired(): Boolean = {
		val c = commits.get
		if (permanentPessimistic.get) {
			true
		// (Alex) TODO: make parameters tunable
		} else if (c > 5 && pessimistic.get > 0.75 * c) {
			logger.info("Entering permanent pessimistic mode. | block = {}", blockClassName)
			permanentPessimistic.set(true)
			true
		} else false
	}
	
	override def toString = "MotStats(commits=%s, aborts=%s, pessimistic=%s)".format(
			commits.get, 
			aborts.get, 
			pessimistic.get
		)
}
