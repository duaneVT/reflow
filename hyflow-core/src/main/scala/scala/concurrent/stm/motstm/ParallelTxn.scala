/**
 * Parallel Transaction (Main Transaction Code)
 */

package scala.concurrent.stm
package motstm

import skel._
import akka.actor._
import scala.concurrent.{ Await, Future, ExecutionContext, Promise }
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import org.hyflow.Hyflow
import org.hyflow.api._
import org.hyflow.core.directory._
import org.hyflow.core._
import org.hyflow.core.store._
import org.hyflow.core.util._
import com.typesafe.scalalogging.slf4j.Logging
import scala.collection.mutable
import scala.annotation.tailrec
import scala.util.Random
import java.util.concurrent.atomic.AtomicLong
import scala.concurrent.stm.motstm.MotUtil.MotCfg
import scala.util.{ Try, Success, Failure }
import com.yammer.metrics.core.Clock
import java.util.concurrent.TimeUnit

private[motstm] object ParallelTxn extends ThreadLocal[ParallelTxn] {
	override def initialValue = new ParallelTxn(null, null, 0, null) // This should not be needed.

	def apply()(implicit mt: MaybeTxn): ParallelTxn = mt match {
		case x: ParallelTxn => x
		case _ => get // Create a new one for the current thread.
	}

	private def active(txn: ParallelTxn): ParallelTxn = if (txn.internalCurrentLevel != null) txn else null
	def dynCurrentOrNull: ParallelTxn = active(get)

	def currentOrNull(implicit mt: MaybeTxn) = active(apply())
}

/** Parallel Transaction **/
private[motstm] class ParallelTxn(rootTxn: MotInTxn, rootParLevel: MotTxnLevel, orderNumber: Int, prevSibling: Future[_]) extends ParallelTxnRefOps with AbstractInTxn
                                                                                                                          with Logging with ReInstrumented
{
	import Txn._
	import MotUtil._

	// Pre-transaction state.
	private var _alternatives: List[InTxn => Any] = Nil
	private var _commitHandler: InTxn => Any = null
	private var _abortHandler: InTxn => Any = null
	private var _siblingRollback: Boolean = false

	val _hy_txnid = scala.util.Random.nextInt
	def getTxnId: Long = (Hyflow._hy_nodeid << 32L) | this._hy_txnid

	implicit val timeout = Timeout(20 seconds)
	private var _pendingFailure: Throwable = null

	// Level for the parallel sub-Txn's.
	protected def internalCurrentLevel: MotTxnLevel = _currentLevel
	private var _currentLevel: MotTxnLevel = null

	// Get relevant info of the transaction.
	protected def getRoot: MotInTxn = rootTxn
	protected def getOrder: Int = orderNumber

	// Parallel Relaxed, allow for immediate retry.
	private var immediate = false

	// Interface.
	def executor: TxnExecutor = throw new AbstractMethodError
	def status: Status = _currentLevel.statusAsCurrent
	override def rootLevel: MotTxnLevel = internalCurrentLevel.root
	def currentLevel: NestingLevel = throw new AbstractMethodError
	def rollback(cause: RollbackCause): Nothing = {
		// We need to grab the version numbers from writes and pessimistic reads
		// before the status is set to rollback, because as soon as the top-level
		// txn is marked rollback other threads can steal ownership.  This is
		// harmless if some other type of rollback occurs.

		_currentLevel.forceRollback(cause)
		logger.debug("Rollback error! cause = {}", cause.toString())
		throw RollbackError
	}

	@throws(classOf[InterruptedException])
	def retry(): Nothing = rollback(ExplicitRetryCause(None))

	def retryFor(timeoutNanos: Long) { throw new AbstractMethodError }

	// Pre-transaction state.
	def pushAlternative(block: InTxn => Any): Boolean = {
		val z = _alternatives.isEmpty
		_alternatives ::= block
		z
	}

	private def takeAlternatives(): List[InTxn => Any] = {
		val z = _alternatives
		_alternatives = Nil
		z
	}

	def setCommitHandler(block: InTxn => Any): Boolean = {
		val z = _commitHandler == null && _abortHandler == null
		_commitHandler = block
		z
	}

	def setAbortHandler(block: InTxn => Any): Boolean = {
		val z = _commitHandler == null && _abortHandler == null
		_abortHandler = block
		z
	}

	private def takeHandlers(): Tuple2[InTxn => Any, InTxn => Any] = {
		val z = (_commitHandler, _abortHandler)
		_commitHandler = null
		_abortHandler = null
		z
	}

	// Cache open object.
	def recordOpen(obj: HObj) {
		_currentLevel.ocRecOpen(obj)
	}

	// Deferred register and cache open object.
	def recordRegister(obj: HObj) {
		_currentLevel.drRecRegister(obj)
		_currentLevel.ocRecOpen(obj)
	}

	// Not used?
	def recordDelete(id: String) {
		//_currentLevel.ddRecDelete(id)
	}

	// Don't allow for another atomic() for now.
	def atomic[Z](exec: TxnExecutor, block: InTxn => Z): Z = throw new AbstractMethodError
    def configAtomic[Z](exec: TxnExecutor, block: InTxn => Z): Z = throw new AbstractMethodError

	/** STRICT PROTOCOL (1) **/
	def runStrict[Z](exec: TxnExecutor, block: InTxn => Z): Z = {
	    var prevFailures = 0
	    var done = false
	    var res: Z = null.asInstanceOf[Z]

        logger.debug("Begin parallel (strict) nested Txn({}) with Root={}. | block = {} | thisTxn = {}",
                     orderNumber.toString,
                     rootTxn.toString,
                     block.##.toHexString,
                     this.toString)

        while (!done) {
            // Begin the level and transaction.
            val level = new MotTxnLevel(rootTxn, this, exec, rootParLevel, false, null)
            level.root.rootAborting = false
            try {
                _currentLevel = level
                logger.warn("({}) Parallel (strict) Txn({}) starting.", this.toString, orderNumber.toString)
                res = runBlock(block)
                rethrowFromStatus(parallelStrictComplete())
                //Hyflow._parallelAborts += prevFailures
                done = true
            } catch {
                case RollbackError =>
                    logger.trace("({}) Parallel (strict) nested atomic block: abort Txn({}).", this.toString, orderNumber.toString)
                    prevFailures += 1

                    // If the root is no longer valid, this child needs to abort.
                    if (!rootParLevel.valid) {
                        logger.trace("({}) Root is no longer valid: abort Txn({}).", this.toString, orderNumber.toString)
                        //Hyflow._parallelAborts += prevFailures
                        res = null.asInstanceOf[Z]
                        done = true
                    }
                    // If the rollback is not due to a sibling, then make a backoff.
                    else if (!_siblingRollback) {
                        val backoff = ContentionManager.prepareBackoff(level, prevFailures)
                        //level.root.rootAbortCounter += 1
                        if (backoff != None) {
                            Await.ready(backoff.get, 30 seconds)
                        }
                    }
            }
        }
	    logger.warn("({}) Parallel (strict) Txn({}) returning.", this.toString, orderNumber.toString)
	    return res
    }

    private def parallelStrictComplete(): Status = {
        logger.warn("({}) Parallel (strict) Txn({}) waiting to complete.", this.toString, orderNumber.toString)

        // Wait until the previous sibling has completed (in-order).
        if (orderNumber > 0 && prevSibling != null) {
            Await.ready(prevSibling, 30 seconds)
        }

        logger.warn("({}) Parallel (strict) Txn({}) in sync section.", this.toString, orderNumber.toString)

        // If the root is not valid, throw a Rollback which will cause an abort (because of !valid) in the main loop.
        if (!rootParLevel.valid) {
            throw RollbackError
        }

        // Sibling validation. Only performed once. If it already rolled back, then it is valid due to order.
        val child = _currentLevel
        if (!_siblingRollback) {
            val myReadSet = child.rsHandleSet.toList
            val myReadSetVer = myReadSet.map(x => (x._hy_id._1, x._hy_ver)).toList
            val myReadSetNames = myReadSetVer.map(_._1).toList

            val rootWriteSet = rootParLevel.wsGetHandles
            val rootWriteSetVer = rootWriteSet.map(x => (x._hy_id._1, x._hy_ver)).toList
            val rootWriteSetNames = rootWriteSetVer.map(_._1).toList

            val intersect = myReadSetNames.intersect(rootWriteSetNames)

            if (!intersect.isEmpty) {
                Hyflow._parallelAborts += 1
                logger.trace("Intersection between {} and {}: {}", orderNumber.toString, (orderNumber-1).toString, intersect.toString)
                /* Duane: The versioning/clock is not intermediately updated. Cannot test based on this.
                 *
                 * for (account <- intersect) {
                 *     if (rootWriteSetVer.find({x => x._1 == account}).get._2 > myReadSetVer.find({x => x._1 == account}).get._2) {
                 *         _currentLevel.valid = false
                 *         throw RollbackError
                 *     }
                 * }
                 */
                _siblingRollback = true
                throw RollbackError
            }
        }

        // Successful validation.
        //logger.info("Merging to parLevel.")
        child.parLevel.mergeFrom(child)
        logger.warn("({}) Parallel (strict) Txn({}) committed.", this.toString, orderNumber.toString)
        Committed
    }


    /** RELAXED PROTOCOL (2) **/
    def runRelaxed[Z](exec: TxnExecutor, block: InTxn => Z): Z = {
        var prevFailures = 0
        var done = false
        var res: Z = null.asInstanceOf[Z]

        logger.debug("Begin parallel (relaxed) Txn({}) with Root={}. | block = {} | thisTxn = {}",
                     orderNumber.toString,
                     rootTxn.toString,
                     block.##.toHexString,
                     this.toString)

        while (!done) {
            // Begin the level and transaction.
            val level = new MotTxnLevel(rootTxn, this, exec, rootParLevel, false, null)
            level.root.rootAborting = false
            try {
                _currentLevel = level
                logger.warn("({}) Parallel (relaxed) Txn({}) starting.", this.toString, orderNumber.toString)
                res = runBlock(block)
                rethrowFromStatus(parallelRelaxedComplete())

                // Note that it is possible for conflicts to be detected after commit, but only
                // if this Txn read some object X that is written by a Txn ordered before it.

                //Hyflow._parallelAborts += prevFailures

                // Mark the transaction as complete.
                done = true
            } catch {
                case RollbackError =>
                    prevFailures += 1

                    logger.trace("({}) Parallel (relaxed) nested atomic block: abort Txn({}).", this.toString, orderNumber.toString)

                    // If the root is no longer valid, this child needs to abort.
                    if (!rootParLevel.valid) {
                        logger.trace("({}) Root is no longer valid: abort Txn({}).", this.toString, orderNumber.toString)
                        //Hyflow._parallelAborts += prevFailures
                        res = null.asInstanceOf[Z]
                        done = true
                    }
                    // Check whether to backoff or not (e.g., an internal conflict might allow immediate retry).
                    else if (!immediate) {
                        val backoff = ContentionManager.prepareBackoff(level, prevFailures)
                        if (backoff != None) {
                            Await.ready(backoff.get, 30 seconds)
                        }
                    }
                    immediate = false
            }
        }
        logger.warn("({}) Parallel (relaxed) Txn({}) returning.", this.toString, orderNumber.toString)
        return res
    }

    private def parallelRelaxedComplete(): Status = {
        // If the root is not valid, throw a Rollback which will cause an abort (because of !valid) in the main loop.
        if (!rootParLevel.valid) {
            throw RollbackError
        }

        logger.warn("({}) Parallel (relaxed) Txn({}) waiting to complete.", this.toString, orderNumber.toString)

        // Grab the read and write maps from the root.
        val readMap = rootTxn._readMap
        val writeMap = rootTxn._writeMap
        val txnWrites = rootTxn._txnWrites

        // Validate this sub-transaction: Observe its read-set and see if any are out-of-date due to sibling updates.
        val rsList = _currentLevel.rsHandleSet.toList

        // Wait until this transaction can synchronize with the root and lock out siblings.
        rootTxn._syncLock.synchronized {
            logger.warn("({}) Parallel (relaxed) Txn({}) entered sync section.", this.toString, orderNumber.toString)

            // Check the root's validity again.
            if (!rootParLevel.valid) { throw RollbackError }

/**
            val Hashes = rsList.map(handle => handle._hy_hash)
            val VersionData = Hashes.map(hash => _currentLevel.vtGet(hash))
            val LocalRead = VersionData.map(versionData => versionData._1 > 0)

            val syncLockOpen = rootTxn._syncLockOpen
            while (!syncLockOpen.compareAndSet(true, false)) { }

            // Check the root's validity again.
            if (!rootParLevel.valid) {
                syncLockOpen.set(true)
                throw RollbackError
            }

            val Contained = Hashes.map(hash => (hash, writeMap.contains(hash)))
            val LRC = LocalRead.zip(Contained)
            val Check_LRC = LRC.map(x => x._1 && !x._2._2)
            if (Check_LRC.contains(true)) {
                syncLockOpen.set(true)
                logger.warn("({}) Parallel (relaxed) nested atomic block: empty map for Txn({}).", this.toString, orderNumber.toString)
                throw RollbackError
            }

            val HashContained = Contained.filter(x => x._2 == true)
            val MapEntries = HashContained.map(x => writeMap(x._1))

            val LesserVersions = MapEntries.map(mapEntry => mapEntry.keys.filter(_ < orderNumber))
            val LVE_LR = LesserVersions.zip(LocalRead).map(x => x._1.isEmpty && x._2)
            if (LVE_LR.contains(true)) {
                syncLockOpen.set(true)
                logger.warn("({}) Parallel (relaxed) nested atomic block: Txn({}) found no versions for an object.", this.toString, orderNumber.toString)
                throw RollbackError
            }

            val LV_DONE = LesserVersions.filter(x => x.isEmpty == false)
            val PV = LV_DONE.map(x => x.max)
            val PV_VD = PV.zip(VersionData)

            val wrongVersion = PV_VD.map(x => x._1 != x._2._1)
            if (wrongVersion.contains(true)) {
                immediate = true
                syncLockOpen.set(true)
                logger.warn("({}) Parallel (relaxed) nested atomic block: wrong version for Txn({}).", this.toString, orderNumber.toString)
                throw RollbackError
            }

            val dataEntries = PV_VD.zip(MapEntries)
            val wrongData = dataEntries.map(x => x._2(x._1._1)._2 == x._1._2._2)
            if (wrongData.contains(false)) {
                immediate = true
                syncLockOpen.set(true)
                logger.warn("({}) Parallel (relaxed) nested atomic block: inconsistent data for Txn({}).", this.toString, orderNumber.toString)
                throw RollbackError
            }
**/

            for (rsEntry <- rsList) {
                // Grab the hash ID and version from the entry.
                val hash = rsEntry._hy_hash
                val (version, data) = _currentLevel.vtGet(hash)

                // Check if the Txn read from a sibling and if the map even contains the object.
                val localRead = (version > 0)
                val contains = writeMap.contains(hash)

                // If the Txn read from a sibling but the map is now empty for the object, rollback (backoff).
                if (localRead && !contains) {
                    logger.trace("({}) Parallel (relaxed) nested atomic block: empty map for Txn({}).", this.toString, orderNumber.toString)
                    throw RollbackError
                }

                // Find the most recent version before the current Txn and check versions.
                if (contains) {
                    val mapEntry = writeMap(hash)
                    val lesserVersions = mapEntry.keys.filter(_ < orderNumber)

                    if (!lesserVersions.isEmpty) {
                        val previous = lesserVersions.max

                        // If another sibling wrote the object, or the same version is not consistent, rollback (immediate).
                        if (previous != version) {
                            Hyflow._parallelAborts += 1
                            immediate = true
                            logger.trace("({}) Parallel (relaxed) nested atomic block: wrong version for Txn({}).", this.toString, orderNumber.toString)
                            throw RollbackError
                        }
                        else if (mapEntry(previous)._2 != data) {
                            Hyflow._parallelAborts +=1
                            immediate = true
                            logger.trace("({}) Parallel (relaxed) nested atomic block: inconsistent data for Txn({}).", this.toString, orderNumber.toString)
                            throw RollbackError
                        }

                        // SUCCESS: This point means that the object versions match up. Continue to the next object.
                    }
                    // If the Txn read from a sibling but no versions exist anymore, rollback (backoff).
                    else if (localRead) {
                        Hyflow._parallelAborts += 1
                        logger.trace("({}) Parallel (relaxed) nested atomic block: Txn({}) found no versions for an object.", this.toString, orderNumber.toString)
                        throw RollbackError
                    }
                }
            }

            if (!rootParLevel.valid) { throw RollbackError }

/**
            val RM_CONT = Hashes.map(x => (readMap.contains(x), x)).zip(rsList).zip(VersionData.map(_._1))
            val RM_CONT_T = RM_CONT.filter(_._1._1._1 == true)
            val RM_CONT_F = RM_CONT.filter(_._1._1._1 == false)

            for (x <- RM_CONT_F) {
                readMap += (x._1._1._2 -> (x._1._2, mutable.Map(x._2 -> mutable.ArrayBuffer[Int](orderNumber))))
            }
            for (y <- RM_CONT_T) {
                val objMap = readMap(y._1._1._2)._2
                if (objMap.contains(y._2)) {
                    objMap(y._2) += orderNumber
                }
                else {
                    objMap += (y._2 -> mutable.ArrayBuffer[Int](orderNumber))
                }
            }
**/

            // Add the read-flag to the root. Note that this is added only now, because if the flags were added in the above loop,
            // the Txn could still abort. If it did, then it would have to remove all of the already-added flags.
            for (rsEntry <- rsList) {
                // Grab the hash ID and version from the entry.
                val hash = rsEntry._hy_hash
                val version = _currentLevel.vtGet(hash)._1

                // If the root read-map does not have the entry, add it.
                if (!readMap.contains(hash)) {
                    readMap += (hash -> (rsEntry, mutable.Map(version -> mutable.ArrayBuffer[Int](orderNumber))))
                }
                // If the read-map does have the object, check for the version entry.
                else {
                    val objMap = readMap(hash)._2
                    if (!objMap.contains(version)) {
                        objMap += (version -> mutable.ArrayBuffer[Int](orderNumber))
                    }
                    else {
                        objMap(version) += orderNumber
                    }
                }
            }

            // Validate previously-committed transactions and formalize the writes.
            val wsList = _currentLevel.wsGetValueList

/**
            val WriteHashes = wsList.map(x => (x, x._1._hy_hash))
            val WriteContained = WriteHashes.map(x => (x._2, readMap.contains(x._2))).filter(x => x._2 == true)
            val RM_WriteEntries = WriteContained.map(x => readMap(x._1)._2)
            val Write_LesserVersions = RM_WriteEntries.map(x => (x, x.keys.filter(_ < orderNumber))).filter(x => x._2.isEmpty == false).map(x => (x._1, x._2.max))
            val InvalidSiblings = mutable.Set[Int](Write_LesserVersions.map(x => (x._1(x._2).filter(_ > orderNumber))).flatten:_*)

            for (sibling <- InvalidSiblings) {
                if (txnWrites.contains(sibling)) {
                    // (2) Iterate objects written by sibling and remove those entries.
                    for (obj <- txnWrites(sibling)) {
                        if (readMap.contains(obj)) {
                            // (3) Flag all of the new invalid siblings.
                            if (readMap(obj)._2.contains(sibling)) {
                                InvalidSiblings ++= readMap(obj)._2(sibling)
                                readMap(obj)._2(sibling).clear()
                            }
                            // (4) Remove the write entry.
                            writeMap(obj) -= sibling
                        }
                    }
                    // (5) Remove the write-log entry for sibling.
                    txnWrites(sibling).clear()
                }
            }

            rootTxn._restartTxns ++= InvalidSiblings

            for (myHash <- WriteHashes) {
            if (!writeMap.contains(myHash._2)) {
                    writeMap += (myHash._2 -> mutable.Map(orderNumber -> myHash._1))
                }
                else {
                    writeMap(myHash._2) += (orderNumber -> myHash._1)
                }

                if (!txnWrites.contains(orderNumber)) {
                    txnWrites += (orderNumber -> mutable.ArrayBuffer[Int](myHash._2))
                }
                else {
                    txnWrites(orderNumber) += myHash._2
                }
            }
**/

            for (wsEntry <- wsList) {
                // Grab the hash ID.
                val hash = wsEntry._1._hy_hash

                // Setup the invalidSiblings to check repeatedly (if a chain occurs).
                val invalidSiblings = mutable.ArrayBuffer[Int]()

                // Check if anything read this object.
                if (readMap.contains(hash)) {
                    val rmEntry = readMap(hash)._2
                    val lesserVersions = rmEntry.keys.filter(_ < orderNumber)

                    // If any Txn's greater than this read an older version, then they're out-of-date.
                    if (!lesserVersions.isEmpty) {
                        val previous = lesserVersions.max
                        invalidSiblings ++= rmEntry(previous).filter(_ > orderNumber)
                        rmEntry(previous) --= invalidSiblings
                    }
                }

                // Removing invalid siblings is done here before other siblings can validate;
                // otherwise transactions could validate while something they read is removed.

                // Follow the chain of invalid siblings:
                // (1) Iterate siblings that were flagged as invalid.
                for (sibling <- invalidSiblings) {
                    if (txnWrites.contains(sibling)) {
                        // (2) Iterate objects written by sibling and remove those entries.
                        for (obj <- txnWrites(sibling)) {
                            if (readMap.contains(obj)) {
                                // (3) Flag all of the new invalid siblings.
                                if (readMap(obj)._2.contains(sibling)) {
                                    invalidSiblings ++= (readMap(obj)._2(sibling) -- invalidSiblings) // Don't double-mark invalid siblings.
                                    readMap(obj)._2(sibling).clear()
                                }
                                // (4) Remove the write entry.
                                writeMap(obj) -= sibling
                            }
                        }
                        // (5) Remove the write-log entry for sibling.
                        txnWrites(sibling).clear()
                    }
                }

                // Append invalid to the root.
                rootTxn._restartTxns ++= invalidSiblings

                // Add this object to the root.
                if (!writeMap.contains(hash)) {
                    writeMap += (hash -> mutable.Map(orderNumber -> wsEntry))
                }
                else {
                    writeMap(hash) += (orderNumber -> wsEntry)
                }

                if (!txnWrites.contains(orderNumber)) {
                    txnWrites += (orderNumber -> mutable.ArrayBuffer[Int](hash))
                }
                else {
                    txnWrites(orderNumber) += hash
                }
            }

            // Signal the root to restart the invalid siblings.
            _currentLevel.parLevel.ocMergeFrom(_currentLevel)
            //_currentLevel.parLevel.mergeFrom(_currentLevel)
            val atomicCommit = rootTxn._atomicCommit
            atomicCommit.set(true)

            logger.warn("({}) Parallel (relaxed) Txn({}) waiting on root.", this.toString, orderNumber.toString)

            //logger.error("Thread {} waiting on root.", orderNumber.toString)
            // Wait on the return from the root. Check the root in case it fails.
            while (atomicCommit.get()) {
                if (!rootParLevel.valid) {
                    atomicCommit.set(false)
                    throw RollbackError
                }
            }
        }
        logger.warn("({}) Parallel (relaxed) Txn({}) committed.", this.toString, orderNumber.toString)
        Committed
    }

	// Begin the attempt of child transaction.
    protected def beginAttempt(level: MotTxnLevel) {
        val par = level.parLevel

        if (par != null)
            if (!par._state.compareAndSet(null, level)) {
                logger.warn("beginAttempt: _state CAS failed. | val = {}", par._state.get)
                par._state.set(level)
            }
        _currentLevel = level
    }

    // Execute the actual block of code.
    private def runBlock[Z](block: InTxn => Z): Z = {
        try {
            val z = block(this)
            z
        } catch {
            case x if x != RollbackError && !_currentLevel.executor.isControlFlow(x) => {
                logger.warn("Exception during transaction", x)
                _currentLevel.forceRollback(UncaughtExceptionCause(x))
                null.asInstanceOf[Z]
            }
        }
    }

    // Throw exceptions from MotTxnLevel Status.
	private def rethrowFromStatus(status: Status) {
		status match {
			case rb: RolledBack => {
				rb.cause match {
					case UncaughtExceptionCause(x) => throw x
					case _: TransientRollbackCause => throw RollbackError
				}
			}
			case _ =>
		}
	}

	// Read-set validation.
	private def rsValidate(abort: Boolean): Boolean = {
        // Need an ExecutionContext, so let's use our ActorSystem's dispatcher.
		implicit val executor: ExecutionContext = Hyflow.system.dispatcher

		// Collect a set of all handles to validate.
		var crt = _currentLevel
		val txnid = getTxnId
		val handles = mutable.Set[Handle[_]]()
		var fidCache = mutable.Map[MotTxnLevel, Set[Handle.FID]]()

		while (crt != null) {
			val crtSet = crt.rsHandleSet
			fidCache(crt) = crtSet.map(_._hy_obj._hy_id)
			handles ++= crtSet
			crt = crt.parLevel
		}

		// Group handles based on the node that they're from.
		val handle_grps = mutable.Map[ActorRef, mutable.Set[Handle[_]]]()
		for (h <- handles) {
			val grp = handle_grps.getOrElseUpdate(h._hy_obj._owner, mutable.Set())
			grp += h._hy_obj
		}

		// Send all of the validation requests.
		val resp_f0 = for ((remote, grp) <- handle_grps if remote != null) yield Hyflow.store.validate(remote, grp.toList, txnid)
		val resp_f = Future.sequence(resp_f0)

		// Block! Await validation results.
		val resp = Await.result(resp_f, 30 seconds)

        // We want to mark all invalid objects (so we know which level to abort to), thus:
        // wait for all of the responses, and do not use resp.exists(...).
		val fidCacheIm = fidCache.toMap
		val res = resp.map(checkValidationResp(_, fidCacheIm)).foldLeft(true)(_ && _)

		// Rollback transactions if needed.
		if (!res) {
			logger.debug("Invalid read-set. Rolling back...")
			_currentLevel.root.verifyChainOrRollback(
				OptimisticFailureCause('invalid_readset, None))
			if (abort)
				throw RollbackError
		} else {
			logger.trace("Validation succeeded.")
		}
		res
	}

	// TFA: Compare newest object versions with the Txn starting time.
	private def checkValidationResp(valRespMsg: StoreProvider.ValidateResp,
		fidCache: Map[MotTxnLevel, Set[Handle.FID]]): Boolean = {

		def failed(fid: (String, Int), ver: Option[Long]) {
			// Validation failed.
			logger.debug("Validating object failed. | fid = {} | rmtver = {} | txstart = {}",
				fid toString,
				ver toString,
				_currentLevel.root.startTime toString)

			// Mark MotTxnLevel containing invalid object.
			var crt = _currentLevel
			while (crt != null) {
				logger.trace("In loop. | crt = {} | fidCache(crt) = {} | fid = {}", crt, fidCache(crt), fid)
				if (fidCache(crt).contains(fid)) {
					crt.valid = false
				}
				crt = crt.parLevel
			}
		}

		TFAClock.incoming(valRespMsg)

		// Run through the responses. If versions are invalid, mark as failed.
		var res = true
		for ((fid, ver) <- valRespMsg.vers) {
			logger.debug("Processing validation response for entry. | fid = {} | ver = {}",
				fid, ver toString)
			ver match {
				case None =>
					failed(fid, ver)
					res = false
				case Some(ver2) if ver2 > _currentLevel.root.startTime =>
					failed(fid, ver)
					res = false
				case _ =>
					logger.debug("Validating object succeeded. | fid = {} | rmtver = {} | txstart = {}",
					             fid,
					             ver.toString,
					             _currentLevel.root.startTime.toString)
			}
		}
		res
	}

	// Attempt to find objects cached in OC.
	def findCachedObject[T <: HObj](id: String): Option[T] = {
		var level = _currentLevel
		do {
			val res = level.ocGetCached[T](id)
			logger.trace("Trying to find cached object. | id = {} | level = {} | res = {}", id, level, res)
			if (res != None) return res
			level = level.parLevel
		} while (level != null)
		None
	}
}
