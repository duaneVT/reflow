/**
 * Transaction Levels (Top-Level + Children)
 */

package scala.concurrent.stm
package motstm

import java.util.concurrent.atomic.AtomicReference

import scala.annotation.tailrec
import scala.concurrent.stm.Txn
import scala.concurrent.stm.TxnExecutor
import scala.concurrent.stm.skel.RollbackError

import com.typesafe.scalalogging.slf4j.Logging

private[motstm] class MotTxnLevel(
	val txn: MotInTxn,
	val parallelTxn: ParallelTxn,
	val executor: TxnExecutor,
	override val parLevel: MotTxnLevel,
	val openNested: Boolean,
	val stats: MotStats) extends skel.AbstractNestingLevel with AccessHistory with Logging {

	// Reference to the root of the Txn tree. This is NOT checking parLevel's state of null, but if parLevel itself is null.
	val root: MotTxnLevel = if (parLevel == null || openNested) this else parLevel.root

	// The current depth of this sub-transaction.
	val depth: Int = if (parLevel == null || openNested) 0 else (parLevel.depth + 1)

	// TFA: Transaction start time.
	var startTime: Long = TFAClock.get

	// Internal variables, most importantly its state (which is a CAS object).
	val _state = new AtomicReference[AnyRef]()
	var valid = true
	var flatNestedDepth = 0
	var rootAbortCounter = 0
	var rootAborting = false

	// Get the sub-transaction's status.
	@tailrec final def status: Txn.Status = {
	    // Read the raw state.
		val raw = _state.get()

		// Active is encoded as null for small requireActive methods. Otherwise,
		// "merged" means the sub-transaction is completed, while raw will point
		// to another TxnLevel if a child is currently active.
		if (raw == null)
			Txn.Active
		else if (raw eq "merged")
			parLevel.status
		else if (raw.isInstanceOf[MotTxnLevel])
			Txn.Active // Child is active.
		else
			raw.asInstanceOf[Txn.Status]
	}

	// Similar to the above, but just return whether currently active or not.
	def statusAsCurrent: Txn.Status = {
		val raw = _state.get
		if (raw == null)
			Txn.Active
		else
			raw.asInstanceOf[Txn.Status]
	}

	// Update the Txn.Status to preset values.
	def setCommitting() {
		_state.set(Txn.Committing)
	}

	def setCommitted() {
		_state.set(Txn.Committed)
	}

	def tryActiveToCommitted(): Boolean = {
		val f = _state.compareAndSet(null, Txn.Committed)
		f
	}

	def tryActiveToPreparing(): Boolean = {
		val f = _state.compareAndSet(null, Txn.Preparing)
		f
	}

	def tryPreparingToPrepared(): Boolean = _state.compareAndSet(Txn.Preparing, Txn.Prepared)

	def tryPreparingToCommitting(): Boolean = _state.compareAndSet(Txn.Preparing, Txn.Committing)

	/** Rollbacks **/
	// Must be called from the transaction's thread.
	def forceRollback(cause: Txn.RollbackCause) {
		val s = rollbackImpl(Txn.RolledBack(cause))
		assert(s.isInstanceOf[Txn.RolledBack])
	}

	// Is this usually called from other threads? (Alex) TODO: Check.
	def requestRollback(cause: Txn.RollbackCause): Txn.Status = {
		if (cause == Txn.ExplicitRetryCause)
			throw new IllegalArgumentException("Explicit retry is not available via requestRollback.")
		rollbackImpl(Txn.RolledBack(cause))
	}

	def chainRollback(rb: Txn.RolledBack): Txn.Status = {
		val raw = _state.get
		logger.trace("In ChainRollback: My current state is {}.", raw)
		if (raw.isInstanceOf[MotTxnLevel]) {
			raw.asInstanceOf[MotTxnLevel].chainRollback(rb)
		}
		rollbackImpl(rb)
	}

	@tailrec
	final def verifyChainOrRollback(cause: Txn.RollbackCause): Boolean = {
		if (!valid) {
			logger.trace("In verifyChainOrRollback, invalid level.")
			chainRollback(Txn.RolledBack(cause))
			return false
		}
		logger.trace("In verifyChainOrRollback: valid level.")
		val raw = _state.get()
		if (raw.isInstanceOf[MotTxnLevel]) {
			raw.asInstanceOf[MotTxnLevel].verifyChainOrRollback(cause)
		} else
			true
	}

	// This is tailrec for retries, but not when we forward to child.
	private def rollbackImpl(rb: Txn.RolledBack): Txn.Status = {
	    logger.trace("In RollbackImpl: My current state is {}.", _state.get)
		val raw = _state.get
		if (raw == null || canAttemptLocalRollback(raw)) {
			// Normal case (Active transaction, or ...)
			if (_state.compareAndSet(raw, rb)) {
				rb
			} else {
				// Lost race to update state. Need to retry using the new state.
				rollbackImpl(rb)
			}
		} else if (raw eq "merged") {
			// We are now taking our status from our parent.
			parLevel.rollbackImpl(rb)
		} else if (raw.isInstanceOf[MotTxnLevel]) {
			// Roll back the child first, then retry.
			raw.asInstanceOf[MotTxnLevel].rollbackImpl(rb)
			rollbackImpl(rb)
		} else {
			// Request denied.
			val res = raw.asInstanceOf[Txn.Status]
			logger.warn("Rollback denied. | status = {}", res)
			res
		}
	}

	// Check if a local rollback can be attempted.
	private def canAttemptLocalRollback(raw: AnyRef): Boolean = raw match {
		//case Txn.Prepared => MotInTxn.get eq txn // Remote cancel is not allowed while preparing. (Duane: This line does not allow ParallelTxn!)
	    case Txn.Prepared => {(MotInTxn.get eq txn) || (ParallelTxn.get eq txn)}
	    case s: Txn.Status => !s.decided
		case ch: MotTxnLevel => ch.rolledBackOrMerged
		case _ => false
	}

	// Check if the Txn is rolled back or merged to its parent.
	private def rolledBackOrMerged = _state.get match {
		case "merged" => true
		case Txn.RolledBack(_) => true
		case _ => false
	}

	// Ensure that a transaction is active--if not, throw exceptions.
	def requireActive() {
		if (_state.get != null) {
			status match {
				case Txn.RolledBack(_) => throw RollbackError
				case s => throw new IllegalStateException(s.toString)
			}
		}
	}

	// Child transaction completes and merges to the parent.
	def attemptMerge(): Boolean = {
		// First we need to set the current state to forwarding. Regardless of
		// whether or not this fails we still need to unlink the parent.
		val f = (_state.get == null) && _state.compareAndSet(null, "merged")

		// We must use CAS to unlink ourselves from parent, because we race with remote cancels.
		if (parLevel._state.get eq this)
			parLevel._state.compareAndSet(this, null)

		f
	}
}