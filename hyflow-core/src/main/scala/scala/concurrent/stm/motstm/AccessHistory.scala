/**
 * Accesses (Read- and Write-Sets, etc.)
 */

package scala.concurrent.stm.motstm

import org.hyflow.api._
import scala.collection.mutable
import scala.annotation.tailrec
import scala.concurrent.stm._
import com.typesafe.scalalogging.slf4j.Logging


/** Access History **/
object AccessHistory {
	trait ReadSet {
		def rsCount: Int                  // Number of elements in read-set
		def rsRecRead(handle: Handle[_])  // Record a read into the read-set
		def rsHandleSet: Set[Handle[_]]   // Set of object Handles
		def rsMergeFrom(child: ReadSet)   // Merge read-set into this one
	}
	trait WriteSet {
		def wsCount: Int                                    // Number of elements in write-set
		def wsRecWrite[T](handle: Handle[T], value: T)      // Record a write into the write-set
		def wsGet[T](handle: Handle[T]): Option[T]          // Read an object from the write-set
		def wsGetSortedHandles: List[Handle[_]]             // Read sorted list of object handles
		def wsGetHandles: List[Handle[_]]                   // Read a list of object handles in write-set
		def wsGetValueList: List[Tuple2[Handle[Any], Any]]  // Read a list of objects and their values
		def wsMergeFrom(child: WriteSet)                    // Merge write-set into this one
	}
	trait OpenCache {
		def ocCount: Int                                   // Size of open-cache
		def ocRecOpen(obj: HObj)                           // Open object into cache
		def ocGetCached[T <: HObj](id: String): Option[T]  // Get the object in the cache
		def ocMergeFrom(child: OpenCache)                  // Merge caches
	}
	trait DeferredRegister {
		def drRecRegister(obj: HObj)
		def drGetDeferred: Map[String,HObj]
	}
	trait OpenNestingRecord {
		def onrAddHandlers(onCommit: InTxn => Any, onAbort: InTxn => Any)
		def onrAddAbsLock(lock: String)
		def onrGetOnCommitHandlers: List[InTxn => Any]
		def onrGetOnAbortHandlers: List[InTxn => Any]
		def onrGetAbsLocks: List[String]
		def onrMergeFrom(child: OpenNestingRecord)
		def onrClear()
	}
}

/** Read-Set Entries (Object Handle + Hash) **/
object SimpleReadSetImpl {
	class Entry(val hash: Int, val handle: Handle[_])
}

/** Read-Set Operations **/
trait SimpleReadSetImpl extends AccessHistory.ReadSet {
    // ListBuffer containing object handles and their hashes.
	private val _rs = mutable.ListBuffer[SimpleReadSetImpl.Entry]()

	// Number of objects read.
	def rsCount: Int = _rs.size

	// Maps (hash, handle) list into set of (handle).
	def rsHandleSet: Set[Handle[_]] = _rs.toList.map(_.handle).toSet

	// Append a (hash, handle) to the ListBuffer.
	def rsRecRead(handle: Handle[_]) { _rs += new SimpleReadSetImpl.Entry(MotSTM.hash(handle), handle) }

	// Pass in another sub-transaction's read-set and merge the lists together.
	def rsMergeFrom(child: AccessHistory.ReadSet) { _rs.appendAll(child.asInstanceOf[SimpleReadSetImpl]._rs) }

	// Empty the read-set list.
	def rsClear() { _rs.clear() }

    // Map of versions read for Parallel Relaxed.
    private val _versionTracker = mutable.Map[Int, Tuple2[Int, _]]()

    // Add versions, get them, or clear them.
    def vtAdd[T](hash: Int, version: Int, value: T) { _versionTracker(hash) = (version, value) }
    def vtGet(hash: Int): Tuple2[Int, _] = { _versionTracker(hash) }
    def vtClear { _versionTracker.clear }
}

/** Write-Set Entries (Object Handle + Hash, Object Value) **/
object SimpleWriteSetImpl {
	class Entry(val hash: Int, val handle: Handle[_], val value: Any)
}

/** Write-Set Operations **/
trait SimpleWriteSetImpl extends AccessHistory.WriteSet {
    // Map containing handle.hash -> (hash, handle, value).
	private val _ws = mutable.Map[Int, SimpleWriteSetImpl.Entry]()

	// Number of objects written.
	def wsCount = _ws.size

	// Add an object to the write-set.
	def wsRecWrite[T](handle: Handle[T], value: T) {
		val hash = handle._hy_hash
		_ws(hash) = new SimpleWriteSetImpl.Entry(hash, handle, value)
		//println(s"Writing handle $handle with hash $hash")
	}

	// Lookup object by handle.hash and return value.
	def wsGet[T](handle: Handle[T]): Option[T] = {
		val hash = handle._hy_hash
		val entry = _ws.get(hash)
		entry.map(_.value.asInstanceOf[T])
	}

	// Return a sorted list of object handles (sorted by handle.hash).
	def wsGetSortedHandles(): List[Handle[_]] = _ws.keySet.toList.sorted.map(_ws(_).handle)

	// Return list of handles (unsorted).
	def wsGetHandles(): List[Handle[_]] = _ws.keySet.toList.map(_ws(_).handle)

	// Return a list of values in the form List[(handle, value)].
	def wsGetValueList(): List[Tuple2[Handle[Any], Any]] = {
		for ( (hash, entry) <- _ws.toList)
			yield (entry.handle.asInstanceOf[Handle[Any]], entry.value)
	}

	// Pass in another sub-transaction's write-set and merge the maps together.
	def wsMergeFrom(child: AccessHistory.WriteSet) { _ws ++= child.asInstanceOf[SimpleWriteSetImpl]._ws }

	// Empty the write-set map.
	def wsClear() { _ws.clear() }
}

/** Open-Cache Operations **/
trait SimpleOpenCacheImpl extends AccessHistory.OpenCache {
    // Track the accessed objects.
	private val _oc = mutable.Map[String, HObj]()

	// Open-cache size.
	def ocCount = _oc.size

	// Open object in the cache.
	def ocRecOpen(obj: HObj) { _oc(obj._id) = obj }

	// See if object was cached.
	def ocGetCached[T <: HObj](id: String): Option[T] = _oc.get(id).map(_.asInstanceOf[T])

	// See handles of objects cached.
	def ocGetHandles(): List[Handle[_]] = _oc.values.toList

	// Merged open-caches.
	def ocMergeFrom(child: AccessHistory.OpenCache) { _oc ++= child.asInstanceOf[SimpleOpenCacheImpl]._oc }

	// Clear the cache.
	def ocClear() { _oc.clear() }
}

/** Deferred Register Operations **/
trait SimpleDeferredRegisterImpl extends AccessHistory.DeferredRegister with Logging {
    private val _dr = mutable.Map[String, HObj]()
    def drRecRegister(obj: HObj) { _dr(obj._id) = obj }
    def drGetDeferred = _dr.toMap
    def drClear() { _dr.clear() }
    def drMergeFrom(child: AccessHistory.DeferredRegister) { _dr ++= child.asInstanceOf[SimpleDeferredRegisterImpl]._dr }
}

/** Open Nesting Record Operations **/
trait SimpleOpenNestingRecordImpl extends AccessHistory.OpenNestingRecord {
    private val _ch = mutable.ListBuffer[InTxn => Any]()
    private val _ah = mutable.ListBuffer[InTxn => Any]()
    private val _al = mutable.ListBuffer[String]()

    def onrAddHandlers(onCommit: InTxn => Any, onAbort: InTxn => Any) {
		if (onCommit != null) _ch.append(onCommit)
		if (onAbort != null) _ch.append(onAbort)
	}

    def onrAddAbsLock(lock: String) {
		if (lock != null) _al.append(lock)
	}

    def onrGetOnCommitHandlers: List[InTxn => Any] = _ch.toList

    def onrGetOnAbortHandlers: List[InTxn => Any] = _ah.toList

    def onrGetAbsLocks: List[String] = _al.toList

    def onrMergeFrom(child: AccessHistory.OpenNestingRecord) {
		val c = child.asInstanceOf[SimpleOpenNestingRecordImpl]
		_ch.appendAll(c._ch)
		_ah.appendAll(c._ah)
		_al.appendAll(c._al)
	}

    def onrClear() {
		_ch.clear()
		_ah.clear()
		_al.clear()
	}
}

/** Entire Access History contains all above **/
trait AccessHistory extends SimpleReadSetImpl
	with SimpleWriteSetImpl
	with SimpleOpenCacheImpl
	with SimpleDeferredRegisterImpl
	with SimpleOpenNestingRecordImpl {

    // Sub-transaction's reference to its parent's AccessHistory.
	def parLevel: AccessHistory

	// Merge all of the AccessHistory.
	def mergeFrom(child: AccessHistory) {
		rsMergeFrom(child)
		wsMergeFrom(child)
		ocMergeFrom(child)
		drMergeFrom(child)
		onrMergeFrom(child)
	}

	// Empty all of the AccessHistory.
	def clear() {
		rsClear()
		wsClear()
		ocClear()
		drClear()
		onrClear()
	}
}
