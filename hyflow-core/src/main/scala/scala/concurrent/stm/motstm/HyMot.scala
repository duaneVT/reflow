package scala.concurrent.stm.motstm

import org.hyflow.api._
import scala.concurrent.stm._
import com.typesafe.scalalogging.slf4j.Logging

// Interface into MotSTM from Hyflow. Implements HyflowBackendAccess methods.
object HyMot extends HyflowBackendAccess with Logging {

    /* Duane: Possible to split up into different files? */

    def isPessimisticMode(implicit mt: MaybeTxn): Boolean = {
        val txn = MotInTxn.currentOrNull
        if (txn != null) {
            return txn.pessimisticMode
        }
        return false

        /* All ParallelTxn's are NOT pessimisticMode.
        val parTxn = ParallelTxn.currentOrNull
        if (parTxn != null) {
            return parTxn.pessimisticMode
        }
        return false
        */
    }

    def recordOpen(obj: HObj)(implicit mt: MaybeTxn): Boolean = {
        val txn = MotInTxn.currentOrNull
        if (txn != null) {
            txn.recordOpen(obj)
            return true
        }

        val parTxn = ParallelTxn.currentOrNull
        if (parTxn != null) {
            parTxn.recordOpen(obj)
            return true
        }
        return false
    }

    def recordRegister(obj: HObj)(implicit mt: MaybeTxn): Boolean = {
        val txn = MotInTxn.currentOrNull
        if (txn != null) {
            if (txn.status != Txn.Active) {
                return false
            }
            txn.recordRegister(obj)
            return true
        }

        val parTxn = ParallelTxn.currentOrNull
        if (parTxn != null) {
            if (parTxn.status != Txn.Active) {
                return false
            }
            parTxn.recordRegister(obj)
            return true
        }
        return false
    }

    def recordDelete(id: String)(implicit mt: MaybeTxn): Boolean = {
        val txn = MotInTxn.currentOrNull
        if (txn != null) {
            txn.recordDelete(id)
            return true
        }

        var parTxn = ParallelTxn.currentOrNull
        if (parTxn != null) {
            parTxn.recordDelete(id)
            return true
        }
        return false
    }

    // (Alex) TODO: Unbind from MotInTxn!
    def getCrtTxnId(implicit mt: MaybeTxn): Long = {
        val txn = MotInTxn.currentOrNull
        if (txn != null) {
            return txn.getTxnId
        }

        val parTxn = ParallelTxn.currentOrNull
        if (parTxn != null) {
            return parTxn.getTxnId
        }
        return 0
    }

    def forward(rclk: Long)(implicit mt: MaybeTxn) {
        val txn = MotInTxn.currentOrNull
        if (txn != null) {
            // (Alex) TODO: is it safe to forward to latest clock available now,
            // or should we stick to the old procedure of forwarding to the
            // remote clock value that triggered this operation?
            txn.forward(rclk)
        }
        /*
        val parTxn = ParallelTxn.currentOrNull
        if (parTxn != null) {
            parTxn.forward(rclk)
        }
        */
    }

    def findCachedObject[T <: HObj](id: String)(implicit mt: MaybeTxn): Option[T] = {
        val txn = MotInTxn.currentOrNull
        if (txn != null) {
            return txn.asInstanceOf[MotInTxn].findCachedObject[T](id)
        }

        val parTxn = ParallelTxn.currentOrNull
        if (parTxn != null) {
            return parTxn.asInstanceOf[ParallelTxn].findCachedObject[T](id)
        }
        return None
    }

    /**
	def isPessimisticMode(implicit mt: MaybeTxn): Boolean = {
		val txn = MotInTxn.currentOrNull
		if (txn == null)
			false
		else {
			txn.pessimisticMode
		}
	}

	def recordOpen(obj: HObj)(implicit mt: MaybeTxn): Boolean = {
		val txn = MotInTxn.currentOrNull
		if (txn == null)
			false
		else {
			txn.recordOpen(obj)
			true
		}
	}

	def recordRegister(obj: HObj)(implicit mt: MaybeTxn): Boolean = {
		val txn = MotInTxn.currentOrNull
		if (txn == null || txn.status != Txn.Active)
			false
		else {
			txn.recordRegister(obj)
			true
		}
	}

	def recordDelete(id: String)(implicit mt: MaybeTxn): Boolean = {
		val txn = MotInTxn.currentOrNull
		if (txn == null)
			false
		else {
			txn.recordDelete(id)
			true
		}
	}

	// (Alex) TODO: unbind from MotInTxn!
	def getCrtTxnId(implicit mt: MaybeTxn): Long = {
		val txn = MotInTxn.currentOrNull
		if (txn == null)
			0
		else
			txn.getTxnId
	}

	def forward(rclk: Long)(implicit mt: MaybeTxn) {
		val txn = MotInTxn.currentOrNull
		if (txn != null) {
			// (Alex) TODO: is it safe to forward to latest clock available now,
			// or should we stick to the old procedure of forwarding to the
			// remote clock value that triggered this operation?
			txn.forward(rclk)
		}
	}

	def findCachedObject[T <: HObj](id: String)(implicit mt: MaybeTxn): Option[T] = {
		val txn = MotInTxn.currentOrNull
		if (txn != null) {
			txn.asInstanceOf[MotInTxn].findCachedObject[T](id)
		} else
			None
	}
	*
	*/
}