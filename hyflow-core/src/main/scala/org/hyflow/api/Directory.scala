/**
 * Distributed Directory
 */

package org.hyflow.api

import org.hyflow.core.util._
import org.hyflow.Hyflow
import scala.concurrent.stm._
import scala.concurrent.stm.impl.TxnContext
import com.typesafe.scalalogging.slf4j.Logging

trait Directory extends Logging {

	def open0(id: String)(implicit mt: MaybeTxn): Option[HObj]

	// Open a single object via the normal open call. It is simply a one-item list.
	def open[T <: HObj](id: String)(implicit mt: MaybeTxn): T = {
		openMany[T](List(id)).apply(0)
	    //val myL: List[T] = openMany[T](List(id))
	    //val myT: T = myL.apply(0)
	    //return myT
	}

	// Implemented in Tracker.
	def openMany0(ids: List[String])(implicit mt: MaybeTxn): Map[String, Either[HObj, Symbol]]

	// Search open-cache for each object (up to the root).
	protected def openMany_lookupCache[T <: HObj](ids: List[String])(implicit mt: MaybeTxn) = {
		// (Alex) TODO: Make sure to remove from cache invalidated objects.
		val (cached, newids) = ids.unzip { id =>
			val cached = HyflowBackendAccess().findCachedObject[T](id)
			val newid = if (cached == None) id else null
			(cached, newid)
		}
		(cached, newids)
	}

	// Process the cached objects and the (externally) requested objects.
	protected def openMany_postProcess[T <: HObj](
			ids: List[String],
			cached: List[Option[T]],
			lookedup: Map[String,Either[org.hyflow.api.HObj,Symbol]])(implicit mt: MaybeTxn) = {
	    // Iterate across all objects. If they are cached, add them to the list. Otherwise,
	    // check if the requested object is valid. If the object is locked or lost, then rollback.
		(0 until ids.length).map { i =>
			if (cached(i) == None) {
				val id = ids(i)
				if (id != null) {
					val res = lookedup(id)
					res match {
						case Left(obj) =>
							HyflowBackendAccess().recordOpen(obj)
							obj.asInstanceOf[T]
						case Right(error) =>
							error match {
								case 'locked_object | 'lost_object =>
									Txn.findCurrent match {
										case Some(txn) =>
											// If we're in a transaction, rollback.
											Txn.rollback(Txn.OptimisticFailureCause('cannot_open_locked_object, Some(ids(i))))(txn)
										case None =>
											null.asInstanceOf[T]
									}
								case _ =>
									null.asInstanceOf[T]
							}
					}
				} else {
					null.asInstanceOf[T]
				}
			} else {
				cached(i).get
			}
		} toList
	}

	// Open a set of objects. First check the open-cache, then look for new objects
	// through the tracker. Lastly, process the cache and requests.
	def openMany[T <: HObj](ids: List[String])(implicit mt: MaybeTxn): List[T] = {
		val (cached, newids) = openMany_lookupCache(ids)
		val lookedup = openMany0(newids)

		openMany_postProcess(ids, cached, lookedup)
	}

	def openMany_map[T <: HObj](ids: List[String])(implicit mt: MaybeTxn): Map[String, T] = {
		ids.zip(openMany[T](ids)).toMap
	}

	def register0(obj: HObj, ack: Boolean)(implicit mt: MaybeTxn)

	// Register the object at startup. Called at the creation of HObj's.
	def register(obj: HObj, ack: Boolean = false)(implicit mt: MaybeTxn) {
	    // Only take the below action if the Txn fails to recordRegister (e.g., initialization without Txn's).
		if (!HyflowBackendAccess().recordRegister(obj)) {
			logger.trace("Before register object. | id = {} | _owner = {}", obj._id, obj._owner)
			Hyflow.store.put(obj)
			register0(obj, ack)
			logger.trace("After register object. | id = {} | _owner = {}", obj._id, obj._owner)
		}
	}

	def delete0(id: String)(implicit mt: MaybeTxn)

	def delete(id: String)(implicit mt: MaybeTxn) {
	    // Only take the below action if the Txn fails to recordDelete.
		if (!HyflowBackendAccess().recordDelete(id))
			delete0(id)
	}
}