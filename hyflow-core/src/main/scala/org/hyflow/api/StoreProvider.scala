package org.hyflow.api

import akka.actor._
import scala.concurrent.Future

object StoreProvider {
	case class GetResp(objects: List[Tuple2[String,Either[HObj,Symbol]]]) extends Message
	case class ValidateResp(vers: Map[Handle.FID, Option[Long]]) extends Message
}

trait StoreProvider {
	def get(ids: List[String], txnid: Long, peer: ActorRef, pessimistic: Boolean): Future[StoreProvider.GetResp]
	def put(obj: HObj)
	def validate(peer: ActorRef, handles: List[Handle[_]], txnid: Long): Future[StoreProvider.ValidateResp]
	def lost(obj: HObj, txnid: Long)
	def update(obj: HObj, txnid: Long)
}
