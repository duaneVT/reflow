/**
 * Data Objects
 */

package org.hyflow.api

import scala.concurrent.stm._
import org.hyflow.core.directory.Tracker
import akka.actor.ActorRef
import org.hyflow.Hyflow

/** Main object class **/
trait HObj extends Handle[HObj] with HRef.Maker with Serializable {
	// The object identifier, must be passed to the constructor of sub-classes.
	def _id: String
	def _hy_id: Tuple2[String, Int] = (_id, -1)

	// Owner Node.
	// (Alex) Do not touch the var. How can we protect this from user interference? - private[motstm] won't work.
	var _owner: ActorRef = null

	// Check if the local node owns the object.
	def isLocalObject: Boolean = (_owner == Hyflow.mainActor)

	// Object associated with the handle.
	def _hy_obj = this

	// (Alex) Below are internal members, not for use by users. We can't enforce this as it's used by
	// a different package (scala.concurrent.stm.motstm) so private[..] won't work.
	// (Alex) TODO: Check that fields are only added during construction. How?

	// Keep track of object fields.
	private var _numFields: Int = 0

	def _addNewField: Int = {
		val res = _numFields
		_numFields += 1
		res
	}

	// Constructor to register object.
	// (Alex) TODO: Choice not to register (so we can operate on it locally before we register it)?
	Hyflow.dir.register(this)
	//println("I made the Handle for Obj: " + _id + " with _hy_id " + _hy_id + " with hash " + _hy_hash)
}