/**
 * Message Tracking and Sending
 */

package org.hyflow.core.directory

import org.hyflow.api._
import org.hyflow.core.util._
import org.hyflow.core.store._
import org.hyflow.Hyflow
import akka.pattern.ask
import scala.concurrent.duration._
import scala.concurrent.{ Await, Future, ExecutionContext }
import akka.actor._
import scala.collection.mutable
import scala.concurrent.stm._
import com.typesafe.scalalogging.slf4j.Logging
import scala.concurrent.stm.motstm.TFAClock
import java.util.concurrent.TimeUnit
import com.yammer.metrics.core.Clock

object Tracker extends Tracker

/** Sets of messages to be sent. **/
object MessagesTracker {
	case class LocateMsg(ids: List[String]) extends Message
	case class RegisterMsg(id: String, ver: Long, owner: ActorRef) extends Message
	case class UpdateVerMsg(id: String, ver: Long) extends Message
	case class DeleteMsg(id: String) extends Message
	case class LocateRespMsg(results: List[Tuple2[String, Option[ActorRef]]]) extends Message
	case class ReqUpdNotifMsg(id: String, minver: Long) extends Message
	case class UpdNotifMsg() extends Message
}

/** Tracker functions to send messages. **/
private[directory] class Tracker extends Directory with Service with Logging with ReInstrumented {
	import MessagesTracker._

	/*
	private val locate_tmr = metrics.timer("tracker-front-locate", durationUnit = TimeUnit.MICROSECONDS)
	private val get_tmr = metrics.timer("tracker-front-get", durationUnit = TimeUnit.MICROSECONDS)
    */

	// Hyflow service definitions.
	val name = "tracker"
	val actorProps = Props[TrackerActor]
	def accepts(message: Any): Boolean = message match {
		case _: LocateMsg => true
		case _: RegisterMsg => true
		case _: DeleteMsg => true
		case _: ReqUpdNotifMsg => true
		case _: UpdateVerMsg => true
		case _ => false
	}

	// Locate the owner for object through hash.
	def responsiblePeer(id: String) = {
		val peers = Hyflow.peers
		logger.trace("Locating responsible peer for object. | id={} | id ## = {} | peers.size = {} | index = {}",
			id,
			id.##.toString,
			peers.size toString,
			(math.abs(id ##) % peers.size) toString)
		peers(math.abs(id ##) % peers.size)
	}

	def open0(id: String)(implicit mt: MaybeTxn): Option[HObj] = {
		openMany0(List(id)).apply(id).left.toOption
	}

	// Open a set of objects which are not currently owned. (Do this in stages, so a minimal
	// number of messages need to be sent at each stage (i.e,. one for each host).
	def openMany0(ids: List[String])(implicit mt: MaybeTxn): Map[String, Either[HObj, Symbol]] = {
		implicit val executor = Hyflow.system.dispatcher.asInstanceOf[ExecutionContext]

		/** STAGE 1: Retrieve Owners. **/
		// Collect the object hosts:
		// * Map objects to responsible nodes.
		// * For each node, create request list for all of their objects.
		val trackerPairs = ids.map(id => (id, responsiblePeer(id)))
		val trackerSet = mutable.Map[ActorRef, mutable.ListBuffer[String]]()
		for (pair <- trackerPairs) {
			val (id, tracker) = pair
			if (id != null) {
				val requests = trackerSet.getOrElseUpdate(tracker, mutable.ListBuffer())
				requests.append(id)
			}
		}

		// Send LocateMsg for objects to each node.
		val owner_futures = trackerSet.toMap.map { args =>
			val (tracker, reqs) = args
			val msg = LocateMsg(reqs.toList)
			tracker.ask(msg)(30 seconds).asInstanceOf[Future[LocateRespMsg]]
		}

		// Wait for trackers to return responses.
		val owner_all_f = Future.sequence(owner_futures)
		//val owners = locate_tmr.time { Await.result(owner_all_f, 30 seconds) }
		val owners = Await.result(owner_all_f, 2 seconds)                            // !!!!!

		/** STAGE 2: Attempt to retrieve owners from tracker responses. **/
		// Go through and get owners, plus forward TFA Clock.
		val ownerSet = mutable.Map[ActorRef, mutable.ListBuffer[String]]()
		for (ans <- owners) {
			TFAClock.incoming(ans)
			for (res <- ans.results) {
				val (id, owner) = res
				if (owner != None && owner.get != null) {
					val requests = ownerSet.getOrElseUpdate(owner.get, mutable.ListBuffer())
					requests.append(id)
				}
			}
		}

		/* NOTE: This is meant to skip the above work by examining node ID's appended to the object ID's. */
		/*
		val ownerSet = mutable.Map[ActorRef, mutable.ListBuffer[String]]()
		for (id <- ids) {
		    try {
		        if (id != null) {
    		        val nodeID = id.split(":").last.toInt
    		        val requests = ownerSet.getOrElseUpdate(Hyflow.peers(nodeID), mutable.ListBuffer())
    		        requests.append(id)
		        }
		    }
		    catch {
		        case _: Throwable =>
		            throw new RuntimeException("Tracker: " + id + " did not have the proper tag for nodes!")
		    }
		}
        */

		// Send messages to retrieve objects from owners.
		val txnid = HyflowBackendAccess().getCrtTxnId
		val obj_futures = ownerSet.toMap.map { args =>
			val (owner, reqs) = args
			Hyflow.store.get(reqs.toList, txnid, owner, HyflowBackendAccess().isPessimisticMode)
		}

		// Wait for objects to come back.
		val obj_all_f = Future.sequence(obj_futures)
		// val obj_all = get_tmr.time { Await.result(obj_all_f, 30 seconds) }
		val obj_all = Await.result(obj_all_f, 30 seconds)

		// Process the GetResp for the objects. Forward if necessary.
		val res = mutable.Map[String, Either[HObj, Symbol]]()
		for (ans <- obj_all) {
			if (ans.objects.length > 0) {
				TFAClock.incoming(ans)
				val rclk = ans.payloads("tfaclock").asInstanceOf[Long]
				if (txnid != 0) {
					HyflowBackendAccess().forward(rclk)
				}

				for (obj_pair <- ans.objects) {
					val (id, obj_e) = obj_pair
					res.put(id, obj_e)
				}
			}
		}

		// Return the map of ID -> Object (if possible).
		res.toMap
	}

	// Send a register message if HyMot fails (or during finalizing writes).
	def register0(obj: HObj, ack: Boolean)(implicit mt: MaybeTxn) {
		val tracker = responsiblePeer(obj._id)
		logger.trace(s"Sending register message. | id = ${obj._id}")
		tracker ! RegisterMsg(obj._id, obj._hy_ver, Hyflow.mainActor)

		if (ack == true)
			throw new AbstractMethodError("Acknowledge on registration not implemented.")
	}

	// Send a delete message if HyMot fails. (Duane: What does this do?)
	def delete0(id: String)(implicit mt: MaybeTxn) {
		val tracker = responsiblePeer(id)
		tracker ! new DeleteMsg(id)
	}
}

/** Tracker actor to receive messages. **/
private[directory] class TrackerActor extends Actor with Logging with ReInstrumented {
	import MessagesTracker._

	// Each entry represents an object (by id) with its owner and current version.
	private class TrackerEntry(val id: String) {
		var owner: ActorRef = null
		var ver: Long = 0L
		var notifReqs = mutable.ListBuffer[ActorRef]()
	}
	private val tracked = mutable.Map[String, TrackerEntry]()

	/*
	private val locate_tmr = metrics.timer("tracker-back-locate", durationUnit = TimeUnit.MICROSECONDS)
	private val register_tmr = metrics.timer("tracker-back-register", durationUnit = TimeUnit.MICROSECONDS)
	private val updver_tmr = metrics.timer("tracker-back-locate", durationUnit = TimeUnit.MICROSECONDS)
	private val delete_tmr = metrics.timer("tracker-back-delete", durationUnit = TimeUnit.MICROSECONDS)
	private val notifreq_tmr = metrics.timer("tracker-back-notifreq", durationUnit = TimeUnit.MICROSECONDS)
    */

	/** Tracker Message Processing **/
	def receive() = {
	    // Locate Objects.
		case m: LocateMsg => //locate_tmr.time {
			val res = m.ids.map(id => (id, tracked.get(id).map(_.owner)))
			logger.trace("Querying tracker. | sender = {} | res = {}", sender, res)
			sender ! LocateRespMsg(res)
		//}

		// Register Object (upon HyMot recordRegister failure or finalizing writes).
		case m: RegisterMsg => //register_tmr.time {
			logger.warn("Registering object. | id = {} | owner = {} | ver = {}",
				m.id,
				m.owner.toString,
				m.ver.toString)
			val entry = tracked.getOrElseUpdate(m.id, new TrackerEntry(m.id))
			if (m.ver >= entry.ver) {
				entry.ver = m.ver
				entry.owner = m.owner
				if (!entry.notifReqs.isEmpty) {
					// Notify all and clear.
					entry.notifReqs.foreach(_.tell(m.id))
					entry.notifReqs.clear()
				}
				// Confirm registration.
				sender ! "ok"
			} else {
				logger.warn("Ignoring out-of-order object registration. | id = {} | entry.ver = {} | m.ver = {}",
					m.id,
					entry.ver toString,
					m.ver toString)
				sender ! "err"
			}
		//}

		// Update Object Versions.
		case m: UpdateVerMsg => //updver_tmr.time {
			logger.trace("Updating object version. | id = {} | ver = {}",
			             m.id,
			             m.ver.toString)
			val entry = tracked.get(m.id).get // It MUST exist (unless deleted by someone else).
			if (m.ver > entry.ver) {
				entry.ver = m.ver
				if (!entry.notifReqs.isEmpty) {
					// Notify all and clear.
					entry.notifReqs.foreach(_.tell(m.id))
					entry.notifReqs.clear()
				}
			}
		//}

		// Delete Object.
		case m: DeleteMsg => //delete_tmr.time {
			logger.trace("Deleting object. | id = {} | sender = {}", m.id, sender)
			val oldEntry = tracked.remove(m.id)
			if (oldEntry != None) {
				// Notify all.
				oldEntry.get.notifReqs.foreach(_.tell(m.id))
			}
			sender ! "ok"
		//}

		// Request Notification upon Object Update.
		case m: ReqUpdNotifMsg => //notifreq_tmr.time {
			logger.trace("Remote party requested to be notified when object gets updated. | id = {} | minver = {}",
				m.id,
				m.minver toString)
			// Check if object is already newer.
			val entry = tracked(m.id)
			if (entry.ver > m.minver) {
				// Reply right away.
				sender ! m.id
			} else {
				// Remember for later.
				entry.notifReqs += sender
			}
		//}

		// case _@ m =>
		//    logger.debug("Unexpected message. | m = {}", m)
	}
}
