package org.hyflow.core

import scala.concurrent.Future
import scala.concurrent.Promise
import scala.concurrent.duration.DurationInt
import scala.concurrent.stm.NestingLevel
import scala.concurrent.stm.Txn
import scala.util.Random
import scala.util.Success
import org.hyflow.Hyflow
import org.hyflow.core.util.HyflowConfig
import com.typesafe.scalalogging.slf4j.Logging
import java.util.concurrent.TimeUnit
import org.hyflow.core.util.ReInstrumented


object ContentionManager extends Logging with ReInstrumented {
	val LOCKED_BASE = HyflowConfig.cfg.getInt("hyflow.backoff.locked.base")
	val LOCKED_OFFSET = HyflowConfig.cfg.getInt("hyflow.backoff.locked.offset")
	val LOCKED_MAX = HyflowConfig.cfg.getInt("hyflow.backoff.locked.max")
	
	val LOCKACQ_BASE = HyflowConfig.cfg.getInt("hyflow.backoff.lock_acq.base")
	val LOCKACQ_OFFSET = HyflowConfig.cfg.getInt("hyflow.backoff.lock_acq.offset")
	val LOCKACQ_MAX = HyflowConfig.cfg.getInt("hyflow.backoff.lock_acq.max")
	
	val RS_BASE = HyflowConfig.cfg.getInt("hyflow.backoff.readset.base")
	val RS_OFFSET = HyflowConfig.cfg.getInt("hyflow.backoff.readset.offset")
	val RS_MAX = HyflowConfig.cfg.getInt("hyflow.backoff.readset.max")
	
	/*
	val tmr_backoff = metrics.timer("backoff", durationUnit = TimeUnit.MICROSECONDS)
	val mtr_backoff = metrics.meter("backoff_reqs", "backoffs", unit = TimeUnit.SECONDS)
	val mtr_notRolledback = metrics.meter("notRolledback", "events", unit = TimeUnit.SECONDS)
	val mtr_notOptimistic = metrics.meter("notOptimistic", "events", unit = TimeUnit.SECONDS)
	val mtr_invalidReadset = metrics.meter("invalidReadset", "events", unit = TimeUnit.SECONDS)
	val mtr_acqLockFailed = metrics.meter("acqLockFailed", "events", unit = TimeUnit.SECONDS)
	val mtr_openLocked = metrics.meter("openLocked", "events", unit = TimeUnit.SECONDS)
	val hst_prevAttempts = metrics.histogram("prevAttempts")
	*/
	
	def prepareBackoff(level: NestingLevel, prevAttempts: Int): Option[Future[_]] = {
		implicit val executor = Hyflow.system.dispatcher
		// Random back-off policy. (20 * Math.pow(2, Math.min(prevAttempts, 5)) * (1 + Random.nextFloat)).toInt

		// If parent level is also aborted, handle later.
		if (level.parent != None && level.parent.get.status != Txn.Active) {
			return None
		}
		
		//mtr_backoff.mark()
		//hst_prevAttempts.update(prevAttempts)

		val backoff = level.status match {
			case rb: Txn.RolledBack =>
				rb.cause match {
					case opt: Txn.OptimisticFailureCause =>
						opt.category match {
							case 'cannot_open_locked_object =>
								//mtr_openLocked.mark()
								LOCKED_BASE * (LOCKED_OFFSET + Math.min(prevAttempts, LOCKED_MAX) * Random.nextFloat) toInt
							case 'acquire_locks_failed =>
								//mtr_acqLockFailed.mark()
								LOCKACQ_BASE * (LOCKACQ_OFFSET + Math.min(prevAttempts, LOCKACQ_MAX) * Random.nextFloat) toInt
							case 'invalid_readset =>
								//mtr_invalidReadset.mark()
								RS_BASE * (RS_OFFSET + Math.min(prevAttempts, RS_MAX) * Random.nextFloat) toInt
							case 'external_decision => 100
							case 'restart_in_pessimistic_mode => 0
							case 'restart_to_enable_partial_rollback => 0
							case 'max_nested_attempts_exceeded => 50 // This is pessimistic mode!
							case other =>
								logger.error("Unknown Optimistic Failure Cause. | category = {}", other)
								0
						}
					case _ =>
						//mtr_notOptimistic.mark()
						0
				}
			case _ =>
				//mtr_notRolledback.mark()
				0
		}

		if (backoff == 0) {
			None
		} else {
			//tmr_backoff.update(backoff, TimeUnit.MILLISECONDS)
			logger.trace(s"Preparing back-off. | sleep = ${backoff} | prevAttempts = ${prevAttempts}")
			val promise = Promise[Boolean]()
			Hyflow.system.scheduler.scheduleOnce(backoff milliseconds) {
				promise.complete(Success(true))
			}
			// End random exponential back-off.
			Some(promise.future)
		}
	}
}