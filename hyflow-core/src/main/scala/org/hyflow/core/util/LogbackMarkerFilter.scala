package org.hyflow.core.util

import ch.qos.logback.core.spi.FilterReply
import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.core.filter.AbstractMatcherFilter
import org.slf4j.{ Marker, MarkerFactory }

class LogbackMarkerFilter extends AbstractMatcherFilter[ILoggingEvent] {

	var markerToMatch: Marker = _

	override def start() {
		if (markerToMatch == null) {
			addError("The marker property must be set for [" + getName() + "]")
		} else {
			super.start()
		}
	}

	def decide(event: ILoggingEvent): FilterReply = {
		if (!isStarted()) {
			FilterReply.NEUTRAL
		} else {
			val marker = event.getMarker()

			if (marker == null) {
				onMismatch
			} else if (marker.contains(markerToMatch)) {
				onMatch
			} else {
				onMismatch
			}
		}
	}
	
	def setMarker(name: String) {
		if (name != null) {
			markerToMatch = MarkerFactory.getMarker(name)
		}
	}
}