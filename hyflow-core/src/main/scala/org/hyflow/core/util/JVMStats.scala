package org.hyflow.core.util

import com.yammer.metrics.core.VirtualMachineMetrics
import org.hyflow.Hyflow
import scala.concurrent.duration.DurationInt
import scala.collection.JavaConversions._
import java.util.concurrent.TimeUnit

object JVMStats extends ReInstrumented {
    /*
	val gcCount = metrics.histogram("gcCount")
	val gcTime = metrics.histogram("gcTime")
    */
    
	var last_time, last_count: Long = 0

	def init() {
		implicit val executor = Hyflow.system.dispatcher
		Hyflow.system.scheduler.schedule(5 seconds, 5 seconds) {
			val jvm = VirtualMachineMetrics.getInstance()
			val gcs = jvm.garbageCollectors()
			var total_count: Long = 0
			var total_time: Long = 0
			for ((gcname, gcstat) <- gcs) {
				total_count += gcstat.getRuns()
				total_time += gcstat.getTime(TimeUnit.MILLISECONDS)
			}

			val crt_count = total_count - last_count
			val crt_time = total_time - last_time
			last_count = total_count
			last_time = total_time
			//gcCount.update(crt_count)
			//gcTime.update(crt_time)
		}
	}
}