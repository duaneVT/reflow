package org.hyflow.core.util

import scala.io.Source
import java.net.URL
import java.io.IOException
import scala.collection.mutable
import com.typesafe.config._

object HyflowConfig {
	import scala.collection.JavaConversions.asJavaMap
	
	// Set properties here in the lack of a better alternative
	System.setProperty("logback.configurationFile", new java.io.File("etc/logback.xml").getAbsolutePath)

	// Some constants
	var CONFIG = System.getProperty("hyflow.config", "etc/hyflow.conf")
	var IDENTITY = System.getProperty("hyflow.id", "0")

	def init(args: Array[String]): mutable.Map[String, String] = {
		val extra = mutable.Map[String, String]()
		for (arg <- args) {
			val res = arg.split("=", 2)
			if (res.length == 2) {
				res(0) match {
					case "config" => CONFIG = res(1)
					case "id" => IDENTITY = res(1)
					case _ => extra(res(0)) = res(1)
				}
			}
		}
		
		cfg = ConfigFactory.parseMap(asJavaMap(extra)) withFallback loadCfg(CONFIG) 
		System.setProperty("hyflow_nodeid", HyflowConfig.cfg.getString("id"))
		System.setProperty("hyflow_hostname", HyflowConfig.cfg.getString("hyflow.logging.hostname"))
		extra
	}

	// Load function
	def loadCfg(cfgFile: String) = {
		//logger.trace("Loading configuration. | file: {}", cfgFile)
		// Start from the node's identity
		var res = ConfigFactory.parseMap(asJavaMap(Map("id" -> IDENTITY)))
		// Add in the local configuration file
		try {
			val localConf = ConfigFactory.parseFile(new java.io.File(cfgFile))
			res = res withFallback localConf
		} catch {
			case e: IOException => println("Could not load configuration. | err: " + e.getMessage())
		}

		// Return configuration
		//logger.trace("Using the following configuration:\n{}", res.format())
		res
	}

	// Config store
	var cfg = ConfigFactory.empty()

	// Shortcuts and defaults
	lazy val basePort = cfg.getInt("hyflow.comm.basePort")
}
