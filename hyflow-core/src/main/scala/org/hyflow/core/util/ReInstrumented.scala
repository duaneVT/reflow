package org.hyflow.core.util

import com.yammer.metrics.Metrics
import com.yammer.metrics.core.MetricsRegistry
import java.util.concurrent.TimeUnit
import scala.collection.mutable
import com.yammer.metrics.reporting.CsvReporter
import java.io.File

object ReInstrumented {
	private var currentEpoch = "init"
	private var currentRegistry = Metrics.defaultRegistry()
	private var reporter: CsvReporter = _
	private var outputDirTmpl: String = _
	private var period: Int = _
	private var unit: TimeUnit = _

	def reset(epoch: String) = synchronized {
		if (epoch != currentEpoch) {
			reporter.shutdown()
			currentEpoch = epoch
			currentRegistry = new MetricsRegistry()
			for (x <- registry) {
				x.resetMetrics(currentRegistry)
			}
			reportStart()
		}
	}

	private var registry = Set[ReInstrumented]()
	def register(x: ReInstrumented) = synchronized {
		registry += x
	}

	def report(outputDirTmpl: String, period: Int, unit: TimeUnit) = synchronized {
		this.outputDirTmpl = outputDirTmpl
		this.period = period
		this.unit = unit
		reportStart()
	}

	private def reportStart() {
		val outDir = new File(outputDirTmpl format currentEpoch)
		outDir.mkdirs()
		reporter = new CsvReporter(currentRegistry, outDir)
		reporter.start(period, unit)
	}
}

trait ReInstrumented {
	private var metricsRegistry = Metrics.defaultRegistry()
	private val metricsGroup = new MetricsGroup(getClass, metricsRegistry)

	def metrics = metricsGroup
	ReInstrumented.register(this)

	def resetMetrics(epochRegistry: MetricsRegistry) {
		metricsRegistry = epochRegistry
		metricsGroup.reset(epochRegistry)
	}
}

class Timer(var metric: com.yammer.metrics.core.Timer) {
	/**
	 * Runs f, recording its duration, and returns the result of f.
	 */
	def time[A](f: => A): A = {
		val ctx = metric.time
		try {
			f
		} finally {
			ctx.stop
		}
	}

	/**
	 * Adds a recorded duration.
	 */
	def update(duration: Long, unit: TimeUnit) {
		metric.update(duration, unit)
	}

	/**
	 * Returns a timing [[com.metrics.yammer.core.TimerContext]],
	 * which measures an elapsed time in nanoseconds.
	 */
	def timerContext() = metric.time()

	/**
	 * Returns the number of durations recorded.
	 */
	def count = metric.count

	/**
	 * Clears all recorded durations.
	 */
	def clear() { metric.clear() }

	/**
	 * Returns the longest recorded duration.
	 */
	def max = metric.max

	/**
	 * Returns the shortest recorded duration.
	 */
	def min = metric.min

	/**
	 * Returns the arithmetic mean of all recorded durations.
	 */
	def mean = metric.mean

	/**
	 * Returns the standard deviation of all recorded durations.
	 */
	def stdDev = metric.stdDev

	/**
	 * Returns a snapshot of the values in the timer's sample.
	 */
	def snapshot = metric.getSnapshot

	/**
	 * Returns the timer's rate unit.
	 */
	def rateUnit = metric.rateUnit

	/**
	 * Returns the timer's duration unit.
	 */
	def durationUnit = metric.durationUnit

	/**
	 * Returns the type of events the timer is measuring.
	 */
	def eventType = metric.eventType

	/**
	 * Returns the fifteen-minute rate of timings.
	 */
	def fifteenMinuteRate = metric.fifteenMinuteRate

	/**
	 * Returns the five-minute rate of timings.
	 */
	def fiveMinuteRate = metric.fiveMinuteRate

	/**
	 * Returns the mean rate of timings.
	 */
	def meanRate = metric.meanRate

	/**
	 * Returns the one-minute rate of timings.
	 */
	def oneMinuteRate = metric.oneMinuteRate
}

class Meter(var metric: com.yammer.metrics.core.Meter) {

	/**
	 * Marks the occurrence of an event.
	 */
	def mark() {
		metric.mark()
	}

	/**
	 * Marks the occurrence of a given number of events.
	 */
	def mark(count: Long) {
		metric.mark(count)
	}

	/**
	 * Returns the meter's rate unit.
	 */
	def rateUnit = metric.rateUnit

	/**
	 * Returns the type of events the meter is measuring.
	 */
	def eventType = metric.eventType

	/**
	 * Returns the number of events which have been marked.
	 */
	def count = metric.count

	/**
	 * Returns the fifteen-minute exponentially-weighted moving average rate at
	 * which events have occurred since the meter was created.
	 * <p>
	 * This rate has the same exponential decay factor as the fifteen-minute load
	 * average in the top Unix command.
	 */
	def fifteenMinuteRate = metric.fifteenMinuteRate

	/**
	 * Returns the five-minute exponentially-weighted moving average rate at
	 * which events have occurred since the meter was created.
	 * <p>
	 * This rate has the same exponential decay factor as the five-minute load
	 * average in the top Unix command.
	 */
	def fiveMinuteRate = metric.fiveMinuteRate

	/**
	 * Returns the mean rate at which events have occurred since the meter was
	 * created.
	 */
	def meanRate = metric.meanRate

	/**
	 * Returns the one-minute exponentially-weighted moving average rate at
	 * which events have occurred since the meter was created.
	 * <p>
	 * This rate has the same exponential decay factor as the one-minute load
	 * average in the top Unix command.
	 */
	def oneMinuteRate = metric.oneMinuteRate
}

class Histogram(var metric: com.yammer.metrics.core.Histogram) {

	/**
	 * Adds the recorded value to the histogram sample.
	 */
	def update(value: Long) {
		metric.update(value)
	}

	/**
	 * Adds the recorded value to the histogram sample.
	 */
	def update(value: Int) {
		metric.update(value)
	}

	/**
	 * Returns the number of values recorded.
	 */
	def count = metric.count

	/**
	 * Clears all recorded values.
	 */
	def clear() { metric.clear() }

	/**
	 * Returns the largest recorded value.
	 */
	def max = metric.max

	/**
	 * Returns the smallest recorded value.
	 */
	def min = metric.min

	/**
	 * Returns the arithmetic mean of all recorded values.
	 */
	def mean = metric.mean

	/**
	 * Returns the standard deviation of all recorded values.
	 */
	def stdDev = metric.stdDev

	/**
	 * Returns a snapshot of the values in the histogram's sample.
	 */
	def snapshot = metric.getSnapshot
}

/**
 * A helper class for creating and registering metrics.
 */
class MetricsGroup(val klass: Class[_], var metricsRegistry: MetricsRegistry) {

	private val meters = mutable.Set[Tuple5[Meter, String, String, String, TimeUnit]]()
	private val timers = mutable.Set[Tuple5[Timer, String, String, TimeUnit, TimeUnit]]()
	private val hists = mutable.Set[Tuple4[Histogram, String, String, Boolean]]()

	def meter(name: String,
		eventType: String,
		scope: String = null,
		unit: TimeUnit = TimeUnit.SECONDS,
		registry: MetricsRegistry = metricsRegistry) = {

		val m = new Meter(registry.newMeter(klass, name, scope, eventType, unit))
		meters.add((m, name, scope, eventType, unit))
		m
	}

	def timer(name: String,
		scope: String = null,
		durationUnit: TimeUnit = TimeUnit.MILLISECONDS,
		rateUnit: TimeUnit = TimeUnit.SECONDS,
		registry: MetricsRegistry = metricsRegistry) = {

		val t = new Timer(registry.newTimer(klass, name, scope, durationUnit, rateUnit))
		timers.add((t, name, scope, durationUnit, rateUnit))
		t
	}

	def histogram(name: String,
		scope: String = null,
		biased: Boolean = false,
		registry: MetricsRegistry = metricsRegistry) = {
		
		val h = new Histogram(registry.newHistogram(klass, name, scope, biased))
		hists.add((h, name, scope, biased))
		h
	}

	def reset(epochRegistry: MetricsRegistry) {
		for (m <- meters) {
			m._1.metric = epochRegistry.newMeter(klass, m._2, m._3, m._4, m._5)
		}
		for (t <- timers) {
			t._1.metric = epochRegistry.newTimer(klass, t._2, t._3, t._4, t._5)
		}
		for (h <- hists) {
			h._1.metric = epochRegistry.newHistogram(klass, h._2, h._3, h._4)
		}
		metricsRegistry = epochRegistry
	}
}
