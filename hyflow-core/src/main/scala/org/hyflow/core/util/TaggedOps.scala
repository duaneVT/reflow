package org.hyflow.core.util

import com.typesafe.scalalogging.slf4j.Logging
import org.hyflow.api._
import scala.concurrent.stm._
import org.slf4j.MarkerFactory

// Tagging support
private[hyflow] abstract class TaggedOps {}

object TaggedOps extends TaggedOps with Logging {
	val tagMarker = MarkerFactory.getMarker("TaggedOpsInterface")
	val tagging = false
	
	def __tagged_OPEN(dir: Directory, id: String, txn: MaybeTxn, tag: Int): HObj = {
		val res = dir.open[HObj](id)(txn)
		logger.info(tagMarker, (HyflowBackendAccess().getCrtTxnId, "__tagged_OPEN", tag, id) toString)
		res
	}
	
	def __tagged_OPEN_MANY(dir: Directory, ids: List[String], txn: MaybeTxn, tag: Int): List[HObj] = {
		val res = dir.openMany[HObj](ids)(txn)
		logger.info(tagMarker, (HyflowBackendAccess().getCrtTxnId, "__tagged_OPEN_MANY", tag, ids) toString)
		res
	}
	
	def __tagged_DELETE(dir: Directory, id: String, txn: MaybeTxn, tag: Int) {
		logger.info(tagMarker, (HyflowBackendAccess().getCrtTxnId, "__tagged_DELETE", tag, id) toString)
		dir.delete(id)(txn)
	}
	
	def __tag_CREATE(obj: HObj, tag: Int) {
		logger.info(tagMarker, (HyflowBackendAccess().getCrtTxnId, "__tag_CREATE", tag, obj._id) toString)
	}
	
	def __tagged_APPLY[T](ref: HRef[T], txn: InTxn, tag: Int): T = {
		logger.info(tagMarker, (HyflowBackendAccess().getCrtTxnId, "__tagged_APPLY", tag, ref.parent._id) toString)
		ref.apply()(txn)
	}
	
	def __tagged_UPDATE[T](ref: HRef[T], newval: T, txn: InTxn, tag: Int) {
		logger.info(tagMarker, (HyflowBackendAccess().getCrtTxnId, "__tagged_UPDATE", tag, ref.parent._id) toString)
		ref.update(newval)(txn)
	}
	
	def __tag_TXN_BEGIN(txn: InTxn, txnClass: String) {
		if (tagging) {
			logger.info(tagMarker, (HyflowBackendAccess().getCrtTxnId, "_tag_TXN_BEGIN", -1, txnClass) toString)
		}
	}
	
	def __tag_TXN_COMMIT(txn: InTxn) {
		if (tagging) {
			logger.info(tagMarker, (HyflowBackendAccess().getCrtTxnId, "_tag_TXN_COMMIT", -1, null) toString)
		}
	}
	
	def __tag_TXN_ABORT(txn: InTxn) {
		if (tagging) {
			logger.info(tagMarker, (HyflowBackendAccess().getCrtTxnId, "_tag_TXN_ABORT", -1, null) toString)
		}
	}
	
}

