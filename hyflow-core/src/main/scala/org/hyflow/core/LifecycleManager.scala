/**
 * System Setup and Initialization
 */

package org.hyflow.core

import org.hyflow.api._
import org.hyflow.Hyflow
import org.hyflow.core.util._
import akka.actor._
import scala.concurrent.duration._
import scala.collection.mutable.ListBuffer
import com.typesafe.scalalogging.slf4j.Logging

object LifecycleManager extends Service {
	// Message Types.
	case class HelloMsg(me: ActorRef) extends Message {
		def this() = this(null)
	}
	case class BeginMsg(peers: List[ActorRef]) extends Message
	case class DoneMsg() extends Message
	case class ByeMsg() extends Message

	// Manager Setup.
	val name = "lifecycle-manager"
	val actorProps = Props[LifecycleManager]
	def accepts(message: Any): Boolean = message match {
		case _: HelloMsg => true
		case _: BeginMsg => true
		case _: DoneMsg => true
		case _: ByeMsg => true
		case _ => false
	}
}

/** Actor that handles system setup **/
class LifecycleManager extends Actor with Logging {
	import LifecycleManager._

	logger.info("Initializing lifecycle manager.")
	var count: Int = 0
	val initialPeers = ListBuffer[ActorRef]()

	def receive() = {
	    // INITIALIZATION. Sent by peers to the coordinator. Used to collect peers.
		case m: HelloMsg =>
			logger.trace(s"Received hello message. | sender = ${sender} | m.me = ${m.me}") 
			initialPeers.prepend(m.me)
			count += 1
			if (count == HyflowConfig.cfg.getInt("hyflow.nodes")) {
				// (Alex) TODO: Remove profiling code.
				val profilerWait = HyflowConfig.cfg.getInt("hyflow.profilerWait")
				println("Will wait %d seconds to attach profiler.".format(profilerWait))
				
				import context.dispatcher
				context.system.scheduler.scheduleOnce(profilerWait seconds) {
					logger.trace("Waiting done. Broadcasting start message and list of peers.")
					val bgnmsg = new BeginMsg(initialPeers.toList)
					for (peer <- initialPeers)
						peer ! bgnmsg
				}
			}

		// INITIALIZATION. Sent by coordinator, received by all in system. Formal setup of peers.
		case m: BeginMsg =>
			logger.trace("Received begin message. Listing peers.")
			Hyflow.peers.clear()
			Hyflow.peers ++= m.peers
			count = Hyflow.peers.length
			val myIdx = Hyflow.peers.indexOf(Hyflow.mainActor)
			
			import context.dispatcher
			for (peer <- Hyflow.peers) {
				val idx = Hyflow.peers.indexOf(peer)
				logger.trace("| peer = {}", peer)
				if (myIdx > idx)
					context.system.scheduler.scheduleOnce((Math.random * 3000).toInt milliseconds) {
						peer ! "ping"
					}
			}

			context.system.scheduler.scheduleOnce(4 seconds) {
				Hyflow.callOnReady()
			}

		// COMPLETION. Sent to the coordinator when each node finishes Benchmark.
		case m: DoneMsg =>
			count -= 1
			if (count == 0) {
				val byemsg = new ByeMsg
				for (peer <- Hyflow.peers)
					peer ! byemsg
			}

		// SYSTEM SHUTDOWN.
		case m: ByeMsg =>
			Hyflow.shutdown()
	}
}
