package org.hyflow.core.store
/*
import org.hyflow.api._
import org.hyflow.Hyflow

import scala.collection.mutable
import com.typesafe.scalalogging.slf4j.Logging

import akka.dispatch.Future
import akka.pattern.ask
import akka.actor._
import akka.util.duration._
import akka.util.Timeout

object Separated_Store extends Service with Logging with StoreProvider {
	
	private implicit val timeout = new Timeout(5 seconds)
	
	// messages
	case class GetMsg(id: String, txnid: Long, pessimistic: Boolean) extends Message
	case class PutMsg(obj: HObj) extends Message
	case class LostObjMsg(id: String, txnid: Long) extends Message
	case class ValidateMsg(fids: List[Handle.FID], txnid: Long) extends Message

	// hyflow service stuff 
	val name = "separated-store"
	val actorProps = Props[Separated_Store_Actor]
	def accepts(message: Any): Boolean = message match {
		case _: GetMsg => true
		case _: PutMsg => true
		case _: ValidateMsg => true
		case _: LostObjMsg => true
		case _ => false
	}
	
	def get(id: String, txnid: Long, peer: ActorRef, pessimistic: Boolean): Future[StoreProvider.GetResp] = {
		ask(peer, GetMsg(id, txnid, pessimistic)).asInstanceOf[Future[StoreProvider.GetResp]]
	}

	def put(obj: HObj) {
		ref ! new PutMsg(obj)
	}
	
	def lost(obj: HObj, txnid: Long) {
		obj._owner ! LostObjMsg(obj._id, txnid) 
	}
	
	def validate(peer: ActorRef, handles: List[Handle[_]], txnid: Long): Future[StoreProvider.ValidateResp] = {
		ask(peer, ValidateMsg(handles.map(_._hy_id), txnid)).asInstanceOf[Future[StoreProvider.ValidateResp]]
	}
}

// (Alex) TODO: update this to hold multiple versions. (how/where?)
// (Alex) TODO: (IMPORTANT) check that the object doesn't get updated elsewhere while in store/transit
class Separated_Store_Actor extends Actor with Logging {
	import Separated_Store._

	val store = mutable.Map[String, HObj]()

	def receive() = {
		case m: GetMsg =>
			// (Alex) TODO: don't give out object if it is already locked!
			val obj = store.get(m.id)
			logger.debug("Retrieving object from store. | id = {} | sender = {} | obj = {}", m.id, sender, obj)
			if (obj == None || obj.get == null)
				sender ! StoreProvider.GetResp(obj)
			else {
				// (Alex) TODO: forward to lock holder, if lock holder may be on a different node 
				// account for lock holder location strategy
				logger.trace("Forwarding message to lockholder.")
				Separated_Lock.ref.forward(Separated_Lock.EnsureUnlockedObj(m.txnid, obj.get, m.pessimistic))
			}
		case m: PutMsg =>
			m.obj._owner = Hyflow.mainActor
			logger.debug("Storing object locally. | id = {} | obj = {} | _owner = {}", 
					m.obj._id, m.obj, m.obj._owner)
			store.put(m.obj._id, m.obj)
			sender ! "ok"
		case m: ValidateMsg =>
			logger.debug("Validating objects for remote node. | ids = {} | sender = {}", m.fids, sender)
			// (Alex) TODO: how do we retrieve field version?
			// This only works for object versions (initial implementation)
			val res = for (fid <- m.fids) yield (fid, store.get(fid._1).filter(_ != null).map(_._hy_ver))
			// Pass this to the lock holder to blank out locked objects
			logger.trace("Forwarding validation to LockHolder. | vers = {}", res)
			Separated_Lock.ref.forward(Separated_Lock.EnsureUnlockedVer(m.txnid, res.toMap))
			//sender ! new ValidateRespMsg(res.toMap)
		case m: LostObjMsg =>
			logger.debug("Object ownership was lost. | id = {} | sender = {}", m.id, sender)
			// So a reply of Some(null) means either: object locked or object not here anymore 
			store.put(m.id, null)
			// Release any locks we may hold
			// (Alex) TODO: what about field locks?
			Separated_Lock.ref.forward(Separated_Lock.LockReleaseMsg((m.id, -1), m.txnid))
		case m: Separated_Lock.LockReqMsg =>
			logger.debug("Checking if object is still here. (for acquiring the lock). | id = {}", m.fid._1)
			store.get(m.fid._1) match {
				case Some(null) | None =>
					sender ! LockProvider.LockResp(m.fid, false)
				case Some(obj) =>
					Separated_Lock.ref.forward(Separated_Lock.LockReqPh2Msg(m))
			}
	}
}
*/