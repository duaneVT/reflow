/**
 * Synchronized Lock and Storage of Objects
 */

package org.hyflow.core.store

import org.hyflow.api._
import org.hyflow.Hyflow
import scala.collection.mutable
import com.typesafe.scalalogging.slf4j.Logging
import akka.pattern.ask
import akka.actor._
import scala.concurrent.duration._
import akka.util.Timeout
import scala.concurrent.Future
import java.util.concurrent.TimeUnit
import org.hyflow.core.util.ReInstrumented

object Combined_LockStore extends Service with Logging with LockProvider with StoreProvider {
	private implicit val timeout = Timeout(30 seconds)

	// Store messages.
	case class GetMsg(ids: List[String], txnid: Long, pessimistic: Boolean) extends Message
	case class PutMsg(obj: HObj) extends Message
	case class LostObjMsg(id: String, ver: Long, txnid: Long) extends Message
	case class ValidateMsg(fids: List[Handle.FID], txnid: Long) extends Message
	case class UpdateObjMsg(obj: HObj, txnid: Long) extends Message

	// Lock messages.
	case class LockReqMsg(fid: Handle.FID, txnid: Long) extends Message
	case class LockReleaseMsg(fid: Handle.FID, txnid: Long) extends Message

	// Hyflow service stuff.
	val name = "combined-lock-store"
	val actorProps = Props[Combined_LockStore_Actor]
	def accepts(message: Any): Boolean = message match {
		case _: GetMsg => true
		case _: PutMsg => true
		case _: ValidateMsg => true
		case _: LostObjMsg => true
		case _: LockReqMsg => true
		case _: LockReleaseMsg => true
		case _: UpdateObjMsg => true
		case _ => false
	}

	// Read objects from the specified owner (peer). Sent when Tracker is opening objects.
	def get(ids: List[String], txnid: Long, peer: ActorRef, pessimistic: Boolean): Future[StoreProvider.GetResp] = {
		ask(peer, GetMsg(ids, txnid, pessimistic)).asInstanceOf[Future[StoreProvider.GetResp]]
	}

	// Write an object (sent to itself) during finalizing writes.
	def put(obj: HObj) {
		ref ! PutMsg(obj)
	}

	// Send message to owner notifying object ownership being lost. Called during committing Txn finalizing writes.
	def lost(obj: HObj, txnid: Long) {
		obj._owner ! LostObjMsg(obj._id, obj._hy_ver, txnid)
	}

	// Send message to owner notifying that the object has been updated. Called during committing Txn finalizing writes.
	def update(obj: HObj, txnid: Long) {
	    obj._owner ! UpdateObjMsg(obj, txnid)
	}

	// Validate read-set by checking with object owners. Called during RS validation only.
	def validate(peer: ActorRef, handles: List[Handle[_]], txnid: Long): Future[StoreProvider.ValidateResp] = {
		ask(peer, ValidateMsg(handles.map(_._hy_id), txnid)).asInstanceOf[Future[StoreProvider.ValidateResp]]
	}

	// Ask owners to lock objects. Called during Txn commit. Locking can fail and result in rollbacks.
	def lock(obj: HObj, fid: Handle.FID, txnid: Long): Future[LockProvider.LockResp] = {
		ask(obj._owner, LockReqMsg(fid, txnid)).asInstanceOf[Future[LockProvider.LockResp]]
	}

	// Ask owners to release locks. Called during successful commit (or rolling back during commit).
	def unlock(obj: HObj, fid: Handle.FID, txnid: Long) {
		obj._owner ! LockReleaseMsg(fid, txnid)
	}
}

class Combined_LockStore_Actor extends Actor with Logging with ReInstrumented {

	import Combined_LockStore._

	// Entry in the storage (containing an object and its single lock).
	private class ObjectStoreEntry {
		var obj: HObj = null
		var lockedBy: Long = 0
	}
	private val store = mutable.Map[String, ObjectStoreEntry]()

	/*
	val get_tmr = metrics.timer("store-back-get", durationUnit = TimeUnit.MICROSECONDS)
	val put_tmr = metrics.timer("store-back-put", durationUnit = TimeUnit.MICROSECONDS)
	val lost_tmr = metrics.timer("store-back-lost", durationUnit = TimeUnit.MICROSECONDS)
	val validate_tmr = metrics.timer("store-back-validate", durationUnit = TimeUnit.MICROSECONDS)
	val lockreq_tmr = metrics.timer("store-back-lockreq", durationUnit = TimeUnit.MICROSECONDS)
	val lockrelease_tmr = metrics.timer("store-back-lockrelease", durationUnit = TimeUnit.MICROSECONDS)
	*/

	// Message Processing.
	def receive() = {
	    // Object Retrieval.
		case m: GetMsg => //get_tmr.time {
			val resp = m.ids.map { id =>
			    // Read object from its store.
				val entryOpt = store.get(id)

				// If the object doesn't exist, give warning.
				val res: Either[HObj, Symbol] = if (entryOpt == None) {
					logger.warn(s"Object store queried for unknown object. | id = ${id} | sender = ${sender}")
					Right('unknown_object)
				} else {
					val entry = entryOpt.get

					// If the object is null, this node does not own it. It has moved elsewhere.
					// If the object shows locked == 0, then nothing is locking it.
					// Otherwise, the object is locked by another Txn.
					if (entry.obj == null) {
						logger.info(s"Object store queried for an unavailable (lost) object. | id = ${id} | sender = ${sender}" ) // CHANGE BACK TO TRACE
						Right('lost_object)
					} else if (entry.lockedBy == 0) {
						logger.trace("Retrieving object from store. | id = {} | sender = {} | obj = {} | pessimistic = {}",
								id,
								sender.toString,
								entry.obj.toString,
								m.pessimistic.toString)
						if (m.pessimistic) {
							entry.lockedBy = m.txnid     // Pessimistically lock objects upon retrieval.
						}
						Left(entry.obj)
					} else if (entry.lockedBy != m.txnid) {
						logger.info(s"Object store queried for a locked object. | id = ${id} | sender = ${sender}")              // CHANGE BACK TO TRACE:
						Right('locked_object)
					} else {
						logger.warn(s"Object store queried for a locked object by the lock holder. | id = ${id} | sender = ${sender}")
						Left(entry.obj)
					}
				}
				(id, res)
			}
			// Return the objects and their responses ([Unknown, Lost, Free, Locked]).
			sender ! StoreProvider.GetResp(resp)
		//}

		// Object Storage. (Owner = Current Node's main Hyflow)
		case m: PutMsg => //put_tmr.time {
			val entry = store.getOrElseUpdate(m.obj._id, new ObjectStoreEntry)
			m.obj._owner = Hyflow.mainActor
			entry.obj = m.obj
			logger.trace("Storing object locally. | id = {} | obj = {} | _owner = {}",
					m.obj._id, m.obj, m.obj._owner)
		//}

		// Object No Longer Owned by Peer.
		case m: LostObjMsg => //lost_tmr.time {
			val entry = store(m.id)
			if (m.ver < entry.obj._hy_ver) {
				logger.trace(s"Received stale lost object message. Ignoring. | id = ${m.id} | sender = ${sender}")
			} else {
				logger.trace(s"Object ownership was lost. | id = ${m.id} | sender = ${sender}")
				entry.obj = null
				entry.lockedBy = 0
			}
		//}

		// Update Object. Sent to owners when Txn's finalize their writes.
        case m: UpdateObjMsg =>
            logger.trace(s"Received message to update object. | id = ${m.obj._id} | sender = ${sender}")
            val obj = m.obj
            val entry = store(obj._id)
            entry.obj = obj
            entry.lockedBy = 0

		// Object Validation. Ensure object is known and not locked by another transaction.
		case m: ValidateMsg => //validate_tmr.time {
			val res = for (fid <- m.fids)
				yield (fid, store.get(fid._1)
					.filter(x => x.obj != null && (x.lockedBy == m.txnid || x.lockedBy == 0))
					.map(_.obj._hy_ver))
			logger.trace(s"Validating objects for remote node. | sender = ${sender} | res = ${res}")
			sender ! StoreProvider.ValidateResp(res.toMap)
		//}

		// Request Lock for Object.
		case m: LockReqMsg => //validate_tmr.time {
			val entry = store(m.fid._1)
			val granted = if (entry.obj == null) {
				logger.trace("Lock request rejected (object unavailable). | fid = {}", m.fid)
				false
			} else if (entry.lockedBy == 0 || entry.lockedBy == m.txnid) {
				logger.trace(s"Lock request granted. | fid = ${m.fid} | re-entry = ${entry.lockedBy == m.txnid}")
				entry.lockedBy = m.txnid
				true
			} else {
				logger.trace("Lock request rejected (object locked). | fid = {}", m.fid)
				false
			}
			sender ! LockProvider.LockResp(m.fid, granted)
		//}

		// Release Lock for Object.
		case m: LockReleaseMsg => //lockrelease_tmr.time {
			val entry = store(m.fid._1)
			if (m.txnid == entry.lockedBy) {
				logger.trace("Lock released for local object. | fid = {}", m.fid)
			} else if (entry.lockedBy != 0) {
				logger.warn("Lock released for local object by wrong txnid. | fid = {} | lockedBy = {} | txnid = {}",
						m.fid toString,
						entry.lockedBy toString,
						m.txnid toString)
			} else {
				logger.debug("WARN: Unlocking unlocked object. | fid = {}", m.fid)
			}
			entry.lockedBy = 0
		//}
	}
}