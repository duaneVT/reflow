/**
 * Main Hyflow Structure
 */

package org.hyflow

import org.hyflow.api._
import org.hyflow.core.util._
import org.hyflow.core._
import org.hyflow.core.store._
import akka.actor._
import ch.qos.logback.classic.util
import scala.collection.mutable
import java.io.File
import akka.routing._
import scala.concurrent.{ Future, Await }
import akka.dispatch.Dispatchers
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import com.typesafe.config.{ Config, ConfigFactory }
import com.typesafe.scalalogging.slf4j.Logging

/** Hyflow Main Actor **/
private class Hyflow extends Actor with Logging
{
	def receive() = {
		// This should never get called. Hyflow actor routes instead of receiving messages.
		case msg @ _ =>
			logger.warn("Unexpected message received by mainActor. | msg = {}", msg toString)
	}
}

/** Hyflow Router **/
case class HyflowRouter() extends RouterConfig with Logging
{
	def routerDispatcher: String = Dispatchers.DefaultDispatcherId
	override val supervisorStrategy = OneForOneStrategy() {
		case e: Throwable =>
			logger.error("Exception thrown by Hyflow service.", e)
			SupervisorStrategy.Escalate
	}

	def createRoute(routeeProvider: RouteeProvider): Route = {
		// Go through all Hyflow Services and initialize actors for them.
		val routees = Hyflow.services.map(svc => {
			svc.ref = routeeProvider.context.actorOf(svc.actorProps, svc.name)
			svc.ref
		})

		// Register all newly initialized actors with the routee provider.
		routeeProvider.registerRoutees(routees.toArray)

		// (Alex) TODO: Cache the services list into a constant time lookup hash-table or trie.
		// ATTENTION: This must be reentrant.
		val res = new Route {
			def isDefinedAt(x: (ActorRef, Any)) = true
			def apply(x: (ActorRef, Any)): Iterable[Destination] = {
				val (sender, message) = x

				// Execute message payload callback (must be reentrant).
				if (message.isInstanceOf[Message]) {
					val hymsg = message.asInstanceOf[Message]
					Hyflow.processMessagePayloads(hymsg)
				}

				// Redirect the message.
				Hyflow.services.find(_.accepts(message)) match {
					case Some(svc) =>
						List(Destination(sender, svc.ref))
					case None =>
						// Discard the message.
						//logger.warn("Unexpected message received by mainActor. Discarded. | msg = {}",
						//		message toString)
						List()
				}
			}
		}
		res
	}
}

/** Main Hyflow Object (entered after init() in BenchApp). **/
object Hyflow extends Logging
{
	implicit val timeout = Timeout(5 seconds)

	val akkaConfigFile = new java.io.File("etc/application.conf")

	// Initialize the actor system.
	var system: ActorSystem = null

	// Hyflow's main actor.
	var mainActor: ActorRef = null

	// Effectively unique int identifying this node.
	val _hy_nodeid = scala.util.Random.nextInt

	// Hyflow service list.
	val services = mutable.ListBuffer[Service]()

	// Hyflow message payloads.
	var payloads = List[PayloadHandler]()

	// Hyflow router, selects first service that can handle the message.
	// (Alex) TODO: this throws exception if it isn't lazy. (Alex) TODO: check, when are we actually using this?
	var routerActor: ActorRef = null

	// Known peers (where 0 == master).
	val peers = mutable.ListBuffer[ActorRef]()

	// Lock & Store & Dir providers.
	var locks: LockProvider = null
	var store: StoreProvider = null
	var dir: Directory = null

	// Callbacks.
	val onInit = mutable.ListBuffer[() => Unit]()
	val onReady = mutable.ListBuffer[() => Unit]()
	val onShutdown = mutable.ListBuffer[() => Unit]()

	// Stats (approximate, aren't synchronized for correctness). Final throughput is calculated elsewhere.
	@volatile var _topAborts: Int = 0
	@volatile var _topCommits: Int = 0
	@volatile var _openNestedAborts: Int = 0
	@volatile var _openNestedCommits: Int = 0
	@volatile var _closedNestedAborts: Int = 0
	@volatile var _closedNestedCommits: Int = 0
	@volatile var _parallelCommits: Int = 0
	@volatile var _parallelAborts: Int = 0

	/** Initialize Hyflow here. **/
	def init(args: Array[String], userServices: List[Service] = List()) {
		logger.info("Initializing Hyflow.")

		// Configure port.
		val akkaConf = ConfigFactory.parseFile(akkaConfigFile)
		val newPort = HyflowConfig.cfg.getInt("hyflow.basePort") + HyflowConfig.cfg.getInt("id")
		val hostname = HyflowConfig.cfg.getString("hyflow.hostname")
		val confOverrides = ConfigFactory.parseString(
				"akka.remote.netty.port = %d\n".format(newPort) +
				"akka.remote.netty.hostname = \"%s\"\n".format(hostname)
		)

		// Initialize Akka actor system.
		val classLoader = getClass.getClassLoader
		system = ActorSystem("hyflow", confOverrides.withFallback(akkaConf), classLoader)

		// Hard-wire MotSTM DTM implementation
		// scala.concurrent.stm.impl.STMImpl.select(scala.concurrent.stm.motstm.MotSTM)
		// HyflowBackendAccess.configure(scala.concurrent.stm.motstm.HyMot)
		// HRef.factory = scala.concurrent.stm.motstm.MotSTM

		// Hard-wire HaiSTM DTM implementation
		// scala.concurrent.stm.impl.STMImpl.select(scala.concurrent.stm.haistm.HaiSTM)
		// HyflowBackendAccess.configure(scala.concurrent.stm.haistm.HyHai)
		// HRef.factory = scala.concurrent.stm.haistm.HaiSTM

		registerPayloadHandler(scala.concurrent.stm.motstm.TFAClock)

		// Configure locks provider.
		val locksProvider = HyflowConfig.cfg.getString("hyflow.modules.locks") match {
			case "Combined_LockStore" => org.hyflow.core.store.Combined_LockStore
			// case "Separated_Lock" => org.hyflow.core.store.Separated_Lock
			case s => throw new Exception("Unknown lock provider: " + s)
		}
		locks = locksProvider
		registerService(locksProvider)

		// Configure store provider.
		val storeProvider = HyflowConfig.cfg.getString("hyflow.modules.store") match {
			case "Combined_LockStore" => org.hyflow.core.store.Combined_LockStore
			// case "Separated_Store" => org.hyflow.core.store.Separated_Store
			case s => throw new Exception("Unkown store provider: " + s)
		}
		store = storeProvider
		registerService(storeProvider)

		/*
		 * val dirProvider = HyflowConfig.cfg.getString("hyflow.modules.directory") match {
			case "Tracker" => org.hyflow.core.directory.Tracker
			case s => throw new Exception("Unknown directory: " + s)
		}
		dir = dirProvider
		registerService(dirProvider)
		*/

		// Hard-wire other Hyflow services.
		registerService(org.hyflow.core.LifecycleManager)
		registerService(org.hyflow.core.BarrierService)
		logger.debug("User services. | userServices = {}", userServices)
		for (usrv <- userServices)
			registerService(usrv)

		// Create main actor with router.
		mainActor = system.actorOf(Props[Hyflow].withRouter(HyflowRouter()), name = "hyflow")
		logger.debug("Main actor created. | mainActor = {}", mainActor)
		// routerActor = system.actorOf(Props().withRouter(HyflowRouter()))

		if (HyflowConfig.cfg.getInt("id") == 0) {
			logger.debug("We are the coordinator node.")
		}

		// Greet the coordinator (even if it's just this node's mainActor).
		val coord = system.actorFor("akka://hyflow@%s:%d/user/hyflow".format(
			HyflowConfig.cfg.getString("hyflow.coord"),
			HyflowConfig.cfg.getInt("hyflow.basePort")))

		coord ! new LifecycleManager.HelloMsg(Hyflow.mainActor)

		for (initCallback <- onInit)
			initCallback()

		val a = TaggedOps.tagging			// Need to touch this class for Hyflow-Soot to work properly.
		logger.info("Initialization complete.")
	}

	/** Register a Hyflow service. **/
	def registerService(s: Service) {
		if (services.exists(_.name == s.name)) {
			logger.debug(s"Service already registered. | name: ${s.name} | class: ${s.getClass}")
		} else {
			logger.info(s"Registering Hyflow service. | name: ${s.name} | class: ${s.getClass}")
			services.append(s)
		}
	}

	/** Register a payload handler. (Alex: TODO: Extract this into a trait. **/
	def registerPayloadHandler(h: PayloadHandler) {
		logger.info(s"Registering payload handler. | name: ${h.name} | class: ${h.getClass}")
		payloads = payloads :+ h
	}

	/** Stop running services. **/
	def shutdown() {
		logger.info("Shutting down.")
		system.shutdown()
		for (shutdownCallback <- onShutdown)
			shutdownCallback()
	}

	/** Handle payload messages. **/
	private[hyflow] def processMessagePayloads(hymsg: Message) {
		// logger.trace("Processing payloads. | message: {} | class: {}", hymsg, hymsg.getClass())
		for (handler <- Hyflow.payloads)
			// if (hymsg.payloads.contains(handler.name))
			handler.incoming(hymsg)
	}

	private[hyflow] def callOnReady() {
		for (readyCallback <- onReady) {
			readyCallback()
		}
	}

	/** Notify master of completion. **/
	def done() {
		peers(0) ! new LifecycleManager.DoneMsg()
	}

	/** Barrier. **/
	def barrier(id: String) {
		logger.info("Awaiting on barrier. | id = {}", id)
		if (HyflowConfig.cfg.getString("hyflow.workload.benchmark") == "tpcc") {
		    val f = ask(Hyflow.peers(0), BarrierService.BarrierMsg(id, HyflowConfig.cfg.getInt("hyflow.nodes")))(240 seconds)
		    Await.ready(f, 240 seconds)
		}
		else {
		     val f = ask(Hyflow.peers(0), BarrierService.BarrierMsg(id, HyflowConfig.cfg.getInt("hyflow.nodes")))(120 seconds)
            Await.ready(f, 120 seconds)
		}
		logger.info("Barrier done. | id = {}", id)
	}
}

/** HyflowMain unused -- see BenchApp.scala **/
object HyflowMain {
	def main(args: Array[String]) { exit(0) }
}
