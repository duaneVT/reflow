package akka.serialization

import com.typesafe.config.ConfigFactory
import java.nio.ByteBuffer
import akka.actor._
import akka.routing._
import akka.remote._
import scala.collection.{ mutable, immutable }
import com.typesafe.scalalogging.slf4j.Logging

/*
// Old Kryo 1.05
import com.esotericsoftware.kryo._
import de.javakaffee.kryoserializers._
import com.esotericsoftware.kryo.serialize.SimpleSerializer

private class ActorRefSerializer(val kryo: Kryo, val system: ExtendedActorSystem) extends SimpleSerializer[ActorRef] {
	override def read(buffer: ByteBuffer): ActorRef = {
		val path = kryo.readObject(buffer, classOf[String])
		system.actorFor(path)
	}

	override def write(buffer: ByteBuffer, ref: ActorRef) {
		Serialization.currentTransportAddress.value match {
			case null => kryo.writeObject(buffer, ref.path.toString)
			case addr => kryo.writeObject(buffer, ref.path.toStringWithAddress(addr))
		}
	}
}

private class EnumerationSerializer(val kryo: Kryo) extends SimpleSerializer[Enumeration#Value] {

	// Caching of parent enum types for value types
	var valueClass2enumClass = immutable.Map[Class[_], Class[_]]()
	// Cache enumeration values for a given enumeration class
	var enumClass2enumValues = immutable.Map[Class[_], mutable.ArrayBuffer[Enumeration#Value]]()

	private def cacheEnumValue(obj: Enumeration#Value) = {

		val enumClass = valueClass2enumClass.get(obj.getClass) getOrElse {
			val parentEnum = obj.asInstanceOf[AnyRef].getClass.getSuperclass.getDeclaredFields.find(f => f.getName == "$outer").get
			val parentEnumObj = parentEnum.get(obj)
			val enumClass = parentEnumObj.getClass
			valueClass2enumClass += obj.getClass -> enumClass
			val enumValues = enumClass2enumValues.get(enumClass) getOrElse {
				val size = parentEnumObj.asInstanceOf[Enumeration].maxId + 1
				val values = new mutable.ArrayBuffer[Enumeration#Value](size)
				0 until size foreach { e => values += null }
				enumClass2enumValues += enumClass -> values
				values
			}
			enumClass
		}

		val enumValues = enumClass2enumValues.get(enumClass).get

		if (enumValues(obj.id) == null) {
			enumValues.update(obj.id, obj)
		}

		enumClass
	}

	override def write(buffer: ByteBuffer, obj: Enumeration#Value) {
		println("Writing enum {}", obj)
		val enumClass = cacheEnumValue(obj)
		kryo.writeClass(buffer, enumClass)
		kryo.writeObject(buffer, obj.id)
	}

	override def read(buffer: ByteBuffer): Enumeration#Value = {
		val clazz = kryo.readClass(buffer).getType
		val id = kryo.readObject(buffer, classOf[Int]) 

		val enumValues = enumClass2enumValues.get(clazz).getOrElse {
			cacheEnumValue(kryo.newInstance(clazz).asInstanceOf[Enumeration](id))
			enumClass2enumValues.get(clazz).get
		}

		val enumInstance = enumValues(id)
		println("Reading enum {}", enumInstance)
		enumInstance
	}
}

class KryoSerializer(val system: ExtendedActorSystem) extends Serializer {
	val buffer = new ThreadLocal[ObjectBuffer] {
		override def initialValue() = {
			val kryo = new KryoReflectionFactorySupport
			kryo.setRegistrationOptional(true)
			kryo.register(classOf[RoutedActorRef], new ActorRefSerializer(kryo, system))
			kryo.register(classOf[RemoteActorRef], new ActorRefSerializer(kryo, system))
			kryo.register(classOf[LocalActorRef], new ActorRefSerializer(kryo, system))
			kryo.register(classOf[org.hyflow.granola.common.TxnType.T], new EnumerationSerializer(kryo))
			new ObjectBuffer(kryo)
		}
	}

	// This is whether "fromBinary" requires a "clazz" or not
	def includeManifest: Boolean = false

	def identifier = 4747835

	def toBinary(obj: AnyRef): Array[Byte] = {
		buffer.get.writeClassAndObject(obj)
	}

	def fromBinary(bytes: Array[Byte], clazz: Option[Class[_]]): AnyRef = {
		buffer.get.readClassAndObject(bytes)
	}
}

*/

// New Kryo 2.20

import com.esotericsoftware.kryo
import com.esotericsoftware.kryo._
import com.esotericsoftware.kryo.io.{ Input, Output }
import org.objenesis.strategy.StdInstantiatorStrategy

private class ActorRefSerializer(val system: ExtendedActorSystem) extends kryo.Serializer[ActorRef] {

	override def read(kryo: Kryo, input: Input, typ: Class[ActorRef]): ActorRef = {
		val path = input.readString()
		system.actorFor(path)
	}

	override def write(kryo: Kryo, output: Output, obj: ActorRef) = {
		Serialization.currentTransportAddress.value match {
			case null => output.writeString(obj.path.toString)
			case addr => output.writeString(obj.path.toStringWithAddress(addr))
		}
	}
}

class ScalaEnumSerializer extends kryo.Serializer[Enumeration#Value] with Logging {

	// Caching of parent enum types for value types
	var valueClass2enumClass = immutable.Map[Class[_], Class[_]]()
	// Cache enumeration values for a given enumeration class
	var enumClass2enumValues = immutable.Map[Class[_], mutable.ArrayBuffer[Enumeration#Value]]()

	private def cacheEnumValue(obj: Enumeration#Value) = {
		logger.warn(s"CacheEnumValue ( obj = ${obj}, class = ${obj.getClass} )") 
		val enumClass = valueClass2enumClass.get(obj.getClass) getOrElse {
			val parentEnum = obj.asInstanceOf[AnyRef].getClass.getSuperclass.getDeclaredFields.find(f => f.getName == "$outer").get
			val parentEnumObj = parentEnum.get(obj)
			val enumClass = parentEnumObj.getClass
			logger.warn(s"OrElse, parentEnum = ${parentEnum}, parentEnumObj = ${parentEnumObj}") 
			valueClass2enumClass += obj.getClass -> enumClass
			val enumValues = enumClass2enumValues.get(enumClass) getOrElse {
				val size = parentEnumObj.asInstanceOf[Enumeration].maxId + 1
				val values = new mutable.ArrayBuffer[Enumeration#Value](size)
				0 until size foreach { e => values += null }
				enumClass2enumValues += enumClass -> values
				values
			}
			enumClass
		}

		val enumValues = enumClass2enumValues.get(enumClass).get
		logger.warn("enumValues = {}", enumValues)

		if (enumValues(obj.id) == null) {
			enumValues.update(obj.id, obj)
			logger.warn("null, updating, enumValues = {}", enumValues)
		}

		logger.warn("res enumClass = {}", enumClass)
		enumClass
	}

	override def write(kryo: Kryo, output: Output, obj: Enumeration#Value) = {
		val enumClass = cacheEnumValue(obj)
		kryo.writeClass(output, enumClass)
		output.writeInt(obj.id)
	}

	override def read(kryo: Kryo, input: Input, typ: Class[Enumeration#Value]): Enumeration#Value = {
		val clazz = kryo.readClass(input).getType
		val id = input.readInt()

		val enumValues = enumClass2enumValues.get(clazz).getOrElse {
			cacheEnumValue(kryo.newInstance(clazz).asInstanceOf[Enumeration](id))
			enumClass2enumValues.get(clazz).get
		}

		val enumInstance = enumValues(id)
		enumInstance
	}
}

class KryoSerializer(val system: ExtendedActorSystem) extends Serializer with Logging {

	import com.romix.scala.serialization.kryo.{
		ScalaOptionSerializer,
		EnumerationSerializer,
		ScalaMapSerializer,
		ScalaSetSerializer,
		ScalaCollectionSerializer
	}
	import com.romix.akka.serialization.kryo.KryoBasedSerializer

	// This is whether "fromBinary" requires a "clazz" or not
	def includeManifest: Boolean = false

	// A unique identifier for this Serializer
	def identifier = 123454323

	// Delegate to a real serializer
	def toBinary(obj: AnyRef): Array[Byte] = {
		val ser = getSerializer
		val bin = ser.toBinary(obj)
		releaseSerializer(ser)
		bin
	}

	def fromBinary(bytes: Array[Byte], clazz: Option[Class[_]]): AnyRef = {
		try {
			val ser = getSerializer
			val obj = ser.fromBinary(bytes, clazz)
			releaseSerializer(ser)
			obj
		} catch {
			case e: KryoException =>
				logger.error("KryoException.getCause()", e.getCause())
				throw e
		}
	}

	//val serializerPool = new ObjectPool[Serializer](8, () => {
	//	new KryoBasedSerializer(getKryo, 16 * 1024, 6, false)
	//})
	
	val threadLocalSerializer = new ThreadLocal[Serializer] {
		override def initialValue() = {
			new KryoBasedSerializer(getKryo, 16 * 1024, 6, false)
		}
	}

	private def getSerializer = threadLocalSerializer.get() //serializerPool.fetch
	private def releaseSerializer(ser: Serializer) = () //serializerPool.release(ser)

	private def getKryo(): Kryo = {
		import com.esotericsoftware.minlog.{Log => MiniLog}
		//MiniLog.TRACE()
		val kryo = new Kryo()
		// Support deserialization of classes without no-arg constructors
		kryo.setInstantiatorStrategy(new StdInstantiatorStrategy())

		// Support serialization of Scala collections
		kryo.addDefaultSerializer(classOf[scala.Option[_]], classOf[ScalaOptionSerializer])
		//kryo.addDefaultSerializer(classOf[scala.Enumeration#Value], classOf[ScalaEnumSerializer])
		kryo.addDefaultSerializer(classOf[scala.collection.Map[_, _]], classOf[ScalaMapSerializer])
		kryo.addDefaultSerializer(classOf[scala.collection.Set[_]], classOf[ScalaSetSerializer])
		kryo.addDefaultSerializer(classOf[scala.collection.generic.MapFactory[scala.collection.Map]], classOf[ScalaMapSerializer])
		kryo.addDefaultSerializer(classOf[scala.collection.generic.SetFactory[scala.collection.Set]], classOf[ScalaSetSerializer])
		kryo.addDefaultSerializer(classOf[scala.collection.Traversable[_]], classOf[ScalaCollectionSerializer])

		// Akka Remote actors
		kryo.addDefaultSerializer(classOf[ActorRef], new ActorRefSerializer(system))

		kryo.setReferences(true)
		kryo
	}
}

