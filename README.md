# Hyflow2 (Parallel Nesting Version)

Hyflow2 is the second iteration of a Distributed Transactional Memory (DTM) library from the [Systems Software Research Group](http://www.ssrg.ece.vt.edu/) at Virginia Tech. Hyflow2 provides a clean API that does not rely on annotations or byte-code rewriting. This repository is an update to the original Hyflow2 (found at https://bitbucket.org/talex/reflow), adding parallel nesting capabiilities.

Hyflow2 is written in the Scala language for the JVM, but provides a Java compatibility API. The Scala API is based on the excellent Scala STM API, considered for inclusion in Scala's standard library. The implementation uses Actors and the Akka library.

## Pre-Requisites

In order to run Hyflow2, you first need to install a Java Virtual Machine (JVM) and Scala Simple Build Tool (SBT). Also, to get the source code, you need mercurial.

On Ubuntu 10.04, installing a stock JVM and mercurial is simple:

```
#!bash
sudo apt-get install openjdk-6 mercurial
```

Adjust the install command and/or Java version as appropriate for your system.

To install SBT, please follow the **Unix manual install** instructions from the [SBT documentation](http://www.scala-sbt.org/release/docs/Setup.html). SBT will automatically download and install Scala and any libraries needed by Hyflow2 the first time it runs.

Hyflow2 has one dependency that needs to be installed manually. Please run these commands:

```
#!bash
git clone https://github.com/talex004/akka-kryo-serialization.git
cd akka-kryo-serialization
sbt compile publish-local
```

Finally, download Hyflow2 (with parallel nesting) source code:

```
#!bash
hg clone https://bitbucket.org/duaneVT/reflow
```

This will create a directory called reflow containing the source code.

## Preparing Hyflow2

If you want to use Eclipse for looking at (or editing) Hyflow2 source code, we have provided a way to generate Eclipse project files. First, install Scala IDE for Eclipse, as described in Section "Downloading and Installing the Scala IDE for Eclipse" from this Getting Started Tutorial.

In the reflow root directory, run:

```
#!bash
./make-eclipse-project.sh
```

then import the directory as an existing project in Eclipse.

Before running Hyflow2, you must compile the source code and prepare the launch scripts for your system. In the reflow folder, run the following commands:

```
#!bash
sbt package
bin/update_scripts.py
```

## Running Tests

You can run a simple test using the following command:

```
#!bash
bin/hytest
```

You can configure the number of nodes, threads, and benchmark settings from the command line. For example, the following will run the Bank benchmark with 200k accounts on 6 nodes with 2 threads per node, running in the Parallel Strict nesting model:

```
#!bash
bin/hytest -b bank -n 6 -t 2 -o 200000 -N parallelStrict
```

For other options see the output of:

```
#!bash
bin/hytest.py --help
```

## Publications

If using our work, please reference our publications:

* D. Niles, R. Palmieri, B. Ravindran, "Exploiting Parallelism of Distributed Nested Transactions." (Pending)
* A. Turcu, B. Ravindran, R. Palmieri, "Hyflow2: A High Performance Distributed Transactional Memory Framework in Scala", 10th International Conference on Principles and Practices of Programming on JAVA platform: virtual machines, languages, and tools, PPPJ 2013, September, 2013, Stuttgart, Germany