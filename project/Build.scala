import sbt._
import Keys._
import com.github.retronym.SbtOneJar



// Define resolvers and dependencies
object Resolvers {
  val typesafeRepo = "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases"
  val typesafeSnapshot = "Typesafe Snapshots Repository" at "http://repo.typesafe.com/typesafe/snapshots/"
  val scalatoolsRepo = "Scala-tools.org Repository" at "http://scala-tools.org/repo-releases/"
  val mavenRepo = "Maven Repo" at "http://repo2.maven.org/maven2/"
  
  val all = Seq(typesafeRepo, scalatoolsRepo, /*mavenRepo, */typesafeSnapshot)
}


object Dependencies {
  val akkaVer = "2.1.1"
  val akka = 		"com.typesafe.akka" %% 	"akka-actor" 			% akkaVer 
  val akkaRemote = 	"com.typesafe.akka" %% 	"akka-remote"			% akkaVer
  val akkaSlf4j = 	"com.typesafe.akka" %% 	"akka-slf4j"	    		% akkaVer 
  val akkaTestkit = 	"com.typesafe.akka" %% 	"akka-testkit" 			% akkaVer 	% "test"

  val scalaStm = 	"org.scala-stm" %% 	"scala-stm" 			% "0.7"
  val tslog = 		"com.typesafe" %%	 "scalalogging-slf4j" 		% "1.0.0"
  val tsconfig = 	"com.typesafe" % 	"config" 			% "1.0.0" 

  val scalatest = 	"org.scalatest" % 	"scalatest_2.10" 		% "1.9.1"	% "test"

  val kryo = 		"com.esotericsoftware.kryo" % "kryo"			% "2.20"
  val kryoAkka = 	"com.romix.akka" 	% "akka-kryo-serialization" 	% "0.2-SNAPSHOT"
  //val chill = 	"com.twitter"		%% "chill"			% "0.1.3-SNAPSHOT"

  val logback = 	"ch.qos.logback" 	% "logback-classic" 		% "1.0.9"
  val graph = 		"com.assembla.scala-incubator" % "graph-core_2.10" 	% "1.6.0"
  val graphDot = 	"com.assembla.scala-incubator" % "graph-dot_2.10" 	% "1.6.0"
  //val graphJson = 	"com.assembla.scala-incubator" % "graph-json_2.9.2"	% "1.5.0"
  val metrics  = 	"nl.grons" 		%% "metrics-scala" 		% "2.2.0"
 
  val all = Seq(akka, akkaRemote, akkaSlf4j, akkaTestkit, scalaStm, scalatest,tslog, tsconfig, kryo, kryoAkka, logback, metrics)
  val bench_deps = Seq(akka, scalaStm, tslog, tsconfig, logback, metrics)
//  val granola_deps = Seq(akka, akkaRemote, akkaSlf4j, tslog, tsconfig, kryo, kryoAkka, logback)
//  val soot_deps = Seq(tslog, logback, graph, graphDot)
//  val part_deps = Seq(tslog, logback, graph, graphDot)
}    

// Build settings
object BuildSettings {
  val buildOrganization = "vt.edu"
  val buildVersion      = "0.0.1"
  val buildScalaVersion = "2.10.0"

  val buildSettings = Defaults.defaultSettings ++ Seq (
    organization := buildOrganization,
    version      := buildVersion,
    scalaVersion := buildScalaVersion,
    //scalaBinaryVersion := buildScalaVersion,
    exportJars := true//,
    //scalacOptions += "-Xprint:cleanup"
  )
  
  val extraSettings = Seq(
    resolvers ++= Resolvers.all
    //libraryDependencies ++= Dependencies.all
  ) ++ SbtOneJar.oneJarSettings

  val all_deps = Seq( libraryDependencies ++= Dependencies.all)
  val bench_deps = Seq( libraryDependencies ++= Dependencies.bench_deps)
 // val granola_deps = Seq( libraryDependencies ++= Dependencies.granola_deps)
 // val soot_deps = Seq( libraryDependencies ++= Dependencies.soot_deps)
 // val part_deps = Seq( libraryDependencies ++= Dependencies.part_deps)
}

// Define the project structure
object HyflowBuild extends Build {
  import BuildSettings._
  
  lazy val hyflow = Project (
    id = "hyflow",
    base = file ("."),
    settings = buildSettings
  ) aggregate (benchmarks)

  lazy val core = Project (
    id = "hyflow-core",
    base = file ("hyflow-core"),
    settings = buildSettings ++ extraSettings ++ all_deps
  ) 
  
  lazy val benchmarks = Project (
    id = "hyflow-benchmarks",
    base = file ("hyflow-benchmarks"),
    settings = buildSettings ++ extraSettings ++ bench_deps
  ) dependsOn (core)
/*  
  lazy val checkpoints = Project (
    id = "hyflow-checkpoints",
    base = file ("hyflow-checkpoints"),
    settings = buildSettings ++ extraSettings ++ all_deps
  ) dependsOn (core)
  
  lazy val granola = Project (
    id = "hyflow-granola",
    base = file ("hyflow-granola"),
    settings = buildSettings ++ extraSettings ++ granola_deps
  )
 
  lazy val soot = Project (
    id = "hyflow-soot",
    base = file ("hyflow-soot"),
    settings = buildSettings ++ extraSettings ++ soot_deps
  ) 
  
  lazy val part = Project (
  	id = "hyflow-part",
  	base = file ("hyflow-part"),
  	settings = buildSettings ++ extraSettings ++ part_deps
  ) dependsOn (soot)
*/
}

