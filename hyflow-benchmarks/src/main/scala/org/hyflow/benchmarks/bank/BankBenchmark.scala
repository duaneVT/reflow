package org.hyflow.benchmarks.bank

import org.hyflow.api._
import org.hyflow.Hyflow
import org.hyflow.benchmarks._
import scala.concurrent.stm._
import com.typesafe.scalalogging.slf4j.Logging
import org.hyflow.core.util.HyflowConfig
import java.util.concurrent.TimeUnit
import org.hyflow.core.util.ReInstrumented
import scala.util.Random

class BankBenchmark extends Benchmark with Logging {

	val name = "bank"

	final val INITIAL_AMOUNT = 100
	val ops = HyflowConfig.cfg.getInt("hyflow.workload.ops")
	val par = HyflowConfig.cfg.getBoolean("hyflow.workload.bank.parallel")

	// Initialization of Bank. Create bank accounts.
	def benchInit() {
		val slice = getLocalSlice
		logger.debug("Initializing Bank Benchmark. | slice = {}", slice)
		val accounts = for (i <- slice) yield
			new BankAccount("account-"+i, INITIAL_AMOUNT)
		for (a <- accounts)
			logger.trace("Post-init. | account = {} | account._owner = {}", a, a._owner)
	}

	// Each iteration of Bank uses a transaction to either Read or Transfer.
	def benchIter() = {
		if (randomlyReadOnly) {
			if (par) {
			    val parIDs = randomObjIdSet(ops*2)
				BankParOps.multipleBalanceCheck(parIDs: _*)
			} else {
			    val ids = randomObjIdSet(ops)
				BankSerOps.balanceCheck(ids: _*)
			}
		} else {
			if (par) {
			    val parIDs = randomObjIdSet(ops*2)
			    BankParOps.multipleTransfer(parIDs: _*)
			} else {
			    val ids = randomObjIdSet(2)
				BankSerOps.transfer(ids(0), ids(1), rand.nextInt(20))
			}
		}
	}

	// Check that all of the Bank accounts sum up to the proper total by the end (INITIAL * numAccounts).
	def benchCheck(): Boolean = {
		val total = BankParOps.balanceCheck((0 until totalObjects).map("account-" + _): _*)
		if (total == INITIAL_AMOUNT * totalObjects) {
			logger.debug("Consistency check PASSED. | INITIAL_AMOUNT = {} | totalObjects = {} | total = {}",
					INITIAL_AMOUNT toString,
					totalObjects toString,
					total toString)
			true
		} else {
			logger.error("Consistency check FAILED. | INITIAL_AMOUNT = {} | totalObjects = {} | total = {}",
					INITIAL_AMOUNT toString,
					totalObjects toString,
					total toString)
			false
		}
	}

	// Grab a random Bank account by ID (account-#).
	override def randomObjId: String = {
		"account-"+rand.nextInt(totalObjects)
	}
}

/** Parallel Operations **/
object BankParOps extends Logging {
	def transfer(id1: String, id2: String, amount: Int) {
		atomic { implicit txn =>
		    val acc0 = Hyflow.dir.open[BankAccount](id1)
            val acc1 = Hyflow.dir.open[BankAccount](id2)
            acc0.balance() = acc0.balance() - amount
		    acc1.balance() = acc1.balance() + amount
		}
	}

	def multipleTransfer(ids: String*) {
        atomic { implicit txn =>
            var pair: Seq[String] = Seq[String]()
            val slider = ids.sliding(2,2)
            val rand: Random = new Random(##)

            while (!slider.isEmpty) {
                pair = slider.next
                BankParOps.transfer(pair(0), pair(1), rand.nextInt(20))
            }
        }
    }

	def multipleBalanceCheck(ids: String*) {
	    atomic { implicit txn =>
            var pair: Seq[String] = Seq[String]()
            val slider = ids.sliding(2,2)
            val rand: Random = new Random(##)

            while (!slider.isEmpty) {
                pair = slider.next
                BankParOps.doubleRead(pair(0), pair(1))
            }
        }
	}

	def doubleRead(id1: String, id2: String) {
	    atomic { implicit txn =>
	        val acc0 = Hyflow.dir.open[BankAccount](id1)
            val acc1 = Hyflow.dir.open[BankAccount](id2)
            val amounts = List[BankAccount](acc0, acc1).map(_.balance())
            amounts.sum
	    }
	}

	def balanceCheck(ids: String*): Int = {
		atomic { implicit txn =>
			val accs = Hyflow.dir.openMany[BankAccount](ids.toList)
			val amounts = accs.map(_.balance())
			amounts.sum
		}
	}
}

/** Serial Operations **/
object BankSerOps extends ReInstrumented {
	//val read_tmr = metrics.timer("bank-read", durationUnit = TimeUnit.MICROSECONDS)
	//val write_tmr = metrics.timer("bank-write", durationUnit = TimeUnit.MICROSECONDS)
	//val open_tmr = metrics.timer("bank-open", durationUnit = TimeUnit.MICROSECONDS)

	def deposit(id: String, amount: Int) {
		atomic { implicit txn =>
			//val acc = open_tmr.time { Hyflow.dir.open[BankAccount](id) }
		    val acc = Hyflow.dir.open[BankAccount](id)
			acc.balance() = acc.balance() + amount
		}
	}

	def withdraw(id: String, amount: Int) {
		atomic { implicit txn =>
			//val acc = open_tmr.time { Hyflow.dir.open[BankAccount](id) }
		    val acc = Hyflow.dir.open[BankAccount](id)
			acc.balance() = acc.balance() - amount
		}
	}

	def transfer(id1: String, id2: String, amount: Int) = //write_tmr.time {
		atomic { implicit txn =>
			withdraw(id1, amount)
			deposit(id2, amount)
		}
	//}

	def balanceCheck(ids: String*): Int = //read_tmr.time {
		atomic { implicit txn =>
			val amounts = for (id <- ids) yield atomic { implicit txn =>
				//val acc = open_tmr.time { Hyflow.dir.open[BankAccount](id) }
			    val acc = Hyflow.dir.open[BankAccount](id)
			    acc.balance()
			}
			amounts.sum
		}
	//}
}

/** Bank Account Object **/
class BankAccount(val _id: String, val initial: Int) extends HObj with Logging {
	def this(_id: String) = this(_id, 100)
	val balance = field(initial)
}

