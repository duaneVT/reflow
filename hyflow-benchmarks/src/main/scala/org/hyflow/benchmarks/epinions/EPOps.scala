package org.hyflow.benchmarks.epinions

import org.hyflow.benchmarks.BenchUtil
import org.hyflow.Hyflow
import scala.concurrent.stm._
import scala.util.Random

object EPOps {

	// (Alex) TODO: fix using this as source: 
	// https://github.com/oltpbenchmark/oltpbench/tree/master/src/com/oltpbenchmark/benchmarks/epinions/procedures
	// We need indexes or lists as in doc stores!
	private val rand = new Random

	def getReviewItemById() {
		val iid = rand.nextInt(EPInit.NUM_ITEMS)

		atomic { implicit txn =>
			val item: EPItem = Hyflow.dir.open(Name.I(iid))
			val reviews: List[EPReview] = Hyflow.dir.openMany(item.reviews().take(10).map(Name.R(_)))
			reviews.map(x => (x.u_id(), x.rating()))
		}
	}

	def getReviewsByUser() {
		val uid = rand.nextInt(EPInit.NUM_USERS)

		atomic { implicit txn =>
			val user: EPUser = Hyflow.dir.open(Name.U(uid))
			val reviews: List[EPReview] = Hyflow.dir.openMany(user.reviews().map(Name.R(_)))
			if (reviews.isEmpty) None else Some(reviews.map(_.rating()).sum / reviews.length)
		}
	}

	def getAverageRatingByTrustedUser() {
		val iid = rand.nextInt(EPInit.NUM_ITEMS)
		val uid = rand.nextInt(EPInit.NUM_USERS)

		atomic { implicit txn =>
			// Get reviews for this product
			val item: EPItem = Hyflow.dir.open(Name.I(iid))
			val reviews: List[EPReview] = Hyflow.dir.openMany(item.reviews().map(Name.R(_)))
			reviews.map(_.rating()).sum

			// Get trust for this user
			val user: EPUser = Hyflow.dir.open(Name.U(uid))
			val trusted: List[EPTrust] = Hyflow.dir.openMany(user.trustedBy().map(Name.T(_, uid)))
			trusted.map(_.trust()).sum
		}
	}

	def getItemAverageRating() {
		val iid = rand.nextInt(EPInit.NUM_ITEMS)

		atomic { implicit txn =>
			val item: EPItem = Hyflow.dir.open(Name.I(iid))
			val reviews: List[EPReview] = Hyflow.dir.openMany(item.reviews().map(Name.R(_)))
			if (reviews.isEmpty) None else Some(reviews.map(_.rating()).sum / reviews.length)
		}
	}

	def getItemReviewsByTrustedUser() {
		val iid = rand.nextInt(EPInit.NUM_ITEMS)
		val uid = rand.nextInt(EPInit.NUM_USERS)

		atomic { implicit txn =>
			// Get reviews for this product
			val item: EPItem = Hyflow.dir.open(Name.I(iid))
			val reviews: List[EPReview] = Hyflow.dir.openMany(item.reviews().map(Name.R(_)))
			reviews.map(x => (x.u_id(), x.rating()))

			// Get trust for this user
			val user: EPUser = Hyflow.dir.open(Name.U(uid))
			val trusted: List[EPTrust] = Hyflow.dir.openMany(user.trustedBy().map(Name.T(_, uid)))
			trusted.map(x => (x.source_u_id, x.trust()))
		}
	}

	def updateUserName() {
		val l = EPInit.NAME_LENGTH
		val name = BenchUtil.randString(l, l)
		val uid = rand.nextInt(EPInit.NUM_USERS)

		atomic { implicit txn =>
			val user: EPUser = Hyflow.dir.open(Name.U(uid))
			user.name() = name
		}
	}

	def updateItemTitle() {
		val l = EPInit.TITLE_LENGTH
		val title = BenchUtil.randString(l, l)
		val iid = rand.nextInt(EPInit.NUM_ITEMS)

		atomic { implicit txn =>
			val item: EPItem = Hyflow.dir.open(Name.I(iid))
			item.title() = title
		}
	}

	def updateReviewRating() {
		val iid = rand.nextInt(EPInit.NUM_ITEMS)
		val uid = rand.nextInt(EPInit.NUM_USERS)
		val rating = rand.nextInt(1000) //?

		atomic { implicit txn =>
			val item: EPItem = Hyflow.dir.open(Name.I(iid))
			val user: EPUser = Hyflow.dir.open(Name.U(uid))
			val rid = item.reviews().toSet.intersect(user.reviews().toSet)
			if (!rid.isEmpty) {
				val review: EPReview = Hyflow.dir.open(Name.R(rid.head))
				review.rating() = rating
			} // What if review doesn't already exist?
		}
	}

	def updateTrustRating() {
		val uid = rand.nextInt(EPInit.NUM_USERS)
		val uid2 = rand.nextInt(EPInit.NUM_USERS)
		val itrust = rand.nextInt(2)

		atomic { implicit txn =>
			val user2: EPUser = Hyflow.dir.open(Name.U(uid2))
			if (user2.trustedBy().contains(uid)) {
				val trust: EPTrust = Hyflow.dir.open(Name.T(uid, uid2))
				trust.trust() = itrust
			} // What if trust entry doesn't already exist?
		}
	}
}