package org.hyflow.benchmarks.epinions

import scala.concurrent.stm._
import scala.collection.mutable
import com.typesafe.scalalogging.slf4j.Logging
import org.hyflow.benchmarks.BenchUtil
import org.hyflow.util.distributions._
import scala.util.Random
import org.hyflow.Hyflow

object EPInit extends Logging {
	// Constants
	val NUM_USERS = 2000
	val NUM_ITEMS = 1000
	
	val NAME_LENGTH = 5
	val TITLE_LENGTH = 20
	
	val REVIEW = 500
	val TRUST = 200
	
	val BATCH_SIZE = 1000
	
	
	val rnd = new Random
	
	def populateAll() {
		loadUsers()
		loadItems()
		loadReviews()
		loadTrust()
	}
	
	def loadUsers() = atomic { implicit txn =>
		val l = NAME_LENGTH
		for (i <- 0 until NUM_USERS) {
			val usr = new EPUser(i)
			usr.name() = BenchUtil.randString(l, l)
		}
		logger.info("Loaded users. | n = %d" format NUM_USERS)
	}
	
	def loadItems() = atomic { implicit txn =>
		val l = TITLE_LENGTH
		for (i <- 0 until NUM_ITEMS) {
			val itm = new EPItem(i)
			itm.title() = BenchUtil.randString(l, l)
		}
		logger.info("Loaded items. | n = %d" format NUM_ITEMS)
	}
	
	def loadReviews() = { 
		val numReviews = new ZipfianGenerator(REVIEW, 1.8)
		val reviewer = new ZipfianGenerator(NUM_USERS)
		var total = 0
		
		for (i <- 0 until NUM_ITEMS) atomic { implicit txn =>
			val review_count = scala.math.max(numReviews.nextInt(), 1)
			val reviewers = mutable.Set[Int]()
			
			var rc = 0
			while (rc < review_count) {
				val u_id = reviewer.nextInt()
				if (reviewers.add(u_id)) {
					// Create review entry
					val rev = new EPReview(total)
					rev.u_id() = u_id
					rev.i_id() = i
					rev.rating() = rnd.nextInt(5)
					rev.rank() = None
					// Update user
					val user: EPUser = Hyflow.dir.open(Name.U(u_id))
					user.reviews() = total :: user.reviews() 
					// Update item
					val item: EPItem = Hyflow.dir.open(Name.I(i))
					item.reviews() = total :: user.reviews()
					// Increment counters
					rc += 1
					total += 1
				}
			}
		}
		logger.info("Loaded reviews. | n = %d" format total)
	}
	
	def loadTrust() = {
		val numTrust = new ZipfianGenerator(TRUST, 1.95)
		val reviewed = new ScrambledZipfianGenerator(NUM_USERS)
		
		val isTrusted = new Random(System.currentTimeMillis)
		var total = 0
		for (i <- 0 until NUM_USERS) atomic { implicit txn =>
			val trusted = mutable.Set[Int]()
			val trust_count = numTrust.nextInt()
			
			var tc = 0
			while (tc < trust_count) {
				val u_id = reviewed.nextInt()
				if (trusted.add(u_id)) {
					// Create trust entry
					val trust = new EPTrust(i, u_id)
					trust.trust() = isTrusted.nextInt(2)
					// Update user
					val user: EPUser = Hyflow.dir.open(Name.U(u_id))
					user.trustedBy() = i :: user.trustedBy()
					// Update counters
					tc += 1
					total += 1
				}
			}
		}
		logger.info("Loaded trust. | n = %d" format total)
	}
}








