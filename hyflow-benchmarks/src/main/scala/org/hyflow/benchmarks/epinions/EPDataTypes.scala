package org.hyflow.benchmarks.epinions

import org.hyflow.api._
import java.util.Date

object Name {
	def U(u_id: Int) = "user-u%d".format(u_id)
	def I(i_id: Int) = "item-i%d".format(i_id)
	def R(a_id: Int) = "review-a%d".format(a_id)
	def RR(a_id: Int) = "review-rating-a%d".format(a_id)
	def T(source_u_id: Int, target_u_id: Int) = 
		"trust-s%d-t%d".format(source_u_id, target_u_id)
}

class EPUser(val u_id: Int) extends HObj {
	def _id = Name.U(u_id)
	val name = field[String](null)
	val reviews = field(List[Int]())
	val trustedBy = field(List[Int]())
}

class EPItem(val i_id: Int) extends HObj {
	def _id = Name.I(i_id)
	val title = field[String](null)
	val reviews = field(List[Int]())
}

class EPReview(val a_id: Int) extends HObj {
	def _id = Name.R(a_id)
	val u_id = field(0)
	val i_id = field(0)
	val rating = field(0)
	val rank = field[Option[Int]](None)
}

class EPReviewRating(val a_id: Int) extends HObj {
	def _id = Name.RR(a_id)
	val u_id = field(0)
	val rating = field(0)
	val status = field(0)
	val creationDate = field[String](new Date toString)
	val lastModDate = field[String](null)
	val `type` = field(0)
	val verticalId = field(0)
}

class EPTrust(val source_u_id: Int, val target_u_id: Int) extends HObj {
	def _id = Name.T(source_u_id, target_u_id)
	//val target_u_id = field(0)
	val trust = field(0)
	val creationDate = field[String](new Date toString)
}

