package org.hyflow.benchmarks.epinions

import com.typesafe.scalalogging.slf4j.Logging
import org.hyflow.benchmarks.Benchmark

class EPBenchmark extends Benchmark with Logging {
	val name = "epinions"
		
	def benchInit() {
		val slice = getLocalSlice
		if (slice.contains(0)) {
			// Initialize data
			EPInit.populateAll()
		}
	}
	
	def benchIter() {
		val opt = rand.nextInt(100)
		if (opt < 10) {
			logger.info("Run Transaction: GetReviewItemById")
			EPOps.getReviewItemById()
		} else if (opt < 20) {
			logger.info("Run Transaction: GetReviewsByUser")
			EPOps.getReviewsByUser()
		} else if (opt < 30) {
			logger.info("Run Transaction: GetAverageRatingByTrustedUser")
			EPOps.getAverageRatingByTrustedUser()
		} else if (opt < 40) {
			logger.info("Run Transaction: GetItemAverageRating")
			EPOps.getItemAverageRating()
		} else if (opt < 50) {
			logger.info("Run Transaction: GetItemReviewsByTrustedUser")
			EPOps.getItemReviewsByTrustedUser()
		} else if (opt < 60) {
			logger.info("Run Transaction: UpdateUserName")
			EPOps.updateUserName()
		} else if (opt < 70) {
			logger.info("Run Transaction: UpdateItemTitle")
			EPOps.updateItemTitle()
		} else if (opt < 80) {
			logger.info("Run Transaction: UpdateReviewRating")
			EPOps.updateReviewRating()
		} else {
			logger.info("Run Transaction: UpdateTrustRating")
			EPOps.updateTrustRating()
		}
	}
	
	def benchCheck() = true
}