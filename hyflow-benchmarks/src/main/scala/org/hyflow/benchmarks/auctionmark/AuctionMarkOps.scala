package org.hyflow.benchmarks.auctionmark

import scala.util.Random
import scala.collection.mutable
import java.util.Date
import scala.concurrent.stm._
import org.hyflow.Hyflow
import java.util.concurrent.atomic.AtomicInteger
import java.util.ArrayList

object AuctionMarkOps{

	private val rand = new Random
	private val userCount = new AtomicInteger(0)
	private val itemCount = new AtomicInteger(0)
	private val itemBidCount = new AtomicInteger(0)
	private val itemMaxBidCount = new AtomicInteger(0)
	private val imageCount = new AtomicInteger(0)
	
  	def newUser() = {

  		val r_id = Name.c(rand.nextInt(AuctionMarkInit.NUM_REGIONS))
  		val u_id = AuctionMarkInit.NUM_USERS + userCount.getAndIncrement()		
  		
		try {
			atomic { implicit txn =>
			  	val user = new User(u_id)
			  	user.u_rating() = 0
			  	user.u_balance() = 0
			  	user.u_r_id() = r_id
			  	user.u_created() = new java.util.Date().toString() 
			  	user.u_sattr0() = "sattr0"
			  	user.u_sattr1() = "sattr1"
			  	user.u_sattr2() = "sattr2"
			  	user.u_sattr3() = "sattr3"
			  	user.u_sattr4() = "sattr4"
			  	user.u_sattr5() = "sattr5"
			  	user.u_sattr6() = "sattr6"
			  	user.u_sattr7() = "sattr7"
			  	println("user created")
			}
		} catch {
			case e: Exception => null
		}
	}

  	def newItem() = {

  		val item_id = AuctionMarkInit.NUM_ITEMS + itemCount.getAndIncrement()		
  		
		try {
			atomic { implicit txn =>
				val item = new Item(item_id)
		  	  	item.i_u_id() = Name.u(rand.nextInt(AuctionMarkInit.NUM_USERS + userCount.get())) // user can be buyer as well
		  	  	item.i_c_id() = Name.c(rand.nextInt(AuctionMarkInit.NUM_CATEGORIES))
		  	  	item.i_name() = "item-"+item_id 
		  	  	item.i_description() = "item-desc-"+item_id // (Alex) TODO : later
		  	  	item.u_user_attributes() = "item-user-attr-"+item_id
		  	  	item.i_initial_price() = rand.nextInt(AuctionMarkInit.PRICE_RANGE) + 1
		  	  	item.i_current_price() = item.i_initial_price()
		  	  	item.i_num_bids() = 0
		  	  	item.i_num_images() = rand.nextInt(AuctionMarkInit.NUM_IMAGES_PER_ITEM)
		  	  	item.i_num_global_attrs() = rand.nextInt(AuctionMarkInit.NUM_GLOBAL_ATTR_PER_ITEM)
		  	  	item.i_start_date() = new java.util.Date().toString() 
		  	  	item.i_end_date() = new java.util.Date().toString() + rand.nextInt(AuctionMarkInit.NUM_DAYS_PER_ITEM) 
		  	  	item.i_status() = 0 // item open

				var num_image = item.i_num_images()
				
				// populate image
				for (i <- 0 to (num_image)) {
					var  ii_i_id = i
					var path = "path-"+ii_i_id+"-"+item_id
					var ii = new Item_Image(ii_i_id, item_id)
					ii.ii_i_id() = ii_i_id + ""
					ii.ii_u_id() = item_id + ""
					ii.ii_path() = path
				}
				
				var user_id = item.i_u_id()
				val user = Hyflow.dir.open[User](user_id)
				user.u_balance() = user.u_balance() - 1.0
			}
		} catch {
			case e: Exception => null
		}
	}

	def newBid() = {
	  // store the bid info in the item itself
	  val item_id = Name.i(rand.nextInt(AuctionMarkInit.NUM_ITEMS + itemCount.get()))
	  val item_bid_id = itemBidCount.getAndIncrement()	  
  	  val item_max_bid_id = itemMaxBidCount.getAndIncrement()
	  val buyer_id = Name.u(rand.nextInt(AuctionMarkInit.NUM_USERS + userCount.get()))
	  var bid:Double = rand.nextInt(AuctionMarkInit.PRICE_RANGE) + 1
	  var max_bid:Double = rand.nextInt(AuctionMarkInit.PRICE_RANGE) + 1
	  
	  		try {
			atomic { implicit txn =>
					val item = Hyflow.dir.open[Item](item_id)
					println("item opened is "+item_id)
					
					// pre-checks
					if (buyer_id == item.i_u_id()) {
						println("buyer same as seller!")
						throw new Exception("Rolling back")
					} else if (item.i_status() != 0) {
					  println("auction closed")
					  throw new Exception("Rolling back")
					} 
					
					
					// update bid count
					item.i_num_bids()  = item.i_num_bids() + 1 

					if (item.i_max_bids() != null) {
						val max_bid_id = Name.imb(item.i_max_bids().get(0))
						// scan all the bids to find the max
						var current_bid = 0.0
						var current_max_bid = 0.0
						var current_bid_obj:Item_Bid = null
						for (i <- 0 until item.i_bids().size()) {
							val bid_id = Name.ib(item.i_bids().get(i))
							val item_bid = Hyflow.dir.open[Item_Bid](bid_id)
							if (item_bid.ib_bid() > current_bid) {
								current_bid = item_bid.ib_bid()
								current_bid_obj = item_bid
							}
							
							if (item_bid.ib_max_bid() > current_max_bid) {
								current_max_bid = item_bid.ib_max_bid()
							}						
						}
						
						var newBidWin = false
						if (max_bid > current_max_bid) {
							newBidWin = true
							if (bid < current_bid) {
								bid = current_max_bid
							}
						} else {
							if (bid > current_bid) {
								current_bid_obj.ib_bid() = bid
							}
						}
						
						if (newBidWin) {
							val max_bid = Hyflow.dir.open[Item_Max_Bid](max_bid_id)
							max_bid.imb_ib_id() = Name.ib(item_bid_id)
							max_bid.imb_ib_u_id() = buyer_id
							max_bid.imb_updated() = new java.util.Date().toString()
						}
					  
					} else {
						val item_max_bid =  new Item_Max_Bid(item_max_bid_id)
						
						item_max_bid.imb_u_id() = item.i_u_id()
						item_max_bid.imb_ib_id() = Name.ib(item_bid_id)
						item_max_bid.imb_i_id() = item._id
						item_max_bid.imb_ib_u_id() = buyer_id
						item.i_max_bids()  = new ArrayList[Int]()
						item.i_max_bids().add(item_max_bid_id)
					}
					
					// create new item bid
					val item_bid =  new Item_Bid(item_bid_id)
					item_bid.ib_i_id() = item._id
					item_bid.ib_u_id() = item.i_u_id()
					item_bid.ib_b_id() = buyer_id
					item_bid.ib_bid() = bid
					item_bid.ib_max_bid() = bid //?
				    item_bid.ib_created() = new java.util.Date().toString() 
				    item_bid.ib_updated() = new java.util.Date().toString()
				    item.i_bids()  = new ArrayList[Int]()
				    item.i_bids().add(item_bid_id)
				    
			}
		} catch {
			case e: Exception => null
		}

	}
	
	
}