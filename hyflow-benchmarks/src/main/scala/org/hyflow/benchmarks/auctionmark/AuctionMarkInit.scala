package org.hyflow.benchmarks.auctionmark

import scala.util.Random
import scala.concurrent.stm._
import com.typesafe.scalalogging.slf4j.Logging
import org.hyflow.api._


object AuctionMarkInit extends Logging {
	
	val NUM_REGIONS = 62
	val NUM_USERS = 10
	val NUM_ITEMS = 100
	val NUM_CATEGORIES = 10 // needs to change
	val PRICE_RANGE = 1000000
	val NUM_IMAGES_PER_ITEM = 10
	val NUM_GLOBAL_ATTR_PER_ITEM = 50
	val NUM_DAYS_PER_ITEM = 30
	
	private val rand = new Random

  
	def populateAll() = {
	  	for (i_id <- 0 to (NUM_REGIONS-1)) {
			regionCreate(i_id)
		}
	  	println("created regions")
	  	
	  	
	  	for (i_id <- 0 to (NUM_USERS-1)) {
			userCreate(i_id)
		}
	  	println("created users")

	  	
	  	for (i_id <- 0 to (NUM_CATEGORIES -1)) {
			categoryCreate(i_id)
		}
	  	println("created categories")

	  	for (i_id <- 0 to (NUM_ITEMS -1)) {
			itemCreate(i_id)
		}
	  	println("created items")



	}
  
  	private def regionCreate(id: Int) = atomic { implicit txn =>
		val region = new Region(id)
		region.r_name() = "region-"+id
	}

  	private def userCreate(id: Int) = atomic { implicit txn =>
		val user = new User(id)
		user.u_rating() = 0
		user.u_balance() = 0
		user.u_r_id() = Name.r(rand.nextInt(NUM_REGIONS))
		user.u_created() = new java.util.Date().toString() 
		user.u_sattr0() = "sattr0"
		user.u_sattr1() = "sattr1"
		user.u_sattr2() = "sattr2"
		user.u_sattr3() = "sattr3"
		user.u_sattr4() = "sattr4"
		user.u_sattr5() = "sattr5"
		user.u_sattr6() = "sattr6"
		user.u_sattr7() = "sattr7"
  	}
  	
  	private def itemCreate(id: Int) = atomic { implicit txn =>
  	  	val item = new Item(id)
  	  	itemPopulate(item, id)
  	}
  	
  	def itemPopulate(item: Item, id : Int) = atomic {  implicit txn =>
  	  	item.i_u_id() = Name.u(rand.nextInt(NUM_USERS)) // user can be buyer as well
  	  	item.i_c_id() = Name.c(rand.nextInt(NUM_CATEGORIES))
  	  	item.i_name() = "item-"+id 
  	  	item.i_description() = "item-desc-"+id // (Alex) TODO : later
	    item.u_user_attributes() = "item-user-attr-"+id
	    item.i_initial_price() = rand.nextInt(PRICE_RANGE) + 1
	    item.i_current_price() = item.i_initial_price()
	    item.i_num_bids() = 0
	    item.i_num_images() = rand.nextInt(NUM_IMAGES_PER_ITEM)
	    item.i_num_global_attrs() = rand.nextInt(NUM_GLOBAL_ATTR_PER_ITEM)
	    item.i_start_date() = new java.util.Date().toString() 
	    item.i_end_date() = new java.util.Date().toString() + rand.nextInt(NUM_DAYS_PER_ITEM) 
	    item.i_status() = 0 // item open
//	    println("item created")
  	}  	
  	
  	
  	
  	
  	private def categoryCreate(id: Int) = atomic { implicit txn =>
  	  	val category = new Category(id)
  	  	category.c_parent_id() = null // (Alex) TODO : get real data
  	  	category.c_name() = "category-"+id
  	}
}

