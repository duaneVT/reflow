
object AuctionMarkConstants {
      // Non-standard txns
    var FREQUENCY_CLOSE_AUCTIONS :Int    = -1; // called at regular intervals
    var ENABLE_CLOSE_AUCTIONS: Boolean = true
    
    var FREQUENCY_GET_ITEM              = 25;
    var  FREQUENCY_GET_USER_INFO         = 15;
    var  FREQUENCY_NEW_BID               = 20;
    var  FREQUENCY_NEW_COMMENT           = 5;  
    var  FREQUENCY_NEW_COMMENT_RESPONSE  = 5;
    var  FREQUENCY_NEW_FEEDBACK          = 5;
    var  FREQUENCY_NEW_ITEM              = 10;
    var  FREQUENCY_NEW_PURCHASE          = 5;
    var  FREQUENCY_UPDATE_ITEM           = 10;
    
        
//    var  FREQUENCY_GET_ITEM              = 50;
//    var  FREQUENCY_GET_USER_INFO         = 0;
//    var  FREQUENCY_NEW_BID               = 50;
//    var  FREQUENCY_NEW_COMMENT           = 0;  
//    var  FREQUENCY_NEW_COMMENT_RESPONSE  = 0;
//    var  FREQUENCY_NEW_FEEDBACK          = 0;
//    var  FREQUENCY_NEW_ITEM              = 0;
//    var  FREQUENCY_NEW_PURCHASE          = 0;
//    var  FREQUENCY_UPDATE_ITEM           = 0;
    
    // ----------------------------------------------------------------
    // DEFAULT TABLE SIZES
    // ----------------------------------------------------------------
    
    var  TABLESIZE_REGION                   = 75;
    var  TABLESIZE_GLOBAL_ATTRIBUTE_GROUP   = 100;
    var  TABLESIZE_GLOBAL_ATTRIBUTE_VALUE   = 1; // HACK: IGNORE
    var  TABLESIZE_GLOBAL_ATTRIBUTE_VALUE_PER_GROUP = 10;
    var  TABLESIZE_USER                     = 100000;
    
    // ----------------------------------------------------------------
    // USER PARAMETERS
    // ----------------------------------------------------------------
    
    var  USER_MIN_ATTRIBUTES = 0;
    var  USER_MAX_ATTRIBUTES = 5;
    
    var  USER_MIN_BALANCE   = 1000;
    var  USER_MAX_BALANCE   = 100000;
    
    var  USER_MIN_RATING   = 0;
    var  USER_MAX_RATING   = 10000;
    
// (Alex) TODO 
//    object ItemStatus extends Enumeration {
//    	
//        OPEN                    (false),
//        ENDING_SOON             (true), // Only used internally
//        WAITING_FOR_PURCHASE    (false),
//        CLOSED                  (false);
//        
//        private final boolean internal;
//        
//        private ItemStatus(boolean internal) {
//            this.internal = internal;
//        }
//        public boolean isInternal() {
//            return internal;
//        }
//        public static ItemStatus get(long idx) {
//            return (values()[(int)idx]);
//        }
//    }

        var  ITEM_MIN_INITIAL_PRICE = 1;
    var  ITEM_MAX_INITIAL_PRICE = 1000;
    var  ITEM_MIN_ITEMS_PER_SELLER = 0;
    var  ITEM_MAX_ITEMS_PER_SELLER = 10000;
    var  ITEM_MIN_BIDS_PER_DAY = 0;
    var  ITEM_MAX_BIDS_PER_DAY = 10;
    var  ITEM_MIN_WATCHES_PER_DAY = 0;
    var  ITEM_MAX_WATCHES_PER_DAY = 20;
    var  ITEM_MIN_IMAGES = 1;
    var  ITEM_MAX_IMAGES = 10;
    var  ITEM_MIN_COMMENTS = 0;
    var  ITEM_MAX_COMMENTS = 5;
    var  ITEM_MIN_GLOBAL_ATTRS = 1;
    var  ITEM_MAX_GLOBAL_ATTRS = 10;
    
    /** When an item receives a bid we will increase its price by this amount */
    var ITEM_BID_PERCENT_STEP = 0.025f;
    
    /** How long should we wait before the buyer purchases an item that they won */
    var  ITEM_MAX_PURCHASE_DURATION_DAYS = 7;
    
    /** Duration in days that expired bids are preserved */
    var  ITEM_PRESERVE_DAYS = 7;
    
    /** The duration in days for each auction */
    var  ITEM_MAX_DURATION_DAYS = 10;
    var  ITEM_MAX_PURCHASE_DAY = 7;
    
    /**
     * This defines the maximum size of a small cache of ItemIds that
     * we maintain in the benchmark profile. For some procedures, the client will 
     * ItemIds out of this cache and use them as txn parameters 
     */
    var  ITEM_ID_CACHE_SIZE  = 1000;
    
    // ----------------------------------------------------------------
    // DEFAULT BATCH SIZES
    // ----------------------------------------------------------------
    
    var  BATCHSIZE_REGION                   = 5000;
    var  BATCHSIZE_GLOBAL_ATTRIBUTE_GROUP   = 5000;
    var  BATCHSIZE_GLOBAL_ATTRIBUTE_VALUE   = 5000;
    var  BATCHSIZE_CATEGORY                 = 5000;
    var  BATCHSIZE_USER                     = 1000;
    var  BATCHSIZE_USER_ATTRIBUTES          = 5000;
    var  BATCHSIZE_USER_FEEDBACK            = 1000;
    var  BATCHSIZE_USER_ITEM                = 5000;
    var  BATCHSIZE_USER_WATCH               = 5000;
    var  BATCHSIZE_ITEM                     = 2000;
    var  BATCHSIZE_ITEM_ATTRIBUTE           = 5000;
    var  BATCHSIZE_ITEM_IMAGE               = 5000;
    var  BATCHSIZE_ITEM_COMMENT             = 1000;
    var  BATCHSIZE_ITEM_BID                 = 5000;
    var  BATCHSIZE_ITEM_MAX_BID             = 5000;
    var  BATCHSIZE_ITEM_PURCHASE            = 5000;
    
    var  BATCHSIZE_CLOSE_AUCTIONS_UPDATES   = 50;
    
    // ----------------------------------------------------------------
    // TABLE NAMES
    // ----------------------------------------------------------------
    var  TABLENAME_CONFIG_PROFILE         = "CONFIG_PROFILE";
    var  TABLENAME_REGION                 = "REGION";
    var  TABLENAME_USER                   = "USER";
    var  TABLENAME_USER_ATTRIBUTES        = "USER_ATTRIBUTES";
    var  TABLENAME_USER_ITEM              = "USER_ITEM";
    var  TABLENAME_USER_WATCH             = "USER_WATCH";
    var  TABLENAME_USER_FEEDBACK          = "USER_FEEDBACK";
    var  TABLENAME_CATEGORY               = "CATEGORY";
    var  TABLENAME_GLOBAL_ATTRIBUTE_GROUP = "GLOBAL_ATTRIBUTE_GROUP";
    var  TABLENAME_GLOBAL_ATTRIBUTE_VALUE = "GLOBAL_ATTRIBUTE_VALUE";
    var  TABLENAME_ITEM                   = "ITEM";
    var  TABLENAME_ITEM_ATTRIBUTE         = "ITEM_ATTRIBUTE";
    var  TABLENAME_ITEM_IMAGE             = "ITEM_IMAGE";
    var  TABLENAME_ITEM_COMMENT           = "ITEM_COMMENT";
    var  TABLENAME_ITEM_BID               = "ITEM_BID";
    var  TABLENAME_ITEM_MAX_BID           = "ITEM_MAX_BID";
    var  TABLENAME_ITEM_PURCHASE          = "ITEM_PURCHASE";

    var TABLENAMES: Array[String] = Array(
        AuctionMarkConstants.TABLENAME_REGION,
        AuctionMarkConstants.TABLENAME_CATEGORY,
        AuctionMarkConstants.TABLENAME_GLOBAL_ATTRIBUTE_GROUP,
        AuctionMarkConstants.TABLENAME_GLOBAL_ATTRIBUTE_VALUE,
        AuctionMarkConstants.TABLENAME_USER,
        AuctionMarkConstants.TABLENAME_USER_ATTRIBUTES,
        AuctionMarkConstants.TABLENAME_USER_ITEM,
        AuctionMarkConstants.TABLENAME_USER_WATCH,
        AuctionMarkConstants.TABLENAME_USER_FEEDBACK,
        AuctionMarkConstants.TABLENAME_ITEM,
        AuctionMarkConstants.TABLENAME_ITEM_ATTRIBUTE,
        AuctionMarkConstants.TABLENAME_ITEM_IMAGE,
        AuctionMarkConstants.TABLENAME_ITEM_COMMENT,
        AuctionMarkConstants.TABLENAME_ITEM_BID,
        AuctionMarkConstants.TABLENAME_ITEM_MAX_BID,
        AuctionMarkConstants.TABLENAME_ITEM_PURCHASE);
    
        // ----------------------------------------------------------------
    // TABLE DATA SOURCES
    // ----------------------------------------------------------------
    
    // If a table exists in this set, then the number of tuples loaded into the table
    // should not be modified by the scale factor
    var FIXED_TABLES: java.util.Collection[String] = new java.util.HashSet
	FIXED_TABLES.add(AuctionMarkConstants.TABLENAME_REGION);
    FIXED_TABLES.add(AuctionMarkConstants.TABLENAME_GLOBAL_ATTRIBUTE_GROUP);
    FIXED_TABLES.add(AuctionMarkConstants.TABLENAME_GLOBAL_ATTRIBUTE_VALUE);
    
    
    var DYNAMIC_TABLES: java.util.Collection[String] = new java.util.HashSet
    DYNAMIC_TABLES.add(AuctionMarkConstants.TABLENAME_USER_ATTRIBUTES);
    DYNAMIC_TABLES.add(AuctionMarkConstants.TABLENAME_ITEM_IMAGE);
    DYNAMIC_TABLES.add(AuctionMarkConstants.TABLENAME_ITEM_ATTRIBUTE);
    DYNAMIC_TABLES.add(AuctionMarkConstants.TABLENAME_ITEM_COMMENT);
    DYNAMIC_TABLES.add(AuctionMarkConstants.TABLENAME_USER_FEEDBACK);
    DYNAMIC_TABLES.add(AuctionMarkConstants.TABLENAME_ITEM_BID);
    DYNAMIC_TABLES.add(AuctionMarkConstants.TABLENAME_ITEM_MAX_BID);
    DYNAMIC_TABLES.add(AuctionMarkConstants.TABLENAME_ITEM_PURCHASE);
    DYNAMIC_TABLES.add(AuctionMarkConstants.TABLENAME_USER_ITEM);
    DYNAMIC_TABLES.add(AuctionMarkConstants.TABLENAME_USER_WATCH);
    
   var DATAFILE_TABLES: java.util.Collection[String] = new java.util.HashSet
   DATAFILE_TABLES.add(AuctionMarkConstants.TABLENAME_CATEGORY);
   
     // ----------------------------------------------------------------
    // TIME PARAMETERS
    // ----------------------------------------------------------------
    
    /**
     * 1 sec in real time equals this value in the benchmark's virtual time in seconds
     */
    var  TIME_SCALE_FACTOR = 3600l; //  * 1000000l; // one hour
    
    /** How often to execute CLOSE_AUCTIONS in virtual milliseconds */
    var  INTERVAL_CLOSE_AUCTIONS    = 3600000l; // Check winning bid's frequency in millisecond
    
    /**
     * If the amount of time (in milliseconds) remaining for an item auction
     * is less than this parameter, then it will be added to a special queue
     * in the client. We will increase the likelihood that a users will bid on these
     * items as it gets closer to their end times
     */
    var  ENDING_SOON = 7200000l; // 10 hours
    
    var  SECONDS_IN_A_DAY = 24 * 60 * 60;
    var  MILLISECONDS_IN_A_DAY = SECONDS_IN_A_DAY * 1000;
    var  MICROSECONDS_IN_A_DAY = MILLISECONDS_IN_A_DAY * 1000;
    
    // ----------------------------------------------------------------
    // PROBABILITIES
    // ----------------------------------------------------------------
    
    /** The probability that a buyer will leave feedback for the seller (1-100)*/
    var  PROB_PURCHASE_BUYER_LEAVES_FEEDBACK = 75;
    /** The probability that a seller will leave feedback for the buyer (1-100)*/
    var  PROB_PURCHASE_SELLER_LEAVES_FEEDBACK = 80;
    
    var  PROB_GETUSERINFO_INCLUDE_FEEDBACK = 33;
    var  PROB_GETUSERINFO_INCLUDE_COMMENTS = 25;
    var  PROB_GETUSERINFO_INCLUDE_SELLER_ITEMS = 25;
    var  PROB_GETUSERINFO_INCLUDE_BUYER_ITEMS = 25;
    var  PROB_GETUSERINFO_INCLUDE_WATCHED_ITEMS = 50;
    
    var  PROB_UPDATEITEM_DELETE_ATTRIBUTE = 25;
    var  PROB_UPDATEITEM_ADD_ATTRIBUTE = -1; // 25;
    
    /** The probability that a buyer will not have enough money to purchase an item (1-100) */
    var  PROB_NEW_PURCHASE_NOT_ENOUGH_MONEY = 1;
    
    /** The probability that the NewBid txn will try to bid on a closed item (1-100) */
    var  PROB_NEWBID_CLOSED_ITEM = 5;
    
    /** The probability that a NewBid txn will target an item whose auction is ending soon (1-100) */
    var  PROB_NEWBID_ENDINGSOON_ITEM = 50;
    
    
    
    
    
    
    
}