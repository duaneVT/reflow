package org.hyflow.benchmarks.auctionmark

import org.hyflow.api._
import java.util.ArrayList


  object Name {
	def r(r_id: Int) = 
		"region-%d".format(r_id)
	def gag(gag_id: Int) = 
		"gag-%d".format(gag_id)
	def gav(gav_id: Int) = 
		"gav-%d".format(gav_id)
	def c(c_id: Int) = 
		"c-%d".format(c_id)
	def u(u_id: Int) = 
		"u-%d".format(u_id)
	def ua(ua_id: Int) = 
		"ua-%d".format(ua_id)
	def i(i_id: Int) = 
		"i-%d".format(i_id)		
	def ii(ii_i_id: Int, ii_u_id: Int) = 
		"ii-%d-%d".format(ii_i_id, ii_u_id)
	def ic(ic_id: Int) = 
		"ic-%d".format(ic_id)		
	def ib(ib_id: Int) = 
		"ib-%d".format(ib_id)		
	def imb(imb_id: Int) = 
		"imb-%d".format(imb_id)
	def ip(ip_id: Int) = 
		"ip-%d".format(ip_id)
	}

    // This class represents the geographical regions of users.
    class Region (val r_id: Int) extends HObj {
	  def _id = Name.r(r_id)
	  val r_name = field[String](null)
 
	}
	
	// Represents merchandises’ global attribute groups (e.g., brand, material, features).
	class Global_Attribute_Group (val gag_id: Int) extends HObj {
	  def _id = Name.gag(gag_id)
	  val  gag_c_id = field[String](null)
	  val gag_name = field[String](null)
	}
	
	// Rrepresents merchandises’ global attributes within each attribute groups (e.g., Rolex, Casio, Seiko within
	//	brand).
	class Global_Attribute_Value(val gav_id: Int) extends HObj {
	  def _id = Name.gav(gav_id)
	  val  gav_gag_id = field[String](null)
	  val gav_name = field[String](null)
	}

	//	Represents merchandises’ categories. Category can be hierarchical aligned using c_parent_id. The
	// records in this table were retrieved from a well-known
	class Category(val c_id: Int) extends HObj {
	  def _id = Name.c(c_id)
	  val  c_parent_id = field[String](null)
	  val c_name = field[String](null)
	}
	
	
	//	Represents users of the system both sellers and buyers.
	class User(val u_id: Int) extends HObj {
	  def _id = Name.u(u_id)
	  val u_rating = field[Int](0)
  	  val u_balance  = field[Double](0)
	  val u_r_id = field[String](null)
	  val u_created = field[String](null)
	  val u_sattr0 = field[String](null)
	  val u_sattr1 = field[String](null)
	  val u_sattr2 = field[String](null)
	  val u_sattr3 = field[String](null)
	  val u_sattr4 = field[String](null)
	  val u_sattr5 = field[String](null)
	  val u_sattr6 = field[String](null)
	  val u_sattr7 = field[String](null)
	}

	//	Represents attributes of users.
	class User_Attributes(val ua_id: Int) extends HObj {
	  def _id = Name.ua(ua_id)
	  val  ua_u_id = field[String](null)
	  val ua_name = field[String](null)
	  val ua_value = field[String](null)
	  val u_created = field[String](null)
	}
	
	//The ITEM id consists of a composite key where the lower 48-bits of the number is the USER.u id and the upper 16-
	//bits is the auction count for that user.
	class Item(val i_id: Int) extends HObj {
	  def _id = Name.i(i_id)
	  val  i_u_id = field[String](null)
  	  val  i_c_id = field[String](null)
	  val i_name = field[String](null)
	  val i_description = field[String](null)
	  val u_user_attributes = field[String](null)
	  val i_initial_price = field[Double](0)
	  val i_current_price = field[Double](0)
	  val i_num_bids = field[Int](0)
	  val i_bids = field[ArrayList[Int]](null) // initialize when needed
	  val i_max_bids = field[ArrayList[Int]](null) // initialize when needed
	  val i_num_images = field[Int](0)
	  val i_num_global_attrs = field[Int](0)
	  val i_start_date = field[String](null) 
	  val i_end_date = field[String](null)
	  val i_status = field[Int](0)
	}
	
	//This table keeps paths to the images of items in the ITEM table.
	class Item_Image(val _ii_i_id: Int, val _ii_u_id: Int) extends HObj {
	  def _id = Name.ii(_ii_i_id, _ii_u_id)
	  val  ii_i_id = field[String](null)
  	  val  ii_u_id = field[String](null)
	  val ii_path = field[String](null)
	}
	
		//This table keeps paths to the images of items in the ITEM table.
	class Item_Comment(val ii_id: Int) extends HObj {
	  def _id = Name.ic(ii_id)
	  val  ii_i_id = field[String](null)
  	  val  ii_u_id = field[String](null)
	  val ii_path = field[String](null)
	}

	//Bids of items in the ITEM table.
	class Item_Bid(val ib_id: Int) extends HObj {
	  def _id = Name.ib(ib_id)
	  val  ib_i_id = field[String](null)
  	  val  ib_u_id = field[String](null)
	  val ib_b_id = field[String](null)
	  val ib_bid = field[Double](0.0)
	  val ib_max_bid = field[Double](0.0)
	  val ib_created = field[String](null)
	  val ib_updated = field[String](null)
	}

	//The maximum bids of each item in the ITEM table. 
	class Item_Max_Bid(val imb_id: Int) extends HObj {
	  def _id = Name.imb(imb_id)
  	  val  imb_u_id = field[String](null)
  	  val  imb_ib_id = field[String](null)
	  val imb_i_id = field[String](null)
	  val imb_ib_u_id = field[String](null)
	  val imb_created = field[String](null)
	  val imb_updated = field[String](null)
	}
	
	class Item_Purchase(val ip_id: Int) extends HObj {
	  def _id = Name.imb(ip_id)
	  val  ip_bid_id = field[String](null)
	  val  ip_i_id = field[String](null)
	  val  ip_u_id = field[String](null)
	  val  ip_date = field[String](null)
	}
	
	