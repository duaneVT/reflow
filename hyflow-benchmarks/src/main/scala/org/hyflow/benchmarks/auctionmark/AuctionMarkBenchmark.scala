package org.hyflow.benchmarks.auctionmark

import org.hyflow.benchmarks._
import com.typesafe.scalalogging.slf4j.Logging
import scala.concurrent.stm._
import org.hyflow.benchmarks.Benchmark

class AuctionMarkBenchmark extends Benchmark with Logging {

  	val name = "auctionmark"
		
	def benchInit() {
  		println("Init AuctionMark")
  		AuctionMarkInit.populateAll();
	}

	def benchIter() {
	  println("Run AuctionMark")
	  AuctionMarkOps.newBid()
	}
	
	def benchCheck() = true

}