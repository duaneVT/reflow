package org.hyflow.benchmarks

import org.hyflow.Hyflow
import org.hyflow.api._
import org.hyflow.core.util.HyflowConfig
import com.typesafe.scalalogging.slf4j.Logging
import akka.actor._
import java.io.{FileWriter, BufferedWriter}
import java.util.Date
import java.security.MessageDigest
import scala.collection.mutable.WrappedArray

object BenchStatsService extends Service with Logging {

	case class AggrStats(thr: Float, commits: Int, aborts: Int, parAborts: Int) extends Message

	val name = "bench-stats"
	val actorProps = Props[BenchStatsService]
	def accepts(message: Any): Boolean = message match {
		case _: AggrStats => true
		case _ => false
	}

	def nodeDone(thr: Float, commits: Int, aborts: Int, parAborts: Int) {
		Hyflow.peers(0) ! AggrStats(thr, commits, aborts, parAborts)
	}

}

class BenchStatsService extends Actor with Logging {

	import BenchStatsService._

	var totalThroughput = 0.0
	var totalCommits = 0
	var totalAborts = 0
	var parAborts = 0
	var count = 0

	def writeToFile(fn: String, s: String) {
		var fw: FileWriter = null
		try {
			fw = new FileWriter(fn, true)
			fw.write(s)
		} finally {
			if (fw != null) fw.close()
		}
	}

	def receive() = {
		case m: AggrStats =>
			totalThroughput += m.thr
			totalCommits += m.commits
			totalAborts += m.aborts
			parAborts += m.parAborts
			count += 1
			if (count >= Hyflow.peers.size) {
				logger.info("All peers completed. Total throughput = {}.", totalThroughput.toString)

				// Parse the arguments of the system for recording.
				val numMachines = HyflowConfig.cfg.getInt("hyflow.nodes")
				val injectorThreads = HyflowConfig.cfg.getInt("hyflow.benchmark.injectorThreads")
				val read = HyflowConfig.cfg.getInt("hyflow.workload.readOnlyRatio")
				val numOps = HyflowConfig.cfg.getInt("hyflow.workload.ops")
				//val numOps = HyflowConfig.cfg.getInt("hyflow.workload.bank.numTransfers")
				val numObj = HyflowConfig.cfg.getInt("hyflow.workload.objects")
				val (protocol, initials) =
				    if (HyflowConfig.cfg.getBoolean("hyflow.motstm.parallelStrict")) ("Parallel Strict", "PS")
				    else if (HyflowConfig.cfg.getBoolean("hyflow.motstm.parallelRelaxed")) ("Parallel Relaxed", "PR")
				    else if (HyflowConfig.cfg.getBoolean("hyflow.motstm.closedNesting")) ("Closed", "C")
				    else ("Open", "O")
                val row = HyflowConfig.cfg.getInt("hyflow.workload.ycsb.recordCount")
                val col = HyflowConfig.cfg.getInt("hyflow.workload.ycsb.fieldCount")
                val numRW = HyflowConfig.cfg.getInt("hyflow.workload.ycsb.numToUse")


                /*
                sb ++= s"Benchmark: ${BenchApp.bench.name}\n"
                sb ++= s"Arguments: ${WrappedArray.make[String](BenchApp.arguments).toList.toString}\n"
                sb ++= s"Ended at: ${new java.util.Date}\n"
                sb ++= s"Throughput: ${totalThroughput}\n"
                sb ++= s"Commits: ${totalCommits}\n"
                sb ++= s"Aborts: ${totalAborts}\n"
                *
                */

				// Create string with the the results.
				val sb = new StringBuilder
				sb ++= s"Benchmark:           ${BenchApp.bench.name.capitalize}\n"

				if (BenchApp.bench.name == "tpcc") {
				    val numWarehouses = HyflowConfig.cfg.getInt("hyflow.nodes") * HyflowConfig.cfg.getInt("hyflow.workload.tpcc.load")
				    /*val numWarehouses = if (numMachines > HyflowConfig.cfg.getInt("hyflow.workload.tpcc.warehouses")) numMachines
                                        else HyflowConfig.cfg.getInt("hyflow.workload.tpcc.warehouses")*/
				    sb ++= s"Warehouse Quantity:  ${numWarehouses}\n"
				    val locality = HyflowConfig.cfg.getInt("hyflow.workload.tpcc.locality")
				    sb ++= s"Locality Skew:       ${locality}\n"
				}
				if (BenchApp.bench.name == "stmbench7") {
				    val locality = HyflowConfig.cfg.getInt("hyflow.workload.stmbench7.locality")
				    sb ++= s"Locality Skew:       ${locality}\n"
				}
				if (BenchApp.bench.name == "ycsb") {
				    val locality = HyflowConfig.cfg.getInt("hyflow.workload.ycsb.locality")
				    sb ++= s"Locality Skew:       ${locality}\n"
				}
				sb ++= s"Number of Machines:  ${numMachines}\n"
				sb ++= s"Threads per Machine: ${injectorThreads}\n"
				if (BenchApp.bench.name != "tpcc") {
    				sb ++= s"Read Percentage (%): ${read}\n"
    				if (BenchApp.bench.name != "stmbench7" && BenchApp.bench.name != "ycsb") {
    				    sb ++= s"Number of Ops:       ${numOps}\n"
    				    sb ++= s"Number of Objects:   ${numObj}\n"
    				}
    				if (BenchApp.bench.name == "ycsb") {
    				    sb ++= s"Number of RC:        ${row} ${col}\n"
    				    sb ++= s"Number of RW:        ${numRW}\n"
    				}
				}
				sb ++= s"Protocol:            ${protocol}\n"
				sb ++= "---------------------------------------------\n"
				sb ++= s"Throughput (txn/s):  ${totalThroughput}\n"
				sb ++= s"Commits:             ${totalCommits}\n"
				sb ++= s"Root Aborts:         ${totalAborts}\n"
				sb ++= s"Parallel Aborts:     ${parAborts}\n"
				val res1 = sb.toString

				// Generate unique hash identifying this result.
				val md = MessageDigest.getInstance("MD5")
				md.update(res1.getBytes)
				val hash = md.digest().map(0xFF & _).map { "%02x".format(_) }.foldLeft("") { _ + _ }
				sb ++= s"MD5 hash: ${hash}\n"
				sb ++= "=============================================\n"

				val resAggr = "results/result-" + HyflowConfig.cfg.getString("hyflow.logging.hostname") + ".txt"
				writeToFile(resAggr, sb.toString)

				// Add to files by settings as well. [Temporary data storage]
				if (BenchApp.bench.name == "bank") {
    				val resSet = s"results/Bank_${initials}_N${numMachines}_O${numObj}_R${read}_Op${numOps}.txt"
    				writeToFile(resSet, sb.toString)
				} else if (BenchApp.bench.name == "tpcc") {
				    val resSet = s"results/TPCC_${initials}_N${numMachines}_T${injectorThreads}.txt"
                    writeToFile(resSet, sb.toString)
				} else if (BenchApp.bench.name == "stmbench7") {
				    val resSet = s"results/STMBench7_${initials}_N${numMachines}_R${read}.txt"
				    writeToFile(resSet, sb.toString)
				} else if (BenchApp.bench.name == "ycsb") {
				    val resSet = s"results/YCSB_${initials}_N${numMachines}_R${read}_Row${row}_Col${col}.txt"
				    writeToFile(resSet, sb.toString)
				}

				writeToFile("var/result.txt", res1)
				writeToFile("var/hash.txt", hash)
				writeToFile("var/testId.txt", HyflowConfig.cfg.getString("hyflow.logging.testId"))
			}
	}
}