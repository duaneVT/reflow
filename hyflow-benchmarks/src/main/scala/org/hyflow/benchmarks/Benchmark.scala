/**
 * General Benchmark Execution
 */

package org.hyflow.benchmarks

import scala.collection.mutable
import org.hyflow.Hyflow
import org.hyflow.core.util.HyflowConfig
import com.typesafe.scalalogging.slf4j.Logging
import scala.util.Random
import com.yammer.metrics.Metrics
import org.hyflow.core.util.TaggedOps
import org.hyflow.core.util.ReInstrumented

abstract class Benchmark extends Logging {

	val rand: Random = new Random(##)

	val name: String
	protected def benchInit(): Unit
	protected def benchIter(): Unit    // Must be re-entrant.

	def benchCheck(): Boolean          // Validation of benchmark (consistency).

	protected def randomObjId: String = "id-" + rand.nextLong

	// Initializing parameters.
	val injectorThreads = HyflowConfig.cfg.getInt("hyflow.benchmark.injectorThreads")
	val testTimeLimit = HyflowConfig.cfg.getInt("hyflow.benchmark.testTime")
	val warmupTimeLimit = HyflowConfig.cfg.getInt("hyflow.benchmark.warmupTime")
	val totalObjects = HyflowConfig.cfg.getInt("hyflow.workload.objects")
	val readOnlyRatio = HyflowConfig.cfg.getDouble("hyflow.workload.readOnlyRatio") * 0.01

	// Random accesses of objects.
	protected def randomlyReadOnly: Boolean = rand.nextFloat() < readOnlyRatio
	protected def randomObjIdSet(count: Int) = randomObjIdSet[String](count, randomObjId _)
	protected def randomObjIdSet[T: Manifest](count: Int, giveRandId: () => T): Array[T] = {
		val idset = mutable.Set[T]()
		while (idset.size < count)
			idset += giveRandId()
		idset.toArray
	}

	// Helps in initializing objects in random order, like in Bank.
	protected def getLocalSlice: Range =
		Range(Hyflow.peers.indexOf(Hyflow.mainActor), totalObjects, Hyflow.peers.size)

	protected def getLocalSliceArg(max: Int): Range =
	    Range(Hyflow.peers.indexOf(Hyflow.mainActor), max, Hyflow.peers.size)

	var warmupWallTimeLimit: Long = 0
	var testWallTimeLimit: Long = 0

	// Actual execution of the benchmark.
	def runBenchmark() {
		// Perform per-bench initialization.
		val beforeInitTime = System.currentTimeMillis
		benchInit()

		// Create injector threads.
		val node = HyflowConfig.cfg.getInt("id")
		val threads = for (i <- 0 until injectorThreads) yield new InjectorThread(node, i)

		// Wait for all nodes to reach this point.
		logger.error("Node {} waiting for barrier.", node.toString)
		Hyflow.barrier("init-done")
		logger.error("Benchmark iteration begin.")

		val afterInitTime = System.currentTimeMillis
		warmupWallTimeLimit = afterInitTime + warmupTimeLimit * 1000
		testWallTimeLimit = warmupWallTimeLimit + testTimeLimit * 1000

		// Start threads.
		threads.foreach(_.start())

		// Wait for threads to complete.
		threads.foreach(_.join())
		val afterFinishedTime = System.currentTimeMillis

		logger.debug("All threads finished.")

		// Gather results.
		val avgtime = 1.0f * threads.map(x => x.doneTime - x.firstTime).sum / injectorThreads
		val totalops = threads.map(_.opsPerformed).sum
		val warmupAborts = threads.map(_.warmupAborts).sum / injectorThreads
		val warmupParAborts = threads.map(_.warmupParAborts).sum / injectorThreads
		val throughput = 1000.0f * totalops / avgtime
		//val timeEachOp = threads.foldLeft(List[Long]())(_ ++ _.timeEachOp)
		logger.trace("injectorThreads = {} | avgtime = {} | totalops = {} | throughput = {}", injectorThreads.toString, avgtime.toString, totalops.toString, throughput.toString)

		// Print statistics.
		logger.trace("Node ops performed. | nodeOps = {}", totalops.toString)
		logger.trace("Node throughput (ops/second). | node-thr = {}", throughput.toString)
		//val times = threads.foldLeft(List[Long]())(_ ++ _.maxTime)
		/*
		val times = mutable.ListBuffer[Long]()
		var x = 0
		for (t <- threads) {
		    times.append(t.maxTime)
		    x += 1
		}
		logger.trace("Max Times ({}) = {} {}", x.toString, times, "")
		*
		*/

		//logger.trace("{}", timeEachOp.sortWith(_ > _))
		//logger.trace("{}", (timeEachOp.sum/timeEachOp.length).toString)

		BenchStatsService.nodeDone(throughput, totalops, Hyflow._topAborts - warmupAborts, Hyflow._parallelAborts - warmupParAborts)

		logger.error("Node sent results.")

		// Consistency check.
		if (Hyflow.peers(0) == Hyflow.mainActor) {
			//benchCheck()       // IMPORTANT: Currently this is commented out for testing because it overflows with Bank.
		}

		Hyflow.done()
	}

	// Application threads to run transactions on each node.
	private class InjectorThread(node: Int, id: Int) extends Thread("bench-n%d-t%d".format(node, id)) with Logging {
		var opsPerformed = 0
		//val timeEachOp = mutable.ListBuffer[Long]()
		//var maxTime = mutable.ListBuffer[Long](0)
		var maxTime: Long = 0
		val rand: Random = new Random(##)
		var firstTime, doneTime: Long = 0
		var warmupCommits: Int = 0
		var warmupAborts: Int = 0
		var warmupParAborts: Int = 0

		def benchStage(warmup: Boolean) {
			val wallTimeLimit = if (warmup) warmupWallTimeLimit else testWallTimeLimit
			var lastTime = System.currentTimeMillis
			firstTime = lastTime
			opsPerformed = 0
			//timeEachOp.clear()
			//maxTime.update(0, 0)
			//maxTime = 0

			while (lastTime < wallTimeLimit) {
				try {
					benchIter()
				} catch {
					case e: Throwable =>
						logger.error("Caught Exception in benchmark iteration.", e)
						//throw e
				}

				// Track throughput (operations over time).
				val newTime = System.currentTimeMillis
				val duration = newTime - lastTime
				//timeEachOp += duration
				//if (duration > maxTime) maxTime = duration
				lastTime = newTime
				opsPerformed += 1
			}

			doneTime = lastTime
		}

		override def run() {
			logger.info(TaggedOps.tagMarker, "Starting Warmup.")
			benchStage(true)
			logger.info(TaggedOps.tagMarker, "Warmup complete. Measuring throughput.")
			//ReInstrumented.reset("test")
			warmupCommits = opsPerformed
			warmupAborts = Hyflow._topAborts
			warmupParAborts = Hyflow._parallelAborts
			benchStage(false)
			logger.info(TaggedOps.tagMarker, "Benchmark done.")
		}
	}
}

