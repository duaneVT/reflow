/**
 * Starting Point of Application
 */

package org.hyflow.benchmarks

import org.hyflow.core.util.HyflowConfig
import org.hyflow.Hyflow
import com.typesafe.scalalogging.slf4j.Logging
import org.hyflow.api._
import org.hyflow.core.directory.Tracker
import java.util.concurrent.TimeUnit
import com.yammer.metrics.reporting.CsvReporter
import java.io.File
import org.hyflow.core.util.ReInstrumented
import org.hyflow.core.util.JVMStats

/**
 *  The initial starting point of the application, after the scripts:
 *
 *  bin/hytest    -> Makes setup and begins the hytest.py script.
 *  bin/hytest.py -> Determines cores, other setups and parsing of arguments, then runs _bench.sh as process.
 *  bin/_bench.sh -> Sets up paths and runs BenchApp as the main program.
 *  java org.hyflow.benchmarks.BenchApp
 */
object BenchApp extends App with Logging
{
	// Initialize Hyflow with arguments from startup script.
	val extra = HyflowConfig.init(args)

	// Class for recording data (time, performance).
	ReInstrumented.report("var/metrics/%s/" + HyflowConfig.cfg.getString("id"), 5, TimeUnit.SECONDS)

	// Actual benchmark (e.g., Bank class).
	var bench: Benchmark = null

	// Let the Master get a head start and setup everything.
	if (HyflowConfig.cfg.getString("id") != "0")
		Thread.sleep(1000)

	// Define the handlers (as set below).
	Hyflow.onInit += onInit
	Hyflow.onReady += onReady
	Hyflow.onShutdown += onShutdown

	// Hard-wire MotSTM DTM implementation.
	scala.concurrent.stm.impl.STMImpl.select(scala.concurrent.stm.motstm.MotSTM)
	HyflowBackendAccess.configure(scala.concurrent.stm.motstm.HyMot)
	HRef.factory = scala.concurrent.stm.motstm.MotSTM

	// Directory.
	Hyflow.dir = Tracker
	Hyflow.registerService(Tracker)

	// Startup the benchmark and track stats. Wait until completion.
	Hyflow.init(args, List(org.hyflow.benchmarks.BenchStatsService))
	JVMStats.init()
	Hyflow.system.awaitTermination()
	logger.info("Hyflow benchmark completed.")

	// On initialization, simply read when benchmark was passed in.
	def onInit() {
		bench = HyflowConfig.cfg.getString("hyflow.workload.benchmark") match {
			case "bank" => new bank.BankBenchmark
			case "counter" => new counter.CounterBenchmark
			case "hashtable" => new hashtable.HashtableBenchmark
			case "skiplist" => new skiplist.SkiplistBenchmark
			case "bst" => new bst.BSTBenchmark
			case "rbt" => new rbt.RBTBenchmark
			case "linkedlist" => new linkedlist.LinkedListBenchmark
			case "tpcc" => new tpcc.TPCCBenchmark
			case "tatp" => new tatp.TATPBenchmark
			case "epinions" => new epinions.EPBenchmark
			case "auctionmark" => new auctionmark.AuctionMarkBenchmark
			case "stmbench7" => new stmbench7.STMBench7_Benchmark
			case "ycsb" => new ycsb.YcsbBenchmark
			case "empty" => new empty.EmptyBenchmark
		}
	}

	// Create the thread that runs the benchmark.
	def onReady() {
		new Thread("bench-master") {
			override def run = {
				try {
					bench.runBenchmark()
				} catch {
					case e: Throwable =>
						logger.error("Caught exception in main benchmark loop.", e)
				}
			}
		}.start()
	}

	// Nothing set for shutdown of Hyflow right now.
	def onShutdown() { }

	// Create an internal copy of the arguments.
	def arguments = args
}
