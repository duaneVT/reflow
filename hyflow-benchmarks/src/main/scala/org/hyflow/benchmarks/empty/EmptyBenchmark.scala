package org.hyflow.benchmarks.empty

import scala.concurrent.stm.atomic
import org.hyflow.benchmarks.Benchmark

class EmptyObjectA {
	val a1 = 0
	val a2 = "hello"
	val a3 = 9.12
	var i1 = 8
	var i2 = "world"
	var i3 = 3.13
}

trait EmptyTraitA {
	val b1 = List(0)
	val b2 = Seq("hello")
	val b3 = Map(9.12 -> 12)
}

class EmptyObject extends EmptyObjectA with EmptyTraitA {
	import scala.collection.mutable
	val c1 = mutable.Set[Int]()
	val c2 = mutable.Map[String, String]()
}

class EmptyBenchmark extends Benchmark  {
	val name = "empty"
	
	def benchInit() = {}
	def benchIter() = atomic { txn =>
		// nothing here
		//val a = 5
		//val b = 7
		//val c = 12
		//val x = a+b+c
		
		//val x = 1 :: 2 :: 3 :: 4 :: Nil
		
		//val x = Range(1, 100).toList
		
		//val x = Range(1, 100).toArray
		
		//val x = new EmptyObject
		
		val x = Range(1, 10).map(i => new EmptyObject).toArray
		x
	}
	def benchCheck(): Boolean = true
}