package org.hyflow.benchmarks.counter

import org.hyflow.api._
import org.hyflow.benchmarks._
import org.hyflow.core.util._
import org.hyflow.Hyflow
import com.typesafe.scalalogging.slf4j.Logging
import scala.concurrent.stm._

class CounterBenchmark extends Benchmark with Logging {
	val name = "counter"
		
	private case class CounterDescr(name: String, pool: Int, ops: Int)

	private val COUNTER_STEPS = HyflowConfig.cfg.getString(
		"hyflow.workload.counter.steps").split(",").toList.map { x =>
			val pool = HyflowConfig.cfg.getInt(s"hyflow.workload.counter._${x}.pool")
			val ops = HyflowConfig.cfg.getInt(s"hyflow.workload.counter._${x}.ops")
			CounterDescr(x, pool, ops)
		}

	def benchInit() {
		val slice = getLocalSlice
		if (slice.contains(0)) {
			for (step <- COUNTER_STEPS) {
				for (i <- 0 to step.pool)
					new Counter(s"ctr_${step.name}_${i}")
			}
		}
	}

	def benchIter() {
		def randId(x: CounterDescr): String = s"ctr_${x.name}_${rand.nextInt(x.pool)}"
		val ids = COUNTER_STEPS.map { x => randomObjIdSet(x.ops, () => randId(x)) }
		if (randomlyReadOnly)
			CounterOps.get(ids)
		else
			CounterOps.inc(ids)
	}

	def benchCheck() = true
}

object CounterOps {

	def inc(ids: List[Array[String]]) {
		atomic { implicit txn =>
			for (step <- ids) {
				for (ctrid <- step) atomic { implicit txn =>
					val ctr = Hyflow.dir.open[Counter](ctrid)
					ctr.v() = ctr.v() + 1
				}
			}
		}
	}

	def get(ids: List[Array[String]]) {
		atomic { implicit txn =>
			for (step <- ids) {
				for (ctrid <- step) atomic { implicit txn =>
					val ctr = Hyflow.dir.open[Counter](ctrid)
					ctr.v()
				}
			}
		}
	}
}

class Counter(val _id: String) extends HObj {
	val v = field(0)
}