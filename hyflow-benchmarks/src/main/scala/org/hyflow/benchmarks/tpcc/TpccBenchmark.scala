package org.hyflow.benchmarks.tpcc

import org.hyflow.api._
import org.hyflow.benchmarks._
import com.typesafe.scalalogging.slf4j.Logging
import scala.concurrent.stm._
import org.hyflow.core.util.HyflowConfig

// According to the general specifications:
// -> W = Warehouses.
// -> 10*W = Districts (D).
// -> 3000*D = Customers per district.
// -> 1 general order per customer at a time.
// -> 100k items per warehouse.
// -> Order-line processing everything (entries generated per order).

class TPCCBenchmark extends Benchmark with Logging {
	val name = "tpcc"
	var sliceW: Range = null
	var sliceI: Range = null

	// Initialization of TPCC by filling the warehouses.
	def benchInit() {
	    val W = HyflowConfig.cfg.getInt("hyflow.workload.tpcc.warehouses")
	    val NODES = HyflowConfig.cfg.getInt("hyflow.nodes")
	    val W_multiple = HyflowConfig.cfg.getInt("hyflow.workload.tpcc.load")
	    //val NUM_WAREHOUSES = if (NODES > W) { NODES } else { W }
	    val NUM_WAREHOUSES = NODES * W_multiple                                // 1 = high contention, 10 = low
	    logger.error("NUM WAREHOUSES = {}", NUM_WAREHOUSES.toString)

	    sliceW = getLocalSliceArg(NUM_WAREHOUSES)
	    sliceI = getLocalSliceArg(HyflowConfig.cfg.getInt("hyflow.workload.tpcc.items"))
	    separate.TpccInit.populateAll(sliceW, sliceI)
	    separate.TpccOps.init(sliceW, sliceI)
	}

	// Each run of the benchmark. Run certain operations with various probabilities:
	// (4%)  Order Status [ 0,   4)
	// (4%)  Delivery     [ 4,   8)
	// (4%)  Stock Level  [ 8,  12)
	// (42%) Payment      [12,  55)
	// (46%) New Order    [55, 100]
	def benchIter() {
		val opt = rand.nextInt(100)
		if (opt < 4) {
			logger.info("Run Transaction: Order Status.")
			separate.TpccOps.orderStatus()
		}
		else if (opt < 8) {
			logger.info("Run Transaction: Delivery.")
			//separate.TpccOps.delivery()
			separate.TpccOps.payment()
		}
		else if (opt < 12) {
			logger.info("Run Transaction: Stock Level.")
			separate.TpccOps.stockLevel()
		}
		else if (opt < 55) {
			logger.info("Run Transaction: Payment.")
			separate.TpccOps.payment()
		}
		else {
			logger.info("Run Transaction: New Order.")
			separate.TpccOps.newOrder()
		}
	}

	def benchCheck() = true
}