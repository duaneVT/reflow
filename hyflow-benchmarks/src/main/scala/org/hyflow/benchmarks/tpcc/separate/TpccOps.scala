package org.hyflow.benchmarks.tpcc.separate

import org.hyflow.core.util.HyflowConfig
import scala.collection.mutable
import scala.concurrent.stm._
import org.hyflow.Hyflow
import com.typesafe.scalalogging.slf4j.Logging

object TpccOps extends Logging {
	val rand = new scala.util.Random
	val C = mutable.Map[Int, Int]()
	var sliceW: Range = null
	var sliceI: Range = null
	//def getNodeID(warehouse: Int): Int = (warehouse - 1) % Hyflow.peers.size
	val LOCAL = HyflowConfig.cfg.getInt("hyflow.workload.tpcc.locality")

	def init(sliceWare: Range, sliceItem: Range) {
	    this.sliceW = sliceWare
	    this.sliceI = sliceItem
	}

	/** New Order Transaction **/
	def newOrder(): Double = {
		class LineOrderData {
			var ol_i_id: Int = _
			var ol_supply_w_id: Int = _
			var ol_quantity: Int = _
			var orderLine: TpccOrderLine = _
		}

		// Randomly pick a warehouse, district, customer.
		val local = (rand.nextInt(100) < LOCAL)
		val w_id = if (local) { sliceW(rand.nextInt(sliceW.size)) + 1 }
		           else { rand.nextInt(TpccInit.NUM_WAREHOUSES) + 1 }

		val d_id = rand.nextInt(TpccInit.NUM_DISTRICTS) + 1
		val c_id = rand.nextInt(TpccInit.NUM_CUSTOMERS_PER_D) + 1

		// Generate the item numbers.
		val ol_cnt = 15 - rand.nextInt(11)
		var picked = 0
		val items = mutable.ArrayBuffer[Int](0)
		while (picked < ol_cnt) {
		    val newItem = rand.nextInt(TpccInit.NUM_ITEMS) + 1
		    if (!items.contains(newItem)) {
		        items += newItem
		        picked += 1
		    }
		}

		try {
			return atomic { implicit txn =>
			    // Set up the order lines.
                //val ol_cnt = 15 - rand.nextInt(11)
                val lines = for (i <- 1 to ol_cnt) yield {
                    val ol = new LineOrderData
                    //ol.ol_i_id = rand.nextInt(TpccInit.NUM_ITEMS) + 1
                    ol.ol_i_id = items(i-1)
                    ol.ol_supply_w_id = w_id
                    ol.ol_quantity = rand.nextInt(10) + 1
                    ol
                }

				// In WAREHOUSE table, retrieve W_TAX.
				val warehouse = Hyflow.dir.open[TpccWarehouse](Name.W(w_id))
				val W_TAX = warehouse.W_TAX()

				// In DISTRICT table, retrieve D_TAX, get and increment D_NEXT_O_ID.
				val district = Hyflow.dir.open[TpccDistrict](Name.D(w_id, d_id))
				val D_TAX = district.D_TAX()
				val o_id = district.D_NEXT_O_ID()
				district.D_NEXT_O_ID() = o_id + 1

				// In CUSTOMER table, retrieve discount, last name, credit status.
				val customer = Hyflow.dir.open[TpccCustomer](Name.C(w_id, d_id, c_id))
				val C_DISCOUNT = customer.C_DISCOUNT()
				val C_LAST = customer.C_LAST()
				val C_CREDIT = customer.C_CREDIT()

				// Create entries in ORDER and NEW-ORDER.
				val order = new TpccOrder(w_id, d_id, o_id)
				order.O_C_ID() = customer.C_ID
				order.O_CARRIER_ID() = null
				order.O_ALL_LOCAL() = !lines.exists(_.ol_supply_w_id != w_id)
				order.O_OL_CNT() = lines.length

				val newOrder = new TpccNewOrder(w_id, d_id, o_id)

				// Set up all order lines.
				for (k <- 1 to lines.length) {
				    atomic { implicit txn =>
				        try {
    				        // Parse order line.
    				        val ol = lines(k - 1)
    				        val item = Hyflow.dir.open[TpccItem](Name.I(ol.ol_i_id))
    				        if (item == null) {
    				            throw new Exception("Null item.")
    				        }

    				        // Get item info.
    				        val I_PRICE = item.I_PRICE()
    				        val I_NAME = item.I_NAME()
    				        val I_DATA = item.I_DATA()

    				        // Get stock info.
    				        val stock = Hyflow.dir.open[TpccStock](Name.S(w_id, ol.ol_i_id))
    				        val S_QUANTITY = stock.S_QUANTITY()
    				        val S_DIST = d_id match {
                                case 1 => stock.S_DIST_01()
                                case 2 => stock.S_DIST_02()
                                case 3 => stock.S_DIST_03()
                                case 4 => stock.S_DIST_04()
                                case 5 => stock.S_DIST_05()
                                case 6 => stock.S_DIST_06()
                                case 7 => stock.S_DIST_07()
                                case 8 => stock.S_DIST_08()
                                case 9 => stock.S_DIST_09()
                                case 10 => stock.S_DIST_10()
                            }
                            val S_DATA = stock.S_DATA()
                            if (S_QUANTITY - 10 > ol.ol_quantity) {
                                stock.S_QUANTITY() = S_QUANTITY - ol.ol_quantity
                            } else {
                                stock.S_QUANTITY() = S_QUANTITY - ol.ol_quantity + 91
                            }
                            stock.S_YTD() = stock.S_YTD() + ol.ol_quantity
                            stock.S_ORDER_CNT() = stock.S_ORDER_CNT() + 1

                            // Set up order line.
                            val orderLine = new TpccOrderLine(w_id, d_id, o_id, k)
                            orderLine.OL_QUANTITY() = ol.ol_quantity
                            orderLine.OL_I_ID() = item.I_ID
                            orderLine.OL_SUPPLY_W_ID() = w_id
                            orderLine.OL_AMOUNT() = ol.ol_quantity * I_PRICE
                            orderLine.OL_DELIVERY_D() = null
                            orderLine.OL_DIST_INFO() = S_DIST

                            ol.orderLine = orderLine
				        } catch {
				            case e: Exception => null
				        }
				    }
				}
				0.0
			}
		} catch {
			case e: Exception => 0.0
		}
	}

	/** Payment Transaction **/
	def payment() = {
		// Randomly select warehouse, district, customer.
	    val local = (rand.nextInt(100) < LOCAL)
        val w_id = if (local) { sliceW(rand.nextInt(sliceW.size)) + 1 }
                   else { rand.nextInt(TpccInit.NUM_WAREHOUSES) + 1 }

		var c_w_id = w_id
		val d_id = rand.nextInt(TpccInit.NUM_DISTRICTS) + 1
		var c_d_id = d_id
		val c_id = rand.nextInt(TpccInit.NUM_CUSTOMERS_PER_D) + 1

		// Generate random amount of payment.
		val h_amount = rand.nextInt(500000) * 0.01

		try {
			atomic { implicit txn =>
				// In WAREHOUSE table, update YTD.
			    atomic { implicit txn =>
    				val warehouse = Hyflow.dir.open[TpccWarehouse](Name.W(w_id))
    				warehouse.W_YTD() += h_amount
				}

				// In DISTRICT table, update YTD.
			    atomic { implicit txn =>
    				val district = Hyflow.dir.open[TpccDistrict](Name.D(w_id, d_id))
    				district.D_YTD() += h_amount
			    }

				// In CUSTOMER table, update their status.
			    atomic { implicit txn =>
    				val customer = Hyflow.dir.open[TpccCustomer](Name.C(w_id, d_id, c_id))
    				customer.C_BALANCE() -= h_amount
    				customer.C_YTD_PAYMENT() += h_amount
    				customer.C_PAYMENT_CNT() += 1
    				if (customer.C_CREDIT() == "BC") {
    					var c_data = customer.C_DATA()
    					c_data = "%s,%s,%s,%s,%s,%s|%s".format(c_id, c_d_id, c_w_id, d_id, w_id, h_amount, c_data)
    					if (c_data.length > 500)
    						c_data = c_data.splitAt(500)._1
    					customer.C_DATA() = c_data
    				}
			    }

				// Create a history entry for this payment.
			    atomic {implicit txn =>
    				val hist = new TpccHistory("history-" + rand.nextInt().toHexString)
    				hist.H_W_ID() = c_w_id
    				hist.H_D_ID() = c_d_id
    				hist.H_C_ID() = c_id
    				hist.H_DATE() = new java.util.Date().toString()
    				hist.H_AMOUNT() = h_amount
    				hist.H_DATA() = "W-" + c_w_id + " D-" + c_d_id
			    }

/*				(warehouse.W_STREET_1(), warehouse.W_STREET_2(), warehouse.W_CITY(),
					warehouse.W_STATE(), warehouse.W_ZIP(), district.D_STREET_1(),
					district.D_STREET_2(), district.D_CITY(), district.D_STATE(),
					district.D_ZIP(), customer.C_FIRST(), customer.C_MIDDLE(),
					customer.C_LAST(), customer.C_CREDIT(), customer.C_CREDIT_LIM(),
					customer.C_DISCOUNT(), customer.C_BALANCE(), customer.C_CREDIT(),
					customer.C_DATA(), hist.H_DATE())
					*
					*/
				0.0
			}
		} catch {
			case e: Exception => null
		}
	}

	/** Order Status Transaction **/
	def orderStatus() = {
		// Randomly select warehouse, district, customer.
	    val local = (rand.nextInt(100) < LOCAL)
        val w_id = if (local) { sliceW(rand.nextInt(sliceW.size)) + 1 }
                   else { rand.nextInt(TpccInit.NUM_WAREHOUSES) + 1 }

		val d_id = rand.nextInt(TpccInit.NUM_DISTRICTS) + 1
		val c_id = rand.nextInt(TpccInit.NUM_CUSTOMERS_PER_D) + 1
		val o_id = rand.nextInt(TpccInit.NUM_ORDERS_PER_D) + 1

		try {
			atomic { implicit txn =>
				// Simply open the customer.
				val customer = Hyflow.dir.open[TpccCustomer](Name.C(w_id, d_id, c_id))

				// Open the order itself.
				val order = Hyflow.dir.open[TpccOrder](Name.O(w_id, d_id, o_id))

				// Read all of the order item amounts.
				for (i <- 1 to order.O_OL_CNT()) {
				    atomic { implicit txn =>
					    val orderItem = Hyflow.dir.open[TpccOrderLine](Name.OL(w_id, d_id, order.O_ID, i))
					    orderItem.OL_AMOUNT()
				    }
				}
			}
		} catch {
			case e: Exception =>
		}
	}

	/** Delivery Transaction **/
	def delivery() = {
	    // Randomly select warehouse.
	    val local = (rand.nextInt(100) < LOCAL)
        val w_id = if (local) { sliceW(rand.nextInt(sliceW.size)) + 1 }
                   else { rand.nextInt(TpccInit.NUM_WAREHOUSES) + 1 }

		val o_carrier_id = rand.nextInt(10) + 1

		// Deliver an order for all of the districts.
		//atomic { implicit txn =>
    		for (d_id <- 1 to TpccInit.NUM_DISTRICTS) {
    			atomic { implicit txn =>
    			    try {
    			        val shipped = Hyflow.dir.open[TpccAuxShipped](Name.shipped(w_id))
    			        val so_id = shipped.AS_O_ID() + 1
    			        //shipped.AS_O_ID() = so_id + TpccInit.NUM_DISTRICTS
    			        shipped.AS_O_ID() = so_id

    			        // Get the proper order ID.
    			        //val o_id = so_id + d_id
    			        val o_id = so_id

        				// Open the order and delete the "new order".
        				val order = Hyflow.dir.open[TpccOrder](Name.O(w_id, d_id, o_id))
        				Hyflow.dir.delete(Name.NO(w_id, d_id, o_id))
        				order.O_CARRIER_ID() = o_carrier_id.toString()

        				// Read all of the order item amounts.
        				var olsum = 0.0
        				val crtdate = new java.util.Date().toString()
        				var i = 1
        				while (i <= order.O_OL_CNT()) {
        				    if (order.O_ID < TpccInit.NUM_ORDERS_PER_D) {
            					val orderItem = Hyflow.dir.open[TpccOrderLine](Name.OL(w_id, d_id, order.O_ID, i))
            					orderItem.OL_DELIVERY_D() = crtdate
            					olsum += orderItem.OL_AMOUNT()
            					i += 1
        				    }
        				}

        				// Update customer balance.
        				val customer = Hyflow.dir.open[TpccCustomer](Name.C(w_id, d_id, order.O_C_ID()))
        				customer.C_BALANCE() += olsum
        				customer.C_DELIVERY_CNT() += 1
    				} catch {
    				    case e: Exception => null
    				}
    			}
    		}
		//}
	}

	/** Stock Level Transaction **/
	def stockLevel() = {
	    // Randomly select a warehouse and district.
	    val local = (rand.nextInt(100) < LOCAL)
        val w_id = if (local) { sliceW(rand.nextInt(sliceW.size)) + 1 }
                   else { rand.nextInt(TpccInit.NUM_WAREHOUSES) + 1 }

		val d_id = rand.nextInt(TpccInit.NUM_DISTRICTS) + 1
		val thresh = rand.nextInt(11) + 10

		// Read the district object.
		atomic { implicit txn =>
		    atomic { implicit txn =>
    		    val warehouse = Hyflow.dir.open[TpccWarehouse](Name.W(w_id))
		    }
		    atomic { implicit txn =>
    		    val district = Hyflow.dir.open[TpccDistrict](Name.D(w_id, d_id))
    		    val d_next_o_id = district.D_NEXT_O_ID()
		    }

		    for (i <- 1 to 20) {
		        atomic { implicit txn =>
		            val o_id = rand.nextInt(TpccInit.NUM_ORDERS_PER_D) + 1
		            val order = Hyflow.dir.open[TpccOrder](Name.O(w_id, d_id, o_id))

		            if (order != null) {
		                for (j <- 1 to order.O_OL_CNT()) {
		                    if (j < TpccInit.NUM_ORDERS_PER_D) {
		                        val ol = Hyflow.dir.open[TpccOrderLine](Name.OL(w_id, d_id, o_id, j))
		                    }
		                }
		            }
		        }
		    }

		    for (k <- 1 to 10) {
		        atomic { implicit txn =>
		            if (k < TpccInit.NUM_ITEMS) {
		                val stock = Hyflow.dir.open[TpccStock](Name.S(w_id, k))
		            }
		        }
		    }
		}
	}
}
