/**
 * Copyright (c) 2010 Yahoo! Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you
 * may not use this file except in compliance with the License. You
 * may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * permissions and limitations under the License. See accompanying
 * LICENSE file.
 */
/*
package org.hyflow.benchmarks.ycsb

import org.hyflow.Hyflow
import org.hyflow.api._
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.stm._
import com.typesafe.scalalogging.slf4j.Logging

import java.util.Properties

import com.yahoo.ycsb._
import com.yahoo.ycsb.generator.CounterGenerator
import com.yahoo.ycsb.generator.DiscreteGenerator
import com.yahoo.ycsb.generator.ExponentialGenerator
import com.yahoo.ycsb.generator.Generator
import com.yahoo.ycsb.generator.ConstantIntegerGenerator
import com.yahoo.ycsb.generator.HotspotIntegerGenerator
import com.yahoo.ycsb.generator.HistogramGenerator
import com.yahoo.ycsb.generator.IntegerGenerator
import com.yahoo.ycsb.generator.ScrambledZipfianGenerator
import com.yahoo.ycsb.generator.SkewedLatestGenerator
import com.yahoo.ycsb.generator.UniformIntegerGenerator
import com.yahoo.ycsb.generator.ZipfianGenerator

import java.io.IOException
import java.nio.ByteBuffer
//import java.util.HashMap
//import java.util.HashSet
//import java.util.Random
//import java.util.Vector

/**
 * The core benchmark scenario. Represents a set of clients doing CRUD operations. The relative proportion of different kinds of
 * operations, and other properties of the workload, are controlled by parameters specified at runtime.
 *
 * Properties to control the client:
 *      fieldCount: the number of fields in a record (Default: 10)
 *
 *      fieldLength: the size of each field (Default: 100)
 *
 *      readAllFields: should readOps read all fields (true) or just one (false) (Default: true)
 *
 *      writeAllFields: should updates and read/modify/writes update all fields (true) or just one (false) (Default: false)
 *
 *      readProportion: what proportion of operations should be reads (Default: 0.95)
 *
 *      updateProportion: what proportion of operations should be updates (Default: 0.05)
 *
 *      insertProportion: what proportion of operations should be inserts (Default: 0)
 *
 *      scanProportion: what proportion of operations should be scans (Default: 0)
 *
 *      readModifyWriteProportion: what proportion of operations should be read a record, modify it, write it back (Default: 0)
 *
 *      requestDistribution: what distribution should be used to select the records to operate on - uniform, zipfian, hotspot, or latest (Default: uniform)
 *
 *      maxScanLength: for scans, what is the maximum number of records to scan (Default: 1000)
 *
 *      scanLengthDistribution: for scans, what distribution should be used to choose the number of records to scan, for each scan, between 1 and maxScanLength (Default: uniform)
 *
 *      insertOrder: should records be inserted in order by key ("ordered"), or in hashed order ("hashed") (Default: hashed)
 */

object Workload extends Logging {
    // The name of the database table to run queries against.
    final val TABLENAME_PROPERTY = "table"

    //The default name of the database table to run queries against.
    final val TABLENAME_PROPERTY_DEFAULT = "usertable"
    var table = "usertable"

    // The name of the property for the number of fields in a record.
    final val  FIELD_COUNT_PROPERTY = "fieldCount"

    // Default number of fields in a record.
    final val FIELD_COUNT_PROPERTY_DEFAULT = "10"
    var fieldCount: Int = 10

    // The name of the property for the field length distribution. Options are "uniform", "zipfian" (favoring short records), "constant", and
    // "histogram". If "uniform", "zipfian" or "constant", the maximum field length will be that specified by the fieldLength property. If
    // "histogram", then the histogram will be read from the filename specified in the "fieldLengthHistogram" property.
    final val FIELD_LENGTH_DISTRIBUTION_PROPERTY = "fieldLengthDistribution"

    // The default field length distribution.
    final val FIELD_LENGTH_DISTRIBUTION_PROPERTY_DEFAULT = "constant"

    // The name of the property for the length of a field in bytes.
    final val FIELD_LENGTH_PROPERTY = "fieldLength"

    // The default maximum length of a field in bytes.
    final val FIELD_LENGTH_PROPERTY_DEFAULT = "100"

    // The name of a property that specifies the filename containing the field length histogram (only used if fieldLengthDistribution is "histogram").
    final val FIELD_LENGTH_HISTOGRAM_FILE_PROPERTY = "fieldLengthHistogram"

    // The default filename containing a field length histogram.
    final val FIELD_LENGTH_HISTOGRAM_FILE_PROPERTY_DEFAULT = "hist.txt"

    // Generator object that produces field lengths. The value of this depends on the properties that start with "FIELD_LENGTH_".
    var fieldLengthGenerator: IntegerGenerator = null.asInstanceOf[IntegerGenerator]

    // The name of the property for deciding whether to read one field (false) or all fields (true) of a record.
    final val READ_ALL_FIELDS_PROPERTY = "readAllFields"

    // The default value for the readAllFields property.
    final val READ_ALL_FIELDS_PROPERTY_DEFAULT = "true"
    var readAllFields: Boolean = true

    // The name of the property for deciding whether to write one field (false) or all fields (true) of a record.
    final val WRITE_ALL_FIELDS_PROPERTY = "writeAllFields"

    // The default value for the writeAllFields property.
    final val WRITE_ALL_FIELDS_PROPERTY_DEFAULT = "false"
    var writeAllFields: Boolean = false

    // The name of the property for the proportion of transactions that are reads.
    final val READ_PROPORTION_PROPERTY = "readProportion"

    // The default proportion of transactions that are reads.
    final val READ_PROPORTION_PROPERTY_DEFAULT = "0.95"

    // The name of the property for the proportion of transactions that are updates.
    final val UPDATE_PROPORTION_PROPERTY = "updateProportion"

    // The default proportion of transactions that are updates.
    final val UPDATE_PROPORTION_PROPERTY_DEFAULT = "0.05"

    // The name of the property for the proportion of transactions that are inserts.
    final val INSERT_PROPORTION_PROPERTY = "insertProportion"

    // The default proportion of transactions that are inserts.
    final val INSERT_PROPORTION_PROPERTY_DEFAULT = "0.0"

    // The name of the property for the proportion of transactions that are scans.
    final val SCAN_PROPORTION_PROPERTY = "scanProportion"

    // The default proportion of transactions that are scans.
    final val SCAN_PROPORTION_PROPERTY_DEFAULT = "0.0"

    // The name of the property for the proportion of transactions that are read-modify-write.
    final val READMODIFYWRITE_PROPORTION_PROPERTY = "readModifyWriteProportion"

    // The default proportion of transactions that are scans.
     final val READMODIFYWRITE_PROPORTION_PROPERTY_DEFAULT = "0.0"

    // The name of the property for the the distribution of requests across the keyspace. Options are "uniform", "zipfian" and "latest".
    final val REQUEST_DISTRIBUTION_PROPERTY = "requestDistribution"

    // The default distribution of requests across the keyspace.
    final val REQUEST_DISTRIBUTION_PROPERTY_DEFAULT = "uniform"

    // The name of the property for the max scan length (number of records).
    final val MAX_SCAN_LENGTH_PROPERTY = "maxScanLength"

    // The default max scan length.
    final val MAX_SCAN_LENGTH_PROPERTY_DEFAULT = "1000"

    // The name of the property for the scan length distribution. Options are "uniform" and "zipfian" (favoring short scans).
    final val SCAN_LENGTH_DISTRIBUTION_PROPERTY = "scanLengthDistribution"

    // The default max scan length.
    final val SCAN_LENGTH_DISTRIBUTION_PROPERTY_DEFAULT = "uniform"

    // The name of the property for the order to insert records. Options are "ordered" or "hashed".
    final val INSERT_ORDER_PROPERTY = "insertOrder"

    // Default insert order.
    final val INSERT_ORDER_PROPERTY_DEFAULT = "hashed"

    // Percentage data items that constitute the hot set.
    final val HOTSPOT_DATA_FRACTION = "hotSpotDataFraction"

    // Default value of the size of the hot set.
    final val HOTSPOT_DATA_FRACTION_DEFAULT = "0.2"

    // Percentage operations that access the hot set.
    final val HOTSPOT_OPN_FRACTION = "hotSpotOpnFraction"

    // Default value of the percentage operations accessing the hot set.
    final val HOTSPOT_OPN_FRACTION_DEFAULT = "0.8"
    final val INSERT_START_PROPERTY = "insertStart"
    final val INSERT_START_PROPERTY_DEFAULT = "0"

    // Default value for the recordCount (number of rows).
    final val RECORD_COUNT_PROPERTY = "recordCount"
    final val RECORD_COUNT_PROPERTY_DEFAULT = "1000"

    var keySequence: IntegerGenerator = null.asInstanceOf[IntegerGenerator]
    var operationChooser: DiscreteGenerator = null.asInstanceOf[DiscreteGenerator]
    var keyChooser: IntegerGenerator = null.asInstanceOf[IntegerGenerator]
    var fieldChooser: Generator = null.asInstanceOf[Generator]
    var transactionInsertKeySequence: CounterGenerator = null.asInstanceOf[CounterGenerator]
    var scanLength: IntegerGenerator = null.asInstanceOf[IntegerGenerator]
    var orderedInserts: Boolean = false
    var recordCount: Int = null.asInstanceOf[Int]
    var operationCount: Int = 0

    def getReadAllFields() = readAllFields
    def getFieldCount() = fieldCount
    def getRecordCount() = recordCount
    def getOpCount() = {
        if(operationCount > 0) {
            operationCount
        }
        else {
            recordCount
        }
    }

    protected def  getFieldLengthGenerator(p: Properties): IntegerGenerator = {
        var fieldLengthGenerator: IntegerGenerator = null.asInstanceOf[IntegerGenerator]
        val fieldLengthDistribution: String = p.getProperty(FIELD_LENGTH_DISTRIBUTION_PROPERTY, FIELD_LENGTH_DISTRIBUTION_PROPERTY_DEFAULT)
        val fieldLength: Int = Integer.parseInt(p.getProperty(FIELD_LENGTH_PROPERTY, FIELD_LENGTH_PROPERTY_DEFAULT))
        val fieldLengthHistogram: String = p.getProperty(FIELD_LENGTH_HISTOGRAM_FILE_PROPERTY, FIELD_LENGTH_HISTOGRAM_FILE_PROPERTY_DEFAULT)

        if (fieldLengthDistribution.compareTo("constant") == 0) {
            fieldLengthGenerator = new ConstantIntegerGenerator(fieldLength)
        }
        else if (fieldLengthDistribution.compareTo("uniform") == 0) {
            fieldLengthGenerator = new UniformIntegerGenerator(1, fieldLength)
        }
        else if (fieldLengthDistribution.compareTo("zipfian") == 0) {
            fieldLengthGenerator = new ZipfianGenerator(1, fieldLength)
        }
        else if (fieldLengthDistribution.compareTo("histogram") == 0) {
            try {
                fieldLengthGenerator = new HistogramGenerator(fieldLengthHistogram)
            } catch {
                case e: IOException =>
                    throw new RuntimeException("Couldn't read field length histogram file: " + fieldLengthHistogram, e)
            }
        }
        else {
            throw new RuntimeException("Unknown field length distribution \"" + fieldLengthDistribution + "\"")
        }

        return fieldLengthGenerator
    }

    /*** Initialize the scenario. Called once, in the main client thread, before any operations are started. ***/
    def init(p: Properties) {
        table = p.getProperty(TABLENAME_PROPERTY, TABLENAME_PROPERTY_DEFAULT)
        fieldCount = Integer.parseInt(p.getProperty(FIELD_COUNT_PROPERTY, FIELD_COUNT_PROPERTY_DEFAULT))
        fieldLengthGenerator = getFieldLengthGenerator(p)
        logger.error("YcsbWorkload: Field Count (col) is {}", fieldCount.toString)


        val readProportion = p.getProperty(READ_PROPORTION_PROPERTY, READ_PROPORTION_PROPERTY_DEFAULT).toDouble
        val updateProportion = p.getProperty(UPDATE_PROPORTION_PROPERTY, UPDATE_PROPORTION_PROPERTY_DEFAULT).toDouble
        val insertProportion = p.getProperty(INSERT_PROPORTION_PROPERTY, INSERT_PROPORTION_PROPERTY_DEFAULT).toDouble
        val scanProportion = p.getProperty(SCAN_PROPORTION_PROPERTY, SCAN_PROPORTION_PROPERTY_DEFAULT).toDouble
        val readModifyWriteProportion = p.getProperty(READMODIFYWRITE_PROPORTION_PROPERTY, READMODIFYWRITE_PROPORTION_PROPERTY_DEFAULT).toDouble

        //recordCount = Integer.parseInt(p.getProperty(Client.RECORD_COUNT_PROPERTY))
        recordCount = Integer.parseInt(p.getProperty(RECORD_COUNT_PROPERTY, RECORD_COUNT_PROPERTY_DEFAULT))
        val requestDistrib = p.getProperty(REQUEST_DISTRIBUTION_PROPERTY, REQUEST_DISTRIBUTION_PROPERTY_DEFAULT)
        val maxScanLength = Integer.parseInt(p.getProperty(MAX_SCAN_LENGTH_PROPERTY, MAX_SCAN_LENGTH_PROPERTY_DEFAULT))
        val scanLengthDistrib = p.getProperty(SCAN_LENGTH_DISTRIBUTION_PROPERTY, SCAN_LENGTH_DISTRIBUTION_PROPERTY_DEFAULT)

        logger.error("YcsbWorkload: Record Count (row) is {}", recordCount.toString)

        val insertStart = Integer.parseInt(p.getProperty(INSERT_START_PROPERTY, INSERT_START_PROPERTY_DEFAULT))

        readAllFields = p.getProperty(READ_ALL_FIELDS_PROPERTY, READ_ALL_FIELDS_PROPERTY_DEFAULT).toBoolean
        writeAllFields = p.getProperty(WRITE_ALL_FIELDS_PROPERTY, WRITE_ALL_FIELDS_PROPERTY_DEFAULT).toBoolean

        if (p.getProperty(INSERT_ORDER_PROPERTY, INSERT_ORDER_PROPERTY_DEFAULT).compareTo("hashed") == 0) {
            orderedInserts = false;
        }
        else if (requestDistrib.compareTo("exponential") == 0) {
            val percentile = p.getProperty(ExponentialGenerator.EXPONENTIAL_PERCENTILE_PROPERTY, ExponentialGenerator.EXPONENTIAL_PERCENTILE_DEFAULT).toDouble
            val frac = p.getProperty(ExponentialGenerator.EXPONENTIAL_FRAC_PROPERTY, ExponentialGenerator.EXPONENTIAL_FRAC_DEFAULT).toDouble
            keyChooser = new ExponentialGenerator(percentile, recordCount * frac)
        }
        else {
            orderedInserts = true
        }

        keySequence = new CounterGenerator(insertStart)
        operationChooser = new DiscreteGenerator()
        if (readProportion > 0) {
            operationChooser.addValue(readProportion, "READ")
        }

        if (updateProportion > 0) {
            operationChooser.addValue(updateProportion, "UPDATE")
        }

        if (insertProportion > 0) {
            operationChooser.addValue(insertProportion, "INSERT")
        }

        if (scanProportion > 0) {
            operationChooser.addValue(scanProportion, "SCAN")
        }

        if (readModifyWriteProportion > 0) {
            operationChooser.addValue(readModifyWriteProportion, "READMODIFYWRITE");
        }

        transactionInsertKeySequence = new CounterGenerator(recordCount)
        if (requestDistrib.compareTo("uniform") == 0) {
            keyChooser = new UniformIntegerGenerator(0, recordCount - 1)
        }
        else if (requestDistrib.compareTo("zipfian") == 0) {
            // It does this by generating a random "next key" in part by taking the modulus over the number of keys.
            // If the number of keys changes, this would shift the modulus, and we don't want that to change which
            // keys are popular so we'll actually construct the scrambled zipfian generator with a keyspace that is
            // larger than exists at the beginning of the test. That is, we'll predict the number of inserts, and
            // tell the scrambled zipfian generator the number of existing keys plus the number of predicted keys
            // as the total keyspace. Then, if the generator picks a key that hasn't been inserted yet, it will
            // just ignore it and pick another key. This way, the size of the keyspace doesn't change from the
            // perspective of the scrambled zipfian generator.

            //val opCount = Integer.parseInt(p.getProperty(Client.OPERATION_COUNT_PROPERTY))
            logger.error("YcsbWorkload: Went into Zipfian to use opCount")
            val opCount = 200
            operationCount = opCount
            val expectedNewKeys = ((opCount.toDouble) * insertProportion * 2.0).toInt   // 2 is fudge factor?

            keyChooser = new ScrambledZipfianGenerator(recordCount + expectedNewKeys)
        }
        else if (requestDistrib.compareTo("latest") == 0) {
            keyChooser = new SkewedLatestGenerator(transactionInsertKeySequence)
        }
        else if (requestDistrib.equals("hotspot")) {
            val hotSetFraction = p.getProperty(HOTSPOT_DATA_FRACTION, HOTSPOT_DATA_FRACTION_DEFAULT).toDouble
            val hotOpnFraction = p.getProperty(HOTSPOT_OPN_FRACTION, HOTSPOT_OPN_FRACTION_DEFAULT).toDouble
            keyChooser = new HotspotIntegerGenerator(0, recordCount - 1, hotSetFraction, hotOpnFraction)
        } else {
            throw new RuntimeException("Unknown request distribution \"" + requestDistrib + "\"")
        }

        fieldChooser = new UniformIntegerGenerator(0, fieldCount - 1)

        if (scanLengthDistrib.compareTo("uniform") == 0) {
            scanLength = new UniformIntegerGenerator(1, maxScanLength)
        }
        else if (scanLengthDistrib.compareTo("zipfian") == 0) {
            scanLength = new ZipfianGenerator(1, maxScanLength)
        }
        else {
            throw new RuntimeException("Distribution \"" + scanLengthDistrib + "\" not allowed for scan length")
        }
    }

    def buildKeyName(keyNum: Long) = {
        if (!orderedInserts) {
            //keyNum = Utils.hash(keyNum)
            Utils.hash(keyNum)
        }
        else {
            "user" + keyNum
        }
    }

    /***
     * Do one transaction operation. Because it will be called concurrently from multiple client
     * threads, this function must be thread safe. However, avoid synchronized, or the threads
     * will block waiting for each other, and it will be difficult to reach the target throughput.
     * Ideally, this function would have no side effects other than DB operations.
    ***/

    final val READ: Byte = 0
    final val UPDATE: Byte = 1
    final val INSERT: Byte = 2
    final val SCAN: Byte = 3
    val DEFAULT_LENGTH = 10

    // ?????????????
    def getTransactionRequest(): Array[Byte] = {
        val op = operationChooser.nextString()
        val request: Array[Byte] = Array.fill(DEFAULT_LENGTH)(0)

        val buffer = ByteBuffer.wrap(request)

        var keyNum = nextKeynum()
        val field = Integer.parseInt(fieldChooser.nextString())

        if (op.compareTo("READ") == 0) {
            buffer.put(READ)
        } else if (op.compareTo("UPDATE") == 0) {
            buffer.put(UPDATE)
        } else if (op.compareTo("INSERT") == 0) {
            keyNum = transactionInsertKeySequence.nextInt()
            buffer.put(INSERT)
        } else if (op.compareTo("SCAN") == 0) {
            buffer.put(SCAN)
        }

        buffer.putInt(keyNum)
        buffer.putInt(field)
        buffer.flip()

        return request
    }

    def nextKeynum(): Int = {
        var keyNum = 0
        if (keyChooser.isInstanceOf[ExponentialGenerator]) {
            do {
                keyNum = transactionInsertKeySequence.lastInt() - keyChooser.nextInt()
            } while (keyNum < 0)
        } else {
            do {
                keyNum = keyChooser.nextInt()
            } while (keyNum > transactionInsertKeySequence.lastInt())
        }
        return keyNum;
    }
}
*/