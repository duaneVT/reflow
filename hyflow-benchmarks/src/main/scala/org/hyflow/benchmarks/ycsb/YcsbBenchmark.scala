package org.hyflow.benchmarks.ycsb

import com.yahoo._
import org.hyflow.Hyflow
import org.hyflow.core.util.HyflowConfig
import org.hyflow.api._
import org.hyflow.benchmarks._
import scala.concurrent.stm._
import com.typesafe.scalalogging.slf4j.Logging

object ID {
    val node = Hyflow.peers.indexOf(Hyflow.mainActor)
    val peerSize = Hyflow.peers.size
    def row(nodeNum: Int, rowNum: Int) = "T" + nodeNum.toString + "_user_" + rowNum.toString
    def col(rowId: String, colNum: Int) = rowId + "_field_" + colNum.toString
}

class YcsbBenchmark extends Benchmark {
    val name = "ycsb"

    val readAllFields: Int = 0
    //val readAllFields: Int = HyflowConfig.cfg.getInt("hyflow.workload.ycsb.readAllFields")
    val fieldCount: Int = HyflowConfig.cfg.getInt("hyflow.workload.ycsb.fieldCount")
    val recordCount: Int = HyflowConfig.cfg.getInt("hyflow.workload.ycsb.recordCount")
    val numToUse: Int = HyflowConfig.cfg.getInt("hyflow.workload.ycsb.numToUse")
    val readPercent: Int = HyflowConfig.cfg.getInt("hyflow.workload.readOnlyRatio")
    val locality: Int = HyflowConfig.cfg.getInt("hyflow.workload.ycsb.locality")

    override val rand = new scala.util.Random(##)

    def benchInit()
    {
        for (row <- 0 to recordCount-1) {
            val row_id = ID.row(ID.node, row)
            // val ycsbRow = new YcsbRow(row, fieldCount)

            for (col <- 0 to fieldCount-1) {
                val ycsbColumn = new YcsbColumn(ID.col(row_id, col), rand.nextInt(150))
            }
        }
    }

    def benchIter()
    {
        val read = rand.nextInt(100) < readPercent
        if (read) {
            executeRead()
        }
        else {
            executeWrite()
        }
    }

    protected def executeRead()
    {
        val row = rand.nextInt(recordCount)
        val row_id = if (rand.nextInt(100) < locality) {
                         ID.row(ID.node, row)
                     } else {
                         ID.row(rand.nextInt(ID.peerSize), row)
                     }

        val readWholeRow = rand.nextInt(100)

        atomic { implicit txn =>
            if (readWholeRow < readAllFields) {
                for (entryNum <- 0 to fieldCount-1) {
                    atomic { implicit txn =>
                        val entry = Hyflow.dir.open[YcsbColumn](ID.col(row_id, entryNum))
                        val value = entry.value()
                    }
                }
            }
            else {
                val cells = collection.mutable.Set[Int]()
                while (cells.size < numToUse*2) {
                    cells += rand.nextInt(fieldCount)
                }

                val cellArray = cells.toArray
                for (entryNum <- 0 to cellArray.size-1 by 2) {
                    atomic { implicit txn =>
                        val entry1 = Hyflow.dir.open[YcsbColumn](ID.col(row_id, cellArray(entryNum)))
                        val value1 = entry1.value()

                        val entry2 = Hyflow.dir.open[YcsbColumn](ID.col(row_id, cellArray(entryNum+1)))
                        val value2 = entry2.value()
                    }
                }
            }
        }
    }

    protected def executeWrite()
    {
        val row = rand.nextInt(recordCount)
        val row_id = if (rand.nextInt(100) < locality) {
                         ID.row(ID.node, row)
                     } else {
                         ID.row(rand.nextInt(ID.peerSize), row)
                     }

        atomic { implicit txn =>
            val cells = collection.mutable.Set[Int]()
            while (cells.size < numToUse*2) {
                cells += rand.nextInt(fieldCount)
            }

            val cellArray = cells.toArray
            for (entryNum <- 0 to cellArray.size-1 by 2) {
                atomic { implicit txn =>
                    val entry1 = Hyflow.dir.open[YcsbColumn](ID.col(row_id, cellArray(entryNum)))
                    entry1.value()
                    entry1.value() = rand.nextInt(1000)

                    val entry2 = Hyflow.dir.open[YcsbColumn](ID.col(row_id, cellArray(entryNum+1)))
                    entry2.value()
                    entry2.value() = rand.nextInt(1000)
                }
            }
        }
    }

    def benchCheck(): Boolean = true
}
