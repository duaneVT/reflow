package org.hyflow.benchmarks.ycsb

import org.hyflow.Hyflow
import org.hyflow.api._
import org.hyflow.benchmarks._
import scala.concurrent.stm._
import com.typesafe.scalalogging.slf4j.Logging

//import stm.transaction.AbstractObject;

class YcsbRow(val rowNum: Int, val initFieldCount: Int) extends HObj {
    override def _id = ID.row(ID.node, rowNum)
    val fieldCount = field[Int](initFieldCount)
}

class YcsbColumn(val colID: String, val initValue: Int) extends HObj with Logging {
    override def _id = colID
    val value = field[Int](initValue)
}