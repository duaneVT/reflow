package org.hyflow.benchmarks.tatp

import scala.collection.mutable
import java.util.Date
import com.typesafe.scalalogging.slf4j.Logging
import scala.concurrent.stm._
import org.hyflow.Hyflow

object TatpOps extends Logging {
	val rand = new scala.util.Random

	def get_subscriber_data() = {
    val s_id = rand.nextInt(TatpInit.NUM_SUBSCRIBER) + 1
    try {
      atomic { implicit txn =>
        val subscriber = Hyflow.dir.open[TatpSubscriber](Name.S(s_id))
        //(s_id, subscriber.sub_nbr(),  subscriber.bit_1(), subscriber.bit_2(), subscriber.bit_3(),
         // subscriber.bit_4(), subscriber.bit_5(), subscriber.bit_6(), subscriber.bit_7(), subscriber.bit_8(),  subscriber.bit_9(),
         // subscriber.bit_10(),  subscriber.hex_1(), subscriber.hex_2(), subscriber.hex_3(), subscriber.hex_4(), subscriber.hex_5(),
         // subscriber.hex_6(), subscriber. hex_7(), subscriber.hex_8(), subscriber.hex_9(), subscriber.hex_10(), subscriber.byte2_1(),
         // subscriber.byte2_2(), subscriber.byte2_3(), subscriber.byte2_4(), subscriber.byte2_5(), subscriber.byte2_6(), subscriber.byte2_7(),
         // subscriber.byte2_8(), subscriber.byte2_9(), subscriber.byte2_10(), subscriber.msc_location(), subscriber.vlr_location())
      }
    } catch {
      case e: Exception => null
    }
	}
  def get_new_destination() = {
    try {
      atomic { implicit txn =>

        val s_id = rand.nextInt(TatpInit.NUM_SUBSCRIBER) + 1
        val s_type = rand.nextInt(4) + 1
        val special_facility = Hyflow.dir.open[TatpSpecial_Facility](Name.SF(s_id, s_type))
        if (special_facility != null && special_facility.is_active() == 1){
          val my_time = rand.nextInt(17)
          var s_time = 0
          if (my_time < 7) s_time = 6
          else if (my_time < 9) s_time = 8
          else s_time = 16
          val call_forwarding = Hyflow.dir.open[TatpCall_Forwarding](Name.CF(s_id, s_type, s_time))
          call_forwarding.numberx()
        }
        else null
        //numberx
      }
    } catch {
      case e: Exception => null
    }
  }

  def get_access_data() = {
    try {
      atomic { implicit txn =>
          val s_id = rand.nextInt(TatpInit.NUM_SUBSCRIBER) + 1
          val i_type =  rand.nextInt(4) + 1
          val access_info = Hyflow.dir.open[TatpAccess_info](Name.AI(s_id))
          if (access_info != null && access_info.ai_type() == i_type ){
            (access_info.data_1(),access_info.data_2(),access_info.data_3(),access_info.data_4())
          }
          else null
      }
    } catch {
      case e: Exception =>  null
    }
  }
  def update_subscriber_data() = {
    var s_id = rand.nextInt(TatpInit.NUM_SUBSCRIBER) + 1
    try {
      atomic { implicit txn =>
          val subscriber = Hyflow.dir.open[TatpSubscriber](Name.S(s_id))
          subscriber.bit_1() = rand.nextInt(2)
          val s_type = rand.nextInt(4) + 1
          val special_facility = Hyflow.dir.open[TatpSpecial_Facility](Name.SF(s_id, s_type))
          if (special_facility != null) {
            special_facility.data_a() = rand.nextInt(255)
            true
          }
          else false

      }
    } catch {
      case e: Exception => false
    }
  }
  def update_location() = {
    val s_id = rand.nextInt(TatpInit.NUM_SUBSCRIBER) + 1
    try {
      atomic { implicit txn =>
        val subscriber = Hyflow.dir.open[TatpSubscriber](Name.S(s_id))
        subscriber.vlr_location() = rand.nextLong()%TatpInit.LOCATION  + 1
      }
   } catch {
      case e: Exception => null
    }
  }

  def insert_call_forwarding(): Boolean = {
    try {
      return atomic { implicit txn =>
        var loop: Boolean = true
        var s_type: Int = 0
        val slist = List(0,8,16);
        val s_id = rand.nextInt(TatpInit.NUM_SUBSCRIBER) + 1
        val subscriber = Hyflow.dir.open[TatpSubscriber](Name.S(s_id))
        for (s_type <- 1 to 4){
          val special_facility = Hyflow.dir.open[TatpSpecial_Facility](Name.SF(s_id, s_type))
          if (special_facility != null){
            for (s_time <- slist){
              val call_forwarding = Hyflow.dir.open[TatpCall_Forwarding](Name.CF(s_id, s_type, s_time))
              if (call_forwarding == null){
                val my_call_forwarding = new TatpCall_Forwarding(s_id, s_type, s_time)
                my_call_forwarding.end_time() = s_time + rand.nextInt(24) + 1  // minutes: min value is 1
                my_call_forwarding.numberx() = rand.nextString(15)
                true
              }
            }
          }
        }
        false
      }
    } catch {
      case e: Exception => false
    }
  }
  def delete_call_forwarding(): Boolean = {

    try {
      return atomic { implicit txn =>
        var loop: Boolean = true
        var s_type: Int = 0
        val slist = List(0,8,16);
        val s_id = rand.nextInt(TatpInit.NUM_SUBSCRIBER) + 1
        val subscriber = Hyflow.dir.open[TatpSubscriber](Name.S(s_id))
        for (s_type <- 1 to 4){
          for (s_time <- slist){
            val call_forwarding = Hyflow.dir.open[TatpCall_Forwarding](Name.CF(s_id, s_type, s_time))
            if (call_forwarding != null){
              Hyflow.dir.delete(Name.CF(s_id, s_type, s_time))
              true
            }
          }
        }
        false
      }
    } catch {
      case e: Exception => false
    }
  }

}
