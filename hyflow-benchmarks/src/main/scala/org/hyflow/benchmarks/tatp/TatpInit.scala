package org.hyflow.benchmarks.tatp

import scala.util.Random
import scala.concurrent.stm._
import com.typesafe.scalalogging.slf4j.Logging
import org.hyflow.core.util.HyflowConfig

object TatpInit extends Logging {

	// subscribers
	//val NUM_SUBSCRIBER = 10
  val NUM_SUBSCRIBER = HyflowConfig.cfg.getInt("hyflow.workload.tatp.subscriber")
  val LOCATION = 4294967295L
	// Utils
	private val rand = new Random

  def idString(x: Int) = {
    val digits: Int = 15
    String.format("%"+digits+"s", x.toString).replace(' ','0')
  }
  def genActive()={
    val active = rand.nextInt(100) + 1
    var res: Int = 0
    if (active < 86) res = 1
    res
  }

  private def Subscriber(s_id: Int) = atomic { implicit txn =>
    val subscriber = new TatpSubscriber(s_id)
    subscriber.sub_nbr() =  idString(s_id)
    subscriber.bit_1() = rand.nextInt(2)
    subscriber.bit_2() = rand.nextInt(2)
    subscriber.bit_3() = rand.nextInt(2)
    subscriber.bit_4() = rand.nextInt(2)
    subscriber.bit_5() = rand.nextInt(2)
    subscriber.bit_6() = rand.nextInt(2)
    subscriber.bit_7() = rand.nextInt(2)
    subscriber.bit_8() = rand.nextInt(2)
    subscriber.bit_9() = rand.nextInt(2)
    subscriber.bit_10() = rand.nextInt(2)
    subscriber.hex_1() = rand.nextInt(16)
    subscriber.hex_2() = rand.nextInt(16)
    subscriber.hex_3() = rand.nextInt(16)
    subscriber.hex_4() = rand.nextInt(16)
    subscriber.hex_5() = rand.nextInt(16)
    subscriber.hex_6() = rand.nextInt(16)
    subscriber.hex_7() = rand.nextInt(16)
    subscriber.hex_8() = rand.nextInt(16)
    subscriber.hex_9() = rand.nextInt(16)
    subscriber.hex_10() = rand.nextInt(16)
    subscriber.byte2_1() = rand.nextInt(256)
    subscriber.byte2_2() = rand.nextInt(256)
    subscriber.byte2_3() = rand.nextInt(256)
    subscriber.byte2_4() = rand.nextInt(256)
    subscriber.byte2_5() = rand.nextInt(256)
    subscriber.byte2_6() = rand.nextInt(256)
    subscriber.byte2_7() = rand.nextInt(256)
    subscriber.byte2_8() = rand.nextInt(256)
    subscriber.byte2_9() = rand.nextInt(256)
    subscriber.byte2_10() = rand.nextInt(256)
    subscriber.msc_location() = rand.nextLong()%LOCATION  + 1
    subscriber.vlr_location() = rand.nextLong()%LOCATION  + 1

    //logger.debug("Creating subscriber with id: {}", subscriber._id)
  }

  private def Access_info(s_id: Int) = atomic { implicit txn =>
    val access_info = new TatpAccess_info(s_id)
    access_info.ai_type() = rand.nextInt(4) + 1
    access_info.data_1() = rand.nextInt(256)
    access_info.data_2() = rand.nextInt(256)
    access_info.data_3() = rand.nextPrintableChar()
    access_info.data_4() = rand.nextPrintableChar()
  }

  private def Special_Facility(s_id: Int, s_Type: Int) = atomic { implicit txn =>
    val special_facility = new TatpSpecial_Facility(s_id, s_Type)
    special_facility.is_active() = genActive()
    special_facility.error_cntrl() = rand.nextInt(256)
    special_facility.data_a() = rand.nextInt(256)
    special_facility.data_b() = rand.nextString(5)
  }

  private def Call_Forwarding(s_id: Int, s_Type: Int, s_time: Int) = atomic { implicit txn =>
    val call_forwarding = new TatpCall_Forwarding(s_id, s_Type, s_time)
    call_forwarding.end_time() = s_time +  rand.nextInt(9) + 1 // minutes: min value is 1
    call_forwarding.numberx() = rand.nextString(15)
  }

  // Main function
	def populateAll() {
    val slist = List(0,8,16);   // we consider only these starting times.
    for (s_id <- 1 to NUM_SUBSCRIBER) {
      Subscriber(s_id)
      Access_info(s_id)
      val s_type = rand.nextInt(4) + 1
      Special_Facility(s_id, s_type)
      //val start_time = slist(rand.nextInt(3)) // 24 hours-based
      Call_Forwarding(s_id, s_type, slist(rand.nextInt(3)))
    }
  }
}