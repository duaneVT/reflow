package org.hyflow.benchmarks.tatp

import org.hyflow.api._
import org.hyflow.benchmarks._
import com.typesafe.scalalogging.slf4j.Logging
import scala.concurrent.stm._

class TATPBenchmark extends Benchmark with Logging {
	val name = "tatp"
		
	def benchInit() {
    val slice = getLocalSlice
    if (slice.contains(0)) {
      // Initialize data
      TatpInit.populateAll()
    }
	}

	def benchIter() {
		val opt = rand.nextInt(100)
		if (opt < 35) {
			logger.info("Run Transaction: subscriber data")
			TatpOps.get_subscriber_data()
		} else if (opt < 45) {
			logger.info("Run Transaction: new destination")
			TatpOps.get_new_destination()
		} else if (opt < 80) {
			logger.info("Run Transaction: access data")
			TatpOps.get_access_data()
		} else if (opt < 82) {
			logger.info("Run Transaction: subsriber data")
			TatpOps.update_subscriber_data()
		} else if (opt < 96){
			logger.info("Run Transaction: location")
			TatpOps.update_location()
		} else if (opt < 98) {
      logger.info("Run Transaction: insert call forwarding")
      TatpOps.insert_call_forwarding()
    } else{
      logger.info("Run Transaction: delete call forwarding")
      TatpOps.delete_call_forwarding()
    }
	}
	
	def benchCheck() = true
}