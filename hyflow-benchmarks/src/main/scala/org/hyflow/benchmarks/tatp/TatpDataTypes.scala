package org.hyflow.benchmarks.tatp

import org.hyflow.api._

import java.util.Date

object Name {
	def S(s_id: Int) =
		"subscriber-%d".format(s_id)
  def AI(s_id: Int) =
    "access_info-%d".format(s_id)
  def SF(s_id: Int, s_type: Int) =
    "special_facility-%d-%d".format(s_id, s_type)
  def CF(s_id: Int, s_type: Int, s_time: Int) =
    "call_forwarding-%d-%d-%d".format(s_id, s_type, s_time)
}

class TatpSubscriber(val S_ID: Int) extends HObj {
	def _id = Name.S(S_ID)
	val sub_nbr = field[String](null)
	val bit_1 = field[Int](0)
	val bit_2 = field[Int](0)
  val bit_3 = field[Int](0)
  val bit_4 = field[Int](0)
  val bit_5 = field[Int](0)
  val bit_6 = field[Int](0)
  val bit_7 = field[Int](0)
  val bit_8 = field[Int](0)
  val bit_9 = field[Int](0)
  val bit_10 = field[Int](0)
  val hex_1 = field[Int](0)
  val hex_2 = field[Int](0)
  val hex_3 = field[Int](0)
  val hex_4 = field[Int](0)
  val hex_5 = field[Int](0)
  val hex_6 = field[Int](0)
  val hex_7 = field[Int](0)
  val hex_8 = field[Int](0)
  val hex_9 = field[Int](0)
  val hex_10 = field[Int](0)
  val byte2_1 = field[Int](0)
  val byte2_2 = field[Int](0)
  val byte2_3 = field[Int](0)
  val byte2_4 = field[Int](0)
  val byte2_5 = field[Int](0)
  val byte2_6 = field[Int](0)
  val byte2_7 = field[Int](0)
  val byte2_8 = field[Int](0)
  val byte2_9 = field[Int](0)
  val byte2_10 = field[Int](0)
  val msc_location = field[Long](0)
	val vlr_location = field[Long](0)
}

class TatpAccess_info(val S_ID: Int) extends HObj {
  def _id = Name.AI(S_ID)
	val ai_type = field[Int](0)
	val data_1 = field[Int](0)
	val data_2 = field[Int](0)
	val data_3 = field[Char](' ')
	val data_4 = field[Char](' ')
}

class TatpSpecial_Facility(val S_ID: Int, val s_type: Int) extends HObj {
	def _id = Name.SF(S_ID, s_type)
	val sf_type = s_type
	val is_active = field[Int](0)
	val error_cntrl = field[Int](0)
	val data_a = field[Int](0)
	val data_b = field[String](null)
}

class TatpCall_Forwarding(val S_ID: Int, val s_Type: Int, val s_time: Int) extends HObj {
  def _id = Name.CF(S_ID, s_Type, s_time)
  val end_time = field[Int](0)
  val numberx = field[String](null)
}



