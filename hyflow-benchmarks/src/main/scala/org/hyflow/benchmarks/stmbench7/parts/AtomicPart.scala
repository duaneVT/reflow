package org.hyflow.benchmarks.stmbench7.parts

import org.hyflow._
import org.hyflow.benchmarks.stmbench7.{ID}
import org.hyflow.benchmarks.stmbench7.backend._
import scala.concurrent.stm._

class AtomicPart(val id: Int, val varType: String, val initBuildDate: Int, val initX: Int, val initY: Int, val node: Int) extends DesignObj with Comparable[AtomicPart] {
    override def _id = ID.AtomicPart(id, node)

    protected val buildDate = field[Int](null.asInstanceOf[Int])                      // Int
    private val x = field[Int](null.asInstanceOf[Int])                                // Int
    private val y = field[Int](null.asInstanceOf[Int])                                // Int
    protected val toConnections_ID = field[String](ID.SmallSet(_id, 0, "c", node))    // SmallSet[Connection]
    protected val fromConnections_ID = field[String](ID.SmallSet(_id, 1, "c", node))  // SmallSet[Connection]
    private val partOf_ID = field[String](null.asInstanceOf[String])                  // CompositePart

    def getX()(implicit txn: InTxn): Int = x()
    def getY()(implicit txn: InTxn): Int = y()
    def getBuildDate()(implicit txn: InTxn): Int = {
        buildDate()
    }
    def setBuildDate(value: Int)(implicit txn: InTxn) {
        buildDate() = value
    }
    def getNumToConnections()(implicit txn: InTxn): Int = {
        Hyflow.dir.open[SmallSet[Connection]](toConnections_ID()).size
    }
    def getToConnections()(implicit txn: InTxn): ImmutableCollection[Connection] = {
        Hyflow.dir.open[SmallSet[Connection]](toConnections_ID()).immutableView()
    }
    def getFromConnections()(implicit txn: InTxn): ImmutableCollection[Connection] = {
        Hyflow.dir.open[SmallSet[Connection]](fromConnections_ID()).immutableView()
    }
    def getPartOf()(implicit txn: InTxn): CompositePart = {
        Hyflow.dir.open[CompositePart](partOf_ID())
    }

    def init()(implicit txn: InTxn) {
        buildDate() = initBuildDate
        x() = initX
        y() = initY

        val newTo = new SmallSet[Connection](_id, 0, "c", node)
        newTo.initBuffer()
        toConnections_ID() = newTo._id

        val newFrom = new SmallSet[Connection](_id, 1, "c", node)
        newFrom.initBuffer()
        fromConnections_ID() = newFrom._id
    }

    def swapXY()(implicit txn: InTxn) {
        val tmp = y()
        y() = x()
        x() = tmp
    }

    def clearPointers()(implicit txn: InTxn) {
        x() = 0
        y() = 0
        Hyflow.dir.open[SmallSet[Connection]](toConnections_ID()).clear()
        Hyflow.dir.open[SmallSet[Connection]](fromConnections_ID()).clear()
        partOf_ID() = null
    }

	def connectTo(destination: AtomicPart, varType: String, length: Int)(implicit txn: InTxn) {
        val connection = new Connection(this, destination, varType, length)
        Hyflow.dir.open[SmallSet[Connection]](toConnections_ID()).add(connection)

        val reverseConnection = new Connection(destination, this, varType, length)
        destination.addConnectionFromOtherPart(reverseConnection)
    }

	def addConnectionFromOtherPart(connection: Connection)(implicit txn: InTxn) {
	    Hyflow.dir.open[SmallSet[Connection]](fromConnections_ID()).add(connection)
    }

	def setCompositePart(partOf: CompositePart)(implicit txn: InTxn) {
	    this.partOf_ID() = partOf._id
    }

	override def compareTo(part: AtomicPart) = (id - part.getId())

	override def equals(obj: scala.Any): Boolean = {
        if (!obj.isInstanceOf[AtomicPart]) {
            return false
        }
        return super.equals(obj)
    }

	def clone()(implicit txn: InTxn): AtomicPart = {
        val clone = super.clone().asInstanceOf[AtomicPart]
        clone.fromConnections_ID() = fromConnections_ID()
        clone.toConnections_ID() = toConnections_ID()

        return clone
    }

	override def toString()(implicit txn: InTxn) = {
	    (super.toString() + ", buildDate=" + buildDate() + ", x=" + x() + ", y=" + y() + ", partOf=[" + partOf_ID() + "]" +
	     ", to={ " + Hyflow.dir.open[SmallSet[Connection]](toConnections_ID()).size + " connections }, from={ " +
	     Hyflow.dir.open[SmallSet[Connection]](fromConnections_ID()).size + " connections }")
	}
}