package org.hyflow.benchmarks.stmbench7.backend

import org.hyflow.benchmarks.stmbench7.{ID}
import org.hyflow.benchmarks.stmbench7.parts._

import scala.collection.mutable.TreeSet
import scala.concurrent.stm._

class LargeSet[E <: Comparable[E]](val owner: String, val id: Int, val varType: String, val node: Int) extends DesignObj {
    override def _id: String = ID.LargeSet(owner, id, varType, node)

    private val buffer = field[TreeSet[E]](null.asInstanceOf[TreeSet[E]])
    def initBuffer()(implicit txn: InTxn) {
        buffer() = new TreeSet[E]()
    }
    def bufferList()(implicit txn: InTxn): List[E] = buffer().toList

    def add(elem: E)(implicit txn: InTxn): Boolean = {
        if (buffer().contains(elem)) {
            return false
        }
        buffer() += elem
        return true
    }

    def remove(elem: E)(implicit txn: InTxn): Boolean = {
        if (!buffer().contains(elem)) {
            return false
        }
        buffer() -= elem
        return true
    }

    def size()(implicit txn: InTxn) = {
        buffer().size
    }

    override def toString() = _id
}