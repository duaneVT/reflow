package org.hyflow.benchmarks.stmbench7.operations

import org.hyflow.benchmarks.stmbench7.{BaseOperation, Operations}
import org.hyflow.benchmarks.stmbench7.parts._
import scala.concurrent.stm._

// Write. Can be parallel.
class Operation12(oo7setup: Setup) extends Operation6(oo7setup) {
    override protected def performOperationInComplexAssembly(assembly: ComplexAssembly)(implicit txn: InTxn) {
        //assembly.updateBuildDate()
        val buildDate = assembly.getBuildDate()
        if (buildDate % 2 == 0) {
            assembly.setBuildDate(buildDate - 1)
        }
        else {
            assembly.setBuildDate(buildDate + 1)
        }
    }

    override def getOperationId(): Operations.Op = Operations.OP12
}