package org.hyflow.benchmarks.stmbench7.operations

import org.hyflow.benchmarks.stmbench7.{BaseOperation, Operations}
import org.hyflow.benchmarks.stmbench7.parts._
import scala.concurrent.stm._

// Read-only. Single-threaded.
class Traversal9(oo7setup: Setup) extends Traversal8(oo7setup) {
    override protected def traverse(manual: Manual)(implicit txn: InTxn): Int = {
        return manual.checkFirstLastCharTheSame()
    }

    override def getOperationId(): Operations.Op = Operations.OP5
}