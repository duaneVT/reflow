package org.hyflow.benchmarks.stmbench7.parts

import org.hyflow._
import org.hyflow.benchmarks.stmbench7.{ID}

import scala.concurrent.stm._

class Manual(val id: Int, val initTitle: String, val initText: String, val node: Int, val varType: String = null) extends DesignObj {
    override def _id: String  = ID.Manual(id, node)

    private val title = field[String](null.asInstanceOf[String])        // String
    private val text = field[String](null.asInstanceOf[String])         // String
    private val module_ID = field[String](null.asInstanceOf[String])    // Module

    def init()(implicit txn: InTxn) {
        title() = initTitle
        text() = initText
    }

    def getModule()(implicit txn: InTxn): Module = {
        Hyflow.dir.open[Module](module_ID())
    }
    def getText()(implicit txn: InTxn): String = {
        text()
    }
    def getTitle()(implicit txn: InTxn): String = {
        title()
    }

    def setModule(module: Module)(implicit txn: InTxn) {
        this.module_ID() = module._id
    }

    def countOccurrences(ch: Char)(implicit txn: InTxn): Int = {
        var position = 0
        var count = 0
        var newPosition = -1
        val textLen = text().length()
        var stayInLoop = true

        do {
            newPosition = text().indexOf(ch, position)
            if (newPosition == -1) {
                stayInLoop = false
            }
            else {
                position = newPosition + 1
                count += 1
            }
        } while (position < textLen && stayInLoop)

        return count
    }

    def checkFirstLastCharTheSame()(implicit txn: InTxn): Int = {
        if (text().charAt(0) == text().charAt(text().length() - 1)) {
            return 1
        }
        return 0
    }

    def startsWith(ch: Char)(implicit txn: InTxn) = (text().charAt(0) == ch)

    def replaceChar(from: Char, to: Char)(implicit txn: InTxn): Int = {
        text() = text().replace(from, to)
        return countOccurrences(to)
    }

    override def equals(obj: scala.Any): Boolean = {
        if (!obj.isInstanceOf[Manual]) {
            return false
        }
        return (obj.asInstanceOf[Manual].getId() == id)
    }

    override def hashCode() = id

    override def clone(): Object = {
        try {
            return super.clone()
        }
        catch {
            case x: Throwable =>
                throw new RuntimeException(x)
        }
    }

    override def toString()(implicit txn: InTxn): String = {
            return getClass().getName() + ": id=" + id + ", title=" + title + ", text=" +
                   text().substring(0, 10) + " (...) " + text().substring(text().length() - 10, text().length())
    }
}