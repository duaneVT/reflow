package org.hyflow.benchmarks.stmbench7.parts

import org.hyflow._
import org.hyflow.benchmarks.stmbench7.{ID, Parameters}
import org.hyflow.benchmarks.stmbench7.backend._
import scala.concurrent.stm._

class ComplexAssembly(override val id: Int, override val varType: String, override val initBuildDate: Int, override val initModule: Module,
                      override val initSuperAssembly: ComplexAssembly, override val node: Int) extends Assembly(id, varType, initBuildDate, initModule, initSuperAssembly, node) {
    override def _id = ID.ComplexAssembly(id, node)

    private val subAssemblySet_ID = field[String](ID.SmallSet(_id, 0, "ai", node))  // SmallSet[Assembly_ID] -> Assembly
    private val level = field[Short](-1)                                            // Short

    def getLevel()(implicit txn: InTxn): Short = level()
    def getSubAssembly_IDs()(implicit txn: InTxn): ImmutableCollection[String] = {
        Hyflow.dir.open[SmallSet[String]](subAssemblySet_ID()).immutableView()
    }

    override def clearPointers()(implicit txn: InTxn) {
        super.clearPointers()
        Hyflow.dir.open[SmallSet[String]](subAssemblySet_ID()).clear()
        level() = -1
    }

    override def init()(implicit txn: InTxn) {
        super.init()
        val newSubAssemblies = new SmallSet[String](_id, 0, "ai", node)
        newSubAssemblies.initBuffer()
        subAssemblySet_ID() = newSubAssemblies._id
        if (initSuperAssembly == null) {
            this.level() = Parameters.NumAssmLevels.asInstanceOf[Short]
        }
        else {
            superAssembly_ID() = initSuperAssembly._id
            this.level() = (initSuperAssembly.getLevel() - 1).asInstanceOf[Short]
        }
    }

	def addSubAssembly(assembly: Assembly)(implicit txn: InTxn): Boolean = {
        if (assembly.isInstanceOf[BaseAssembly] && level() != 2) {
            throw new RuntimeException("ComplexAssembly.addSubAssembly: BaseAssembly at wrong level!")
        }
        val notAddedBefore = Hyflow.dir.open[SmallSet[String]](subAssemblySet_ID()).add(assembly._id)
        return notAddedBefore
    }

	def removeSubAssembly(assembly: Assembly)(implicit txn: InTxn): Boolean = {
	    Hyflow.dir.open[SmallSet[String]](subAssemblySet_ID()).remove(assembly._id)
	}

	override def equals(obj: scala.Any): Boolean = {
        if (!obj.isInstanceOf[ComplexAssembly]) {
            return false
        }
        return super.equals(obj)
    }

    def clone()(implicit txn: InTxn): ComplexAssembly = {
        val clone = super.clone().asInstanceOf[ComplexAssembly]
        clone.subAssemblySet_ID() = subAssemblySet_ID()

        return clone
    }

	override def toString()(implicit txn: InTxn): String = {
        var subAssmString = "{ "
        val subAssmSet = Hyflow.dir.open[SmallSet[String]](subAssemblySet_ID())
        for (subAssembly_ID <- subAssmSet.immutableView()) {
            subAssmString += subAssembly_ID + " "
        }
        subAssmString += "}"
        return super.toString() + ", buildDate=" + buildDate() + ", level=" + level() + ", subAssemblies=" + subAssmString
    }
}