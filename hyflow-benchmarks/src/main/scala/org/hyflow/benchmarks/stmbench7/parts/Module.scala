package org.hyflow.benchmarks.stmbench7.parts

import org.hyflow._
import org.hyflow.benchmarks.stmbench7.{ID}
import scala.concurrent.stm._

class Module(val id: Int, val varType: String, val initBuildDate: Int, val initMan: Manual, val node: Int) extends DesignObj {
    override def _id: String = ID.Module(id, node)

    protected val buildDate = field[Int](null.asInstanceOf[Int])            // Int
    private val manual_ID = field[String](null.asInstanceOf[String])        // Manual
    private val designRoot_ID = field[String](null.asInstanceOf[String])    // ComplexAssembly

    def getBuildDate()(implicit txn: InTxn): Int = {
        buildDate()
    }
    def setBuildDate(value: Int)(implicit txn: InTxn) {
        buildDate() = value
    }
    def getDesignRoot()(implicit txn: InTxn): ComplexAssembly = {
        Hyflow.dir.open[ComplexAssembly](designRoot_ID())
    }
    def getManual()(implicit txn: InTxn): Manual = {
        Hyflow.dir.open[Manual](manual_ID())
    }

    def init()(implicit txn: InTxn) {
        buildDate() = initBuildDate
        manual_ID() = initMan._id
        initMan.setModule(this)
    }

    def setDesignRoot(designRoot: ComplexAssembly)(implicit txn: InTxn) {
        this.designRoot_ID() = designRoot._id
    }

    override def equals(obj: scala.Any): Boolean = {
        if (!obj.isInstanceOf[Module]) {
            return false
        }
        return super.equals(obj)
    }
}