package org.hyflow.benchmarks.stmbench7.operations

import org.hyflow.Hyflow
import org.hyflow.benchmarks.stmbench7.{BaseOperation, Operations}
import org.hyflow.benchmarks.stmbench7.parts._
import scala.concurrent.stm._

// Write. Single-threaded.
class ShortTraversal7(oo7setup: Setup) extends ShortTraversal2(oo7setup) {
    override protected def traverse(documentation: Document)(implicit txn: InTxn): Int = {
        if (documentation.textBeginsWith("I am")) {
            return documentation.replaceText("I am", "This is")
        }
        if (documentation.textBeginsWith("This is")) {
            return documentation.replaceText("This is", "I am")
        }

        throw new RuntimeException("ST7: Unexpected beginning of Document.text!")
    }

    override def getOperationId(): Operations.Op = Operations.ST7
}