package org.hyflow.benchmarks.stmbench7.operations

import org.hyflow.Hyflow
import org.hyflow.benchmarks.stmbench7.{BaseOperation, ID, Operations, Parameters}
import org.hyflow.benchmarks.stmbench7.backend._
import org.hyflow.benchmarks.stmbench7.parts._
import scala.concurrent.stm._
import scala.util.Random
import com.typesafe.scalalogging.slf4j.Logging

// Read-only. Can be parallel.
class Operation7(oo7setup: Setup) extends BaseOperation {
    override def performOperation()(implicit txn: InTxn): Int = {
        logger.warn("Operation: {}", getOperationId().getClass.toString)
        val local = (Parameters.rand.nextInt(100) < Parameters.Locality)
        val initialID = if (local) { ID.currentNode }
                        else { Parameters.rand.nextInt(ID.peerSize) }

      /*  var done = false
        var baseAssemblyIdIndex = null.asInstanceOf[Index[Integer, String]]
        while (!done) {
            try {*/
                val baseAssemblyIdIndex = Hyflow.dir.open[Index[Integer, String]](oo7setup.getBaseAssemblyIdIndex_ID(initialID))
         /*   } catch {
                    case x: java.util.NoSuchElementException =>
                        logger.trace("Node {} missing Index.", initialID.toString)
                    case y: Throwable =>
                        throw y
            }
        }*/

        val keySize = baseAssemblyIdIndex.getKeys.size

        var baseAssemblyId = Parameters.rand.nextInt(Parameters.MaxBaseAssemblies) + 1
        while (baseAssemblyId >= keySize) {
            baseAssemblyId = Parameters.rand.nextInt(Parameters.MaxBaseAssemblies) + 1
        }

        val baseAssembly_ID = baseAssemblyIdIndex.get(baseAssemblyId)
        if (baseAssembly_ID == null) {
            throw new RuntimeException("OP7: BaseAssembly is null!")
        }

        val baseAssembly = Hyflow.dir.open[BaseAssembly](baseAssembly_ID)
        val superAssembly = Hyflow.dir.open[ComplexAssembly](baseAssembly.getSuperAssembly_ID())

        var count = 0
        for (siblingAssembly_ID <- superAssembly.getSubAssembly_IDs()) {
            atomic { implicit txn: InTxn =>
                val siblingAssembly = Hyflow.dir.open[BaseAssembly](siblingAssembly_ID)
                performOperationInBaseAssembly(siblingAssembly)
                count += 1
            }
        }
        return count
    }

    protected def performOperationInBaseAssembly(assembly: BaseAssembly)(implicit txn: InTxn) {
        assembly.nullOperation()
    }

    override def getOperationId(): Operations.Op = Operations.OP7
}