package org.hyflow.benchmarks.stmbench7.operations

import org.hyflow.Hyflow
import org.hyflow.benchmarks.stmbench7.{BaseOperation, ID, Operations, Parameters}
import org.hyflow.benchmarks.stmbench7.backend._
import org.hyflow.benchmarks.stmbench7.parts._
import scala.util.Random
import scala.collection.mutable.HashSet
import scala.concurrent.stm._

// Read-only. Can be parallel.
class Traversal7(oo7setup: Setup) extends BaseOperation {
    override def performOperation()(implicit txn: InTxn): Int = {
        logger.warn("Operation: {}", getOperationId().getClass.toString)
        val local = (Parameters.rand.nextInt(100) < Parameters.Locality)
        val initialID = if (local) { ID.currentNode }
                        else { Parameters.rand.nextInt(ID.peerSize) }

        val partIdIndex = Hyflow.dir.open[Index[Integer, String]](oo7setup.getAtomicPartIdIndex_ID(initialID))
        val keySize = partIdIndex.getKeys.size

        var partId = Parameters.rand.nextInt(Parameters.MaxAtomicParts) + 1
        while (partId >= keySize) {
            partId = Parameters.rand.nextInt(Parameters.MaxAtomicParts) + 1
        }

        //println("\n\nAccessing AP " + partId)

        val part_ID = partIdIndex.get(partId)
        if (part_ID == null) {
            throw new RuntimeException("T7: AtomicPart is null!")
        }

        return traverse(Hyflow.dir.open[AtomicPart](part_ID).getPartOf())
    }

    protected def traverse(part: CompositePart)(implicit txn: InTxn): Int = {
        var result = 0

        //println("The CP " + part._id + " has ids " + part.getUsedIn_IDs().getClass + " -> " + part.getUsedIn_IDs())

        if (!part.getUsedIn_IDs().isEmpty && !part.getUsedIn_IDs().contains(null)) {
            for (assembly_ID <- part.getUsedIn_IDs()) {
                atomic { implicit txn: InTxn =>
                    result += traverse(Hyflow.dir.open[Assembly](assembly_ID), new HashSet[Assembly]())
                }
            }
        }

        return result
    }

    protected def traverse(assembly: Assembly, visitedAssemblies: HashSet[Assembly])(implicit txn: InTxn): Int = {
        if (assembly == null) {
            return 0
        }
        if (visitedAssemblies.contains(assembly)) {
            return 0
        }

        visitedAssemblies.add(assembly)
        performOperationOnAssembly(assembly)

        val nextAssemblyId = assembly.getSuperAssembly_ID()
        if (nextAssemblyId == null) {
            return 1
        }

        return traverse(Hyflow.dir.open[Assembly](assembly.getSuperAssembly_ID()), visitedAssemblies) + 1
    }

    protected def performOperationOnAssembly(assembly: Assembly)(implicit txn: InTxn) {
        assembly.nullOperation()
    }

    override def getOperationId(): Operations.Op = Operations.ST3
}