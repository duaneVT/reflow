package org.hyflow.benchmarks.stmbench7.operations

import org.hyflow.Hyflow
import org.hyflow.benchmarks.stmbench7.{BaseOperation, Operations}
import org.hyflow.benchmarks.stmbench7.parts._
import scala.concurrent.stm._

// Write. Can be parallel.
class ShortTraversal8(oo7setup: Setup) extends Traversal7(oo7setup) {
    override protected def performOperationOnAssembly(assembly: Assembly)(implicit txn: InTxn) {
        //assembly.updateBuildDate()
        val buildDate = assembly.getBuildDate()
        if (buildDate % 2 == 0) {
            assembly.setBuildDate(buildDate - 1)
        }
        else {
            assembly.setBuildDate(buildDate + 1)
        }
    }

    override def getOperationId(): Operations.Op = Operations.ST8
}