package org.hyflow.benchmarks.stmbench7.parts

import org.hyflow._
import org.hyflow.benchmarks.stmbench7.{ID, Parameters}
import org.hyflow.benchmarks.stmbench7.backend._
import scala.concurrent.stm._

class ModuleBuilder(val owner: String, val id: Int, val node: Int, val varType: String = null) extends DesignObjBuilder {
    override def _id: String = ID.ModuleBuilder(owner, id, node)

    private val idPool = field[IdPool](null.asInstanceOf[IdPool])              // IdPool
    private val manualFactory_ID = field[String](null.asInstanceOf[String])    // ManualBuilder
    private val assemblyBuilder_ID = field[String](null.asInstanceOf[String])  // AssemblyBuilder

    def getAssemblyBuilder()(implicit txn: InTxn): AssemblyBuilder = Hyflow.dir.open[AssemblyBuilder](assemblyBuilder_ID())

    def init(baseAssemblyIdIndex_ID: String, complexAssemblyIdIndex_ID: String)(implicit txn: InTxn) {
        val newManualFactory = new ManualBuilder(_id, 0, node)
        newManualFactory.init()
        this.manualFactory_ID() = newManualFactory._id

        val newAssemblyBuilder = new AssemblyBuilder(_id, 0, node)
        newAssemblyBuilder.init(baseAssemblyIdIndex_ID, complexAssemblyIdIndex_ID)
        this.assemblyBuilder_ID() = newAssemblyBuilder._id
        this.idPool() = BackendFactory.instance.createIdPool(Parameters.NumModules)
    }

    def createRegisterModule()(implicit txn: InTxn): Module = {
        val moduleId = idPool().getId()
        val manual: Manual = Hyflow.dir.open[ManualBuilder](manualFactory_ID()).createManual(moduleId)
        val varType = createType()
        val newBuildDate = createBuildDate(Parameters.MinModuleDate, Parameters.MaxModuleDate)

        val module: Module = Hyflow.dir.open[Module](designObjFactory.createModule(moduleId, varType, newBuildDate, manual))
        val designRoot: ComplexAssembly = Hyflow.dir.open[AssemblyBuilder](assemblyBuilder_ID()).createAndRegisterAssembly(module, null).asInstanceOf[ComplexAssembly]
        module.setDesignRoot(designRoot)

        return module
    }
}