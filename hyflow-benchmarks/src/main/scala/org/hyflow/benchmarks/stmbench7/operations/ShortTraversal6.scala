package org.hyflow.benchmarks.stmbench7.operations

import org.hyflow.Hyflow
import org.hyflow.benchmarks.stmbench7.{BaseOperation, Operations}
import org.hyflow.benchmarks.stmbench7.parts._
import scala.concurrent.stm._

// Write. Single-threaded.
class ShortTraversal6(oo7setup: Setup) extends ShortTraversal1(oo7setup) {
    override protected def traverse(atomicPart: AtomicPart)(implicit txn: InTxn): Int = {
        atomicPart.swapXY()
        return super.traverse(atomicPart)
    }

    override def getOperationId(): Operations.Op = Operations.ST6
}