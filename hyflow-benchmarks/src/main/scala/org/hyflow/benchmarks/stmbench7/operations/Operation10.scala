package org.hyflow.benchmarks.stmbench7.operations

import org.hyflow.benchmarks.stmbench7.{BaseOperation, Operations}
import org.hyflow.benchmarks.stmbench7.parts._
import scala.concurrent.stm._

// Write. Can be parallel.
class Operation10(oo7setup: Setup) extends Query2(oo7setup) {
    override protected def performOperationInAtomicPart(atomicPart: AtomicPart)(implicit txn: InTxn) {
        atomicPart.swapXY()
    }

    override def getOperationId(): Operations.Op = Operations.OP10
}