package org.hyflow.benchmarks.stmbench7

import org.hyflow.Hyflow
import org.hyflow.benchmarks.stmbench7.backend._
import org.hyflow.benchmarks.stmbench7.parts._
import org.hyflow.benchmarks.stmbench7.operations._
import com.typesafe.scalalogging.slf4j.Logging

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.stm._

// Operation types.
object OpType extends Enumeration {
    type OpType = Value
    val TRAVERSAL_RO, TRAVERSAL, SHORT_TRAVERSAL_RO, SHORT_TRAVERSAL,
        OPERATION_RO, OPERATION, STRUCTURAL_MODIFICATION = Value
}

// Operations.
import OpType._
object Operations {
    // General template class for operation.
    sealed abstract class Op(val opClass: Class[_ <: BaseOperation], val opType: OpType)

    /*
    val OperationList = ArrayBuffer(ST1, ST2, ST3, ST4, ST5, ST6, ST7, ST8, ST9, ST10,
                                    OP1, OP2, OP3, OP4, OP5, OP6, OP7, OP8, OP9, OP10, OP11, OP12, OP13, OP14, OP15)
    */

    // Single-threaded (ST1, ST2, ST6, ST7, OP11, T8/OP4, T9/OP5) removed. ST10 runs across the same graph.
    // Worst for Relaxed (ST4, ST5, OP3).
    //val OperationList = ArrayBuffer(ST3, ST4, ST5, ST8, ST9, OP1, OP2, OP6, OP7, OP8, OP9, OP10, OP12, OP13, OP14)
    val OperationList = ArrayBuffer(OP1, OP2, OP6, OP7, OP8, OP9, OP10, OP12, OP13, OP14)

    // Long Traversals.
    /*
    case object T1 extends Op(classOf[Traversal1], OpType.TRAVERSAL_RO)
    case object T2a extends Op(classOf[Traversal2a], OpType.TRAVERSAL)
    case object T2b extends Op(classOf[Traversal2b], OpType.TRAVERSAL)
    case object T2c extends Op(classOf[Traversal2c], OpType.TRAVERSAL)
    case object T3a extends Op(classOf[Traversal3a], OpType.TRAVERSAL)
    case object T3b extends Op(classOf[Traversal3b], OpType.TRAVERSAL)
    case object T3c extends Op(classOf[Traversal3c], OpType.TRAVERSAL)
    case object T4 extends Op(classOf[Traversal4], OpType.TRAVERSAL_RO)
    case object T5 extends Op(classOf[Traversal5], OpType.TRAVERSAL)
    case object T6 extends Op(classOf[Traversal6], OpType.TRAVERSAL_RO)
    case object Q6 extends Op(classOf[Query6], OpType.TRAVERSAL_RO)
    case object Q7 extends Op(classOf[Query7], OpType.TRAVERSAL_RO)
    */

    // Short Traversals.
    case object ST1 extends Op(classOf[ShortTraversal1], OpType.SHORT_TRAVERSAL_RO)
    case object ST2 extends Op(classOf[ShortTraversal2], OpType.SHORT_TRAVERSAL_RO)
    case object ST3 extends Op(classOf[Traversal7], OpType.SHORT_TRAVERSAL_RO)
    case object ST4 extends Op(classOf[Query4], OpType.SHORT_TRAVERSAL_RO)
    case object ST5 extends Op(classOf[Query5], OpType.SHORT_TRAVERSAL_RO)
    case object ST6 extends Op(classOf[ShortTraversal6], OpType.SHORT_TRAVERSAL)
    case object ST7 extends Op(classOf[ShortTraversal7], OpType.SHORT_TRAVERSAL)
    case object ST8 extends Op(classOf[ShortTraversal8], OpType.SHORT_TRAVERSAL)
    case object ST9 extends Op(classOf[ShortTraversal9], OpType.SHORT_TRAVERSAL_RO)
    case object ST10 extends Op(classOf[ShortTraversal10], OpType.SHORT_TRAVERSAL)

    // Operations.
    case object OP1 extends Op(classOf[Query1], OpType.OPERATION_RO)
    case object OP2 extends Op(classOf[Query2], OpType.OPERATION_RO)
    case object OP3 extends Op(classOf[Query3], OpType.OPERATION_RO)
    case object OP4 extends Op(classOf[Traversal8], OpType.OPERATION_RO)
    case object OP5 extends Op(classOf[Traversal9], OpType.OPERATION_RO)
    case object OP6 extends Op(classOf[Operation6], OpType.OPERATION_RO)
    case object OP7 extends Op(classOf[Operation7], OpType.OPERATION_RO)
    case object OP8 extends Op(classOf[Operation8], OpType.OPERATION_RO)
    case object OP9 extends Op(classOf[Operation9], OpType.OPERATION)
    case object OP10 extends Op(classOf[Operation10], OpType.OPERATION)
    case object OP11 extends Op(classOf[Operation11], OpType.OPERATION)
    case object OP12 extends Op(classOf[Operation12], OpType.OPERATION)
    case object OP13 extends Op(classOf[Operation13], OpType.OPERATION)
    case object OP14 extends Op(classOf[Operation14], OpType.OPERATION)
    case object OP15 extends Op(classOf[Operation15], OpType.OPERATION)

    // Structural Modifications.
    /*
    case object SM1 extends Op(classOf[StructuralModification1], OpType.STRUCTURAL_MODIFICATION)
    case object SM2 extends Op(classOf[StructuralModification2], OpType.STRUCTURAL_MODIFICATION)
    case object SM3 extends Op(classOf[StructuralModification3], OpType.STRUCTURAL_MODIFICATION)
    case object SM4 extends Op(classOf[StructuralModification4], OpType.STRUCTURAL_MODIFICATION)
    case object SM5 extends Op(classOf[StructuralModification5], OpType.STRUCTURAL_MODIFICATION)
    case object SM6 extends Op(classOf[StructuralModification6], OpType.STRUCTURAL_MODIFICATION)
    case object SM7 extends Op(classOf[StructuralModification7], OpType.STRUCTURAL_MODIFICATION)
    case object SM8 extends Op(classOf[StructuralModification8], OpType.STRUCTURAL_MODIFICATION)
    */
}

object BaseOperation {
    def addAtomicPartToBuildDateIndex(atomicPartBuildDateIndex_ID: String, atomicPart_ID: String, buildDate: Integer)(implicit txn: InTxn) {
        val atomicPartBuildDateIndex: Index[Integer, String] = Hyflow.dir.open[Index[Integer, String]](atomicPartBuildDateIndex_ID)     // Index[Integer, LargeSet_ID]
        val buildDateSet_ID: String = atomicPartBuildDateIndex.get(buildDate)                                                           // LargeSet_ID

        var buildDateSet: LargeSet[String] = null.asInstanceOf[LargeSet[String]]
        if (buildDateSet_ID == null) {
            val buildDateSet_newID = BackendFactory.instance.createLargeSet[String]("date", buildDate, "int", atomicPartBuildDateIndex.getNode())
            buildDateSet = Hyflow.dir.open[LargeSet[String]](buildDateSet_newID)
            buildDateSet.initBuffer()
            atomicPartBuildDateIndex.put(buildDate, buildDateSet._id)
        }
        else {
            buildDateSet = Hyflow.dir.open[LargeSet[String]](buildDateSet_ID)
        }

        buildDateSet.add(atomicPart_ID)
    }

    def removeAtomicPartFromBuildDateIndex(atomicPartBuildDateIndex_ID: String, atomicPart_ID: String, buildDate: Integer)(implicit txn: InTxn) {
        val atomicPartBuildDateIndex: Index[Integer, String] = Hyflow.dir.open[Index[Integer, String]](atomicPartBuildDateIndex_ID)     // Index[Integer, LargeSet_ID]
        val buildDateSet_ID: String = atomicPartBuildDateIndex.get(buildDate)                                                           // LargeSet_ID

        val buildDateSet: LargeSet[String] = Hyflow.dir.open[LargeSet[String]](buildDateSet_ID)
        buildDateSet.remove(atomicPart_ID)
    }
}

abstract class BaseOperation extends Logging {
    def performOperation()(implicit txn: InTxn): Int
    def getOperationId(): Operations.Op
}

object OperationExecutorFactory {
    var instance: OperationExecutorFactory = null
    def setInstance(newInstance: OperationExecutorFactory) { instance = newInstance }
    def executeSequentialOperation(op: BaseOperation) {
        val executor = OperationExecutorFactory.instance.createOperationExecutor(op)
        executor.execute()
    }
}

class OperationExecutorFactory {
    def createOperationExecutor(op: BaseOperation): OperationExecutor = { new OperationExecutor(op) }
}

class OperationExecutor(val op: BaseOperation) extends Logging {
    def execute() = {
        var done = false
        while (!done) {
            try {
                atomic { implicit txn =>
                    op.performOperation()
                }
                done = true
            } catch {
                case x: java.util.NoSuchElementException =>
                    logger.trace("Node operation was missing the relevant objects.")
                case y: java.util.concurrent.TimeoutException =>
                    logger.debug("Transaction timed out.")
                case z: Throwable =>
                    throw z
            }
        }
    }
}