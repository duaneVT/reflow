package org.hyflow.benchmarks.stmbench7.parts

import org.hyflow.api._
import org.hyflow.benchmarks.stmbench7.{ID, Parameters}
import com.typesafe.scalalogging.slf4j.Logging

import scala.concurrent.stm._
import scala.util.Random

object DesignObjFactory {
    var instance: DesignObjFactory = null
    def setInstance(newInstance: DesignObjFactory) { instance = newInstance }
}

class DesignObjFactory extends Logging {
    def createAtomicPart(id: Int, varType: String, buildDate: Int, x: Int, y: Int)(implicit txn: InTxn): String = {
        val newAtomicPart = new AtomicPart(id, varType, buildDate, x, y, ID.currentNode)
        newAtomicPart.init()
        logger.trace("Created AtomicPart {}.", newAtomicPart._id)
        newAtomicPart._id
    }

    def createBaseAssembly(id: Int, varType: String, buildDate: Int, module: Module, superAssembly: ComplexAssembly)(implicit txn: InTxn): String = {
        val newBaseAssembly = new BaseAssembly(id, varType, buildDate, module, superAssembly, ID.currentNode)
        newBaseAssembly.init()
        logger.trace("Created BaseAssembly {}.", newBaseAssembly._id)
        newBaseAssembly._id
    }

    def createComplexAssembly(id: Int, varType: String, buildDate: Int, module: Module, superAssembly: ComplexAssembly)(implicit txn: InTxn): String = {
        val newComplexAssembly = new ComplexAssembly(id, varType, buildDate, module, superAssembly, ID.currentNode)
        newComplexAssembly.init()
        logger.trace("Created {}.", newComplexAssembly._id)
        newComplexAssembly._id
    }

    def createCompositePart(id: Int, varType: String, buildDate: Int, documentation: Document)(implicit txn: InTxn): String = {
        val newCompositePart = new CompositePart(id, varType, buildDate, documentation, ID.currentNode)
        newCompositePart.init()
        logger.trace("Created CompositePart {}.", newCompositePart._id)
        newCompositePart._id
    }

    def createDocument(id: Int, title: String, text: String)(implicit txn: InTxn): String = {
        val newDocument = new Document(id, title, text, null.asInstanceOf[CompositePart], ID.currentNode)
        newDocument.init()
        logger.trace("Created Document {}.", newDocument._id)
        newDocument._id
    }

    def createManual(id: Int, title: String, text: String)(implicit txn: InTxn): String = {
        val newManual = new Manual(id, title, text, ID.currentNode)
        newManual.init()
        logger.trace("Created Manual {}.", newManual._id)
        newManual._id
    }

    def createModule(id: Int, varType: String, buildDate: Int, man: Manual)(implicit txn: InTxn): String = {
        val newModule = new Module(id, varType, buildDate, man, ID.currentNode)
        newModule.init()
        logger.trace("Created Module {}.", newModule._id)
        newModule._id
    }
}

abstract class DesignObjBuilder extends DesignObj {
    protected val designObjFactory: DesignObjFactory = DesignObjFactory.instance
    protected val owner: String

    protected def createType(): String = {
        val varType = "t#" + Parameters.rand.nextInt(Parameters.NumTypes)        //!!!!!!!!!!
        return varType
    }

    protected def createBuildDate(minBuildDate: Int, maxBuildDate: Int): Int = (minBuildDate + Parameters.rand.nextInt(maxBuildDate - minBuildDate + 1))

    protected def createText(textSize: Int, textPattern: String): String = {
        val patternSize = textPattern.length()
        var size = 0

        val stringBuilder: StringBuilder = new StringBuilder(textSize)
        while ((size + patternSize) <= textSize) {
            stringBuilder.append(textPattern)
            size += patternSize
        }
        return stringBuilder.toString()
    }
}

abstract class DesignObj extends HObj {
    override def _id: String = null.asInstanceOf[String]

    protected val id: Int
    protected val varType: String
    def getId(): Int = id
    def getVarType(): String = varType

    def nullOperation() { }

    override def equals(obj: scala.Any): Boolean = {
        if (!obj.isInstanceOf[DesignObj]) { return false }
        else { return obj.asInstanceOf[DesignObj].getId() == id }
    }

    override def hashCode(): Int = id

    def toString()(implicit txn: InTxn) = {
        this.getClass().getName() + ": id=" + id + ", type=" + varType
    }
}
