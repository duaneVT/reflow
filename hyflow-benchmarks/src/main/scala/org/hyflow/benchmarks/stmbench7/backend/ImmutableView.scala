package org.hyflow.benchmarks.stmbench7.backend

abstract class ImmutableCollection[E] extends Iterable[E] {
    override def size(): Int = 0
    override def clone(): ImmutableCollection[E] = throw new RuntimeException("Clone not supported in abstract class!")
    def contains(element: E): Boolean = throw new RuntimeException("Contains not supported in abstract class!")
}

class ImmutableView[E] extends ImmutableCollection[E] {
    // The initial list is non-existant.
    private var elements: List[E] = null

    // The list is only populated with another object's data.
    def this(otherElements: List[E]) {
        this()
        this.elements = otherElements
    }

    override def contains(element: E): Boolean = {
        elements.contains(element)
    }

    override def size(): Int = elements.size
    override def iterator: Iterator[E] = elements.iterator
    override def clone(): ImmutableCollection[E] = new ImmutableView[E](elements)
}