package org.hyflow.benchmarks.stmbench7.operations

import org.hyflow.benchmarks.stmbench7.{BaseOperation, Operations}
import org.hyflow.benchmarks.stmbench7.parts._
import scala.concurrent.stm._

// Write. Can be parallel.
class Operation14(oo7setup: Setup) extends Operation8(oo7setup) {
    override protected def performOperationInComponent(component: CompositePart)(implicit txn: InTxn) {
        //component.updateBuildDate()
        val buildDate = component.getBuildDate()
        if (buildDate % 2 == 0) {
            component.setBuildDate(buildDate - 1)
        }
        else {
            component.setBuildDate(buildDate + 1)
        }
    }

    override def getOperationId(): Operations.Op = Operations.OP14
}