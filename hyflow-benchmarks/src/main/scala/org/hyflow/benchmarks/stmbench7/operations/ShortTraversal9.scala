package org.hyflow.benchmarks.stmbench7.operations

import org.hyflow.Hyflow
import org.hyflow.benchmarks.stmbench7.{BaseOperation, Operations}
import org.hyflow.benchmarks.stmbench7.parts._
import scala.collection.mutable.HashSet
import scala.concurrent.stm._

// Read-only. Can be parallel.
class ShortTraversal9(oo7setup: Setup) extends ShortTraversal1(oo7setup) {
    override protected def traverse(component: CompositePart)(implicit txn: InTxn): Int = {
        //HashSet<AtomicPart> setOfVisitedPartIds = new HashSet<AtomicPart>()
        //return traverse(component.getRootPart(), new HashSet[AtomicPart]())

        val atomicPart = component.getRootPart()
        if (atomicPart == null) {
            return 0
        }

        var result = performOperationInAtomicPart(atomicPart)
        val startSet = new HashSet[AtomicPart]()
        startSet.add(atomicPart)

        val toConnections = atomicPart.getToConnections()
        if (!toConnections.isEmpty && !toConnections.contains(null)) {
            for (connection <- atomicPart.getToConnections()) {
                atomic { implicit txn: InTxn =>
                    result += traverse(connection.getDestination(), startSet.clone())
                }
            }
        }
        return result
    }

    protected def traverse(atomicPart: AtomicPart, setOfVisitedPartIds: HashSet[AtomicPart])(implicit txn: InTxn): Int = {
        if (atomicPart == null) {
            return 0
        }
        if (setOfVisitedPartIds.contains(atomicPart)) {
            return 0
        }

        var result = performOperationInAtomicPart(atomicPart)
        setOfVisitedPartIds.add(atomicPart)

        val toConnections = atomicPart.getToConnections()
        if (!toConnections.isEmpty && !toConnections.contains(null)) {
            for (connection <- atomicPart.getToConnections()) {
                result += traverse(connection.getDestination(), setOfVisitedPartIds)
            }
        }
        return result
    }

    protected def performOperationInAtomicPart(part: AtomicPart)(implicit txn: InTxn): Int = {
        part.nullOperation()
        return 1
    }

    override protected def traverse(atomicPart: AtomicPart)(implicit txn: InTxn): Int = {
        throw new RuntimeException("ST9: Unexpected call to traverse(AtomicPart)!")
    }

    override def getOperationId(): Operations.Op = Operations.ST9
}