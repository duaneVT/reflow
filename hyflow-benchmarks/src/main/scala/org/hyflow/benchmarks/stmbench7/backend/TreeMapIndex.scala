package org.hyflow.benchmarks.stmbench7.backend

import org.hyflow.benchmarks.stmbench7.{ID}
import org.hyflow.benchmarks.stmbench7.parts._
import java.util.TreeMap

import scala.collection.JavaConverters._
import scala.concurrent.stm._

abstract class Index[K <: Comparable[K], V] extends DesignObj {
    def get(key: K)(implicit txn: InTxn): V
    def put(key: K, value: V)(implicit txn: InTxn)
    def putIfAbsent(key: K, value: V)(implicit txn: InTxn): V
    def remove(key: K)(implicit txn: InTxn): Boolean
    def getRange(minKey: K, maxKey: K)(implicit txn: InTxn): List[V]
    def getKeys()(implicit txn: InTxn): List[K]
    def valueList()(implicit txn: InTxn): Iterator[V]
    def getSize()(implicit txn: InTxn): Int
    def getNode(): Int
}

class TreeMapIndex[K <: Comparable[K], V](val owner: String, val id: Int, val keyType: String, val valueType: String, val node: Int) extends Index[K, V] {
    override def _id: String = ID.TreeMapIndex(owner, id, keyType, valueType, node)
    val varType = null.asInstanceOf[String]
    def getNode(): Int = node

    private val index = field[TreeMap[K, V]](null.asInstanceOf[TreeMap[K, V]])
    def initIndex()(implicit txn: InTxn) {
        index() = new TreeMap[K, V]()
    }

    def put(key: K, value: V)(implicit txn: InTxn) {
        if (value == null) { throw new RuntimeException("TreeMapIndex does not support null values!") }
        index().put(key, value)
    }

    def putIfAbsent(key: K, value: V)(implicit txn: InTxn): V = {
        if (value == null) { throw new RuntimeException("TreeMapIndex does not support null values!") }

        val oldVal = index().get(key)
        if (oldVal != null) {
            return oldVal
        }

        index().put(key, value)
        null.asInstanceOf[V]
    }

    def get(key: K)(implicit txn: InTxn): V = {
        index().get(key)
    }

    def getRange(minKey: K, maxKey: K)(implicit txn: InTxn): List[V] = {
        index().subMap(minKey, maxKey).values().asScala.toList
    }

    def remove(key: K)(implicit txn: InTxn): Boolean = {
        val removedValue = index().remove(key)
        return (removedValue != null)
    }

    //def iterator = throw new RuntimeException("No direct iterator in TreeMapIndex!")

    def valueList()(implicit txn: InTxn): Iterator[V] = {
        index().values().iterator().asScala
    }

    def getKeys()(implicit txn: InTxn): List[K] = {
        index().keySet().asScala.asInstanceOf[Iterable[K]].toList
    }

    def getSize()(implicit txn: InTxn): Int = {
        index().size
    }

    override def toString() = _id
}
