package org.hyflow.benchmarks.stmbench7.operations

import org.hyflow.benchmarks.stmbench7.{BaseOperation, Operations}
import org.hyflow.benchmarks.stmbench7.parts._
import scala.concurrent.stm._

// Write. Single-threaded. [DISABLE THIS?]
class Operation11(oo7setup: Setup) extends Traversal8(oo7setup) {
    override protected def traverse(manual: Manual)(implicit txn: InTxn): Int = {
        if (manual.startsWith('I')) {
            return manual.replaceChar('I', 'i')
        }
        if (manual.startsWith('i')) {
            return manual.replaceChar('i', 'I')
        }
        throw new RuntimeException("OP11: Unexpected Manual.text!")
    }

    override def getOperationId(): Operations.Op = Operations.OP11
}