package org.hyflow.benchmarks.stmbench7.operations

import org.hyflow.Hyflow
import org.hyflow.benchmarks.stmbench7.{BaseOperation, ID, Operations, Parameters}
import org.hyflow.benchmarks.stmbench7.parts._
import scala.concurrent.stm._
import scala.util.Random

// Read-only. Single-threaded.
class Traversal8(oo7setup: Setup) extends BaseOperation {
    override def performOperation()(implicit txn: InTxn): Int = {
        logger.warn("Operation: {}", getOperationId().getClass.toString)
        val local = (Parameters.rand.nextInt(100) < Parameters.Locality)
        val initialID = if (local) { ID.currentNode }
                        else { Parameters.rand.nextInt(ID.peerSize) }

        val module = Hyflow.dir.open[Module](oo7setup.getModule_ID(initialID))
        val manual = module.getManual()
        return traverse(manual)
    }

    protected def traverse(manual: Manual)(implicit txn: InTxn): Int = {
            return manual.countOccurrences('I')
    }

    override def getOperationId(): Operations.Op = Operations.OP4
}