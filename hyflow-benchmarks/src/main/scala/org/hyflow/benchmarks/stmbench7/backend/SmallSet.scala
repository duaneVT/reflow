package org.hyflow.benchmarks.stmbench7.backend

import org.hyflow.benchmarks.stmbench7.{ID}
import org.hyflow.benchmarks.stmbench7.parts._

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.stm._

class SmallSet[E](val owner: String, val id: Int, val varType: String, val node: Int) extends DesignObj {
    override def _id: String = ID.SmallSet(owner, id, varType, node)

    private val buffer = field[ArrayBuffer[E]](null.asInstanceOf[ArrayBuffer[E]])
    def initBuffer()(implicit txn: InTxn) {
        buffer() = new ArrayBuffer[E]()
    }
    def immutableView()(implicit txn: InTxn): ImmutableCollection[E] = {
        try {
            val view = new ImmutableView[E](buffer().toList)
            return view
        }
        catch {
            case x: java.lang.NullPointerException =>
                return new ImmutableView[E](List[E]())
        }
    }

    def add(elem: E)(implicit txn: InTxn): Boolean = {
        if (buffer().contains(elem)) {
            return false
        }

        buffer() += elem
        return true
    }

    def remove(elem: E)(implicit txn: InTxn): Boolean = {
        if (!buffer().contains(elem)) {
            return false
        }
        buffer() -= elem
        return true
    }

    def clear()(implicit txn: InTxn) {
        buffer().clear()
    }

    def size()(implicit txn: InTxn) = {
        buffer().size
    }

    override def toString() = _id
}