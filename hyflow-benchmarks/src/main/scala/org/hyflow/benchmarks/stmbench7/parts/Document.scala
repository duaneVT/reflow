package org.hyflow.benchmarks.stmbench7.parts

import org.hyflow._
import org.hyflow.benchmarks.stmbench7.{ID}

import scala.concurrent.stm._

class Document(val id: Int, val initTitle: String, val initText: String, val initPart: CompositePart, val node: Int, val varType: String = null) extends DesignObj {
    override def _id: String = ID.Document(id, node)

    private val title = field[String](null.asInstanceOf[String])    // String
    private val text = field[String](null.asInstanceOf[String])     // String
    private val part_ID = field[String](null.asInstanceOf[String])  // CompositePart

    def init()(implicit txn: InTxn) {
        title() = initTitle
        text() = initText
        if (initPart != null) {
            part_ID() = initPart._id
        }
        else {
            part_ID() = null.asInstanceOf[String]
        }
    }

    def getCompositePart()(implicit txn: InTxn): CompositePart = {
        Hyflow.dir.open[CompositePart](part_ID())
    }
    def getDocumentId(): Int = id
    def getTitle()(implicit txn: InTxn): String = title()
    def getText()(implicit txn: InTxn): String = text()

    def setPart(part: CompositePart)(implicit txn: InTxn) {
        if (part == null) {
            this.part_ID() = null.asInstanceOf[String]
        }
        else {
            this.part_ID() = part._id
        }
    }

    def searchText(symbol: Char)(implicit txn: InTxn): Int = {
        var occurrences = 0
        for (i <- 0 to (text().length() - 1)) {
            if (text().charAt(i) == symbol) {
                occurrences += 1
            }
        }
        return occurrences
    }

    def replaceText(from: String, to: String)(implicit txn: InTxn): Int = {
        if (!text().startsWith(from)) {
            return 0
        }
        text() = text().replaceFirst(from, to)
        return 1
    }

    def textBeginsWith(prefix: String)(implicit txn: InTxn): Boolean = {
        return text().startsWith(prefix)
    }

    override def equals(obj: scala.Any): Boolean = {
        if (!obj.isInstanceOf[Document]) {
            return false
        }
        return (obj.asInstanceOf[Document].getDocumentId() == id)
    }

    override def hashCode(): Int = id

    override def clone(): Object = {
        try {
            return super.clone()
        }
        catch {
            case x: Throwable =>
                throw new RuntimeException(x)
        }
    }
}