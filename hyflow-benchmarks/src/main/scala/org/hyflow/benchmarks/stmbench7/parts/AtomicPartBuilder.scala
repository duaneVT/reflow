package org.hyflow.benchmarks.stmbench7.parts

import org.hyflow._
import org.hyflow.benchmarks.stmbench7.{BaseOperation, ID, Parameters}
import org.hyflow.benchmarks.stmbench7.backend._
import org.hyflow.benchmarks.stmbench7.parts._

import scala.concurrent.stm._
import scala.util.Random

class AtomicPartBuilder(val owner: String, val id: Int, val node: Int, val varType: String = null) extends DesignObjBuilder {
    override def _id: String = ID.AtomicPartBuilder(owner, id, node)

    private val idPool = field[IdPool](null.asInstanceOf[IdPool])              // IdPool
    private val partIdIndex_ID = field[String](null.asInstanceOf[String])        // Index[Integer, AtomicPart_ID] -> AtomicPart
    private val partBuildDateIndex_ID = field[String](null.asInstanceOf[String]) // Index[Integer, LargeSet_ID] -> LargeSet[AtomicPart_ID] -> AtomicPart

    def init(partIdIndex_ID: String, partBuildDateIndex_ID: String)(implicit txn: InTxn) {
        this.partIdIndex_ID() = partIdIndex_ID
        this.partBuildDateIndex_ID() = partBuildDateIndex_ID
        this.idPool() = BackendFactory.instance.createIdPool(Parameters.MaxAtomicParts)
    }

    def createAndRegisterAtomicPart()(implicit txn: InTxn): AtomicPart = {
        val nextId = idPool().getId()
        val varType = createType()
        val buildDate = createBuildDate(Parameters.MinAtomicDate, Parameters.MaxAtomicDate)
        val x = Parameters.rand.nextInt(Parameters.XYRange)
        val y = x + 1

        val part_ID: String = designObjFactory.createAtomicPart(nextId, varType, buildDate, x, y)
        val part: AtomicPart = Hyflow.dir.open[AtomicPart](part_ID)
        val partIdIndex = Hyflow.dir.open[Index[Integer, String]](partIdIndex_ID())
        partIdIndex.put(nextId, part_ID)

        BaseOperation.addAtomicPartToBuildDateIndex(partBuildDateIndex_ID(), part_ID, buildDate)

        return part
    }

    def unregisterAndRecycleAtomicPart(part: AtomicPart)(implicit txn: InTxn) {
        val partId = part.getId()
        BaseOperation.removeAtomicPartFromBuildDateIndex(partBuildDateIndex_ID(), part._id, part.getBuildDate())

        val partIdIndex = Hyflow.dir.open[Index[Integer, String]](partIdIndex_ID())
        partIdIndex.remove(partId)
        part.clearPointers()
        idPool().putUnusedId(partId)
    }
}