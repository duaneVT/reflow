package org.hyflow.benchmarks.stmbench7.operations

import org.hyflow.Hyflow
import org.hyflow.benchmarks.stmbench7.{BaseOperation, Operations}
import org.hyflow.benchmarks.stmbench7.backend._
import org.hyflow.benchmarks.stmbench7.parts._
import scala.concurrent.stm._

// Write. Can be parallel.
class Operation15(oo7setup: Setup) extends Query1(oo7setup) {
    override protected def performOperationInAtomicPart(atomicPart: AtomicPart)(implicit txn: InTxn) {
        BaseOperation.removeAtomicPartFromBuildDateIndex(oo7setup.getAtomicPartBuildDateIndex_ID(queryID), atomicPart._id, atomicPart.getBuildDate())
        //atomicPart.updateBuildDate()
        val buildDate = atomicPart.getBuildDate()
        if (buildDate % 2 == 0) {
            atomicPart.setBuildDate(buildDate - 1)
        }
        else {
            atomicPart.setBuildDate(buildDate + 1)
        }
        BaseOperation.addAtomicPartToBuildDateIndex(oo7setup.getAtomicPartBuildDateIndex_ID(queryID), atomicPart._id, atomicPart.getBuildDate())
    }

    override def getOperationId(): Operations.Op = Operations.OP15
}