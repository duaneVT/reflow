package org.hyflow.benchmarks.stmbench7.parts

import org.hyflow._
import org.hyflow.benchmarks.stmbench7.{ID}

import scala.concurrent.stm._

class Connection extends Cloneable {
    protected var length: Int = 0
    protected var from_ID: String = null.asInstanceOf[String]
    protected var to_ID: String = null.asInstanceOf[String]
    protected var varType: String = null.asInstanceOf[String]

    def getSource()(implicit txn: InTxn): AtomicPart = Hyflow.dir.open[AtomicPart](from_ID)
    def getDestination()(implicit txn: InTxn): AtomicPart = Hyflow.dir.open[AtomicPart](to_ID)
    def getLength(): Int = length
    def getVarType(): String = varType

    def this(from: AtomicPart, to: AtomicPart, varType: String, length: Int) {
        this()
        this.from_ID = from._id
        this.to_ID = to._id
        this.varType = varType
        this.length = length
    }

    def this(from_ID: String, to_ID: String, varType: String, length: Int) {
        this()
        this.from_ID = from_ID
        this.to_ID = to_ID
        this.varType = varType
        this.length = length
    }

    def getReversed(): Connection = {
        new Connection(to_ID, from_ID, new String(varType), length)
    }

    override def clone(): Object = {
        try {
            return super.clone()
        }
        catch {
            case x: Throwable =>
                throw new RuntimeException(x)
        }
    }

    override def equals(obj: scala.Any): Boolean = {
        if (!obj.isInstanceOf[Connection]) { return false }
        else { return ((obj.asInstanceOf[Connection].from_ID == from_ID) &&
                       (obj.asInstanceOf[Connection].to_ID == to_ID) &&
                       (obj.asInstanceOf[Connection].varType == varType) &&
                       (obj.asInstanceOf[Connection].length == length)) }
    }
}