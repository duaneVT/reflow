package org.hyflow.benchmarks.stmbench7.operations

import org.hyflow.Hyflow
import org.hyflow.benchmarks.stmbench7.{BaseOperation, ID, Operations, Parameters}
import org.hyflow.benchmarks.stmbench7.backend._
import org.hyflow.benchmarks.stmbench7.parts._
import scala.concurrent.stm._
import scala.util.Random

// Read-only. Can be parallel.
class Query4(oo7setup: Setup) extends BaseOperation {
    override def performOperation()(implicit txn: InTxn): Int = {
        logger.warn("Operation: {}", getOperationId().getClass.toString)
        val local = (Parameters.rand.nextInt(100) < Parameters.Locality)
        val initialID = if (local) { ID.currentNode }
                        else { Parameters.rand.nextInt(ID.peerSize) }

        val documentTitleIndex = Hyflow.dir.open[Index[String, String]](oo7setup.getDocumentTitleIndex_ID(initialID))
        var result = 0

        for (i <- 0 to 9) {
            atomic { implicit txn: InTxn =>
                val partId = Parameters.rand.nextInt(Parameters.MaxCompParts) + 1
                val docTitle = "Composite Part #" + partId
                val document_ID = documentTitleIndex.get(docTitle)
                if (document_ID != null) {
                    val document = Hyflow.dir.open[Document](document_ID)
                    if (!document.getCompositePart().getUsedIn_IDs().isEmpty && !document.getCompositePart().getUsedIn_IDs().contains(null)) {
                        for (assembly_ID <- document.getCompositePart().getUsedIn_IDs()) {
                            val assembly = Hyflow.dir.open[Assembly](assembly_ID)
                            assembly.nullOperation()
                            result += 1
                        }
                    }
                }
            }
        }
        return result
    }

    override def getOperationId(): Operations.Op = Operations.ST4
}