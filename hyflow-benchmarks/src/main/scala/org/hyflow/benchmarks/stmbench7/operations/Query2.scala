package org.hyflow.benchmarks.stmbench7.operations

import org.hyflow.Hyflow
import org.hyflow.benchmarks.stmbench7.{BaseOperation, ID, Operations, Parameters}
import org.hyflow.benchmarks.stmbench7.backend._
import org.hyflow.benchmarks.stmbench7.parts._
import scala.concurrent.stm._
import scala.util.Random

// Read-only. Can be parallel.
class Query2(oo7setup: Setup) extends BaseOperation {
    def percent = 1

    override def performOperation()(implicit txn: InTxn): Int = {
        logger.warn("Operation: {}", getOperationId().getClass.toString)
        val local = (Parameters.rand.nextInt(100) < Parameters.Locality)
        val initialID = if (local) { ID.currentNode }
                        else { Parameters.rand.nextInt(ID.peerSize) }

        val maxAtomicDate = Parameters.MaxAtomicDate
        val minAtomicDate = Parameters.MaxAtomicDate - (percent * (Parameters.MaxAtomicDate - Parameters.MinAtomicDate))/100

   /*     var done = false
        var partBuildDateIndex = null.asInstanceOf[Index[Integer, String]]
        while (!done) {
            try {*/
                val partBuildDateIndex = Hyflow.dir.open[Index[Integer, String]](oo7setup.getAtomicPartBuildDateIndex_ID(initialID))
            /*} catch {
                    case x: java.util.NoSuchElementException =>
                        logger.trace("Node {} missing Index {}.", initialID.toString)
                    case y: Throwable =>
                        throw y
            }
        }*/

        val partSet_IDs = partBuildDateIndex.getRange(minAtomicDate, maxAtomicDate).toList

        var count = 0
        for (partSet_ID <- partSet_IDs) {
            atomic { implicit txn: InTxn =>
      /*          var done = false
                var partSet = null.asInstanceOf[LargeSet[String]]
                while (!done) {
                    try {*/
                        val partSet = Hyflow.dir.open[LargeSet[String]](partSet_ID)
                        for (part_ID <- partSet.bufferList()) {
                            performOperationInAtomicPart(Hyflow.dir.open[AtomicPart](part_ID))
                            count += 1
                        }
                 /*   } catch {
                            case x: java.util.NoSuchElementException =>
                                logger.trace("Node {} missing the Set {} or its objects.", initialID.toString, partSet_ID)
                            case y: Throwable =>
                                throw y
                    }
                }*/
            }
        }
        return count
    }

    protected def performOperationInAtomicPart(atomicPart: AtomicPart)(implicit txn: InTxn) {
        atomicPart.nullOperation()
    }

    override def getOperationId(): Operations.Op = Operations.OP2
}