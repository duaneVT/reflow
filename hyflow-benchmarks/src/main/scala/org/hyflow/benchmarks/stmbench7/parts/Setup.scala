package org.hyflow.benchmarks.stmbench7.parts

import org.hyflow.Hyflow
import org.hyflow.benchmarks.stmbench7.{BaseOperation, ID, Operations, OperationExecutorFactory, Parameters}
import org.hyflow.benchmarks.stmbench7.backend._
import com.typesafe.scalalogging.slf4j.Logging

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.stm._
import scala.util.Random

class Setup extends Logging {
    protected var module_ID: String = null.asInstanceOf[String]                      // Module
    protected var atomicPartIdIndex_ID: String = null.asInstanceOf[String]           // Index[Integer, AtomicPart_ID] -> AtomicPart
    protected var atomicPartBuildDateIndex_ID: String = null.asInstanceOf[String]    // Index[Integer, LargeSet_ID] -> LargeSet[AtomicPart_ID] -> AtomicPart
    protected var documentTitleIndex_ID: String = null.asInstanceOf[String]          // Index[String, Document_ID] -> Document
    protected var compositePartIdIndex_ID: String = null.asInstanceOf[String]        // Index[Integer, CompositePart_ID] -> CompositePart
    protected var baseAssemblyIdIndex_ID: String = null.asInstanceOf[String]         // Index[Integer, BaseAssembly_ID] -> BaseAssembly
    protected var complexAssemblyIdIndex_ID: String = null.asInstanceOf[String]      // Index[Integer, ComplexAssembly_ID] -> ComplexAssembly
    protected var compositePartBuilder_ID: String = null.asInstanceOf[String]        // CompositePartBuilder
    protected var moduleBuilder_ID: String = null.asInstanceOf[String]               // ModuleBuilder

    def getModule_ID(node: Int): String = (module_ID.split(":")(0)) + ":" + node
    def getAtomicPartIdIndex_ID(node: Int): String = (atomicPartIdIndex_ID.split(":")(0)) + ":" + node
    def getAtomicPartBuildDateIndex_ID(node: Int): String = (atomicPartBuildDateIndex_ID.split(":")(0)) + ":" + node
    def getDocumentTitleIndex_ID(node: Int): String = (documentTitleIndex_ID.split(":")(0)) + ":" + node
    def getCompositePartIdIndex_ID(node: Int): String = (compositePartIdIndex_ID.split(":")(0)) + ":" + node
    def getBaseAssemblyIdIndex_ID(node: Int): String = (baseAssemblyIdIndex_ID.split(":")(0)) + ":" + node
    def getComplexAssemblyIdIndex_ID(node: Int): String = (complexAssemblyIdIndex_ID.split(":")(0)) + ":" + node
    def getCompositePartBuilder_ID(node: Int): String = (compositePartBuilder_ID.split(":")(0)) + ":" + node
    def getModuleBuilder_ID(node: Int): String = (moduleBuilder_ID.split(":")(0)) + ":" + node
    def getAssemblyBuilder_ID(node: Int)(implicit txn: InTxn): String = Hyflow.dir.open[ModuleBuilder](getModuleBuilder_ID(node)).getAssemblyBuilder()._id
/*
    def getModule_ID(): String = module_ID
    def getAtomicPartIdIndex_ID(): String = atomicPartIdIndex_ID
    def getAtomicPartBuildDateIndex_ID(): String = atomicPartBuildDateIndex_ID
    def getDocumentTitleIndex_ID(): String = documentTitleIndex_ID
    def getCompositePartIdIndex_ID(): String = compositePartIdIndex_ID
    def getBaseAssemblyIdIndex_ID(): String = baseAssemblyIdIndex_ID
    def getComplexAssemblyIdIndex_ID(): String = complexAssemblyIdIndex_ID
    def getCompositePartBuilder_ID(): String = compositePartBuilder_ID
    def getModuleBuilder_ID(): String = moduleBuilder_ID
    def getAssemblyBuilder_ID()(implicit txn: InTxn): String = Hyflow.dir.open[ModuleBuilder](getModuleBuilder_ID()).getAssemblyBuilder()._id
*/

    def initialize() {//(implicit txn: InTxn) {
        val node = ID.currentNode
        val backendFactory = BackendFactory.instance

        atomic { implicit txn =>
            atomicPartIdIndex_ID = backendFactory.createIndex[Integer, String]("i", 0, "int", "api", node)
            Hyflow.dir.open[TreeMapIndex[Integer, String]](atomicPartIdIndex_ID).initIndex()

            atomicPartBuildDateIndex_ID = backendFactory.createIndex[Integer, String]("i", 1, "int", "lsi", node)
            Hyflow.dir.open[TreeMapIndex[Integer, String]](atomicPartBuildDateIndex_ID).initIndex()

            documentTitleIndex_ID = backendFactory.createIndex[String, String]("i", 2, "str", "doci", node)
            Hyflow.dir.open[TreeMapIndex[String, String]](documentTitleIndex_ID).initIndex()

            compositePartIdIndex_ID = backendFactory.createIndex[Integer, String]("i", 3, "int", "cpi", node)
            Hyflow.dir.open[TreeMapIndex[Integer, String]](compositePartIdIndex_ID).initIndex()

            baseAssemblyIdIndex_ID = backendFactory.createIndex[Integer, String]("i", 4, "int", "bai", node)
            Hyflow.dir.open[TreeMapIndex[Integer, String]](baseAssemblyIdIndex_ID).initIndex()

            complexAssemblyIdIndex_ID = backendFactory.createIndex[Integer, String]("i", 5, "int", "cai", node)
            Hyflow.dir.open[TreeMapIndex[Integer, String]](complexAssemblyIdIndex_ID).initIndex()
        }

        atomic { implicit txn =>
            val newCompositePartBuilder = new CompositePartBuilder("i", 6, node)
            //newCompositePartBuilder.init(getCompositePartIdIndex_ID(node), getDocumentTitleIndex_ID(node), getAtomicPartIdIndex_ID(node), getAtomicPartBuildDateIndex_ID(node))
            newCompositePartBuilder.init(compositePartIdIndex_ID, documentTitleIndex_ID, atomicPartIdIndex_ID, atomicPartBuildDateIndex_ID)
            compositePartBuilder_ID = newCompositePartBuilder._id
        }

        atomic { implicit txn =>
            val newModuleBuilder = new ModuleBuilder("i", 7, ID.currentNode)
            //newModuleBuilder.init(getBaseAssemblyIdIndex_ID(node), getComplexAssemblyIdIndex_ID(node))
            newModuleBuilder.init(baseAssemblyIdIndex_ID, complexAssemblyIdIndex_ID)
            moduleBuilder_ID = newModuleBuilder._id
        }

        val setupOperation: SetupDataStructure = new SetupDataStructure(this)
        OperationExecutorFactory.executeSequentialOperation(setupOperation)
        module_ID = setupOperation.getModule_ID()
    }
}

class SetupDataStructure(val setup: Setup) extends BaseOperation {
    private var module_ID: String = null.asInstanceOf[String]    // Module

    def getOperationId(): Operations.Op = null.asInstanceOf[Operations.Op]
    def getModule_ID(): String = module_ID

    def performOperation()(implicit txn: InTxn): Int = {
        val node = ID.currentNode
        //val designLibrary: ArrayBuffer[String] = ArrayBuffer.fill(Parameters.InitialTotalCompParts)(null.asInstanceOf[String])          // CompositePart
        val compositePartBuilder: CompositePartBuilder = Hyflow.dir.open[CompositePartBuilder](setup.getCompositePartBuilder_ID(node))  // CompositePartBuilder

        // Making one-to-one connections between BaseAssembly and CompositePart.
        // for (i <- 0 to (Parameters.InitialTotalCompParts - 1)) {

        try {
            module_ID = Hyflow.dir.open[ModuleBuilder](setup.getModuleBuilder_ID(node)).createRegisterModule()._id
        }
        catch {
            case x: Throwable =>
                throw new RuntimeException("Unexpected failure of createRegisterModule!")
        }

        val baseAssemblyIdIndex: Index[Integer, String] = Hyflow.dir.open[Index[Integer, String]](setup.getBaseAssemblyIdIndex_ID(node))
        val designLibrary: ArrayBuffer[String] = ArrayBuffer.fill(baseAssemblyIdIndex.getSize())(null.asInstanceOf[String])

        var compNum = 0
        for (baseAssembly_ID <- baseAssemblyIdIndex.valueList()) {
            designLibrary(compNum) = compositePartBuilder.createAndRegisterCompositePart()._id
            val baseAssembly: BaseAssembly = Hyflow.dir.open[BaseAssembly](baseAssembly_ID)
            baseAssembly.addComponent(Hyflow.dir.open[CompositePart](designLibrary(compNum)))
            compNum += 1
            /*
            for (connections <- 0 to (Parameters.NumCompPerAssm - 1)) {
                val compositePartNum = Parameters.rand.nextInt(designLibrary.length)
                baseAssembly.addComponent(Hyflow.dir.open[CompositePart](designLibrary(compositePartNum)))
            }
            */
        }

        return 0
    }
}