package org.hyflow.benchmarks.stmbench7.operations

import org.hyflow.benchmarks.stmbench7.{BaseOperation, Operations}
import org.hyflow.benchmarks.stmbench7.parts._

// Read-only. Can be parallel.
class Query3(oo7setup: Setup) extends Query2(oo7setup) {
    override def percent = 10

    override def getOperationId(): Operations.Op = Operations.OP3
}