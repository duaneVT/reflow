package org.hyflow.benchmarks.stmbench7.parts

import org.hyflow.benchmarks.stmbench7.{ID}
import org.hyflow.benchmarks.stmbench7.backend._
import scala.collection.mutable.ListBuffer
import scala.concurrent.stm._

class BaseAssembly(override val id: Int, override val varType: String, override val initBuildDate: Int, override val initModule: Module,
                   override val initSuperAssembly: ComplexAssembly, override val node: Int) extends Assembly(id, varType, initBuildDate, initModule, initSuperAssembly, node) {
    override def _id: String = ID.BaseAssembly(id, node)

    protected val component_IDs = field[ListBuffer[String]](null.asInstanceOf[ListBuffer[String]])    // ListBuffer[CompositePart_ID] -> CompositePart

    override def init()(implicit txn: InTxn) {
        super.init()
        component_IDs() = new ListBuffer[String]()
    }

    def getComponent_IDs()(implicit txn: InTxn): ImmutableCollection[String] = {
        new ImmutableView[String](component_IDs().toList)
    }

    override def clearPointers()(implicit txn: InTxn) {
        super.clearPointers()
        // The List ID remains the same.
        component_IDs().clear()
    }

	def addComponent(component: CompositePart)(implicit txn: InTxn) {
	    component_IDs() += component._id
        component.addAssembly(this)
    }

	def removeComponent(component: CompositePart)(implicit txn: InTxn): Boolean = {
        val componentExists = component_IDs().contains(component._id)
        if (!componentExists) {
            return false
        }

        component_IDs() -= component._id
        component.removeAssembly(this)
        return true
    }

	override def equals(obj: scala.Any): Boolean = {
	    if (!obj.isInstanceOf[BaseAssembly]) {
	        return false
	    }
	    return super.equals(obj)
    }

	def clone()(implicit txn: InTxn): BaseAssembly = {
        val clone = super.clone().asInstanceOf[BaseAssembly]
        clone.component_IDs() = component_IDs().clone()

        return clone
    }

	override def toString()(implicit txn: InTxn): String = {
        var componentIds = "{ "
        for (component <- component_IDs()) {
            componentIds += component + " "
        }
        componentIds += "}"
        return super.toString() + ", buildDate=" + buildDate() + ", components=" + componentIds
    }
}