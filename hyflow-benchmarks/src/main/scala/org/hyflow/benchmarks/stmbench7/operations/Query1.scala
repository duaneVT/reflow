package org.hyflow.benchmarks.stmbench7.operations

import org.hyflow.Hyflow
import org.hyflow.benchmarks.stmbench7.{BaseOperation, ID, Operations, Parameters}
import org.hyflow.benchmarks.stmbench7.backend._
import org.hyflow.benchmarks.stmbench7.parts._
import scala.concurrent.stm._
import scala.util.Random

// Read-only. Can be parallel.
class Query1(oo7setup: Setup) extends BaseOperation {
    var queryID: Int = -1

    override def performOperation()(implicit txn: InTxn): Int = {
        logger.warn("Operation: {}", getOperationId().getClass.toString)
        val local = (Parameters.rand.nextInt(100) < Parameters.Locality)
        val initialID = if (local) { ID.currentNode }
                        else { Parameters.rand.nextInt(ID.peerSize) }
        queryID = initialID

/*        var done = false
        var partIdIndex = null.asInstanceOf[Index[Integer, String]]
        while (!done) {
            try {*/
              val  partIdIndex = Hyflow.dir.open[Index[Integer, String]](oo7setup.getAtomicPartIdIndex_ID(initialID))
/*                done = true
            } catch {
                case x: java.util.NoSuchElementException =>
                    logger.trace("Node {} missing Index.", initialID.toString)
                case y: Throwable =>
                    throw y
            }
        }*/

        var count = 0
        for (i <- 0 to 9) {
            atomic { implicit txn: InTxn =>
               /* var done = false
                while (!done) {
                    try {
*/                        val partId = Parameters.rand.nextInt(Parameters.MaxAtomicParts) + 1
                        val part_ID = partIdIndex.get(partId)
                        if (part_ID != null) {
                            val part = Hyflow.dir.open[AtomicPart](part_ID)
                            performOperationInAtomicPart(part)
                        }
                        count += 1
  /*                  } catch {
                        case x: java.util.NoSuchElementException =>
                            logger.trace("Node {} missing Atomic Part.", initialID.toString)
                        case y: Throwable =>
                            throw y
                    }
                }*/
            }
        }
        return count
    }

    protected def performOperationInAtomicPart(atomicPart: AtomicPart)(implicit txn: InTxn) {
        atomicPart.nullOperation()
    }

    override def getOperationId(): Operations.Op = Operations.OP1
}