package org.hyflow.benchmarks.stmbench7.operations

import org.hyflow.Hyflow
import org.hyflow.benchmarks.stmbench7.{BaseOperation, ID, Operations, Parameters}
import org.hyflow.benchmarks.stmbench7.parts._
import scala.util.Random
import scala.concurrent.stm._

// Read-only. Single-threaded.
class ShortTraversal1(oo7setup: Setup) extends BaseOperation {
    override def performOperation()(implicit txn: InTxn): Int = {
        logger.warn("Operation: {}", getOperationId().getClass.toString)
        val local = (Parameters.rand.nextInt(100) < Parameters.Locality)
        val initialID = if (local) { ID.currentNode }
                        else { Parameters.rand.nextInt(ID.peerSize) }

        val module: Module = Hyflow.dir.open[Module](oo7setup.getModule_ID(initialID))
        val designRoot: ComplexAssembly = module.getDesignRoot()

        return traverse(designRoot)
    }

    protected def traverse(assembly: Assembly)(implicit txn: InTxn): Int = {
        if (assembly.isInstanceOf[BaseAssembly]) {
            traverse(assembly.asInstanceOf[BaseAssembly])
        }
        else {
            traverse(assembly.asInstanceOf[ComplexAssembly])
        }
    }

    protected def traverse(complexAssembly: ComplexAssembly)(implicit txn: InTxn): Int = {
        val subAssembly_IDs = complexAssembly.getSubAssembly_IDs()
        val numOfSubAssemblies = subAssembly_IDs.size()
        val nextAssembly = Parameters.rand.nextInt(numOfSubAssemblies)

        var subAssemblyNumber = 0
        if (!subAssembly_IDs.isEmpty && !subAssembly_IDs.contains(null)) {
            for (subAssembly_ID <- subAssembly_IDs) {
                if (subAssemblyNumber == nextAssembly) {
                    return traverse(Hyflow.dir.open[Assembly](subAssembly_ID))
                }
                subAssemblyNumber += 1
            }
        }
        else {
            return 0
        }

        throw new RuntimeException("ST1: Size of ComplexAssembly.subAssemblies has changed!")
    }

    protected def traverse(baseAssembly: BaseAssembly)(implicit txn: InTxn): Int = {
        val component_IDs = baseAssembly.getComponent_IDs()
        val numOfComponents = component_IDs.size()
        if (numOfComponents == 0) {
            throw new RuntimeException("ST1: BaseAssembly has no components!")
        }
        val nextComponent = Parameters.rand.nextInt(numOfComponents)

        var componentNumber = 0
        if (!component_IDs.isEmpty && !component_IDs.contains(null)) {
            for (component_ID <- component_IDs) {
                if (componentNumber == nextComponent) {
                    return traverse(Hyflow.dir.open[CompositePart](component_ID))
                }
                componentNumber += 1
            }
        }
        else {
            return 0
        }

        throw new RuntimeException("ST1: Size of BaseAssembly.components has changed!")
    }

    protected def traverse(component: CompositePart)(implicit txn: InTxn): Int = {
        val atomicPart_IDs = component.getPart_IDs()
        val numOfAtomicParts = atomicPart_IDs.size()
        val nextAtomicPart = Parameters.rand.nextInt(numOfAtomicParts)

        var atomicPartNumber = 0
        if (!atomicPart_IDs.bufferList().isEmpty && !atomicPart_IDs.bufferList().contains(null)) {
            for (atomicPart_ID <- atomicPart_IDs.bufferList()) {
                if (atomicPartNumber == nextAtomicPart) {
                    return traverse(Hyflow.dir.open[AtomicPart](atomicPart_ID))
                }
                atomicPartNumber += 1
            }
        }
        else {
            return 0
        }

        throw new RuntimeException("ST1: Illegal size of CompositePart.parts!")
    }

    protected def traverse(atomicPart: AtomicPart)(implicit txn: InTxn): Int = {
        return atomicPart.getX() + atomicPart.getY()
    }

    override def getOperationId(): Operations.Op = Operations.ST1
}