package org.hyflow.benchmarks.stmbench7

import org.hyflow.Hyflow
import org.hyflow.benchmarks._
import org.hyflow.core.util.HyflowConfig
import org.hyflow.benchmarks.stmbench7.backend._
import org.hyflow.benchmarks.stmbench7.parts._
import scala.concurrent.stm._
import com.typesafe.scalalogging.slf4j.Logging
import scala.collection.mutable.ArrayBuffer
import scala.util.Random

// ID Strings for all variable types.
object ID {
    val currentNode = Hyflow.peers.indexOf(Hyflow.mainActor)
    val peerSize = Hyflow.peers.size

    // Backend IDs.
    def BackendFactory(id: Int, node: Int) = "bf-%d:%d".format(id, node)
    def IdPool(id: Int, node: Int) = "ip-%d:%d".format(id, node)
    def LargeSet(owner: String, id: Int, varType: String, node: Int) = (owner + "_" + "ls[" + varType + "]-%d:%d".format(id, node))
    def SmallSet(owner: String, id: Int, varType: String, node: Int) = (owner + "_" + "ss[" + varType + "]-%d:%d".format(id, node))
    def TreeMapIndex(owner: String, id: Int, key: String, value: String, node: Int) = (owner + "_" + "tmi[" + key + ", " + value + "]-%d:%d".format(id, node))

    // Part IDs.
    def Assembly(id: Int, node: Int) = "a-%d:%d".format(id, node)
    def AssemblyBuilder(owner: String, id: Int, node: Int) = (owner + "_" + "ab-%d:%d".format(id, node))
    def AtomicPart(id: Int, node: Int) = "ap-%d:%d".format(id, node)
    def AtomicPartBuilder(owner:String, id: Int, node: Int) = (owner + "_" + "apb-%d:%d".format(id, node))
    def BaseAssembly(id: Int, node: Int) = "ba-%d:%d".format(id, node)
    def ComplexAssembly(id: Int, node: Int) = "ca-%d:%d".format(id, node)
    def CompositePart(id: Int, node: Int) = "cp-%d:%d".format(id, node)
    def CompositePartBuilder(owner: String, id: Int, node: Int) = (owner + "_" + "cpb-%d:%d".format(id, node))
    def Connection(id: Int, node: Int) = "c-%d:%d".format(id, node)
    def DesignObj(id: Int, node: Int) = "do-%d:%d".format(id, node)
    def Document(id: Int, node: Int) = "doc-%d:%d".format(id, node)
    def DocumentBuilder(owner: String, id: Int, node: Int) = (owner + "_" + "docb-%d:%d".format(id, node))
    def Manual(id: Int, node: Int) = "man-%d:%d".format(id, node)
    def ManualBuilder(owner: String, id: Int, node: Int) = (owner + "_" + "manb-%d:%d".format(id, node))
    def Module(id: Int, node: Int) = "mod-%d:%d".format(id, node)
    def ModuleBuilder(owner: String, id: Int, node: Int) = (owner + "_" + "modb-%d:%d".format(id, node))
/*
    // Backend IDs.
    def BackendFactory(id: Int) = "backendFactory-%d".format(id)
    def IdPool(id: Int) = "idPool-%d".format(id)
    def LargeSet(owner: String, id: Int, varType: String) = (owner + "_" + "largeSet[" + varType + "]-%d".format(id))
    def SmallSet(owner: String, id: Int, varType: String) = (owner + "_" + "smallSet[" + varType + "]-%d".format(id))
    def TreeMapIndex(owner: String, id: Int, key: String, value: String) = (owner + "_" + "treeMapIndex[" + key + ", " + value + "]-%d".format(id))

    // Part IDs.
    def Assembly(id: Int) = "assembly-%d".format(id)
    def AssemblyBuilder(owner: String, id: Int) = (owner + "_" + "assemblyBuilder-%d".format(id))
    def AtomicPart(id: Int) = "atomicPart-%d".format(id)
    def AtomicPartBuilder(owner:String, id: Int) = (owner + "_" + "atomicPartBuilder-%d".format(id))
    def BaseAssembly(id: Int) = "baseAssembly-%d".format(id)
    def ComplexAssembly(id: Int) = "complexAssembly-%d".format(id)
    def CompositePart(id: Int) = "compositePart-%d".format(id)
    def CompositePartBuilder(owner: String, id: Int) = (owner + "_" + "compositePartBuilder-%d".format(id))
    def Connection(id: Int) = "connection-%d".format(id)
    def DesignObj(id: Int) = "designObj-%d".format(id)
    def Document(id: Int) = "document-%d".format(id)
    def DocumentBuilder(owner: String, id: Int) = (owner + "_" + "documentBuilder-%d".format(id))
    def Manual(id: Int) = "manual-%d".format(id)
    def ManualBuilder(owner: String, id: Int) = (owner + "_" + "manualBuilder-%d".format(id))
    def Module(id: Int) = "module-%d".format(id)
    def ModuleBuilder(owner: String, id: Int) = (owner + "_" + "moduleBuilder-%d".format(id))
*/
}

// General Parameters for Benchmark.
object Parameters {
    // Locality skew.
    val Locality = HyflowConfig.cfg.getInt("hyflow.workload.stmbench7.locality")

    // Random variables.
    val rand = new scala.util.Random

    // Occurrence percent for operations.
    val TraversalsRatio = 5
    val ShortTraversalsRatio = 40
    val OperationsRatio = 45
    val StructuralModificationsRatio = 10

    // Parameters for data structure. [NOTE: Modified due to lack of memory]
/*
    val NumAtomicPerComp = 100      // Atomic Parts per Composite Part  [Original: 200]
    val NumConnPerAtomic = 3        // Connections per Atomic Part      [Original: 6]
    val DocumentSize = 200          // Document Size                    [Original: 20k]
    val ManualSize = 10000          // Manual Size                      [Original: 1M]
    val NumCompPerModule = 400      // Composite Parts per Module       [Original: 500]
    val NumAssmPerAssm = 3          // Children per Assembly            [Original: 3]
    val NumAssmLevels = 5           // Number of Assembly Levels        [Original: 7]
    val NumCompPerAssm = 3          // Composite Parts per BaseAssembly [Original: 3]
    val NumModules = 1              // Root Modules                     [Original: 1]
*/
    val NumAtomicPerComp = HyflowConfig.cfg.getInt("hyflow.workload.stmbench7.numAtomicPerComp")
    val NumConnPerAtomic = HyflowConfig.cfg.getInt("hyflow.workload.stmbench7.numConnPerAtomic")
    val DocumentSize = HyflowConfig.cfg.getInt("hyflow.workload.stmbench7.documentSize")
    val ManualSize = HyflowConfig.cfg.getInt("hyflow.workload.stmbench7.manualSize")
    val NumCompPerModule = HyflowConfig.cfg.getInt("hyflow.workload.stmbench7.numCompPerModule")
    val NumAssmPerAssm = HyflowConfig.cfg.getInt("hyflow.workload.stmbench7.numAssmPerAssm")
    val NumAssmLevels = HyflowConfig.cfg.getInt("hyflow.workload.stmbench7.numAssmLevels")
    val NumCompPerAssm = HyflowConfig.cfg.getInt("hyflow.workload.stmbench7.numCompPerAssm")
    val NumModules = HyflowConfig.cfg.getInt("hyflow.workload.stmbench7.numModules")

    // Initial OO7-derived parameters.
    //val InitialTotalCompParts = NumModules * NumCompPerModule
    val InitialTotalBaseAssemblies = Math.pow(NumAssmPerAssm, NumAssmLevels - 1).asInstanceOf[Int]
    val InitialTotalComplexAssemblies = ((1 - Math.pow(NumAssmPerAssm, NumAssmLevels - 1).asInstanceOf[Int]) / (1 - NumAssmPerAssm)).asInstanceOf[Int]
    val InitialTotalCompParts = InitialTotalBaseAssemblies // 1-to-1

    // Deviation of the data structure size. [NOTE: Not applicable without Structural Modifications]
    //val MaxCompParts = (1.05 * InitialTotalCompParts).asInstanceOf[Int]
    //val MaxAtomicParts = MaxCompParts * NumAtomicPerComp
    //val MaxBaseAssemblies = (1.05 * InitialTotalBaseAssemblies).asInstanceOf[Int]
    //val MaxComplexAssemblies = (1.05 * InitialTotalComplexAssemblies).asInstanceOf[Int]
    val MaxCompParts = InitialTotalCompParts
    val MaxAtomicParts = MaxCompParts * NumAtomicPerComp
    val MaxBaseAssemblies = InitialTotalBaseAssemblies
    val MaxComplexAssemblies = InitialTotalComplexAssemblies

    // Other OO7 constants.
    val MinModuleDate = 1000
    val MaxModuleDate = 1999
    val MinAssmDate = 1000
    val MaxAssmDate = 1999
    val MinAtomicDate = 1000
    val MaxAtomicDate = 1999
    val MinOldCompDate = 0
    val MaxOldCompDate = 999
    val MinYoungCompDate = 2000
    val MaxYoungCompDate = 2999
    val YoungCompFrac = 10
    val TypeSize = 10
    val NumTypes = 10
    val XYRange = 100000
    val TitleSize = 40
}

// General Benchmark Class.
class STMBench7_Benchmark extends Benchmark with Logging {
    val name = "stmbench7"

    // General settings from the config file.
    val LONG_TRAVERSALS = HyflowConfig.cfg.getBoolean("hyflow.workload.stmbench7.useLongTraversals")
    val STRUCTURE_MODS = HyflowConfig.cfg.getBoolean("hyflow.workload.stmbench7.useStructureMods")
    val PRINT_HIST = HyflowConfig.cfg.getBoolean("hyflow.workload.stmbench7.printHist")
    val WORKLOAD = HyflowConfig.cfg.getDouble("hyflow.workload.readOnlyRatio") * 0.01   // Read-Dominated (90%), Read-Write (60%), Write-Dominated (10%)

    // Probability distribution for the operations.
    val operationCDF = ArrayBuffer[Double]()

    // Setup for the operations.
    val setup = new Setup()
    var operations = ArrayBuffer[OperationExecutor]()

    // Create the CDF for the operations.
    def createProbabilities() {
        // Operation ratios.
        var shortTraversalsRatio = Parameters.ShortTraversalsRatio / 100.0
        var operationsRatio = Parameters.OperationsRatio / 100.0
        var traversalsRatio = if (LONG_TRAVERSALS) { Parameters.TraversalsRatio / 100.0 } else { 0.0 }
        var structuralModificationsRatio = if (STRUCTURE_MODS) { Parameters.StructuralModificationsRatio / 100.0 } else { 0.0 }

        // Read-only ratio.
        val readOnlyOperationsRatio = WORKLOAD
        val updateOperationsRatio = 1.0 - readOnlyOperationsRatio

        // Balanced ratios.
        val sumRatios = traversalsRatio + shortTraversalsRatio + operationsRatio + (structuralModificationsRatio * updateOperationsRatio)
        shortTraversalsRatio /= sumRatios
        operationsRatio /= sumRatios
        traversalsRatio /= sumRatios
        structuralModificationsRatio /= sumRatios

        // Count the number of each type of operation:
        // [TRAVERSAL, TRAVERSAL_RO, SHORT_TRAVERSAL, SHORT_TRAVERSAL_RO,
        //  OPERATION, OPERATION_RO, STRUCTURAL_MODIFICATION]
        val counts = ArrayBuffer[Int](0, 0, 0, 0, 0, 0, 0)
        for (op <- Operations.OperationList) {
            op.opType match {
                case OpType.TRAVERSAL =>               counts(0) += 1
                case OpType.TRAVERSAL_RO =>            counts(1) += 1
                case OpType.SHORT_TRAVERSAL =>         counts(2) += 1
                case OpType.SHORT_TRAVERSAL_RO =>      counts(3) += 1
                case OpType.OPERATION =>               counts(4) += 1
                case OpType.OPERATION_RO =>            counts(5) += 1
                case OpType.STRUCTURAL_MODIFICATION => counts(6) += 1
            }
        }

        // Set up their probabilities properly.
        val prob = ArrayBuffer[Double](0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
        prob(0) = (traversalsRatio * updateOperationsRatio) / counts(0)
        prob(1) = (traversalsRatio * readOnlyOperationsRatio) / counts(1)
        prob(2) = (shortTraversalsRatio * updateOperationsRatio) / counts(2)
        prob(3) = (shortTraversalsRatio * readOnlyOperationsRatio) / counts(3)
        prob(4) = (operationsRatio * updateOperationsRatio) / counts(4)
        prob(5) = (operationsRatio * readOnlyOperationsRatio) / counts(5)
        prob(6) = (structuralModificationsRatio * updateOperationsRatio) / counts(6)

        // Create the cumulative distribution function for all operations.
        var prevProb = 0.0
        for (op <- Operations.OperationList) {
            val currentProb = op.opType match {
                case OpType.TRAVERSAL =>               prob(0)
                case OpType.TRAVERSAL_RO =>            prob(1)
                case OpType.SHORT_TRAVERSAL =>         prob(2)
                case OpType.SHORT_TRAVERSAL_RO =>      prob(3)
                case OpType.OPERATION =>               prob(4)
                case OpType.OPERATION_RO =>            prob(5)
                case OpType.STRUCTURAL_MODIFICATION => prob(6)
            }
            operationCDF += (prevProb + currentProb)
            prevProb += currentProb
        }

        // Normalize the last probability.
        operationCDF(operationCDF.length - 1) = 1.0
    }

    // Start setting up the operations.
    def createOperations(newSetup: Setup): ArrayBuffer[OperationExecutor] = {
        var index = 0
        val operations = ArrayBuffer[OperationExecutor]()
        for (op <- Operations.OperationList) {
            val operationClass = op.opClass
            try {
                val operationConstructor = operationClass.getConstructor(newSetup.getClass)
                val operation = operationConstructor.newInstance(setup)
                operations += OperationExecutorFactory.instance.createOperationExecutor(operation)
                if (!operation.getOperationId().opClass.equals(operationClass)) {
                    throw new RuntimeException
                }
                index += 1
            } catch {
                case _: Throwable =>
                    logger.error("Operation creation failure: index {}", index.toString)
                    exit(0)
            }
        }
        operations
    }

    def setFactoryInstances() {
        DesignObjFactory.setInstance(new DesignObjFactory())
        BackendFactory.setInstance(new BackendFactory())
        OperationExecutorFactory.setInstance(new OperationExecutorFactory())
    }

    def benchInit() {
        logger.error("STMBench7: Starting initialization.")
        setFactoryInstances()
        createProbabilities()

        setup.initialize()

        operations = createOperations(setup)
        logger.error("STMBench7: Finished initialization.")

    }

    def benchIter() {
        val whichOperation = Parameters.rand.nextDouble()
        var operationNumber = 0
        while (whichOperation >= operationCDF(operationNumber)) {
            operationNumber += 1
        }
        val currentExecutor = operations(operationNumber)

        try {
            //atomic { implicit txn =>
                val result = currentExecutor.execute()
                //val nodeNum = (ID.currentNode + 1) % 19
                //logger.error("Opening on node {}", nodeNum.toString)

/*
                logger.error("Opening CompositeParts")
                for (i <- 1 to Parameters.MaxCompParts) {
                    Hyflow.dir.open[CompositePart](ID.CompositePart(i, nodeNum))
                }

                logger.error("Opening AtomicParts")
                for (i <- 1 to Parameters.MaxAtomicParts) {
                    Hyflow.dir.open[AtomicPart](ID.AtomicPart(i, nodeNum))
                }

                logger.error("Opening BaseAssemblies")
                for (i <- 1 to Parameters.MaxBaseAssemblies) {
                    Hyflow.dir.open[BaseAssembly](ID.BaseAssembly(i, nodeNum))
                }

                logger.error("Opening ComplexAssemblies")
                for (i <- 1 to Parameters.MaxComplexAssemblies) {
                    Hyflow.dir.open[ComplexAssembly](ID.ComplexAssembly(i, nodeNum))
                }
*/
/*
                var x = setup.getAtomicPartIdIndex_ID(nodeNum)
                logger.error("Opening Atomic Part Id Index {}", x)
                val a = Hyflow.dir.open[Index[Integer, String]](x)
                for (item <- a.valueList()) {
                    logger.warn("Opening Atomic Part {}", item)
                    val aaa = Hyflow.dir.open[AtomicPart](item)
                }

                x = setup.getAtomicPartBuildDateIndex_ID(nodeNum)
                logger.error("Opening Atomic Part Build Date Index {}", x)
                val b = Hyflow.dir.open[Index[Integer, String]](x)
                for (item <- b.valueList()) {
                    logger.warn("Opening Atomic Part Set {}", item)
                    val bbb = Hyflow.dir.open[LargeSet[String]](item)
                    for (item2 <- bbb.bufferList()) {
                        logger.warn("Opening Atomic Part {}", item2)
                        val bbbbb = Hyflow.dir.open[AtomicPart](item2)
                    }
                }

                //x = setup.getDocumentTitleIndex_ID(nodeNum)
                x = setup.getCompositePartIdIndex_ID(nodeNum)
                logger.error("Opening Composite Part Id Index {}", x)
                val d = Hyflow.dir.open[Index[Integer, String]](x)
                for (item <- d.valueList()) {
                    logger.warn("Opening Composite Part {}", item)
                    val ddd = Hyflow.dir.open[CompositePart](item)
                }

                x = setup.getBaseAssemblyIdIndex_ID(nodeNum)
                logger.error("Opening Base Assembly Id Index {}", x)
                val e = Hyflow.dir.open[Index[Integer, String]](x)
                for (item <- e.valueList()) {
                    logger.warn("Opening Base Assembly {}", item)
                    val eee = Hyflow.dir.open[BaseAssembly](item)
                }

                x = setup.getComplexAssemblyIdIndex_ID(nodeNum)
                logger.error("Opening Complex Assembly Id Index {}", x)
                val f = Hyflow.dir.open[Index[Integer, String]](x)
                for (item <- f.valueList()) {
                    logger.warn("Opening Complex Assembly {}", item)
                    val fff = Hyflow.dir.open[ComplexAssembly](item)
                }
*/
           // }
        }
        catch {
            case x: Throwable =>
                logger.error("Random Error in Iteration: {}.", x.toString)
                throw x
                //exit(0)
        }

        //exit(0)
    }

    def benchCheck() = true
}