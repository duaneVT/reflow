package org.hyflow.benchmarks.stmbench7.operations

import org.hyflow.Hyflow
import org.hyflow.benchmarks.stmbench7.{BaseOperation, Operations}
import org.hyflow.benchmarks.stmbench7.parts._

import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.HashSet
import scala.concurrent.stm._

// Read-only. Can be parallel.
/*
class Traversal1(oo7setup: Setup) extends BaseOperation {
    override def performOperation()(implicit txn: InTxn): Int = {
        val local = (Parameters.rand.nextInt(100) < Parameters.Locality)
        val initialID = if (local) { ID.currentNode }
                        else { Parameters.rand.nextInt(ID.peerSize) }

        val module: Module = Hyflow.dir.open[Module](oo7setup.getModule_ID(initialID))
        val designRoot: ComplexAssembly = module.getDesignRoot()

        var partsVisited = 0
        for (assembly_ID <- designRoot.getSubAssembly_IDs()) {
            atomic { implicit txn =>
                val assembly: Assembly = Hyflow.dir.open[Assembly](assembly_ID)
                partsVisited += traverse(assembly)
            }
        }
        partsVisited
    }

    protected def traverse(assembly: Assembly)(implicit txn: InTxn): Int = {
        if (assembly.isInstanceOf[BaseAssembly]) {
            traverse(assembly.asInstanceOf[BaseAssembly])
        }
        else {
            traverse(assembly.asInstanceOf[ComplexAssembly])
        }
    }

    protected def traverse(complexAssembly: ComplexAssembly)(implicit txn: InTxn): Int = {
        var partsVisited = 0
        for (assembly_ID <- complexAssembly.getSubAssembly_IDs()) {
            val assembly: Assembly = Hyflow.dir.open[Assembly](assembly_ID)
            partsVisited += traverse(assembly)
        }
        partsVisited
    }

    protected def traverse(baseAssembly: BaseAssembly)(implicit txn: InTxn): Int = {
        var partsVisited = 0
        for (component_ID <- baseAssembly.getComponent_IDs()) {
            val component: CompositePart = Hyflow.dir.open[CompositePart](component_ID)
            partsVisited += traverse(component)
        }
        partsVisited
    }

    protected def traverse(component: CompositePart)(implicit txn: InTxn): Int = {
        val rootPart: AtomicPart = component.getRootPart()
        val setOfVisitedPartIds = new HashSet[AtomicPart]()
        traverse(rootPart, setOfVisitedPartIds)
    }

    protected def traverse(part: AtomicPart, setOfVisitedPartIds: HashSet[AtomicPart])(implicit txn: InTxn): Int = {
        if (part == null) { return 0 }
        if (setOfVisitedPartIds.contains(part)) { return 0 }

        var result = performOperationInAtomicPart(part, setOfVisitedPartIds)
        setOfVisitedPartIds.add(part)
        for (connection <- part.getToConnections()) {
            result += traverse(connection.getDestination(), setOfVisitedPartIds)
        }
        result
    }

    protected def performOperationInAtomicPart(part: AtomicPart, setOfVisitedPartIds: HashSet[AtomicPart])(implicit txn: InTxn): Int = {
        part.nullOperation()
        return 1
    }

    override def getOperationId(): Operations.Op = Operations.T1
}
*/