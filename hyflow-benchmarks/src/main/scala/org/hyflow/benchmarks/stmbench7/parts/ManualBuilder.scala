package org.hyflow.benchmarks.stmbench7.parts

import org.hyflow._
import org.hyflow.benchmarks.stmbench7.{ID, Parameters}
import org.hyflow.benchmarks.stmbench7.backend._
import scala.concurrent.stm._

class ManualBuilder(val owner: String, val id: Int, val node: Int, val varType: String = null) extends DesignObjBuilder {
    override def _id: String = ID.ManualBuilder(owner, id, node)
    private val idPool = field[IdPool](null.asInstanceOf[IdPool])

    def init()(implicit txn: InTxn) {
        idPool() = BackendFactory.instance.createIdPool(Parameters.NumModules)
    }

    def createManual(moduleId: Int)(implicit txn: InTxn): Manual = {
        val manualId = idPool().getId()
        val title = "Manual for module #" + moduleId
        val text = createText(Parameters.ManualSize, "I am the manual for module #" + moduleId + "\n")

        return Hyflow.dir.open[Manual](designObjFactory.createManual(manualId, title, text))
    }
}