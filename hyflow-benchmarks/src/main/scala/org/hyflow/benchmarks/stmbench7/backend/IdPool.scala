package org.hyflow.benchmarks.stmbench7.backend

import scala.collection.mutable.ArrayBuffer

class IdPool {
    var idPool: ArrayBuffer[Int] = ArrayBuffer[Int]()

    def this(maxNumberOfIds: Int) {
        this()
        idPool = new ArrayBuffer[Int]()
        for (i <- 1 to maxNumberOfIds) {
            idPool += i
        }
    }

    def this(otherPool: ArrayBuffer[Int]) {
        this()
        this.idPool = otherPool
    }

    def getId(): Int = {
        if (idPool.size <= 0) {
            throw new RuntimeException("idPool empty!")
        }
        val id = idPool.remove(0)
        if (id == null.asInstanceOf[Int]) {
            throw new RuntimeException("id was null!")
        }
        id
    }

    def putUnusedId(id: Int) {
        idPool += id
    }

    override def toString() = {
        "Iterator too large to print"
    }
}