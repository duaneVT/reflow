package org.hyflow.benchmarks.stmbench7.parts

import org.hyflow.benchmarks.stmbench7.{ID}
import scala.concurrent.stm._

class Assembly(val id: Int, val varType: String, val initBuildDate: Int, val initModule: Module, val initSuperAssembly: ComplexAssembly, val node: Int) extends DesignObj {
    override def _id = ID.Assembly(id, node)

    protected val buildDate = field[Int](null.asInstanceOf[Int])                 // Int
    protected val superAssembly_ID = field[String](null.asInstanceOf[String])    // ComplexAssembly
    protected val module_ID = field[String](null.asInstanceOf[String])           // Module

    def getBuildDate()(implicit txn: InTxn): Int = {
        buildDate()
    }
    def setBuildDate(value: Int)(implicit txn: InTxn) {
        buildDate() = value
    }
    def getSuperAssembly_ID()(implicit txn: InTxn): String = {
        superAssembly_ID()
    }
    def getModule_ID()(implicit txn: InTxn): String = {
        module_ID()
    }

    def clearPointers()(implicit txn: InTxn) {
        superAssembly_ID() = null
        module_ID() = null
    }

    def init()(implicit txn: InTxn) {
        buildDate() = initBuildDate
        if (initSuperAssembly != null) {
            superAssembly_ID() = initSuperAssembly._id
        }
        module_ID() = initModule._id
    }

    override def hashCode(): Int = id

    override def equals(obj: scala.Any): Boolean = {
        if (!obj.isInstanceOf[Assembly]) { return false }
        else { return super.equals(obj) }
    }

    override def toString()(implicit txn: InTxn): String = {
        super.toString() + ", buildDate=" + buildDate() + ", superAssembly=[" + superAssembly_ID() + "]"
    }
}