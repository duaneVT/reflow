package org.hyflow.benchmarks.stmbench7.operations

import org.hyflow.Hyflow
import org.hyflow.benchmarks.stmbench7.{BaseOperation, ID, Operations, Parameters}
import org.hyflow.benchmarks.stmbench7.backend._
import org.hyflow.benchmarks.stmbench7.parts._
import scala.concurrent.stm._
import scala.util.Random
import com.typesafe.scalalogging.slf4j.Logging

// Read-only. Can be parallel.
class Operation6(oo7setup: Setup) extends BaseOperation {
    override def performOperation()(implicit txn: InTxn): Int = {
        logger.warn("Operation: {}", getOperationId().getClass.toString)
        val local = (Parameters.rand.nextInt(100) < Parameters.Locality)
        val initialID = if (local) { ID.currentNode }
                        else { Parameters.rand.nextInt(ID.peerSize) }

     /*   var done = false
        var complexAssemblyIdIndex = null.asInstanceOf[Index[Integer, String]]
        while (!done) {
            try {*/
                val complexAssemblyIdIndex = Hyflow.dir.open[Index[Integer, String]](oo7setup.getComplexAssemblyIdIndex_ID(initialID))
      /*      } catch {
                    case x: java.util.NoSuchElementException =>
                        logger.trace("Node {} missing Index.", initialID.toString)
                    case y: Throwable =>
                        throw y
            }
        }*/

        val keySize = complexAssemblyIdIndex.getKeys.size

        var complexAssemblyId = Parameters.rand.nextInt(Parameters.MaxComplexAssemblies) + 1
        while (complexAssemblyId >= keySize) {
            complexAssemblyId = Parameters.rand.nextInt(Parameters.MaxComplexAssemblies) + 1
        }

        val complexAssembly_ID = complexAssemblyIdIndex.get(complexAssemblyId)
        if (complexAssembly_ID == null) {
            throw new RuntimeException("OP6: ComplexAssembly was null!")
        }

        /*done = false
        var complexAssembly = null.asInstanceOf[ComplexAssembly]
        while (!done) {
            try {*/
                val complexAssembly = Hyflow.dir.open[ComplexAssembly](complexAssembly_ID)
         /*   } catch {
                    case x: java.util.NoSuchElementException =>
                        logger.trace("Node {} missing Complex Assembly {}.", initialID.toString, complexAssembly_ID)
                    case y: Throwable =>
                        throw y
            }
        }*/

        val superAssembly_ID = complexAssembly.getSuperAssembly_ID()
        if (superAssembly_ID == null) {
            performOperationInComplexAssembly(complexAssembly)
            return 1
        }

       /* done = false
        var superAssembly = null.asInstanceOf[ComplexAssembly]
        while (!done) {
            try {*/
                val superAssembly = Hyflow.dir.open[ComplexAssembly](superAssembly_ID)
         /*   } catch {
                    case x: java.util.NoSuchElementException =>
                        logger.trace("Node {} missing Complex Assembly {}.", initialID.toString, superAssembly_ID)
                    case y: Throwable =>
                        throw y
            }
        }*/

        for (siblingAssembly_ID <- superAssembly.getSubAssembly_IDs()) {
            var count = 0
            atomic { implicit txn: InTxn =>
             /*   var done = false
                while (!done) {
                    try {*/
                        val siblingAssembly = Hyflow.dir.open[ComplexAssembly](siblingAssembly_ID)
                        performOperationInComplexAssembly(siblingAssembly)
                        count += 1
              /*      } catch {
                            case x: java.util.NoSuchElementException =>
                                logger.trace("Node {} missing Complex Assembly {}.", initialID.toString, siblingAssembly_ID)
                            case y: Throwable =>
                                throw y
                    }
                }*/
            }
        }
        return 0
    }

    protected def performOperationInComplexAssembly(assembly: ComplexAssembly)(implicit txn: InTxn) {
        assembly.nullOperation()
    }

    override def getOperationId(): Operations.Op = Operations.OP6
}