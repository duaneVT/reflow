package org.hyflow.benchmarks.stmbench7.parts

import org.hyflow._
import org.hyflow.benchmarks.stmbench7.backend._
import org.hyflow.benchmarks.stmbench7.{ID, Parameters}
import org.hyflow.benchmarks.stmbench7.parts._

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.stm._
import scala.util.Random

class CompositePartBuilder(val owner: String, val id: Int, val node: Int, val varType: String = null) extends DesignObjBuilder {
    override def _id: String = ID.CompositePartBuilder(owner, id, node)

    private val idPool = field[IdPool](null.asInstanceOf[IdPool])                    // IdPool
    private val compositePartIdIndex_ID = field[String](null.asInstanceOf[String])   // Index[Integer, CompositePart_ID] -> CompositePart
    private val documentBuilder_ID = field[String](null.asInstanceOf[String])        // DocumentBuilder
    private val atomicPartBuilder_ID = field[String](null.asInstanceOf[String])      // AtomicPartBuilder

    def init(compositePartIdIndex_ID: String, documentTitleIndex_ID: String, partIdIndex_ID: String, partBuildDateIndex_ID: String)(implicit txn: InTxn) {
        this.compositePartIdIndex_ID() = compositePartIdIndex_ID

        val newDocumentBuilder = new DocumentBuilder(owner, id, node)
        newDocumentBuilder.init(documentTitleIndex_ID)
        this.documentBuilder_ID() = newDocumentBuilder._id

        val newAtomicPartBuilder = new AtomicPartBuilder(owner, id, node)
        newAtomicPartBuilder.init(partIdIndex_ID, partBuildDateIndex_ID)
        this.atomicPartBuilder_ID() = newAtomicPartBuilder._id

        this.idPool() = BackendFactory.instance.createIdPool(Parameters.MaxCompParts)
    }

    // Creates a component (composite part) with the documentation and a graph of atomic parts and updates all the relevant indexes.
    def createAndRegisterCompositePart()(implicit txn: InTxn): CompositePart = {
        val nextId = idPool().getId()
        val varType = createType()
        var buildDate = 0

        if (Parameters.rand.nextInt(100) < Parameters.YoungCompFrac) {
            buildDate = createBuildDate(Parameters.MinYoungCompDate, Parameters.MaxYoungCompDate)
        }
        else {
            buildDate = createBuildDate(Parameters.MinOldCompDate, Parameters.MaxOldCompDate)
        }

        var documentation: Document = null
        var documentBuilder: DocumentBuilder = null
        val parts: ArrayBuffer[AtomicPart] = ArrayBuffer.fill(Parameters.NumAtomicPerComp)(null.asInstanceOf[AtomicPart])

        try {
            documentBuilder = Hyflow.dir.open[DocumentBuilder](documentBuilder_ID())
            documentation = documentBuilder.createAndRegisterDocument(nextId)
            createAtomicParts(parts)
        }
        catch {
            case x: Throwable =>
                ///println("RegisterCompositePart: We failed at CompositePart " + nextId)
                val atomicPartBuilder: AtomicPartBuilder = Hyflow.dir.open[AtomicPartBuilder](atomicPartBuilder_ID())
                if (documentation != null) {
                    documentBuilder.unregisterAndRecycleDocument(documentation)
                }
                for (part <- parts) {
                    if (part != null) {
                        atomicPartBuilder.unregisterAndRecycleAtomicPart(part)
                    }
                }
                idPool().putUnusedId(nextId)
                throw x
        }

        createConnections(parts)
        val component_ID: String = designObjFactory.createCompositePart(nextId, varType, buildDate, documentation)
        val component = Hyflow.dir.open[CompositePart](component_ID)

        for (part <- parts) {
            component.addPart(part)
        }
        Hyflow.dir.open[Index[Integer, String]](compositePartIdIndex_ID()).put(nextId, component._id)

        return component
    }

    def unregisterAndRecycleCompositePart(compositePart: CompositePart)(implicit txn: InTxn) {
        val nextId = compositePart.getId()
        Hyflow.dir.open[Index[Integer, String]](compositePartIdIndex_ID()).remove(nextId)
        Hyflow.dir.open[DocumentBuilder](documentBuilder_ID()).unregisterAndRecycleDocument(compositePart.getDocumentation())

        val atomicPartBuilder: AtomicPartBuilder = Hyflow.dir.open[AtomicPartBuilder](atomicPartBuilder_ID())
        val atomicPartSet: LargeSet[String] = compositePart.getPart_IDs()
        for (atomicPart_ID <- atomicPartSet.bufferList()) {
            atomicPartBuilder.unregisterAndRecycleAtomicPart(Hyflow.dir.open[AtomicPart](atomicPart_ID))
        }

        val usedIn_IDs: ImmutableCollection[String] = compositePart.getUsedIn_IDs()
        for (ownerID <- usedIn_IDs) {
            val ownerAssembly: BaseAssembly = Hyflow.dir.open[BaseAssembly](ownerID)
            while (ownerAssembly.removeComponent(compositePart)) {}
        }

        compositePart.clearPointers()
        idPool().putUnusedId(nextId)
    }

    // Create all atomic parts of a given composite part.
    private def createAtomicParts(parts: ArrayBuffer[AtomicPart])(implicit txn: InTxn) {
        val atomicPartBuilder: AtomicPartBuilder = Hyflow.dir.open[AtomicPartBuilder](atomicPartBuilder_ID())
        for (partNum <- 0 to (Parameters.NumAtomicPerComp - 1)) {
            val part: AtomicPart = atomicPartBuilder.createAndRegisterAtomicPart()
            parts(partNum) = part
        }
    }

    // Create connections between the parts.
    private def createConnections(parts: ArrayBuffer[AtomicPart])(implicit txn: InTxn) {
        // First, make all atomic parts be connected in a ring (so that the resulting graph is fully connected).
        for (partNum <- 0 to (Parameters.NumAtomicPerComp - 1)) {
            val connectTo = (partNum + 1) % Parameters.NumAtomicPerComp
            parts(partNum).connectTo(parts(connectTo), createType(), Parameters.rand.nextInt(Parameters.XYRange) + 1)
        }

        // Then add other connections randomly, taking into account the NumConnPerAtomic parameter.
        // The procedure is non-deterministic but it should eventually terminate.
        for (partNum <- 0 to (Parameters.NumAtomicPerComp - 1)) {
            val currentPart: AtomicPart = parts(partNum)
            while (currentPart.getNumToConnections() < Parameters.NumConnPerAtomic) {
                val connectTo = Parameters.rand.nextInt(Parameters.NumAtomicPerComp)
                parts(partNum).connectTo(parts(connectTo), createType(), Parameters.rand.nextInt(Parameters.XYRange) + 1)
            }
        }
    }
}