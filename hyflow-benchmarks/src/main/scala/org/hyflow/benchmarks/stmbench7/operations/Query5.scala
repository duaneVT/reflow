package org.hyflow.benchmarks.stmbench7.operations

import org.hyflow.Hyflow
import org.hyflow.benchmarks.stmbench7.{BaseOperation, ID, Operations, Parameters}
import org.hyflow.benchmarks.stmbench7.backend._
import org.hyflow.benchmarks.stmbench7.parts._
import scala.concurrent.stm._
import scala.util.Random

// Read-only. Can be parallel.
class Query5(oo7setup: Setup) extends BaseOperation {
    override def performOperation()(implicit txn: InTxn): Int = {
        logger.warn("Operation: {}", getOperationId().getClass.toString)
        val local = (Parameters.rand.nextInt(100) < Parameters.Locality)
        val initialID = if (local) { ID.currentNode }
                        else { Parameters.rand.nextInt(ID.peerSize) }

        val baseAssemblyIdIndex = Hyflow.dir.open[Index[Integer, String]](oo7setup.getBaseAssemblyIdIndex_ID(initialID))

        var result = 0
        if (!baseAssemblyIdIndex.valueList().isEmpty && !baseAssemblyIdIndex.valueList().contains(null)) {
            for (assembly_ID <- baseAssemblyIdIndex.valueList()) {
                atomic { implicit txn: InTxn =>
                    result += checkBaseAssembly(Hyflow.dir.open[BaseAssembly](assembly_ID))
                }
            }
        }

        return result
    }

    protected def checkBaseAssembly(assembly: BaseAssembly)(implicit txn: InTxn): Int = {
        val assmBuildDate = assembly.getBuildDate()
        if (!assembly.getComponent_IDs().isEmpty && !assembly.getComponent_IDs().contains(null)) {
            for (part_ID <- assembly.getComponent_IDs()) {
                if (Hyflow.dir.open[CompositePart](part_ID).getBuildDate() > assmBuildDate) {
                    assembly.nullOperation()
                    return 1
                }
            }
        }

        return 0
    }

    override def getOperationId(): Operations.Op = Operations.ST5
}