package org.hyflow.benchmarks.stmbench7.parts

import org.hyflow._
import org.hyflow.benchmarks.stmbench7.{ID, Parameters}
import org.hyflow.benchmarks.stmbench7.backend._

import scala.concurrent.stm._

class DocumentBuilder(val owner: String, val id: Int, val node: Int, val varType: String = null) extends DesignObjBuilder {
    override def _id: String = ID.DocumentBuilder(owner, id, node)

    private val idPool = field[IdPool](null.asInstanceOf[IdPool])                  // IdPool
    private val documentTitleIndex_ID = field[String](null.asInstanceOf[String])   // Index[String, Document_ID] -> Document

    def init(documentTitleIndex_ID: String)(implicit txn: InTxn) {
        this.documentTitleIndex_ID() = documentTitleIndex_ID
        this.idPool() = BackendFactory.instance.createIdPool(Parameters.MaxCompParts)
    }

    def createAndRegisterDocument(compositePartId: Int)(implicit txn: InTxn): Document = {
        val docId = idPool().getId()
        val docTitle = "Composite Part #" + compositePartId
        val docText = createText(Parameters.DocumentSize, "I am the documentation for Composite Part #" + compositePartId + "\n")

        val documentation_ID: String = designObjFactory.createDocument(docId, docTitle, docText)
        val documentTitleIndex: Index[String, String] = Hyflow.dir.open[Index[String, String]](documentTitleIndex_ID())
        documentTitleIndex.put(docTitle, documentation_ID)

        return Hyflow.dir.open[Document](documentation_ID)
    }

    def unregisterAndRecycleDocument(document: Document)(implicit txn: InTxn) {
        document.setPart(null.asInstanceOf[CompositePart])
        val documentTitleIndex: Index[String, String] = Hyflow.dir.open[Index[String, String]](documentTitleIndex_ID())
        documentTitleIndex.remove(document.getTitle())
        idPool().putUnusedId(document.getDocumentId())
    }
}