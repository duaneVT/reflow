package org.hyflow.benchmarks.stmbench7.parts

import org.hyflow._
import org.hyflow.benchmarks.stmbench7.{ID}
import org.hyflow.benchmarks.stmbench7.backend._

import scala.collection.mutable.ListBuffer
import scala.concurrent.stm._

class CompositePart(val id: Int, val varType: String, val initBuildDate: Int, val initDocumentation: Document, val node: Int) extends DesignObj {
    override def _id: String = ID.CompositePart(id, node)

    protected val buildDate = field[Int](null.asInstanceOf[Int])                                 // Int
    private val documentation_ID = field[String](null.asInstanceOf[String])                      // Document
    private val usedIn_IDs = field[ListBuffer[String]](null.asInstanceOf[ListBuffer[String]])    // ListBuffer[BaseAssembly_ID] -> BaseAssembly
    private val partSet_ID = field[String](null.asInstanceOf[String])                            // LargeSet_ID -> LargeSet[AtomicPart_ID] -> AtomicPart
    private val rootPart_ID = field[String](null.asInstanceOf[String])                           // AtomicPart

    def getUsedIn_IDs()(implicit txn: InTxn): ImmutableCollection[String] = {
        new ImmutableView[String](usedIn_IDs().toList)
    }
    def getRootPart()(implicit txn: InTxn): AtomicPart = Hyflow.dir.open[AtomicPart](rootPart_ID())
    def getDocumentation()(implicit txn: InTxn): Document = Hyflow.dir.open[Document](documentation_ID())
    def getPart_IDs()(implicit txn: InTxn): LargeSet[String] = Hyflow.dir.open[LargeSet[String]](partSet_ID())
    def getBuildDate()(implicit txn: InTxn): Int = {
        buildDate()
    }
    def setBuildDate(value: Int)(implicit txn: InTxn) {
        buildDate() = value
    }

    def clearPointers()(implicit txn: InTxn) {
        documentation_ID() = null.asInstanceOf[String]
        partSet_ID() = null.asInstanceOf[String]
        rootPart_ID() = null.asInstanceOf[String]
        usedIn_IDs().clear()
    }

    def init()(implicit txn: InTxn) {
        buildDate() = initBuildDate
        documentation_ID() = initDocumentation._id
        usedIn_IDs() = new ListBuffer[String]()
        Hyflow.dir.open[Document](documentation_ID()).setPart(this)
        partSet_ID() = BackendFactory.instance.createLargeSet[String](_id, 0, "s", node)
    }

    def addAssembly(assembly: BaseAssembly)(implicit txn: InTxn) {
        usedIn_IDs() += assembly._id
    }

    def addPart(part: AtomicPart)(implicit txn: InTxn): Boolean = {
        val part_IDs = Hyflow.dir.open[LargeSet[String]](partSet_ID())
        val notAddedBefore = part_IDs.add(part._id)
        if (!notAddedBefore) {
            return false
        }
        part.setCompositePart(this)
        if (rootPart_ID() == null) {
            rootPart_ID() = part._id
        }
        return true
    }

    def setRootPart(part: AtomicPart)(implicit txn: InTxn) {
        rootPart_ID() = part._id
    }

    def removeAssembly(assembly: BaseAssembly)(implicit txn: InTxn) {
        if (usedIn_IDs().contains(assembly._id)) {
            usedIn_IDs() -= assembly._id
        }
    }

    override def equals(obj: scala.Any): Boolean = {
        if (!obj.isInstanceOf[CompositePart]) {
            return false
        }
        return super.equals(obj)
    }

    def clone()(implicit txn: InTxn): Object = {
        val clone = super.clone().asInstanceOf[CompositePart]
        clone.usedIn_IDs() = usedIn_IDs()

        return clone
    }
}