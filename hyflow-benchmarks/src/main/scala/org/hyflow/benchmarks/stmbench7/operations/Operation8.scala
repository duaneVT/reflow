package org.hyflow.benchmarks.stmbench7.operations

import org.hyflow.Hyflow
import org.hyflow.benchmarks.stmbench7.{BaseOperation, ID, Operations, Parameters}
import org.hyflow.benchmarks.stmbench7.backend._
import org.hyflow.benchmarks.stmbench7.parts._
import scala.concurrent.stm._
import scala.util.Random

// Read-only. Can be parallel.
class Operation8(oo7setup: Setup) extends BaseOperation {
    override def performOperation()(implicit txn: InTxn): Int = {
        logger.warn("Operation: {}", getOperationId().getClass.toString)
        val local = (Parameters.rand.nextInt(100) < Parameters.Locality)
        val initialID = if (local) { ID.currentNode }
                        else { Parameters.rand.nextInt(ID.peerSize) }

        val baseAssemblyIdIndex = Hyflow.dir.open[Index[Integer, String]](oo7setup.getBaseAssemblyIdIndex_ID(initialID))
        val keySize = baseAssemblyIdIndex.getKeys.size

        var baseAssemblyId = Parameters.rand.nextInt(Parameters.MaxBaseAssemblies) + 1
        while (baseAssemblyId >= keySize) {
            baseAssemblyId = Parameters.rand.nextInt(Parameters.MaxBaseAssemblies) + 1
        }

        val baseAssembly_ID = baseAssemblyIdIndex.get(baseAssemblyId)
        if (baseAssembly_ID == null) {
            throw new RuntimeException("OP8: BaseAssembly is null!")
        }

        val baseAssembly = Hyflow.dir.open[BaseAssembly](baseAssembly_ID)

        var count = 0
        for (component_ID <- baseAssembly.getComponent_IDs()) {
            atomic { implicit txn: InTxn =>
                val component = Hyflow.dir.open[CompositePart](component_ID)
                performOperationInComponent(component)
                count += 1
            }
        }
        return count
    }

    protected def performOperationInComponent(component: CompositePart)(implicit txn: InTxn) {
        component.nullOperation()
    }

    override def getOperationId(): Operations.Op = Operations.OP8
}