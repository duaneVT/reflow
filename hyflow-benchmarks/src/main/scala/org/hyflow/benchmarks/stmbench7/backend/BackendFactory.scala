package org.hyflow.benchmarks.stmbench7.backend

import org.hyflow.benchmarks.stmbench7.backend._
import scala.concurrent.stm._

object BackendFactory {
    var instance: BackendFactory = null
    def setInstance(newInstance: BackendFactory) { instance = newInstance }
}

class BackendFactory {
    def createLargeSet[E <: Comparable[E]](owner: String, id: Int, varType: String, node: Int)(implicit txn: InTxn): String = {
        val newLargeSet = new LargeSet[E](owner, id, varType, node)
        newLargeSet.initBuffer()
        newLargeSet._id
    }

    def createIndex[K <: Comparable[K], V](owner: String, id: Int, keyType: String, valueType: String, node: Int)(implicit txn: InTxn): String = {
        val newIndex = new TreeMapIndex[K, V](owner, id, keyType, valueType, node)
        newIndex.initIndex()
        newIndex._id
    }

    def createIdPool(maxNumberOfIds: Int) = {
        new IdPool(maxNumberOfIds)
    }
}