package org.hyflow.benchmarks.stmbench7.operations

import org.hyflow.Hyflow
import org.hyflow.benchmarks.stmbench7.{BaseOperation, Operations}
import org.hyflow.benchmarks.stmbench7.parts._
import scala.concurrent.stm._

// Read-only. Single-threaded.
class ShortTraversal2(oo7setup: Setup) extends ShortTraversal1(oo7setup) {
    override protected def traverse(component: CompositePart)(implicit txn: InTxn): Int = {
        val documentation = component.getDocumentation()
        return traverse(documentation)
    }

    protected def traverse(documentation: Document)(implicit txn: InTxn): Int = {
        return documentation.searchText('I')
    }

    override protected def traverse(atomicPart: AtomicPart)(implicit txn: InTxn): Int = {
        throw new RuntimeException("ST2: Unexpected call to traverse(AtomicPart)!")
    }

    override def getOperationId(): Operations.Op = Operations.ST2
}