package org.hyflow.benchmarks.stmbench7.operations

import org.hyflow.Hyflow
import org.hyflow.benchmarks.stmbench7.{BaseOperation, Operations}
import org.hyflow.benchmarks.stmbench7.parts._
import scala.concurrent.stm._

// Write. Can be parallel.
class ShortTraversal10(oo7setup: Setup) extends ShortTraversal9(oo7setup) {
    override protected def performOperationInAtomicPart(part: AtomicPart)(implicit txn: InTxn): Int = {
        part.swapXY()
        return 1
    }

    override def getOperationId(): Operations.Op = Operations.ST10
}