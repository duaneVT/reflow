package org.hyflow.benchmarks.stmbench7.parts

import org.hyflow._
import org.hyflow.benchmarks.stmbench7.{ID, Parameters}
import org.hyflow.benchmarks.stmbench7.backend._

import scala.concurrent.stm._

// Used to create and destroy assembly elements while maintaining the consistency of the data structure and the indexes.
class AssemblyBuilder(val owner: String, val id: Int, val node: Int, val varType: String = null) extends DesignObjBuilder {
    override def _id: String = ID.AssemblyBuilder(owner, id, node)

    private val baseAssemblyIdPool = field[IdPool](null.asInstanceOf[IdPool])          // IdPool
    private val complexAssemblyIdPool = field[IdPool](null.asInstanceOf[IdPool])       // IdPool
    private val baseAssemblyIdIndex_ID = field[String](null.asInstanceOf[String])      // Index[Integer, BaseAssembly_ID] -> BaseAssembly
    private val complexAssemblyIdIndex_ID = field[String](null.asInstanceOf[String])   // Index[Integer, ComplexAssembly_ID] -> ComplexAssembly

    def init(baseAssemblyIdIndex_ID: String, complexAssemblyIdIndex_ID: String)(implicit txn: InTxn) {
        this.baseAssemblyIdIndex_ID() = baseAssemblyIdIndex_ID
        this.complexAssemblyIdIndex_ID() = complexAssemblyIdIndex_ID
        this.baseAssemblyIdPool() = BackendFactory.instance.createIdPool(Parameters.MaxBaseAssemblies)
        this.complexAssemblyIdPool() = BackendFactory.instance.createIdPool(Parameters.MaxComplexAssemblies)
    }

    def createAndRegisterAssembly(module: Module, superAssembly: ComplexAssembly)(implicit txn: InTxn): Assembly = {
        if ((superAssembly == null) || (superAssembly.getLevel() > 2)) {
            return createAndRegisterComplexAssembly(module, superAssembly)
        }
        return createAndRegisterBaseAssembly(module, superAssembly)
    }

    def unregisterAndRecycleAssembly(assembly: Assembly)(implicit txn: InTxn) {
        if (assembly.isInstanceOf[ComplexAssembly]) {
            unregisterAndRecycleComplexAssembly(assembly.asInstanceOf[ComplexAssembly])
        }
        else {
            unregisterAndRecycleBaseAssembly(assembly.asInstanceOf[BaseAssembly])
        }
    }

    def unregisterAndRecycleBaseAssembly(baseAssembly: BaseAssembly)(implicit txn: InTxn) {
        val baseAssemblyId: Int = baseAssembly.getId()
        Hyflow.dir.open[Index[Integer, String]](baseAssemblyIdIndex_ID()).remove(baseAssemblyId)
        Hyflow.dir.open[ComplexAssembly](baseAssembly.getSuperAssembly_ID()).removeSubAssembly(baseAssembly)

        val component_IDs: ImmutableCollection[String] = baseAssembly.getComponent_IDs()
        for (component_ID <- component_IDs) {
            baseAssembly.removeComponent(Hyflow.dir.open[CompositePart](component_ID))
        }
        baseAssembly.clearPointers()
        baseAssemblyIdPool().putUnusedId(baseAssemblyId)
    }

    def unregisterAndRecycleComplexAssembly(complexAssembly: ComplexAssembly)(implicit txn: InTxn) {
        val currentLevel: Short = complexAssembly.getLevel()
        val superAssembly_ID: String = complexAssembly.getSuperAssembly_ID()
        val superAssembly: ComplexAssembly = if (superAssembly_ID == null) { null } else { Hyflow.dir.open[ComplexAssembly](superAssembly_ID) }

        if (superAssembly == null) { throw new RuntimeException("ComplexAssemblyFactory: Root Complex Assembly cannot be removed!") }
        superAssembly.removeSubAssembly(complexAssembly)

        val subAssembly_IDs = complexAssembly.getSubAssembly_IDs()
        for (assembly_ID <- subAssembly_IDs) {
            val assembly = Hyflow.dir.open[Assembly](assembly_ID)
            if (currentLevel > 2) {
                unregisterAndRecycleComplexAssembly(assembly.asInstanceOf[ComplexAssembly])
            }
            else {
                unregisterAndRecycleBaseAssembly(assembly.asInstanceOf[BaseAssembly])
            }
        }

        val id = complexAssembly.getId()
        Hyflow.dir.open[Index[Integer, String]](complexAssemblyIdIndex_ID()).remove(id)
        complexAssembly.clearPointers()
        complexAssemblyIdPool().putUnusedId(id)
    }

    private def createAndRegisterBaseAssembly(module: Module, superAssembly: ComplexAssembly)(implicit txn: InTxn): BaseAssembly = {
        val date = createBuildDate(Parameters.MinAssmDate, Parameters.MaxAssmDate)
        val assemblyId = baseAssemblyIdPool().getId()

        val baseAssembly_ID: String = designObjFactory.createBaseAssembly(assemblyId, createType(), date, module, superAssembly)
        Hyflow.dir.open[Index[Integer, String]](baseAssemblyIdIndex_ID()).put(assemblyId, baseAssembly_ID)
        val baseAssembly: BaseAssembly = Hyflow.dir.open[BaseAssembly](baseAssembly_ID)
        superAssembly.addSubAssembly(baseAssembly)

        return baseAssembly
    }

    private def createAndRegisterComplexAssembly(module: Module, superAssembly: ComplexAssembly)(implicit txn: InTxn): ComplexAssembly = {
        val id = complexAssemblyIdPool().getId()
        val date = createBuildDate(Parameters.MinAssmDate, Parameters.MaxAssmDate)
        val complexAssembly_ID: String = designObjFactory.createComplexAssembly(id, createType(), date, module, superAssembly)
        val complexAssembly: ComplexAssembly = Hyflow.dir.open[ComplexAssembly](complexAssembly_ID)

        try {
            for (i <- 0 to (Parameters.NumAssmPerAssm - 1)) {
                createAndRegisterAssembly(module, complexAssembly)
            }
        }
        catch {
            case x: Throwable =>
                for (subAssembly_ID <- complexAssembly.getSubAssembly_IDs()) {
                    unregisterAndRecycleAssembly(Hyflow.dir.open[Assembly](subAssembly_ID))
                }

                complexAssembly.clearPointers()
                throw x
        }

        Hyflow.dir.open[Index[Integer, String]](complexAssemblyIdIndex_ID()).put(id, complexAssembly_ID)
        if (superAssembly != null) {
            superAssembly.addSubAssembly(complexAssembly)
        }

        return complexAssembly
    }
}