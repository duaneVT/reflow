package org.hyflow.benchmarks

import scala.util.Random

object BenchUtil {
	private val rand = new Random
	
	def randString(x: Int, y: Int) = {
		val len = x + rand.nextInt(y - x + 1)
		var res = ""
		(" " * len).map(x => rand.nextPrintableChar())
	}

	def randNumString(x: Int, y: Int) = {
		val len = x + rand.nextInt(y - x + 1)
		var i = 0
		var res = ""
		while (i < len) {
			res += rand.nextInt(9) + 1
			i += 1
		}
		res
	}
}