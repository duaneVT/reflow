#!/usr/bin/python

# Requires python-tables

import sys
sys.path.append("./bin/classes")

from result_store2 import *
from plot_bars import *
import plot_descr
from result_plots import *
import matplotlib.pyplot as plt

import matplotlib
font = {'size'   : 7}
matplotlib.rc('font', **font)


db = HyResultStore("results/db")
#db.reset()
#db.load("results/result-talexpc-vt-N61Jv.txt")
#db.load("results/result-lost.txt")
#db.load("results/result-mario.txt")
	
res = db.results()

rp = ResultPlots()
#rp.make_all(db.results())
#rp.make_webpage()
#exit(0)

#res = res.filter("hyflow.workload.benchmark", "skiplist")
#res = res.filter("hyflow.workload.readOnlyRatio", "50")
#res = res.filter("hyflow.workload.objects", "32")
#res = res.filter("hyflow.workload.ops", "8")
#res = res.filter("hyflow.nodes", "4")

#res = res.filter("hyflow.benchmark.injectorThreads", "2")
#res = res.filter("hyflow.logging.revId", "406f388e468c+") # second run of hashtable, improved metrics
#res = res.filter("hyflow.logging.revId", ["4a0dab6c72f8+", "2d99dcde9652+"]) # third run of hashtable, 2 cores

#res = res.filter("hyflow.logging.hostname", "lost")
grouped = res.group("hyflow.logging.testId")

rp.make_summary(grouped, "tpcc skiplist:8ops bst rbt bank hashtable".split())

import plot_descr
#rp.make_one(grouped, "noAbortTxn2")
#rp.make_one(grouped, "failedRetry-dur")
#rp.make_one(grouped, "timeratio-failedRetry")
#rp.make_one(grouped, "ckptTimeRatio")
#rp.make_group(grouped, "skiplist-rr")
#rp.make_group(grouped, "tpcc-2")
#rp.make_group(grouped, "bst")
#rp.make_webpage()

	
	
db.close()


