#!/usr/bin/python

import os
import os.path
import csv
import numpy as np

sums = {}
header = {}
AVERAGES = ["# time", "min", "max", "mean", "median", "stddev", "95%", "99%", "99.9%"]

def process_csv(fn, f):
	global header
	with open(fn, 'rt') as csvfile:
		c = csv.reader(csvfile)
		crt_header = None
		i = 0
		for row in c:
			if crt_header == None:
				crt_header = row[:]
				header[f] = crt_header
			else:
				if not f in sums:
					sums[f] = {}
				if not i in sums[f]:
					sums[f][i] = []
				sums[f][i].append(row[:])
				i += 1

def summarize():
	for f in sums:
		for i in sums[f]:
			result = None
			cnt = len(sums[f][i])
			for x in sums[f][i]:
				if len(x) != len(header[f]):
					continue
				if result == None:
					result = np.array(x, dtype=np.float32)
				else:
					result = result + np.array(x, dtype=np.float32)
			if result != None:
				for j in range(len(result)):
					if header[f][j] in AVERAGES:
						result[j] /= cnt
			sums[f][i] = result

def output():
	for f in sums:
		with open(f, 'wt') as csvfile:
			c = csv.writer(csvfile)
			c.writerow(header[f])
			for (k, v) in sums[f].items():
				if v != None:
					c.writerow(v)

def main(): 
	dirs = os.listdir(".")
	for d in dirs:
		if not os.path.islink(d) and os.path.isdir(d):
			files = os.listdir(d)
			for f in files:
				if os.path.splitext(f)[1] == ".csv":
					fn = os.path.join(d, f)
					process_csv(fn, f)
	summarize()
	output()

main()


