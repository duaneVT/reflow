#!/usr/bin/python

import sys
sys.path.append("./bin/classes")

from result_store2 import *
from plot_bars import *
import plot_descr
from result_plots import *
import matplotlib.pyplot as plt

db = HyResultStore("results/db")
res = db.results()

#t185-ctr-10-20-flat t185-ctr-12-6-flat t185-ctr-15-15-flat t185-ctr-15-3-flat t185-ctr-17-1-flat t185-ctr-20-10-flat t185-ctr-25-5-flat t185-ctr-28-2-flat t185-ctr-6-12-flat t185-ctr-9-9-flat t186-ctr-10-20-cpman t186-ctr-12-6-cpman t186-ctr-15-15-cpman t186-ctr-15-3-cpman t186-ctr-17-1-cpman t186-ctr-20-10-cpman t186-ctr-25-5-cpman t186-ctr-28-2-cpman t186-ctr-6-12-cpman t186-ctr-9-9-cpman t187-ctr-10-20-closed t187-ctr-12-6-closed t187-ctr-15-15-closed t187-ctr-15-3-closed t187-ctr-17-1-closed t187-ctr-20-10-closed t187-ctr-25-5-closed t187-ctr-28-2-closed t187-ctr-6-12-closed t187-ctr-9-9-closed

tx_short = ["6-12", "9-9", "12-6", "15-3", "17-1"]
tx_long = ["10-20", "15-15", "20-10", "25-5", "28-2"]

BASE = "t185-ctr-%s-flat"
PLOTS = {"flat":"t185-ctr-%s-flat", "cpman": "t186-ctr-%s-cpman", "closed":"t187-ctr-%s-closed"}

xaxis = "hyflow.workload.counter._step2.pool"
yaxis = "throughput"

total = {"flat": 0.0, "cpman": 0.0, "closed": 0.0}
count = {"flat": 0, "cpman": 0, "closed": 0}

vals_cp = []
vals_cl = []

for pid in tx_short:
	for model in PLOTS:
		base = res.sub(BASE % pid).getSeries(xaxis, yaxis)
		plot = res.sub(PLOTS[model] % pid).getSeries(xaxis, yaxis)
		rel = plot.divide(base)
		avg = rel.yvals[2]
		if "cpman" in model:
			vals_cp.append(avg)
		elif "closed" in model:
			vals_cl.append(avg)
		#avg = rel.average()
		total[model] += avg
		count[model] += 1
		print PLOTS[model] % pid, avg
		
print "Overall:"
for model in total:
	print model, total[model]/count[model]
print "closed", vals_cl
print "cpman", vals_cp
