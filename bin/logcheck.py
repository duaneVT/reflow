#!/usr/bin/python

def parset(s):
	(h, m, s) = s.split(":")
	t = float(s) + int(m) * 60
	return t

f = open("log-1-talexpc-vt-N61Jv.txt", "rt")
prev = None
for ln in f:
	if prev == None:
		prev = ln
		tm1 = parset(ln.split()[0])
	else:
		tm2 = parset(ln.split()[0])
		diff = tm2 - tm1
		if (diff > 0.010):
			print diff, prev.strip()
		prev = ln
		tm1 = tm2
f.close()
