#!/usr/bin/env python

graph_id = "graph-id.txt"
metis_res = "graph.txt.part.3"

f1 = open(graph_id, "rt")
f2 = open(metis_res, "rt")

for vname in f1:
    part = f2.readline()
    print("%40s             part%s" % (vname.strip(), part.strip()))
    
f1.close()
f2.close()
