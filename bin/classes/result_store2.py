import os.path
import subprocess
import csv
import numpy as np
import shelve

from result_ops import *

METRIC_SHORTCUTS = {
	"CpTracker_checkpoint" : "org.hyflow.core.directory.CpTracker.checkpoint",
	"Tracker_frontget": 
		("org.hyflow.core.directory.CpTracker.tracker-front-get",
		"org.hyflow.core.directory.Tracker.tracker-front-get"),
	"Tracker_frontlocate": 
		("org.hyflow.core.directory.CpTracker.tracker-front-locate",
		"org.hyflow.core.directory.Tracker.tracker-front-locate"),
	"TrackerActor_delete": "org.hyflow.core.directory.TrackerActor.tracker-back-delete",
	"TrackerActor_locate": "org.hyflow.core.directory.TrackerActor.tracker-back-locate",
	"TrackerActor_notifreq": "org.hyflow.core.directory.TrackerActor.tracker-back-notifreq",
	"TrackerActor_register": "org.hyflow.core.directory.TrackerActor.tracker-back-register",
	"Store_get": "org.hyflow.core.store.Combined_LockStore_Actor.store-back-get",
	"Store_lockrelease": "org.hyflow.core.store.Combined_LockStore_Actor.store-back-lockrelease",
	"Store_lockreq": "org.hyflow.core.store.Combined_LockStore_Actor.store-back-lockreq",
	"Store_lost": "org.hyflow.core.store.Combined_LockStore_Actor.store-back-lost",
	"Store_put": "org.hyflow.core.store.Combined_LockStore_Actor.store-back-put",
	"Store_validate": "org.hyflow.core.store.Combined_LockStore_Actor.store-back-validate",
	"manual_checkpoint": "scala.concurrent.stm.haistm.HyHai.checkpoint",
	"ckptSave": "scala.concurrent.stm.haistm.HaiInTxn.ckptSave",
	"ckptResume": "scala.concurrent.stm.haistm.HaiInTxn.ckptResume",
	"gcTime": "org.hyflow.core.util.JVMStats.gcTime",
	"gcCount": "org.hyflow.core.util.JVMStats.gcCount",
	# Contention Manager
	"backoff": "org.hyflow.core.ContentionManager.backoff",
	"backoff_reqs": "org.hyflow.core.ContentionManager.backoff_reqs",
	"acqLockFailed": "org.hyflow.core.ContentionManager.acqLockFailed",
	"invalidReadset": "org.hyflow.core.ContentionManager.invalidReadset",
	"notOptimistic": "org.hyflow.core.ContentionManager.notOptimistic",
	"notRolledback": "org.hyflow.core.ContentionManager.notRolledback",
	"openLocked": "org.hyflow.core.ContentionManager.openLocked",
	"prevAttempts": "org.hyflow.core.ContentionManager.prevAttempts",
	# InTxn
	"failedRetry": 
		("scala.concurrent.stm.haistm.HaiInTxn.failedRetry", 
		"scala.concurrent.stm.motstm.MotInTxn.failedRetry"),
	"firstAbort":
		("scala.concurrent.stm.haistm.HaiInTxn.firstAbort", 
		"scala.concurrent.stm.motstm.MotInTxn.firstAbort"),
	"noAbortTxn":
		("scala.concurrent.stm.haistm.HaiInTxn.noAbortTxn", 
		"scala.concurrent.stm.motstm.MotInTxn.noAbortTxn"),
	"succRetry":
		("scala.concurrent.stm.haistm.HaiInTxn.succRetry", 
		"scala.concurrent.stm.motstm.MotInTxn.succRetry"),
	"abortedLevels":
		("scala.concurrent.stm.haistm.HaiInTxn.abortedLevels",
		"scala.concurrent.stm.motstm.MotInTxn.abortedLevels"),
	"abortToLevel":
		("scala.concurrent.stm.haistm.HaiInTxn.abortToLevel",
		"scala.concurrent.stm.motstm.MotInTxn.abortToLevel"),
}

class HyTest(object):
	def __init__(self):
		self.benchmark = None
		self.ended_at = None
		self.throughput = None
		self.commits = None
		self.aborts = None
		self.md5_hash = None
		self.args = None
		self.metrics = None
	
	def parse(self, ln):
		pairs = ln.split(": ", 2)
		key = pairs[0].lower().replace(" ", "_")
		value = pairs[1]
		if key == "arguments":
			args = value[5:-1].split(", ")
			args = dict([x.split("=") for x in args])
			self.args = args
		else:
			setattr(self, key, value)
	
	def update(self):
		if self.md5_hash == None:
			return False
		if self.metrics == None:
			path_arch = os.path.join("results", self.args["hyflow.logging.testId"], self.args["hyflow.logging.hostname"])
			# extract archive
			tarball = self.md5_hash + ".tar.bz2"
			if not os.path.exists(os.path.join(path_arch, tarball)):
				return False
			print "Updating info. ", self.md5_hash, self.args["hyflow.logging.testId"]
			subprocess.call(["tar", "-xjf", tarball], cwd=path_arch)
			# summarize metrics
			path_metrics = os.path.join(path_arch, "var", "metrics", "test")
			if not os.path.exists(path_metrics):
				path_metrics = os.path.join(path_arch, "var", "metrics")
				if not os.path.exists(path_metrics):
					return false
			subprocess.call(["sum_metrics.py"], cwd=path_metrics)
			# Load metrics summary
			self.metrics = {}
			files = os.listdir(path_metrics)
			for fn in files:
				if os.path.splitext(fn)[1] == ".csv":
					self.update_metric(path_metrics, fn)
			# Remove working dir
			subprocess.call(["rm", "-r", "./var"], cwd=path_arch)
			return True
		else:
			return False
	
	def update_metric(self, path, fn):
		with open(os.path.join(path, fn), "rb") as csvfile:
			c = csv.reader(csvfile)
			lines = []
			for x in c:
				lines.append(x)
			metric_name = os.path.splitext(fn)[0]
			self.metrics[metric_name] = np.array(lines[-2], dtype=np.float32)

	def metric(self, m):
		if self.metrics == None:
			return None
		if m in METRIC_SHORTCUTS:
			ms = METRIC_SHORTCUTS[m]
			if type(ms) == str:
				if ms in self.metrics:
					return self.metrics[ms]
				else:
					return None
			else:
				for msx in ms:
					if msx in self.metrics:
						return self.metrics[msx]
				return None
		else:
			return self.metrics[m]
	
	def matches(self, key, value):
		if hasattr(self, key):
			return getattr(self, key) == value
		elif key in self.args:
			return self.args[key] == value
		else:
			return value == None

	def get(self, key, submetric=None):
		if type(key) == str and submetric==None:
			if hasattr(self, key):
				return float(getattr(self, key))
			elif key in self.args:
				return float(self.args[key])
		elif type(key) == str:
			m = self.metric(key)
			if m != None:
				return m[submetric]
		elif type(key) == tuple:
			m = self.metric(key[0])
			if m != None and len(m) > key[1]:
				return m[key[1]]
		return None
	def getStr(self, key):
		if hasattr(self, key):
			return getattr(self, key)
		elif key in self.args:
			return self.args[key]
	
class HyResultStore(object):
	def __init__(self, dbname):
		self.dbname = dbname
		self.db = shelve.open(dbname)
		if "data" in self.db:
			self.data = self.db["data"]
		else:
			self.reset()
	
	def reset(self):
		self.data = {}
		self.db["data"] = self.data
		self.db.sync()
		print "File reset."

	def _add_entry(self, entry):
		updated = False
		testid = entry.args["hyflow.logging.testId"]
		if testid not in self.data:
			self.data[testid] = {}
			updated = True
		if entry.md5_hash not in self.data[testid]:
			self.data[testid][entry.md5_hash] = entry
			updated = True
		else:
			entry = self.data[testid][entry.md5_hash]
		if entry.update():
			updated = True
		return updated
	
	def load(self, resultFile):
		added = 0
		f = open(resultFile, "rt")
		entry = HyTest()
		for ln in f:
			ln = ln.strip()
			if ln == "" or ln[0] == "#":
				continue
			elif ln == "===":
				if self._add_entry(entry):
					added += 1
				entry = HyTest()
			else:
				entry.parse(ln)
		f.close()
		if added != 0:
			self.db["data"] = self.data
			self.db.sync()
		print "Added", added, "new records from", resultFile

	def close(self):
		self.db.close()
	
	def results(self):
		return HyResultOps(self.data)

