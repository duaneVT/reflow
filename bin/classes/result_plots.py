#!/usr/bin/python

import plot_descr
import os
import os.path
import matplotlib.pyplot as plt

from plot_bars import *

class ResultPlots(object):
	def __init__(self):
		descr = {}
		execfile("etc/plots.py", descr)
		self.descr = descr
		self.missing = []
		self.figures = {}

	def make_all(self, results):
		groups = self.descr["groups"]
		subgroup_filters = self.descr["subgroup_filters"]
		
		results = results.group("hyflow.logging.testId")
		
		# For each benchmark group
		for group in groups:
			benchids = groups[group].split()
			self.figures[group] = {}
			
			# For each subgroup for the benchmark
			if group in subgroup_filters:
				for filter_name in subgroup_filters[group]:
					self.figures[group][filter_name] = {}
					self.handle_subgroup(subgroup_filters[group][filter_name], benchids, results, (group, filter_name) )
					
			
			# Also do full group
			self.figures[group]["all"] = {}
			self.handle_subgroup(lambda x: x, benchids, results, (group, "all") )
	
	def make_group(self, results, group_names):
		groups = self.descr["groups"]
		group_names = group_names.split()
		subgroup_filters = self.descr["subgroup_filters"]
		
		results = results.group("hyflow.logging.testId")
		
		# For each benchmark group
		for group in group_names:
			benchids = groups[group].split()
			self.figures[group] = {}
			
			# For each subgroup for the benchmark
			if group in subgroup_filters:
				for filter_name in subgroup_filters[group]:
					self.figures[group][filter_name] = {}
					self.handle_subgroup(subgroup_filters[group][filter_name], benchids, results, (group, filter_name) )
					
			
			# Also do full group
			self.figures[group]["all"] = {}
			self.handle_subgroup(lambda x: x, benchids, results, (group, "all") )
	
	def make_one(self, results, plot_name):
		groups = self.descr["groups"]
		subgroup_filters = self.descr["subgroup_filters"]
		
		results = results.group("hyflow.logging.testId")
		
		# For each benchmark group
		for group in groups:
			benchids = groups[group].split()
			self.figures[group] = {}
			
			# For each subgroup for the benchmark
			if group in subgroup_filters:
				for filter_name in subgroup_filters[group]:
					self.make_plots(plot_descr.yaxis[plot_name], 
									benchids, 
									subgroup_filters[group][filter_name](results), 
									(group, filter_name, plot_name))
			
			# Also do full group
			self.make_plots(plot_descr.yaxis[plot_name], benchids, results, (group, "all", plot_name))
		
			
	def handle_subgroup(self, filter, benchids, results, names):
		print "Subgroup:", names, benchids
		results = filter(results)
		outputs = []
		
		for categ_name in plot_descr.ycateg_order:
			plot_names = plot_descr.ycateg[categ_name].split()
			for plot_name in plot_names:
				plot = plot_descr.yaxis[plot_name]
				figures = self.make_plots(plot, benchids, results, names + (plot_name, ))
				self.figures[names[0]][names[1]][plot_name] = figures
				

	def get_xaxis(self, groupName):
		xdescr = self.descr["xaxis"][groupName]
		if type(xdescr) == tuple:
			return (plot_descr.xaxis[xdescr[0]], xdescr[1])
		else:
			return (plot_descr.xaxis[xdescr], False)
	
	def make_plots(self, plot, benchids, result, names):
		return self.make_plots_metric(plot, benchids, result, names)
	
	def make_plots_metric(self, plot, benchids, result, names):
		plots = []
		plot = plot.copy()
		if "metric" in plot:
			metric = plot["metric"]
			if type(metric) != tuple:
				mtype = plot["type"]
				for submetric in plot_descr.metric_types[mtype]:
					plot["metric"] = (metric, submetric[0])
					plots.extend(
						self.make_plots_rel(plot, benchids, result, names, {"label suffix": submetric[2], "file suffix": submetric[1]})
					)
			else:
				plots.extend(self.make_plots_rel(plot, benchids, result, names, {} ))
		else:
			plots.extend(self.make_plots_rel(plot, benchids, result, names, {} ))
		return plots
	
	def make_plots_rel(self, plot, benchids, result, names, extra):
		plots = []
		if "relative" in plot:
			extra["relative"] = True
			plots.extend(self.make_plot(plot, benchids, result, names, extra))
		extra["relative"] = False
		plots.extend(self.make_plot(plot, benchids, result, names, extra))
		return plots
	
	def make_plot(self, plot, benchids, result, names, extra):
		#plot
		(xdescr, logaxis) = self.get_xaxis(names[0])
		if "data" in xdescr:
			xaxis = xdescr["data"]
		elif "func" in xdescr:
			xaxis = xdescr["func"]
		else:
			xaxis = None
		
		if "data" in plot:
			yaxis = plot["data"]
		elif "metric" in plot:
			metric = plot["metric"]
			if type(metric) != tuple:
				metric = (metric, 3)
			yaxis = metric
		elif "func" in plot:
			yaxis = plot["func"]
		else:
			yaxis = None
		
		ref_series = None
		for benchid in benchids:
			if "-flat" in benchid and "cp-flat" not in benchid:
				try:
					ref_series = result.sub(benchid).getSeries(xaxis, yaxis)
					if not ref_series.yvals.all():
						ref_series = None
				except Exception as e:
					print e
					pass
				break
		
		hasdata = False
		plt.figure(figsize=(3.5, 1.5))
		for benchid in benchids:
			subbed = result.sub(benchid)
			if subbed != None:
				try:
					data = subbed.getSeries(xaxis, yaxis)
					if not data.yvals.any():
						continue
					if extra["relative"] and ref_series != None:
						data = data.divide(ref_series)
					if logaxis:
						data.semilogx("x-", label=benchid)
					else:
						data.plot("x-", label=benchid)
					hasdata = True
				except Exception as e:
					print names, benchid, e
			elif benchid not in self.missing:
				print "Missing", benchid
				self.missing.append(benchid)
		
		if hasdata:
			# THESE ARE VOLATILE PER PLOT SETTINGS
			#TODO: bst/all/timeratio-failedRetry-rel.pdf
			#plt.ylim([0.95, 2])
			#plt.xlim([1, 100])
			#TODO: bst/all/failedRetry-dur-rel.pdf
			#plt.ylim([0.3, 3.5])
			#plt.xlim([1, 20])
			#TODO skiplist/8-ops/...:
			plt.xlim([3, 25])
			# END PER PLOT
			# Annotations
			plt.xlabel(xdescr["label"])
			if "label suffix" in extra:
				plt.ylabel(plot["label"] + " " + extra["label suffix"])
			else:
				plt.ylabel(plot["label"])
			plt.legend(loc="center right", prop={"size": "small"}, markerscale=0.6, ncol=2)
		
			#save
			dirName = os.path.join("plots", names[0], names[1])
			if not os.path.exists(dirName):
				os.makedirs(dirName)
			
			baseName = names[2]
			if "file suffix" in extra:
				baseName += "-" + extra["file suffix"]
			if extra["relative"]:
				baseName += "-rel"
			
			print dirName, baseName
			#plt.show()
			pdfName = os.path.join(dirName, baseName + ".pdf")
			plt.savefig(pdfName, bbox_inches="tight")
		
			pngName = os.path.join(dirName, baseName + ".png")
			plt.savefig(pngName, bbox_inches="tight")
			
			plt.clf()
			return [ baseName ]
		else:
			if extra["relative"]:
				print "Nothing:("
			return []
	
	def make_webpage(self):
		for bench in self.figures:
			for subgroup in self.figures[bench]:
				html = "<html>\n"
				html += "<h1>%s</h1>\n" % bench
				html += "<h2>%s</h2>\n" % subgroup
				for categ_name in plot_descr.ycateg_order:
					html += "<h3>%s</h3>\n" % categ_name
					plot_names = plot_descr.ycateg[categ_name].split()
					for plot_name in plot_names:
						figures = self.figures[bench][subgroup][plot_name]
						for figure in figures:
							html += "<img src='%s/%s.png' />\n" % (subgroup, figure)
						if len(figures) > 0:
							html += "<br/>\n"
				html += "</html>\n"
				fname = os.path.join("plots", bench, subgroup + ".html")
				webpg = open(fname, "wt")
				webpg.write(html)
				webpg.close()
	
	def _add_barplot_grp(self, grpid, bp, results, x, y):
		SERIES = ["flat", "cp-flat", "cp100", "cp-e7", "cp-zero", "closed"]
		
		if ":" in grpid:
			(grpid, grpid_f) = grpid.split(":")
			grpid_f = self.descr["subgroup_filters"][grpid][grpid_f]
		else:
			grpid_f = lambda x: x
		
		benchids = self.descr["groups"][grpid].split()
		res = {}
		for bid in benchids:
			serid = None
			if "flat" in bid and "cp-flat" not in bid:
				serid = "flat"
			else:
				for nm in SERIES:
					if nm in bid:
						serid = nm
			if serid != None:
				# Filter
				res[serid] = grpid_f(results.sub(bid)).getSeries(x,y)
		flat = res["flat"]
		for serid in SERIES:
			if serid in res:
				res[serid] = res[serid].divide(flat).average()
		bp.add_group(grpid, res)
	
	def make_summary(self, results, groups):
		bp = BarPlot()
		plt.figure(figsize=(4.0, 2))
		
		x = "hyflow.nodes"
		y = "throughput"

		for grpid in groups:
			self._add_barplot_grp(grpid, bp, results, x, y)
		
		bp.add_group("ctr", {"flat": 1.0, "closed": 1.0036591524, "cpman": 1.01843032693} )

		#plt.ylabel('Relative Throughput')
		bp.sort()
		bp.plot()

		plt.savefig("plots/summary.pdf", bbox_inches="tight")
		plt.savefig("plots/summary.png", bbox_inches="tight")
		plt.show()
		
