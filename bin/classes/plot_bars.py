#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt

COLORS = ['blue', 'red', 'green', 'yellow', 'cyan', 'magenta', 'gray', 'black']

class BarPlot(object):
	def __init__(self):
		self.groups = []
		self.series = []
		self.group_data = {}
		self.series_data = {}
	
	def add_group(self, groupName, groupData):
		self.groups.append(groupName)
		self.group_data[groupName] = groupData
		for bar in groupData:
			if bar not in self.series:
				self.series.append(bar)
	
	def _autolabel(self, rects):
		# attach some text labels
		for rect in rects:
			height = rect.get_height()
			if height != 0.0:
				plt.text(rect.get_x() + rect.get_width() / 2.0, 1.05*height, '%.2f' % height,
					ha='center', va='bottom', rotation=90, fontsize=5)
	
	def sort(self):
		aggr = {}
		count = {}
		for serid in self.series:
			aggr[serid] = 0.0
			count[serid] = 0
			for g in self.group_data:
				if serid in self.group_data[g]:
					aggr[serid] += self.group_data[g][serid]
					count[serid] += 1
		self.series = sorted(self.series, key=lambda x: aggr[x]/count[x])
		print [ (x, aggr[x]/count[x]) for x in self.series]
	
	def plot(self):
		plt.subplot(111)
		
		N = len(self.groups)
		ind = np.arange(N)
		width = 0.7 / len(self.series)

		rects = []
		for i in range(len(self.series)):
			bar = self.series[i]
			data = [ self.group_data[g][bar] if bar in self.group_data[g] else 0 for g in self.groups]
			rect = plt.bar(ind+i*width, data, width, ecolor="pink", color=COLORS[i], linewidth=1)
				#yerr=menStd,
				#error_kw=dict(elinewidth=6, ecolor='pink')
			self._autolabel(rect)
			rects.append(rect) 

		plt.xticks(ind + width*0.5*len(self.series), self.groups)

		plt.legend( [ r[0] for r in rects ], self.series, loc="lower right", ncol=3, prop={"size": "medium"})
		#plt.legend(loc="lower right", prop={"size": "small"}, markerscale=0.6, ncol=2)
		plt.plot( [0,N], [1, 1], "k")
		plt.ylim(0.6, 1.2)
 
