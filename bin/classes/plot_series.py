#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt

class DataSeries(object):
	def __init__(self, xvals, yvals):
		self.xvals = xvals
		self.yvals = yvals
	
	def plot(self, format="", **kv):
		plt.plot(self.xvals, self.yvals, format, **kv)
	
	def semilogx(self, format="", **kv):
		plt.semilogx(self.xvals, self.yvals, format, **kv)
	
	def divide(self, other):
		assert (self.xvals == other.xvals).all()
		ynew = self.yvals / other.yvals
		return DataSeries(self.xvals, ynew)
	
	def average(self):
		return self.yvals.mean()
	

