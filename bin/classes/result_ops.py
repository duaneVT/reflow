
#import numpy
import matplotlib.pyplot as plt

from plot_series import *

class HyResultOps(object):
	def __init__(self, data, grp = "hyflow.logging.testId"):
		self.data = data
		self.group_by = grp
	
	def filter(self, key, value):
		new = {}
		if key == self.group_by:
			for k, v in self.data:
				if type(value) in [str, int, float]:
					if k == value:
						new[k] = v
				elif type(value) == list:
					if k in value:
						new[k] = v
		else:
			for k, v in self.data.items():
				new[k] = {}
				for md5, entry in v.items():
					#if entry.matches(key, value):
					#	new[k][md5] = entry
					if type(value) in [str, int, float]:
						if entry.get(key) == value:
							new[k][md5] = entry
					elif type(value) == list:
						if entry.get(key) in value:
							new[k][md5] = entry
		return HyResultOps(new, self.group_by)

	def group(self, key):
		if key == self.group_by:
			return self
		new = {}
		for k, v in self.data.items():
			for md5, entry in v.items():
				if isinstance(key, type(lambda: None)):
					x = key(entry)
				else:
					x = entry.get(key)
				if x not in new:
					new[x] = {}
				new[x][md5] = entry
		return HyResultOps(new, key)
	
	def sub(self, key):
		if key in self.data:
			return HyResultOps( { key: self.data[key] }, self.group_by)
		else:
			return None
	
	def getSeries(self, xkey, ykey):
		grouped = self.group(xkey)
		if None in grouped.data.keys():
			print xkey, type(xkey)
			print grouped.data
			print self.data
		
		xvals = sorted([float(x) for x in grouped.data.keys()])
		yvals = [grouped.average(x, ykey) for x in xvals]
		return DataSeries(np.array(xvals), np.array(yvals))
	
	def average(self, groupkey, ykey):
		yvals = self.data[groupkey].values()
		if type(ykey) in [str, tuple]:
			yvals = [entry.get(ykey) for entry in yvals]
		else:
			yvals = [ykey(entry) for entry in yvals]
		yvals = [float(val) for val in yvals if val != None]
		if len(yvals) != 0:
			return 1.0 * sum(yvals) / len(yvals)
		else:
			return None
	
	def __repr__(self):
		return "HyResultOps[%d groups by %s]" % (len(self.data), self.group_by)



