
from tables import *
import os.path
import subprocess
import csv
import numpy as np

from result_ops import *

class HyTest(IsDescription):
    benchmark = StringCol(32)
    arguments = StringCol(512)
    ended_at = StringCol(32)
    throughput = Float64Col()
    commits = Int64Col()
    aborts = Int64Col()
    md5_hash = StringCol(32)
    # Metrics
    metrics_CpTracker_checkpoint = Float32Col(shape=(14,))
    metrics_CpTracker_frontget = Float32Col(shape=(14,))
    metrics_CpTracker_frontlocate = Float32Col(shape=(14,))
    metrics_TrackerActor_delete = Float32Col(shape=(14,))
    metrics_TrackerActor_locate = Float32Col(shape=(14,))
    metrics_TrackerActor_notifreq = Float32Col(shape=(14,))
    metrics_TrackerActor_register = Float32Col(shape=(14,))
    metrics_Store_get = Float32Col(shape=(14,))
    metrics_Store_lockrelease = Float32Col(shape=(14, ))
    metrics_Store_lockreq = Float32Col(shape=(14, ))
    metrics_Store_lost = Float32Col(shape=(14, ))
    metrics_Store_put = Float32Col(shape=(14, ))
    metrics_Store_validate = Float32Col(shape=(14, ))
    metrics_failedRetry = Float32Col(shape=(14, ))
    metrics_firstAbort = Float32Col(shape=(14, ))
    metrics_noAbortTxn = Float32Col(shape=(14, ))
    metrics_succRetry = Float32Col(shape=(14, ))


class HyResultStore(object):
	def __init__(self, h5name):
		self.filter = None
		self.h5name = h5name
		if os.path.exists(h5name):
			self.h5 = openFile(self.h5name, mode="a")
			self.group = self.h5.root.results
			self.table = self.group.summary
			self.row = self.table.row
		else:
			self.reset()
	
	def reset(self):
		if hasattr(self, "h5"):
			self.close()
		self.h5 = openFile(self.h5name, mode="w", title="Hyflow2 results file")
		self.group = self.h5.createGroup("/", 'results', 'Results group')
		self.table = self.h5.createTable(self.group, "summary", HyTest, "Results table")
		self.row = self.table.row
		print "File reset."

	def load(self, resultFile):
		ex0 = self.table.cols.md5_hash[:]
		existing = set(ex0)
		print len(existing), "existing records", len(ex0)
		added = 0
		
		f = open(resultFile, "rt")
		buff = {}
		for ln in f:
			ln = ln.strip()
			if ln == "":
				continue
			elif ln == "===":
				if buff["md5_hash"] in existing:
					buff = {}
				else:
					for (k, v) in buff.items():
						self.row.__setitem__(k, v)
					if self.filter == None:
						self._summarize_one(self.row)
					else:
						args = buff["arguments"][5:-1].split(", ")
						args = dict([x.split("=") for x in args])
						if args["hyflow.logging.testId"] in self.filter:
							self._summarize_one(self.row)
					self.row.append()
					self.table.flush()
					print added
					buff = {}
					added += 1
			else:
				pairs = ln.split(": ", 2)
				key = pairs[0].lower().replace(" ", "_")
				value = pairs[1]
				buff[key] = value
		f.close()
		print "Added", added, "new records from", resultFile

	def close(self):
		self.h5.close()
	
	def results(self):
		result = []
		for it in self.table.iterrows():
			crt = {}
			for key in ["benchmark", "ended_at", "throughput", "commits", "aborts", "md5_hash",
					"metrics_CpTracker_checkpoint", "metrics_CpTracker_frontget",
					"metrics_CpTracker_frontlocate", "metrics_TrackerActor_delete",
					"metrics_TrackerActor_locate", "metrics_TrackerActor_notifreq",
					"metrics_TrackerActor_register", "metrics_Store_get",
					"metrics_Store_lockrelease", "metrics_Store_lockreq",
					"metrics_Store_lost", "metrics_Store_put",
					"metrics_Store_validate", "metrics_failedRetry",
					"metrics_firstAbort", "metrics_noAbortTxn", "metrics_succRetry"]:
				crt[key] = it[key]
			args = it["arguments"][5:-1].split(", ")
			crt["ARGS"] = dict([x.split("=") for x in args])
			result.append(crt)
		return HyResultOps(result)

	def _update_one_metric(self, row, path, key, *files):
		for f in files:
			fn = os.path.join(path, f)
			if os.path.exists(fn):
				with open(fn, "rt") as csvfile:
					c = csv.reader(csvfile)
					for x in c:
						pass
					row[key] = np.array(x, dtype=np.float32)
					return
	
	def _summarize_one(self, row):
		args = row["arguments"][5:-1].split(", ")
		args = dict([x.split("=") for x in args])
		cwd = os.path.join("results", args["hyflow.logging.testId"], args["hyflow.logging.hostname"])
		# extract archive
		tarball = row["md5_hash"] + ".tar.bz2"
		subprocess.call(["tar", "-xjf", tarball], cwd=cwd)
		# summarize metrics
		cwd2 = os.path.join(cwd, "var", "metrics")
		subprocess.call(["sum_metrics.py"], cwd=cwd2)
		# read results and update row
		self._update_one_metric(row, cwd2, "metrics_CpTracker_checkpoint", "org.hyflow.core.directory.CpTracker.checkpoint.csv")
		self._update_one_metric(row, cwd2, "metrics_CpTracker_frontget", "org.hyflow.core.directory.CpTracker.tracker-front-get.csv")
		self._update_one_metric(row, cwd2, "metrics_CpTracker_frontlocate", "org.hyflow.core.directory.CpTracker.tracker-front-locate.csv")
		self._update_one_metric(row, cwd2, "metrics_TrackerActor_delete", "org.hyflow.core.directory.TrackerActor.tracker-back-delete.csv")
		self._update_one_metric(row, cwd2, "metrics_TrackerActor_locate", "org.hyflow.core.directory.TrackerActor.tracker-back-locate.csv")
		self._update_one_metric(row, cwd2, "metrics_TrackerActor_notifreq", 
			"org.hyflow.core.directory.TrackerActor.tracker-back-notifreq.csv")
		self._update_one_metric(row, cwd2, "metrics_TrackerActor_register", 
			"org.hyflow.core.directory.TrackerActor.tracker-back-register.csv")
		self._update_one_metric(row, cwd2, "metrics_Store_get", "org.hyflow.core.store.Combined_LockStore_Actor.store-back-get.csv")
		self._update_one_metric(row, cwd2, "metrics_Store_lockrelease", 
			"org.hyflow.core.store.Combined_LockStore_Actor.store-back-lockrelease.csv")
		self._update_one_metric(row, cwd2, "metrics_Store_lockreq", "org.hyflow.core.store.Combined_LockStore_Actor.store-back-lockreq.csv")
		self._update_one_metric(row, cwd2, "metrics_Store_lost", "org.hyflow.core.store.Combined_LockStore_Actor.store-back-lost.csv")
		self._update_one_metric(row, cwd2, "metrics_Store_put", "org.hyflow.core.store.Combined_LockStore_Actor.store-back-put.csv")
		self._update_one_metric(row, cwd2, "metrics_Store_validate", "org.hyflow.core.store.Combined_LockStore_Actor.store-back-validate.csv")
		self._update_one_metric(row, cwd2, "metrics_failedRetry", 
			"scala.concurrent.stm.haistm.HaiInTxn.failedRetry.csv", 
			"scala.concurrent.stm.motstm.MotInTxn.failedRetry.csv")
		self._update_one_metric(row, cwd2, "metrics_firstAbort", 
			"scala.concurrent.stm.haistm.HaiInTxn.firstAbort.csv", 
			"scala.concurrent.stm.motstm.MotInTxn.firstAbort.csv")
		self._update_one_metric(row, cwd2, "metrics_noAbortTxn",
			"scala.concurrent.stm.haistm.HaiInTxn.noAbortTxn.csv", 
			"scala.concurrent.stm.motstm.MotInTxn.noAbortTxn.csv")
		self._update_one_metric(row, cwd2, "metrics_succRetry", 
			"scala.concurrent.stm.haistm.HaiInTxn.succRetry.csv", 
			"scala.concurrent.stm.motstm.MotInTxn.succRetry.csv")
		# Remove working dir
		subprocess.call(["rm", "-r", "./var"], cwd=cwd)
	
	def summarize_all(self):
		cnt = 0
		for row in self.table.iterrows():
			self._summarize_one(row)
			# Save
			row.update()
			self.table.flush()
			cnt += 1
			print cnt
