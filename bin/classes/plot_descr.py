#!/usr/bin/python

def ckpt_every(x):
	ce = x.get("hyflow.haistm.checkpointEvery")
	if ce != None:
		return ce
	name = x.getStr("hyflow.logging.testId")
	if "-cp" in name:
		return 1.0
	return 0.0

xaxis = {
	"n": { "data": "hyflow.nodes", "label": "Nodes"},
	"ops": { "data": "hyflow.workload.ops", "label": "Ops per Transaction"},
	"objs": { "data": "hyflow.workload.objects", "label": "Object Pool Size"},
	"reads": { "data": "hyflow.workload.readOnlyRatio", "label": "% Read-only Transactions"},
	"writes": { "func": lambda x : int(100-x.get("hyflow.workload.readOnlyRatio")), "label": "% Write Transactions"},
	"ckpt-every": { "func": ckpt_every, "label": "Checkpoint Granularity" },
}

total = lambda x: x.get("noAbortTxn", 3) * x.get("noAbortTxn", 8) + x.get("failedRetry", 3) * x.get("failedRetry", 8) + \
					x.get("succRetry", 3) * x.get("succRetry", 8) + x.get("firstAbort", 3) * x.get("firstAbort", 8)

ycateg_order = ["Base Figures", "InTxn Metrics", "InTxn Relative", "Checkpoint Metrics", "Backend & Frontend Ops", "Contention Manager & Aborts", "Time Ratios", "Misc"]

ycateg = {
	"Base Figures": "throughput commits aborts",
	"InTxn Metrics": "firstAbort noAbortTxn failedRetry succRetry",
	"InTxn Relative": "noAbortTxn2 firstAbort-dur failedRetry-dur  succRetry-dur noAbortTxn-rate firstAbort-rate failedRetry-rate",
	"Checkpoint Metrics": "checkpoint ckptSave ckptResume",
	"Backend & Frontend Ops": "Store_validate Store_put Store_get Tracker_frontlocate Tracker_frontget TrackerActor_locate netdelay0 netdelay1",
	"Contention Manager & Aborts": "backoff backoff_reqs acqLockFailed invalidReadset notOptimistic notRolledback openLocked prevAttempts backoff-time backoffs-per-abort backoffs-per-req abortToLevel abortedLevels ratio-failed",
	"Time Ratios": "accounted-time timeratio-failedRetry timeratio-succRetry timeratio-firstAbort timeratio-noAbortTxn timeratio-backoff",
	"Misc": "throughput2 abort-ratio norm-noAbortTxn-time gcCount gcTime"
}

yaxis = {
	# Base numbers
	"throughput": { "data": "throughput", "label": "Throughput (txn/s)" },
	"commits": { "data": "commits", "label": "Commits" },
	"aborts": { "data": "aborts", "label": "Aborts" },
	
	# InTxn Metrics
	"noAbortTxn": { "metric": "noAbortTxn", "type": "timer", "label": "Non-Aborting Transaction" },
	"firstAbort": { "metric": "firstAbort", "type": "timer", "label": "First Abort" },
	"failedRetry": { "metric": "failedRetry", "type": "timer", "label": "Failed Retry" },
	"succRetry": { "metric": "succRetry", "type": "timer", "label": "Successful Retry" },
	
	# InTxn Relative
	"noAbortTxn2": { "func": lambda x: x.get("noAbortTxn", 8), "label": "Relative Overhead", "relative": True},
	"firstAbort-dur": { "func": lambda x: x.get("firstAbort", 8), "label": "First Abort Duration (rel)", "relative": True},
	"failedRetry-dur": { "func": lambda x: x.get("failedRetry", 8), "label": "Failed Retry Duration (rel)", "relative": True },
	"succRetry-dur": { "func": lambda x: x.get("succRetry", 8), "label": "Successful Retry Duration (rel)", "relative": True },
	
	"noAbortTxn-rate": { "func": lambda x: x.get("noAbortTxn", 3), "label": "Non-Aborting Txn Rate (rel)", "relative": True},
	"firstAbort-rate": { "func": lambda x: x.get("firstAbort", 3), "label": "First Abort Rate (rel)", "relative": True},
	"failedRetry-rate": { "func": lambda x: x.get("failedRetry", 3), "label": "Failed Retry Rate (rel)", "relative": True },
	
	# Checkpoint Metrics
	"checkpoint": { "metric": "CpTracker_checkpoint", "type": "timer", "label": "Checkpoint" },
	"ckptSave": { "metric": "ckptSave", "type": "timer", "label": "Checkpoint Save" },
	"ckptSave2": { "func": lambda x: x.get("ckptSave", 8), "label": "Checkpoint Save" },
	"ckptResume": { "metric": "ckptResume", "type": "timer", "label": "Checkpoint Resume" },
	"ckptTimeRatio": { "func": lambda x: x.get("ckptSave", 3) * x.get("ckptSave", 8) / total(x), "label": "Rel Ckpt Time" },
	
	# Backend & Frontend ops
	"Store_validate": { "metric": "Store_validate", "type": "timer", "label": "Back-end Validation" },
	"Store_put": { "metric": "Store_put", "type": "timer", "label": "Back-end Put" },
	"Store_get": { "metric": "Store_get", "type": "timer", "label": "Back-end Get" },
	"Tracker_frontlocate": { "metric": "Tracker_frontlocate", "type": "timer", "label": "Front-end Locate", "relative": True},
	"Tracker_frontget": { "metric": "Tracker_frontget", "type": "timer", "label": "Front-end Get" , "relative": True},
	"TrackerActor_locate": { "metric": "TrackerActor_locate", "type": "timer", "label": "Back-end Locate" },
	"netdelay0": { "func": lambda x: x.get("Tracker_frontget", 8) - x.get("Store_get", 8), "label": "Observed Message Roundtrip" , "relative": True},
	"netdelay1": { "func": lambda x: x.get("Tracker_frontlocate", 8) - x.get("TrackerActor_locate", 8), "label": "Observed Message Roundtrip" , "relative": True},
	
	# Contention Manager and Aborts
	"backoff": { "metric": "backoff", "type": "timer", "label": "Actual Backoffs" },
	"backoff_reqs": { "metric": "backoff_reqs", "type": "meter", "label": "Requested Backoffs" },
	
	"acqLockFailed": { "func": lambda x: x.get("acqLockFailed", 3) / x.get("backoff_reqs", 3), "label": "Failures due to Lock Acquisition" },
	"invalidReadset": { "func": lambda x: x.get("invalidReadset", 3) / x.get("backoff_reqs", 3), "label": "Failures due to Invalid Readset" },
	"notOptimistic": { "func": lambda x: x.get("notOptimistic", 3) / x.get("backoff_reqs", 3), "label": "Permanent Aborts" },
	"notRolledback": { "func": lambda x: x.get("notRolledback", 3) / x.get("backoff_reqs", 3), "label": "Wrong Backoff Requests (Not Rolledback)" },
	"openLocked": { "func": lambda x: x.get("openLocked", 3) / x.get("backoff_reqs", 3), "label": "Failures due to Opening Locked Obj" },
	
	"prevAttempts": { "metric": ("prevAttempts", 3), "type": "histogram", "label": "Average Previous Failed Retries" },
	"backoff-time": { "func": lambda x: x.get("backoff", 3) * x.get("backoff", 8), "label": "Time Spent in Backoff (us/s)" },
	"backoffs-per-abort": { "func": lambda x: x.get( ("backoff", 3) ) / ( x.get( ("failedRetry", 3) ) + x.get( ("succRetry", 3) )), "label": "Actual Backoffs per Abort" },
	"backoffs-per-req": { "func": lambda x: x.get( ("backoff", 3) ) / x.get( ("backoff_reqs", 3) ), "label": "Actual Backoffs per Request" },
	"abortToLevel": { "metric": "abortToLevel", "type": "histogram", "label": "Aborting to Nesting Level or Checkpoint" },
	"abortedLevels": { "metric": "abortedLevels", "type": "histogram", "label": "Aborted Nesting Levels or Checkpoints" },
	"ratio-failed": { "func": lambda x: 1.0 + (x.get("failedRetry", 3) / x.get("succRetry", 3)), "label": "Failed Retries per Aborted Txn" },
	
	# Time Ratios
	"accounted-time": { "func": lambda x: total(x) / x.get("hyflow.nodes") / x.get("hyflow.benchmark.injectorThreads"), "label": "Accounted Benchmark Time (us/s)" },
	
	"timeratio-failedRetry": { "func": lambda x: x.get("failedRetry", 3) * x.get("failedRetry", 8) / total(x), "label": "Rel Time in Failed Retries", "relative": True},
	"timeratio-succRetry": { "func": lambda x: x.get("succRetry", 3) * x.get("succRetry", 8) / total(x), "label": "Time Spent in Successful Retries", "relative": True},
	"timeratio-firstAbort": { "func": lambda x: x.get("firstAbort", 3) * x.get("firstAbort", 8) / total(x), "label": "Time Spent in First Aborts", "relative": True},
	"timeratio-noAbortTxn": { "func": lambda x: x.get("noAbortTxn", 3) * x.get("noAbortTxn", 8) / total(x), "label": "Time Spent in Non-Aborting Txns", "relative": True},
	"timeratio-backoff": { "func": lambda x: x.get("backoff", 3) * x.get("backoff", 8) / total(x), "label": "Time Spent in Backoff" },
	
	# General / Misc
	"throughput2": { "func": lambda x: x.get("noAbortTxn", 3) + x.get("succRetry", 3), "label": "Throughput (norm to flat)", "relative": True },
	"abort-ratio": { "func": lambda x: x.get("aborts") / x.get("commits"), "label": "Aborts per Commit Ratio"},
	"abort-ratio2": { "func": lambda x: x.get("aborts") / x.get("commits"), "label": "Aborts per Commit Ratio", "relative": True},
	"norm-noAbortTxn-time": { "func": lambda x: x.get("noAbortTxn", 8) / x.get("Tracker_frontget", 8), "label": "Normalized Non-Aborting Txn Exec Time" },
	"gcCount": { "metric": "gcCount", "type": "histogram", "label": "GC runs every 5s"},
	"gcTime": { "metric": "gcTime", "type": "histogram", "label": "GC time used every 5s (ms)" },

	#"": { "data": "", "label": "" },
	#"": { "data": "", "label": "" },
	#"": { "data": "", "label": "" },
	#"": { "data": "", "label": "" },
	#"": { "data": "", "label": "" },
}

metric_types = {
	"timer": [
		(3, "rate", "Rate (1/s)"),
		(8, "mean", "Time (us)"),
		(9, "median", "Median (us)"),
		(10, "stddev", "stddev (us)"),
	],
	"histogram": [
		(3, "mean", "Mean"),
		(4, "median", "Median"),
		(5, "stddev", "stddev"),
	],
	"meter": [
		(3, "rate", "Rate (1/s)"),
	],
}


