#!/usr/bin/env python

import random

log_name = "log-0-talexpc-vt.txt"
start_after = "Starting Warmup"
process_when = "SCHISM: "

sample_count = 10000

vertex_map = {}
vertex_next = 1
vertex_adj = {}


# Convert a vertex-name into a vertex-id.
# Returns None if the vertex is to be dropped, due to sampling.
def lookup_vertex(vname):
    global vertex_map
    global vertex_next
    global vertex_adj
    if vname in vertex_map:
        vid = vertex_map[vname]
    elif vertex_next <= sample_count:
        vid = vertex_next
        vertex_next += 1
        vertex_map[vname] = vid
        vertex_adj[vid] = set()
    else:
        vid = None
    return vid

# Entry point for processing transactions. 
# The function is passed a transaction-id and a set of entries read and written.
def process_txn(tid, sets):
    global vertex_adj
    
    # sample transactions: drop 90% of them
    if random.randrange(10) != 0:
        return
    
    vids = [ lookup_vertex(vname) for vname in sets ]
    vset = set(vids)
    for x in vset:
        if x != None:
            vertex_adj[x] |= vset

# Converts a log-line into a set of object ids.
def parse_set(s0):
    (throw, s) = s0.strip().split("(", 1)
    s = s[:-1]
    if len(s) == 0:
        return set()
    vids = s.split(", ")
    #vids = [ x[1:-1] for x in vids ]
    return set(vids)

num_edges = 0

# Drops double edges and null entries. Also counts edges.
def post_process():
    global vertex_adj
    global num_edges
    for vid in vertex_adj:
        adj = vertex_adj[vid]
        adj.remove(vid)
        if None in adj:
            adj.remove(None)
        for nbr in adj:
            if nbr > vid:
                num_edges += 1


def _vid_to_clones(vid, parts):
    vertex_next * parts
    
def explode_nodes(parts):
    global vertex_adj
    global num_edges
    nva = {}
    for vid in vertex_adj:
        # Each node is connected to its clones (star-shaped)
        nva[vid] = set()
    

# Main loop processing lines from log file in order to create the transaction graph.
def process_log_file():
    # Parse log file
    started = (start_after == None)
    fin = open(log_name, "rt")
    tid = None
    rds = None
    wrs = None
    for ln in fin:
        if not started:
            if start_after in ln:
                started = True
        else:
            if not process_when in ln:
                pass
            else:
                (throw, data) = ln.split(process_when)
                (key, val) = data.split("=")
                if key == "txn":
                    tid = val
                elif key == "rs":
                    rds = parse_set(val)
                elif key == "ws":
                    wrs = parse_set(val)
                    process_txn(tid, rds | wrs)

# Writes output files
def write_output():
    # Write graph file in metis format
    fout = open("graph.txt", "wt")
    fout.write("%d %d\n" % (vertex_next - 1, num_edges) )
    for vid in vertex_adj:
        fout.write(" ".join([str(d) for d in vertex_adj[vid]]) + "\n")
    fout.close()

    # Write id file
    fout = open("graph-id.txt", "wt")
    vid_to_vname = {}
    for vname in vertex_map:
        vid_to_vname[vertex_map[vname]] = vname
    for vid in vid_to_vname:
        fout.write("%s\n" % vid_to_vname[vid])
    fout.close()

# Main function
def main():
    process_log_file()
    post_process()
    #explode_nodes()
    write_output()
    
# Program entry point
if __name__ == "__main__":
    main()
