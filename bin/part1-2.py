#!/usr/bin/env python

import networkx as nx
import random

log_name = "log-tag0-talexpc-vt-N61Jv.txt"
start_at = "Starting Warmup."

g = nx.Graph()

count_txn = 0
# Entry point for processing transactions. 
# The function is passed a transaction-id and a set of entries read and written.
def process_txn(tid, rds, wrs):
    global count_txn
    
    # In sample mode, only add connections to existing tuples
    sample = False
    if random.randrange(700) != 0:
        sample = True
        return
        
    count_txn += 1
    count_writes(wrs)
    sets = rds | wrs
    
    if sample:
        # Only process existing objects
        nodes = [x for x in sets if x in g.node]
    else:
        # Add all objects as new nodes
        nodes = list(sets)
        for n in nodes:
            g.add_node(n, coalesced=set())
    
    # Store transaction information in nodes
    for n in nodes:
        if "txns" in g.node[n]:
            g.node[n]["txns"].add(count_txn)
        else:
            g.node[n]["txns"] = set([count_txn])
    
    # Store transaction as a clique
    for n1 in nodes:
        for n2 in nodes:
            if n1 != n2:
                if n2 in g[n1]:
                    g[n1][n2]["txns"].add(count_txn)
                else:
                    g.add_edge(n1, n2, weight=10, txns=set([count_txn]))
    
def process_duplicates_old():    
    # Add node duplicates for this transaction
    duplicates = [ "%s---%d" % (node, count_txn) for node in nodes ]
    g.add_nodes_from(duplicates)
    
    # Connect duplicates to their center node
    g.add_edges_from(zip(nodes, duplicates), weight=1) # We can't have zero weights... heff
    
    # Connect all duplicates in a clique for this transaction
    for n1 in duplicates:
        for n2 in duplicates:
            if n1 != n2:
                g.add_edge(n1, n2, weight=10)




# Count number of transactions updating each tuple for the purpose of replication stars
num_writes = {}
def count_writes(writeset):
    global num_writes
    for vname in writeset:
        if vname in num_writes:
            num_writes[vname] += 1
        else:
            num_writes[vname] = 1

# Add weights to replication stars
def affix_weights():
    for n in g.node:
        if "---" not in n:
            if n in num_writes:
                w = num_writes[n]
                w += sum( [ num_writes[x] for x in g.node[n]["coalesced"] ] ) 
                for nbr in g[n]:
                    g[n][nbr]["weight"] = 10 * w
            
    #for (k, v) in num_writes.items():
    #    if k in g.node:
    #        for nbr in g[k]:
    #            g[k][nbr]["weight"] = 10 * v

# Main loop processing lines from log file in order to create the transaction graph.
def process_log_file():
	# Parse log file
	started = False
	fin = open(log_name, "rt")
	level = -1
	tid = ""
	rds = []
	wrs = []
	cnt = 0
	for ln in fin:
		cnt += 1
		if started and cnt % 1000 == 0:
			coalesce()
		if not started:
			if start_at in ln:
				started = True
				print("Starting at line %d" % cnt)
			continue
		ln = ln.strip()[1:-1]
		pairs = ln.split(",", 3)
		if len(pairs) != 4:
			continue
		(txnid, op, nodeid, param) = pairs

		if op == "_tag_TXN_BEGIN":
			level = level + 1
			if level == 0:
				tid = param
		elif op == "_tag_TXN_COMMIT":
			if level == 0:
				process_txn(tid, set(rds), set(wrs))
			level = level - 1
		elif op == "_tag_TXN_ABORT":
			if level == 0:
				tid = ""
				rds = []
				wrs = []
			level = level -1
		elif op == "__tagged_OPEN":
			pass
		elif op == "__tagged_OPEN_MANY":
			pass
		elif op == "__tagged_APPLY":
			if level == 0:
				rds.append(param)
		elif op == "__tagged_UPDATE":
			if level == 0:
				wrs.append(param)
		elif op == "__tag_CREATE":
			if level == 0:
				wrs.append(param)
		else:
			print("[WARN] Encountered unhandeled tag: %s, at line %d" % (op,cnt))

		if level > 0:
			# TODO: this only catches open nesting. Closed nested aborts are ignored!
			print("[ERROR] Nested transactions not handeled!")

def write_metis():
    fout = open("graph.txt", "wt")
    fout.write("%d %d 001\n" % (g.number_of_nodes(), g.number_of_edges()))
    for vid in g.nodes():
        fout.write(" ".join([ "%d %d" % (nbr, g[vid][nbr]["weight"]) for nbr in g[vid] ]) + "\n")
    fout.close()

def write_labels():
    # get inverse mapping
    map1 = g.node_labels
    map2 = {}
    for (k,v) in map1.items():
        map2[v] = k
    # write to file
    fout = open("graph-id.txt", "wt")
    for vid in g.nodes():
        fout.write(map2[vid] + "\n")
    fout.close()
    
def add_test_data():
    g.add_node(1)
    g.add_node(2)
    g.add_edge(1, 2)
    


# Heuristics
def coalesce():
    remove = set()
    for node in g.node:
        for nbr in g[node]:
            if nbr not in remove:
                if g.node[node]["txns"] == g.node[nbr]["txns"]:
                    #print "coalesce %s %s" % (node, nbr)
                    g.node[node]["coalesced"].add( nbr )
                    g.node[node]["coalesced"] |= g.node[nbr]["coalesced"] 
                    g.node[node]["txns"] |= g.node[nbr]["txns"]
                    remove.add( nbr )
    print("total:%d, removing %d nodes" % (g.number_of_nodes(), len(remove)))
    g.remove_nodes_from(remove)

# Explode for replication
def explode():
    e = nx.Graph()
    for n in g.node:
        e.add_node(n, coalesced=g.node[n]["coalesced"], txns=g.node[n]["txns"])
    x = 0
    for n in g.node:
        for nbr in g[n]:
            if nbr > n:
                x += 1
                dup1 = n+"---"+str(x)
                dup2 = nbr + "---"+str(x)
                e.add_node(dup1)
                e.add_node(dup2)
                e.add_edge(n, dup1, weight=1)
                e.add_edge(nbr, dup2, weight=1)
                e.add_edge(dup1, dup2, weight=g[n][nbr]["weight"])
    return e

# Find tuples rarely accessed 
def drop_useless():
    import matplotlib.pyplot as plt
    #nx.draw(g)
    drop = [n for n in g.node if len(g[n]) < 5]
    g.remove_nodes_from(drop)
    x = [len(g[n]) for n in g.node]
    plt.hist(x, bins=100, log=True)
    plt.show()
        

#add_test_data()
process_log_file()
print "processed file, \t nodes =", g.number_of_nodes()
print "\t\t\t conn cpts =", nx.algorithms.components.connected.number_connected_components(g)

coalesce()
print "coalesced,\t\t nodes =", g.number_of_nodes()

#drop_useless()
#print "dropped low-n, \t\t nodes = ", g.number_of_nodes()
#print "\t\t\t conn cpts =", nx.algorithms.components.connected.number_connected_components(g)

g = explode()
print "exploded,\t\t nodes =", g.number_of_nodes()

affix_weights()
print "weighted"

g = nx.relabel.convert_node_labels_to_integers(g, discard_old_labels=False, first_label=1)
print "relabeled"

write_metis()
write_labels()
print "done"

