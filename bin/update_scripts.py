#!/usr/bin/python

import subprocess
import os
import os.path
from stat import *

PROJECTS=["hyflow-benchmarks"] #soot, etc

# First get deps
def get_deps(projects):
	sbt_path = subprocess.check_output(["which", "sbt"])
	
	cmd = ["bash", sbt_path.strip()]
	for proj in projects:
		cmd.extend(["project %s" % proj, "show full-classpath"])
	
	sbt_out = subprocess.check_output(cmd)
	res = []
	
	for proj in projects:
		i1 = sbt_out.find("List(")
		i2 = sbt_out.find("))")
		dep_str = sbt_out[i1+5:i2+1]
		sbt_out = sbt_out[i2+2:]
		dep_list = dep_str.split(", ")
		dep_list = [x[11:-1] for x in dep_list]
		dep_str = ":".join(dep_list)
		res.append(dep_str)
	return tuple(res)

def update_script(fname, template, args):

	fname = "bin/%s" % fname
	f = open(fname, "wt")
	f.write(template % args)
	f.close()
	os.chmod(fname, S_IWUSR | S_IXUSR | S_IRUSR | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH)

def union(d1, d2):
	res = d1.copy()
	res.update(d2)
	return res

def get_jdk_dir():
	path0 = subprocess.check_output(["which", "java"]).strip()
	path1 = os.path.realpath(path0)
	path2 = os.path.dirname(path1)
	path3 = os.path.normpath(os.path.join(path2, ".."))
	return path3

def main():
	deps_result = get_deps(PROJECTS)
	pairs = zip(PROJECTS, deps_result)
	
	bench_app = "org.hyflow.benchmarks.BenchApp"
	soot_main = "org.hyflow.soot.Main"
	java_home = get_jdk_dir()
	java_args = "-server -XX:CompileThreshold=400 -Xmx1152m -Xms256m -Xss256k"
	
	default_args = {
		"java_home" : java_home,
		"prog_args" : "$@",
		"java_args" : java_args,
		"extra" : "",
	}

	for (name, deps) in pairs:
		if name == "hyflow-benchmarks":
			bench_args = union(default_args, { 
				"main_class": bench_app, 
				"deps": deps
			})
			update_script("_bench.sh", JAVA_TEMPLATE, bench_args)
	
			tag_args = union(bench_args, {
				"deps" : "sootOutput/:" + deps
			})
			update_script("_tag-bench.sh", JAVA_TEMPLATE, tag_args)
			
			debug_args = union(bench_args, {
				# TODO: configure port number
				"java_args" : "%s %s" % (java_args, "-Xdebug -Xrunjdwp:transport=dt_socket,server=y,address=12000"), 
			})
			update_script("_debug-bench.sh", JAVA_TEMPLATE, debug_args)
			
#		elif name == "hyflow-checkpoints":
#			cp_args = union(default_args, { 
#				"main_class": bench_app, 
#				"deps": deps
#			})
#			update_script("_cp-bench.sh", JAVA_TEMPLATE, cp_args)
#		elif name == "hyflow-soot":
#			soot_inner_args = {"target_class": bench_app, "target_deps": deps_benchmarks}
#			soot_args = union(default_args, {
#				"deps" : deps_soot,
#				"main_class" : soot_main,
#				"extra": SOOT_EXTRA % soot_inner_args,
#			})
#			update_script("_soot.sh", JAVA_TEMPLATE, soot_args)



SOOT_EXTRA = """
SOOT_TARGET_CLASS="%(target_class)s"
SOOT_CLASSPATH="%(target_deps)s"
SOOT_EXCLUDE="-x akka -x ch.qos -x scala -x com.typesafe -x org.slf4j"
SOOT_OPTS="-allow-phantom-refs -f shimple -app -main-class $SOOT_TARGET_CLASS"
PROG_ARGS="-cp $SOOT_CLASSPATH:$JAVA_JARS $SOOT_EXCLUDE $SOOT_OPTS $SOOT_TARGET_CLASS"
""" 

JAVA_TEMPLATE = """
MAIN_CLASS="%(main_class)s"

JAVA_HOME="%(java_home)s"
JAVA_JARS="$JAVA_HOME/lib/rt.jar:$JAVA_HOME/lib/jce.jar:$JAVA_HOME/lib/jsse.jar"
JAVA_BIN=$JAVA_HOME/bin/java

CLASSPATH="%(deps)s"

#-XX:+UnlockDiagnosticVMOptions -XX:-DTraceMonitorProbes -agentlib:hprof=monitor=y
PROG_ARGS="%(prog_args)s"
JAVA_ARGS="%(java_args)s"

%(extra)s

$JAVA_BIN -cp $CLASSPATH $JAVA_ARGS $MAIN_CLASS $PROG_ARGS
"""

if __name__ == "__main__":
	main()

