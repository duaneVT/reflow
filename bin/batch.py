#!/usr/bin/python

import sys
import subprocess

env = {}
execfile("etc/tests.py", env)
templates = env["templates"]
tests = env["tests"]
cmd = env["cmd"]

def apply_template(descr, templ):
	args = ""
	# Join args by concatenation
	if "ARGS" in descr:
		args += descr["ARGS"]
	if "ARGS" in templ:
		args += templ["ARGS"]
	# Update remaining by overwriting
	descr.update(templ)
	# Update args
	descr["ARGS"] = args

def make_descr(tid):
	args = tests[tid]
	descr = {"ARGS": ""}
	for arg in args:
		if arg in templates:
			apply_template(descr, templates[arg])
		else:
			descr["ARGS"] += " " + arg
	return descr

def cmdline_arg(k, v):
	if len(k) == 0:
		return str(v)
	if len(k) == 1:
		return "-" + k + str(v)
	else:
		return "--" + k + "=" + str(v)

def explode_rec(descr, tid, arglist, result, choices, idx):
	if idx == len(arglist):
		if "cmd_line" in env:
			result.append(env["cmd_line"](choices[:], descr) + ["-I"+testid])
		else:
			result.append([cmd] + ["-I"+testid] + sorted(choices[:] + descr["ARGS"].split(" ")))
	else:
		argname = arglist[idx]
		argdesc = descr[argname]
		if type(argdesc) in [str, int, float]:
			choices[idx] = cmdline_arg(argname, argdesc)
			explode_rec(descr, tid, arglist, result, choices, idx+1)
		elif type(argdesc) == list:
			for choice in argdesc:
				choices[idx] = cmdline_arg(argname, choice)
				explode_rec(descr, tid, arglist, result, choices, idx+1)


def explode_test(descr, tid):
	arglist = descr.keys()
	arglist.remove("ARGS")
	arglist.remove("REPEAT")
	result = []
	explode_rec(descr, tid, arglist, result, [None] * len(arglist), 0)
	repeat = 1
	if "REPEAT" in descr:
		repeat = int(descr["REPEAT"])
	return result * repeat

def all_tests(tid):
	descr = make_descr(tid)
	return explode_test(descr, tid)


testids = sys.argv[1:]
if len(testids) == 0:
	print "Must specify which tests to run. Available tests:"
	print " ".join(sorted(tests.keys()))
	exit(0)

for testid in testids:
	count = 0
	skip = 0
	if ":" in testid:
		pairs = testid.split(":")
		testid = pairs[0]
		skip = int(pairs[1])
		print "Skipping %d test(s)." % skip
	for testcmd in all_tests(testid):
		if count >= skip:
			#testcmd = testcmd 
			print testcmd
			subprocess.call(testcmd)
			print "Completed %s:%d" % (testid, count+1)
		count += 1
