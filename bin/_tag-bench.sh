
MAIN_CLASS="org.hyflow.benchmarks.BenchApp"

JAVA_HOME="/usr/lib/jvm/java-7-openjdk-amd64/jre"
JAVA_JARS="$JAVA_HOME/lib/rt.jar:$JAVA_HOME/lib/jce.jar:$JAVA_HOME/lib/jsse.jar"
JAVA_BIN=$JAVA_HOME/bin/java

CLASSPATH="sootOutput/:/home/duane/Desktop/reflow/hyflow-benchmarks/target/scala-2.10/hyflow-benchmarks_2.10-0.0.1.jar:/home/duane/Desktop/reflow/hyflow-core/target/scala-2.10/hyflow-core_2.10-0.0.1.jar:/home/duane/.ivy2/cache/org.scala-lang/scala-library/jars/scala-library-2.10.0.jar:/home/duane/.ivy2/cache/com.typesafe.akka/akka-actor_2.10/bundles/akka-actor_2.10-2.1.1.jar:/home/duane/.ivy2/cache/com.typesafe/config/bundles/config-1.0.0.jar:/home/duane/.ivy2/cache/com.typesafe.akka/akka-remote_2.10/bundles/akka-remote_2.10-2.1.1.jar:/home/duane/.ivy2/cache/io.netty/netty/bundles/netty-3.5.8.Final.jar:/home/duane/.ivy2/cache/com.google.protobuf/protobuf-java/jars/protobuf-java-2.4.1.jar:/home/duane/.ivy2/cache/org.uncommons.maths/uncommons-maths/jars/uncommons-maths-1.2.2a.jar:/home/duane/.ivy2/cache/com.typesafe.akka/akka-slf4j_2.10/bundles/akka-slf4j_2.10-2.1.1.jar:/home/duane/.ivy2/cache/org.slf4j/slf4j-api/jars/slf4j-api-1.7.2.jar:/home/duane/.ivy2/cache/org.scala-stm/scala-stm_2.10/jars/scala-stm_2.10-0.7.jar:/home/duane/.ivy2/cache/com.typesafe/scalalogging-slf4j_2.10/jars/scalalogging-slf4j_2.10-1.0.0.jar:/home/duane/.ivy2/cache/org.scala-lang/scala-reflect/jars/scala-reflect-2.10.0.jar:/home/duane/.ivy2/cache/com.esotericsoftware.kryo/kryo/jars/kryo-2.20.jar:/home/duane/.ivy2/cache/com.esotericsoftware.reflectasm/reflectasm/jars/reflectasm-1.07-shaded.jar:/home/duane/.ivy2/cache/org.ow2.asm/asm/jars/asm-4.0.jar:/home/duane/.ivy2/cache/com.esotericsoftware.minlog/minlog/jars/minlog-1.2.jar:/home/duane/.ivy2/cache/org.objenesis/objenesis/jars/objenesis-1.2.jar:/home/duane/.ivy2/local/com.romix.akka/akka-kryo-serialization/0.2-SNAPSHOT/jars/akka-kryo-serialization.jar:/home/duane/.ivy2/cache/com.typesafe.akka/akka-kernel_2.10/jars/akka-kernel_2.10-2.1.0.jar:/home/duane/.ivy2/cache/ch.qos.logback/logback-classic/jars/logback-classic-1.0.9.jar:/home/duane/.ivy2/cache/ch.qos.logback/logback-core/jars/logback-core-1.0.9.jar:/home/duane/.ivy2/cache/nl.grons/metrics-scala_2.10/jars/metrics-scala_2.10-2.2.0.jar:/home/duane/.ivy2/cache/com.yammer.metrics/metrics-core/jars/metrics-core-2.2.0.jar"

#-XX:+UnlockDiagnosticVMOptions -XX:-DTraceMonitorProbes -agentlib:hprof=monitor=y
PROG_ARGS="$@"
JAVA_ARGS="-server -XX:CompileThreshold=400 -Xmx1152m -Xms256m -Xss256k"



$JAVA_BIN -cp $CLASSPATH $JAVA_ARGS $MAIN_CLASS $PROG_ARGS
